import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, ROUTES } from '@angular/router';
import { ServiceHandlerService } from '../service/service-handler.service';


import {HttpClient, HttpClientModule} from '@angular/common/http';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],

  providers: [
    {
      provide: ROUTES,
      useFactory: handlerModule,
      deps: [ServiceHandlerService],
      multi: true
    }
  ]
})
export class ProjectsHandlerModule {}

export function handlerModule(service: ServiceHandlerService) {

    let routes: Routes = [];

    if (service.setValue() == 't1') {
        routes = [
          {
            path: '', loadChildren: () => import('../Projects/LssOne/template-one.module').then(m => m.TemplateOneModule)
          }
        ];
    }else if(service.setValue() == 't2'){
      routes = [
          {
            path: '', loadChildren: () => import('../Projects/projectTwo/project-two-mdule.module').then(m => m.ProjectTwoMduleModule)
          }
      ];
    }
    return routes;

  }
