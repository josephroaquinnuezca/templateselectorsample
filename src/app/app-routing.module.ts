import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SelectorPageComponent } from './selector-page/selector-page.component';

const routes: Routes = [
  { path:  'templateSelect', component:  SelectorPageComponent},
  { path:  'projects', loadChildren: () => import('./pojectHandlerModule/projects-handler.module').then(m => m.ProjectsHandlerModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
