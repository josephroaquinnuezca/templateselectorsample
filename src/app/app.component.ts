import { Component } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private titleService: Title, private metaService: Meta) {}

 ngOnInit() {
    this.titleService.setTitle('LSS');
      this.metaService.addTags([
        { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1' },
        { name: 'robots', content: 'index, follow'},
        { name: 'author', content: 'Easycom Japan Philippines Inc.' }
      ]);
      this.metaService.updateTag(
        { name: 'keywords', content: ''}
      );
      this.metaService.updateTag(
      { name: 'description', content: ''},
      );
  }
}
