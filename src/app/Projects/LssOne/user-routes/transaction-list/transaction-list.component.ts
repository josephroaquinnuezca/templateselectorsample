import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';
import { Transaction, OrderTransaction } from '../shared/transaction.model';
import { Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import { map, catchError } from 'rxjs/operators';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { UserProvider } from '../shared/users.provider';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss']
})
export class TransactionListComponent implements OnInit {

  @ViewChild('editTemplate', { read: true, static: false }) sort: MatSort;
  transList: OrderTransaction[] = [];
  transactionLists: any[] = [];
  dataSource: MatTableDataSource<OrderTransaction>;
  displayedColumns = ['id', 'date', 'order_no', 'summary', 'status', 'viewAction'];
  summaryList: any[] = [];
  search_loading = false;

  statMess = ['Waiting for confirmation', 'Payment Confirmed', 'Ready for pick-up', 'Out for delivery',
    'Delivered', 'Cancelled', 'Closed'];

  constructor(private router: Router, private userService: UserService, private userProvider: UserProvider) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.transList);
    this.dataSource.sort = this.sort;
    // console.log(`DataSource: ${this.transList}`);
    // console.log(`DataSource Sort: ${this.dataSource.sort}`);
    this.getTrans()
  }

  sortASC(itemA, itemB)
  {
      return parseFloat(itemA.id) - parseFloat(itemB.id);
  }
  sortDESC(itemA, itemB)
  {
      return parseFloat(itemB.id) - parseFloat(itemA.id);
  }

  getTrans() {
    let observables = new Array();
    let currentUser = JSON.parse(localStorage.getItem('currentUser'))
    this.userService.getTransaction(currentUser.id).subscribe(
      res => {
        if (res) {
          console.log(res);
          this.transList = res.data.items[0].order_list
          this.dataSource.data = this.transList
          this.transactionLists = this.transList.sort(this.sortDESC)
          console.log('this.transactionLists:', this.transactionLists)
          // for (let wawen of this.transactionLists) {
          //   console.log(`DataSource: ${wawen.order_no}`);
          // }

        }
      }
    )
  }

  openView(row) {
    // console.log(row);
    // return;
    this.userProvider.transData = row;
    this.router.navigate(['./transaction-view/' + row.order_no]);
  }

}
