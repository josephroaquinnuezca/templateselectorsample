import { UserService } from 'src/app/user-routes/shared/user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserProvider } from '../shared/users.provider';
import { TransactionDesc } from '../shared/transaction.model';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { TestimonyDialogComponent } from '../testimony-dialog/testimony-dialog.component';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderPackageService, ConfirmOrderService } from '../../_client-services'
import { PackageDialogComponent } from './../transactio-view/package-dialog/package-dialog.component';
import { SuccessMesageDialogComponent } from 'src/app/static-pages/success-mesage-dialog/success-mesage-dialog.component';
import { OrderItem } from 'src/app/ordering-routes/shared/ordering.model';

@Component({
  selector: 'app-transaction-view',
  templateUrl: './transaction-quick-view.component.html',
  styleUrls: ['./transaction-quick-view.component.scss']
})
export class TransactionQuickViewComponent implements OnInit {

  deliveryMeth = ["Delivery", "Pick-up at store", "Deliver and Pick-up at LBC Branch"]
	statMess = ['Waiting for proof of payment', 'Payment Confirmed', 'Ready for pick-up', 'Out for delivery',
		'Order Completed', 'Cancelled', 'Closed'];
  transDesc: TransactionDesc
  imgFile: File[] = []
	hasFile: boolean = false;

  itemDesc: OrderItem

  isUpload = false

  mRemarks: any[] = []
	// order_package: any;
  can_comment: boolean = false;

  constructor(private confirmOrderService: ConfirmOrderService,
              private userProvider: UserProvider,
              private userService: UserService,
              public dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              private orderPackageService: OrderPackageService) { }

  ngOnInit() {
    let data = JSON.parse(sessionStorage.getItem('orderQuickView'))
    if (!data) {
      this.router.navigate(['confirm-order'])
      return;
    }
    this.getOrderRecord();
    // this.getTransDesc();
  }

  ngOnDestroy(){
    sessionStorage.removeItem('orderQuickView')
  }

  localUrl = "";
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
			this.hasFile = true;
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrl = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
      this.imgFile = event.target.files[0];
      // this.isUpload = true
    }
  }

  uploadProof(){
    if(this.isUpload) return
    this.isUpload = true
    this.userService.setProofPaymentConfirmOder(this.imgFile, this.transDesc.id).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.Sent:
            return

          case HttpEventType.Response:
            let res: any = event.body
            this.isUpload = false
						this.hasFile = false;
            // alert('Upload finished')
            const dialogRef = this.dialog.open(SuccessMesageDialogComponent, {
              panelClass: 'app-full-bleed-dialog-p-26',
              data: 'Thank you for uploading your payment , You will receive an email once payment is confirmed.'
            });
            this.getOrderRecord()
            return
        }
      }),
      catchError((err: HttpErrorResponse) => {
        // console.log('HttpErrorResponse', err)
        this.isUpload = false
        return Observable.throw(err);
      })
    ).subscribe()
  }

  getTransDesc() {
    this.mRemarks = []
    let res = JSON.parse(sessionStorage.getItem('orderQuickView'))
    console.log('res.data.items[0].client_info[0].id:', res.data.items[0].client_info[0].id)
    for (let itm of res.data.items[0].order_items)
      this.mRemarks.push({
        item_id: itm.item_id,
        user_id: res.data.items[0].client_info[0].id,
        rate: itm.rate,
        message: itm.message
      })
    this.transDesc = res.data.items[0]
    this.itemDesc = res.data.items[0].order_items
    this.localUrl = this.transDesc.proofofpayment
    this.can_comment = false
    for(let itm of this.transDesc.order_items){
      console.log('itm.can_comment: ', itm.can_comment)
      if (itm.can_comment) {
        console.log('if itm.can_comment: ', itm.can_comment)
        this.can_comment = true
      }
    }
    console.log('can_comment: ', this.can_comment)
		// if (this.deliveryMeth[this.transDesc.service_type - 1] == 'Delivery'){
		// 	var x = this.transDesc.order_no;
		// 	this.orderPackageService.fetchOrderPackage(x).subscribe(res => {
		// 		const res_data: any = res;
    //
		// 		if (res_data.status.error === false ) {
		// 			console.log(res_data);
		// 			this.order_package = res_data.data;
		// 		}
		// 	});
		// }
  }

  getOrderRecord(){
    let data = JSON.parse(sessionStorage.getItem('orderQuickView'))
    this.confirmOrderService.getOrderTransaction(data.data.items[0].order_no, data.data.items[0].client_info[0].email_address).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
      const dataResult: any = res;
      console.log('NEW dataResult:', dataResult);
      sessionStorage.setItem('orderQuickView', JSON.stringify(dataResult));
    }, err => {
      console.log('HTTP Error:', err.error);
      if(err.error.type == 'error'){
        console.log('HTTP Error Type:', err.error.type);
      }
      else{
      }
    }, () => {
      this.getTransDesc();
      console.log('Done Fetching...');
    });
  }

  // saveRemarks() {
  //   this.userService.inserRemarks(this.mRemarks).subscribe(
  //     res => {
  //       this.getTransDesc()
  //     }
  //   )
  // }

  setTestimony(row){
    let res = JSON.parse(sessionStorage.getItem('orderQuickView'))
    const dialogRef = this.dialog.open(TestimonyDialogComponent, {
      width: '80%', height: '800px',
      data: {
        user_id: res.data.items[0].client_info[0].id,
        item_id: row,
        order_no: this.transDesc.order_no //this.userProvider.transData.order_no
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      this.getOrderRecord();
    });
  }
	openDialog(data: any) {
		// console.log(email)
    const dialogRef = this.dialog.open(PackageDialogComponent, {
		  panelClass: 'app-full-bleed-dialog-p-10',
      data: {
				data
			}
      // disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
			// console.log(result);
      // if (result) {
      //   this.userService.signout();
			// 	this.router.navigate(['/user/signin']);
      // }
    });
  }
  handleError(error) {
    console.log(error);
    return throwError(error);
  }
}
