import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSuccessMessageComponent } from './user-success-message.component';

describe('UserSuccessMessageComponent', () => {
  let component: UserSuccessMessageComponent;
  let fixture: ComponentFixture<UserSuccessMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSuccessMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSuccessMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
