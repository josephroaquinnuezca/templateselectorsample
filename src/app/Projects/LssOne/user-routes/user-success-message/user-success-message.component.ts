import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-success-message',
  templateUrl: './user-success-message.component.html',
  styleUrls: ['./user-success-message.component.scss']
})
export class UserSuccessMessageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
