import { Component, OnInit } from '@angular/core';
import { UserDetail } from '../shared/transaction.model';
import { UserService } from '../shared/user.service';
import { UserProvider } from '../../user-routes/shared/users.provider';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { filter, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
// import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material';
import { RemoveDeleteDialogComponent } from './remove-delete-dialog/remove-delete-dialog.component';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  userDetail: UserDetail
  isUpdate = false;
  isUpdatePassword: boolean = false;
  isUpdateEmail: boolean = false;

  public destroyed = new Subject<any>();
  user = JSON.parse(localStorage.getItem('currentUser'))

  constructor(public userService: UserService, private router: Router, public dialog: MatDialog, private userProvider: UserProvider) { }

  ngOnInit() {
    this.isUpdate = this.router.url.includes('update');
    this.userService.getUser(this.user.id).subscribe(
      resData => {
        const res: any = resData; 
        this.userDetail = res.data.items[0]
        console.log("User details", this.userDetail);
      }
    )
    this.router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationEnd),
      takeUntil(this.destroyed)
    ).subscribe((res) => {
      let tmp: any = res;
      this.isUpdate = tmp.urlAfterRedirects.includes('update');
      this.isUpdatePassword = tmp.urlAfterRedirects.includes('update-password');
      this.isUpdateEmail = tmp.urlAfterRedirects.includes('update-email');
      this.userService.getUser(this.user.id).subscribe(
        resData => {
          const res: any = resData;
          this.userDetail = res.data.items[0]
          console.log(this.userDetail);
        }
      )
    });
  }

  updatebtn(){
    	this.router.navigate(['./information-update']);
  }
  updatePasswordBTN(){
    	this.router.navigate(['./password-update']);
  }
  updateEmailBTN(){
    	this.router.navigate(['./email-update']);
  }
  localUrl = "";
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
          this.localUrl = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

	// removeAccount(){
	// 	// setInterval(() => {
	// 	// 	Swal.close();
	// 	// },5000)
	// 	var emailVerify = this.userDetail.email_address;
	// 	const swalWithBootstrapButtons = Swal.mixin({
	// 	  customClass: {
	// 			header: 'border-bottom mb-2',
	// 			title: 'swal-font-20 text-left w-100 mb-1',
	// 	    confirmButton: 'btn mr-auto',
	// 			content: 'swal-font-14 text-left'
	// 	    // cancelButton: 'btn btn-danger mx-2'
	// 	  },
	// 	  buttonsStyling: false
	// 	})
  //
	// 	swalWithBootstrapButtons.fire({
	// 		showLoaderOnConfirm: true,
	// 	  title: 'Confirmation required',
	// 	  html: '<span class="d-block text-danger">You are going to remove your <strong>ACCOUNT</strong> permanently.</span>' +
	// 					'<span class="d-block text-danger mb-2">Permanent removal of account <strong>CANNOT</strong> be restored! Are you <strong>ABSOLUTELY</strong> sure?</span>' +
	// 					'<span class="d-block">This action can lead to data loss. To prevent accidental actions we ask you to confirm your intention.</span>' +
	// 					`<span class="d-block">Please type <span class="focus-danger">${this.userDetail.email_address}</span> to proceed or close this modal to cancel.</span>`,
	// 		input: 'email',
  // 		inputPlaceholder: 'Confirm you email address',
	// 	  // icon: 'warning',
	// 	  showCancelButton: false,
	// 	  confirmButtonText: 'Yes, delete it!',
	// 	  // cancelButtonText: 'No, cancel!',
	// 	  reverseButtons: false,
	// 	  onOpen: function () {
	// 			//other options
	// 			// classList.contains('MyClass')
	// 			// classList.toggle('MyClass')
	// 	    Swal.getConfirmButton().setAttribute('disabled', '')
	// 			Swal.getConfirmButton().classList.add('btn-secondary')
	// 	    Swal.getInput().addEventListener('keyup', function(event) {
	// 				// console.log(event.target)
	// 	      if((<HTMLInputElement>event.target).value !== emailVerify) {
	// 	        Swal.getConfirmButton().setAttribute('disabled', '')
	// 			    Swal.getConfirmButton().classList.remove('btn-danger')
	// 			    Swal.getConfirmButton().classList.add('btn-secondary')
	// 	      } else {
	// 	        Swal.getConfirmButton().removeAttribute('disabled')
	// 			    Swal.getConfirmButton().classList.remove('btn-secondary')
	// 			    Swal.getConfirmButton().classList.add('btn-danger')
	// 	      }
	// 	    })
	// 	  },
	// 		preConfirm: (login) => {
	// 			console.log(login);
	// 			Swal.showLoading()
	// 			this.userService.getUser(this.user.id).subscribe(
	// 	      res => {
	// 	        console.log(res.data.items[0])
	// 	      }
	// 	    )
	// 			return false
	// 			// return fetch(`https://api.github.com/users/${login}`)
	// 			// 	.then(response => {
	// 			// 		if (!response.ok) {
	// 			// 			throw new Error(response.statusText)
	// 			// 		}
	// 			// 		return response.json()
	// 			// 	})
	// 			// 	.catch(error => {
	// 			// 		Swal.showValidationMessage(
	// 			// 			`Request failed: ${error}`
	// 			// 		)
	// 			// 	})
	// 		},
	// 		allowOutsideClick: () => !Swal.isLoading()
	// 	}).then((result) => {
	// 		console.log(result);
	// 		if (result.value) {
	// 			swalWithBootstrapButtons.fire(
	// 	      'Cancelled',
	// 	      'Your imaginary file is safe :)',
	// 	      'error'
	// 	    )
	// 		}
	// 		// console.log(result.value);
	// 	  // if (result.value) {
	// 	  //   // swalWithBootstrapButtons.fire(
	// 	  //   //   'Deleted!',
	// 	  //   //   'Your file has been deleted.',
	// 	  //   //   'success'
	// 	  //   // )
	// 	  // } else if (
	// 	  //   /* Read more about handling dismissals below */
	// 	  //   result.dismiss === Swal.DismissReason.backdrop || result.dismiss === Swal.DismissReason.close
	// 	  // ) {
	// 	    // swalWithBootstrapButtons.fire(
	// 	    //   'Cancelled',
	// 	    //   'Your imaginary file is safe :)',
	// 	    //   'error'
	// 	    // )
	// 	  // }
	// 	})
	// }
  updateInfo(){
    this.userProvider.isFromCheckoutPersonal = false;
    this.isUpdate = true;
    // this.router.navigate(['./information-update']);
  }

	openDialog(email: any, option: any, userId: any) {
		// console.log(email)
    const dialogRef = this.dialog.open(RemoveDeleteDialogComponent, {
		  panelClass: 'app-full-bleed-dialog-p-10',
      data: {
				email,
				option,
				userId
			}
      // disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
			// console.log(result);
      if (result) {
        this.userService.signout();
				this.router.navigate(['/user/signin']);
      }
    });
  }
}
