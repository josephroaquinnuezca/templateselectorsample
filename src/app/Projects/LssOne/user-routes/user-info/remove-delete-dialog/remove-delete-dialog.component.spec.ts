import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveDeleteDialogComponent } from './remove-delete-dialog.component';

describe('RemoveDeleteDialogComponent', () => {
  let component: RemoveDeleteDialogComponent;
  let fixture: ComponentFixture<RemoveDeleteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveDeleteDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
