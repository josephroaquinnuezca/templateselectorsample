import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from '../../shared/user.service';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Component({
  selector: 'app-remove-delete-dialog',
  templateUrl: './remove-delete-dialog.component.html',
  styleUrls: ['./remove-delete-dialog.component.scss']
})
export class RemoveDeleteDialogComponent implements OnInit {

	//can be call on ngAfterViewInit
	// @ViewChild('confirmEmail', {static: false}) inputElement: ElementRef;
	//can be call on ngOnInit
	@ViewChild('confirmEmail', {static: true}) inputElements: ElementRef;
	details: any;
	disableConfirmBtn: boolean = true;
	validateEmailConfirmError: boolean = false;
	validateEmailConfirmErrorRequest: boolean = false;
	submitSuccess: boolean = false;
	emailMatch: boolean = false;
	isSpinner: boolean = false;
	isSuccessProcess: boolean = false;

  constructor(public dialogRef: MatDialogRef<RemoveDeleteDialogComponent>, @Inject(MAT_DIALOG_DATA) data, private _snackBar: MatSnackBar, private userService: UserService) {
		this.details = data;
	}

  ngOnInit() {
		console.log(this.inputElements);
		console.log(<HTMLInputElement>this.inputElements.nativeElement);
		setTimeout(()=>{ // this will make the execution after the above boolean has changed
			<HTMLInputElement>this.inputElements.nativeElement.focus();
		},1000);
		console.log(this.details.email);
		//manually trigger
		// this.dialogRefAGM.close();
		//listen to back drop & close
		this.dialogRef.beforeClose().subscribe(() => this.dialogRef.close(this.isSuccessProcess));
  }

	validateConfirm(event){
		console.log(event.target.value);

		this.isSpinner = false;

		this.submitSuccess = false;
		(event.target.value === this.details.email) ? (this.disableConfirmBtn = false, this.emailMatch = true) : (this.disableConfirmBtn = true, this.emailMatch = false);
		// (event.target.value === this.details.email) ? this.emailMatch = true : this.emailMatch = false;
	}
	copyText() {
		this.copyTextValue(this.details.email);
		this._snackBar.open('Copied ' + this.details.email, 'close', {
			duration: 2000,
		});
	}
	copyTextValue(val: string){
  let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
	onSubmit(inputValue, option, userId){
		this.isSpinner = true;
		console.log(inputValue);
		if(inputValue !== this.details.email) {
			this.validateEmailConfirmError = true;
			this.submitSuccess = false;
			this.emailMatch = false;
		}else{
			this.emailMatch = true;
			//run api here
			this.deleteUser(inputValue, option, userId)
		}


	}
	deleteUser(inputValue, option, userId) {
		const data = {
			'id': userId,
			'email': inputValue,
			'option': option
		}
		this.userService.delete(data).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
			const data: any = res;
				console.log(data);
				this.validateEmailConfirmError = false;
				this.validateEmailConfirmErrorRequest = false;
				this.submitSuccess = true;
				this.isSuccessProcess = true;
			}, err => {
				console.log('HTTP Error:', err.error);
				// console.log(err.error.status.error);
				if(err.error.type == 'error'){
					this.validateEmailConfirmErrorRequest = true;
					this.submitSuccess = false;
				}
				else{
					//else do something here
					this.validateEmailConfirmErrorRequest = true;
					this.submitSuccess = false;
				}
			}, () => {
				this.isSpinner = false;
				console.log("request completed");
		});
	}

	handleError(error) {
		console.log(error);
		return throwError(error);
	}
}
