import { Carousel } from '../../shared/carousel.config';
import { UserService } from '../shared/user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as jwt_decode from "jwt-decode";
import { UsersService } from '../../_admin-services';
import { UserProvider } from '../../user-routes/shared/users.provider';
import { OverridecssService } from '../../ordering-routes/shared/overridecss.service';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
	hasErrorPassword: boolean = false;
  successPrompt: boolean = false;

  showSuccess: boolean = false;

  submitted: boolean = false;

  signinForm: FormGroup;
  okFlg: boolean;
  email_address_value: any;
  constructor(
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private userService: UsersService,
    private router: Router,
    private route: ActivatedRoute, private userProvider: UserProvider , private overridecssService: OverridecssService) { }

  ngOnDestroy(){
    this.userProvider.isSignComponent = false;
    this.overridecssService.emitOverrideCssEvent(this.userProvider.isSignComponent);
    // console.log(`isSignComponent: ${this.userProvider.isSignComponent}`);
  }

  ngOnInit() {
    this.userProvider.isSignComponent = true;
    this.overridecssService.emitOverrideCssEvent(this.userProvider.isSignComponent);
    // console.log(`isSignComponent: ${this.userProvider.isSignComponent}`);
    // this.createForm();

    this.createForm();
  }

    get signinFormControl() {
      return this.signinForm.controls;
    }

  createForm() {
    this.signinForm = this.formBuilder.group({
      email_address: [ '', [Validators.required, Validators.email]]
    });
  }

  get f() { return this.signinForm.controls; }

  onSubmit() {
    if (this.signinForm.invalid) {
      // alert('Please complete the signin form');
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Incomplete information or invalid input provided!'
    });
      return;
    }

    // this.userService.forgotPassword(this.f.email_address.value).subscribe( res => {
		//
    //   this.okFlg = true;
    //   alert('pls check you email address');
    //   this.signinForm.reset();
    //   // this.myform.reset();
    //   return;
    // });
		this.userService.forgotPassword(this.f.email_address.value).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
			const data: any = res;
				this.successPrompt = true;
				this.hasErrorPassword = false;
        this.email_address_value = this.f.email_address.value;
			}, err => {
				console.log('HTTP Error:', err.error);
				console.log(err.error.status.error);
				if(err.error.status.error){
					this.okFlg = true;
					this.hasErrorPassword = true;
					this.successPrompt = false;
          this.signinForm.reset();
          this.showSuccess = true;
				}
				else{
					//else do something here
				}
			}, () => {
				console.log("request completed");
		});
  }
	handleError(error) {
		console.log(error);
    return throwError(error);
  }
	clearAlertMessage(){
		if(this.successPrompt || this.hasErrorPassword){
			this.hasErrorPassword = false;
			this.successPrompt = false;
		}
	}
}
