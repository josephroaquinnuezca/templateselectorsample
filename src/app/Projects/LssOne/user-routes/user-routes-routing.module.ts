import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { RouterModule } from  '@angular/router';
import { UsertypeGuard } from './../shared/usertype.guard';
import { SigninRouterComponent } from './signin-router/signin-router.component';
import { UserSigninComponent } from './user-signin/user-signin.component';
import { UserSuccessMessageComponent } from './user-success-message/user-success-message.component';
import { UserSignupComponent } from './user-signup/user-signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordVerifyComponent } from './reset-password-verify/reset-password-verify.component';
import { EmailVerifyComponent } from './email-verify/email-verify.component';

// @NgModule({
//   declarations: [],
//   imports: [
//     CommonModule
//   ]
// })
const routes: Routes = [
  {
    // path: '', component: SigninRouterComponent,
    path: '',
    children: [
      { path: '', redirectTo: 'signin', pathMatch: 'full' },
      { path: 'signin', component: UserSigninComponent },
      { path: 'user-success-message', component: UserSuccessMessageComponent },
      { path: 'signup', component: UserSignupComponent },
      { path: 'forgot-password', component: ForgotPasswordComponent },
      { path: 'reset-password-verify/:token', component: ResetPasswordVerifyComponent },
      { path: 'email-verification', component: EmailVerifyComponent },
      { path: 'email-verification/:token', component: EmailVerifyComponent },
      // { path: 'reset-password/edit', component: ForgotPasswordResetComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class UserRoutesRoutingModule { }
