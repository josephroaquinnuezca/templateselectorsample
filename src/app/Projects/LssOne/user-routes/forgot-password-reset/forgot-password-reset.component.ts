import { Component, OnInit } from '@angular/core';
import { ResetPasswordPrv } from '../shared/reserpassword.provider';
import { UsersService } from '../../_admin-services';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { SuccessMesageDialogComponent } from 'src/app/static-pages/success-mesage-dialog/success-mesage-dialog.component';
import { first, retry, catchError, startWith, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';


@Component({
  selector: 'app-forgot-password-reset',
  templateUrl: './forgot-password-reset.component.html',
  styleUrls: ['./forgot-password-reset.component.scss']
})
export class ForgotPasswordResetComponent implements OnInit {

  pass1 = '';
  pass2 = '';

  constructor(
    private usersService: UsersService,
    public dialog: MatDialog,
    private rData: ResetPasswordPrv,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    if ( !this.rData.data) {
      this.router.navigate(['/']);
    }
  }

  resetPass() {
    if (this.pass1 !== this.pass2  ) {
      // alert('Password does not match!');
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
  		  panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Password does not match!'
      });
      return;
    }

    if (!this.pass1 || !this.pass2  ) {
      // alert('Invalid password');
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
  		  panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Invalid password!'
      });
      return;
    }

    const data = {
      'password' : this.pass1,
      'pwd_url' : this.rData.data.pwd_url,
      'email_address' : this.rData.data.email
    };

    this.usersService.passwordReset(data).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
      // alert('Successfully updated your password, redirecting you to login page.');
      const dialogRef = this.dialog.open(SuccessMesageDialogComponent, {
  		  panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Successfully updated your password, redirecting you to login page.'
      });
      this.pass1 = '';
      this.pass2 = '';
      dialogRef.afterClosed().subscribe(res => {
        this.router.navigate(['./user/signin']);
        return;
      });
    }, err => {
      this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: err.error.status.description
      });
      return;
    }, () => {
      console.log("request completed");
    });
  }

  handleError(error) {
    console.log(error);
    return throwError(error);
  }
}
