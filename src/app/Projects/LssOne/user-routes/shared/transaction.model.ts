export interface Transaction {
	id: number
	date: string
	order_no: string
	summary: string
	status: string
}

export interface UserDetail {
	id: number;
	user_no: number;
	usertype_id: number;
	username: string;
	dob: string;
	email_address: string;
	firstname: string;
	middlename: string;
	surename: string;
	contact_no: string;
	img_profile: string;
	pwd_reset: number;
	pwd_reset_date: string;
	pwd_url: string;
	log_count: number;
	token?: any;
	pwd_old?: any;
	login_attempt_stamp: string;
	status: number;
	created_by: number;
	created_date: string;
	updated_by: number;
	updated_date: string;
	deleted_by?: any;
	deleted_date?: any;
	age: number
	gender: number
	imgProfile: any[]
	province: string
	region: string
	city: string
	barangay: string
	address: string
	remarks: string
}

export interface OrderTransaction {
	id: number;
	order_no: string;
	user_id: number;
	service_type: number;
	firstname: string;
	lastname: string;
	address: string;
	city: string;
	postal_code: string;
	total_amt: number;
	ship_fee: number;
	courier: string;
	status: number;
	pickup_location: string;
	pickup_date?: any;
	modeofpayment: string;
	region?: any;
	item_list: string;
	pickup_time?: any;
	proofofpayment: string;
	tracking_no: string;
	est_deliverydate: string;
}

export interface ClientInfo {
	id: number;
	user_no: number;
	usertype_id: number;
	username: string;
	email_address: string;
	firstname: string;
	middlename: string;
	surename: string;
	contact_no: string;
	region: string;
	province: string;
	city: string;
	barangay: string;
	address: string;
}

export interface OrderItem {
	quantity: number;
	order_group: string;
	item_name: string;
	can_comment: boolean;
}

export interface OrderHistory {
	id: number;
	order_no: string;
	user_id: number;
	service_type: number;
	firstname: string;
	lastname: string;
	address: string;
	city: string;
	postal_code: string;
	total_amt: number;
	ship_fee: number;
	courier: string;
	status: number;
	pickup_location: string;
	pickup_date?: any;
	modeofpayment: string;
	region?: any;
	pickup_time?: any;
	proofofpayment?: any;
	tracking_no: string;
	est_deliverydate: string;
}

export interface TransactionDesc {
	id: number;
	order_no: string;
	client_info: ClientInfo[];
	service_type: number;
	firstname: string;
	lastname: string;
	address: string;
	city: string;
	postal_code: string;
	total_amt: number;
	ship_fee: number;
	courier: string;
	status: number;
	pickup_location: string;
	pickup_date?: any;
	modeofpayment: string;
	region?: any;
	pickup_time?: any;
	order_date?: any;
	payment_date?: any;
	proofofpayment?: any;
	tracking_no: string;
	est_deliverydate: string;
  order_items: OrderItem[];
  receiver_name?: string;
	order_history: OrderHistory[];
}
