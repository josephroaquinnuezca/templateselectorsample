import { ApisService, signinURL, saveItemURL, addClient, userUrl, custOrder, getOrderID, proofPaymentURL, remarksUrl, resetPass, proofPaymentConfirmURL, addGuest, HTTP_API_URL } from './../../shared/apis.service';
import { Injectable, Inject } from '@angular/core';
import * as jwt_decode from "jwt-decode";
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  isSignedIn = false;
  currentUser: any;

  constructor(private apiService: ApisService, private http: HttpClient, public router: Router, private route: ActivatedRoute) { }

  setUser(token: any) {
    let tokenData: any = this.tokenData(token)
    console.log('TOKEN', tokenData)
		if(tokenData.user_info.status == 0 || tokenData.user_info.verification_url != 'verified'){
			return false
		}else{
	    let body = {
	      id: tokenData.user_info.id,
	      username: tokenData.user_info.username,
	      email_address: tokenData.user_info.email_address,
	      contact_no: tokenData.user_info.contact_no,
	      img_profile: tokenData.user_info.img_profile,
	      firstname: tokenData.user_info.firstname,
	      middlename: tokenData.user_info.middlename,
	      surename: tokenData.user_info.surename,
	      usertype_id: tokenData.user_info.usertype_id,
	      // destination: tokenData.user_info.destination,
				dob : tokenData.user_info.dob,
				age : tokenData.user_info.age
	    }
	    this.currentUser = body
	    //remove current session
	    localStorage.removeItem('currentAdmin');
	    localStorage.removeItem('lss-token');
	    //replace the current session with this.
	    localStorage.setItem('currentUser', JSON.stringify(body));
	    localStorage.setItem('lss-token', token)

			return true
		}
  }

  tokenData(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

  getToken(): string {
    return localStorage.getItem('lss-token');
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);
    if (decoded.exp === undefined) return null;

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    if(!token) token = this.getToken();
    if(!token) return true;

    const date = this.getTokenExpirationDate(token);
    if(date === undefined) return false;
    return !(date.valueOf() > new Date().valueOf());
  }

  hasCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser != null) {
			this.isSignedIn = true;
      // this.isSignedIn = this.currentUser.isSignedIn;
			// console.log("xxxxxx");
			// console.log(this.route.snapshot);
			// console.log(this.router.url);
      return true;
    }

    return false;
  }

  signout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('lss-token');
    //remove all active storage
    localStorage.removeItem('deliveryOpts');
    localStorage.removeItem('infocustomer');
    localStorage.removeItem('infocustomerNSA');
    localStorage.removeItem('orderInfo');
  }
  signoutAdmin() {
    localStorage.removeItem('currentAdmin');
    localStorage.removeItem('lss-token');
    this.router.navigate(['/admin/login']);
  }
  // API SERVICE CALLS

  signin(body: any) {
    return this.apiService.post(signinURL, body);
  }

  insertItem(body: any) {
    return this.apiService.post(saveItemURL, body);
  }

  inserRemarks(body: any) {
    return this.apiService.post(remarksUrl, body)
  }

  insertClient(body: any, files: any) {

    const formData: FormData = new FormData()

    formData.append('usertype_id', '2')
    formData.append('username', body.email_address)
    formData.append('email_address', body.email_address)
    formData.append('dob', body.dob)
    formData.append('password', body.password)
    formData.append('firstname', body.firstname)
    formData.append('middlename', body.middlename)
    formData.append('surename', body.surename)
    formData.append('contact_no', body.contact_no)
    formData.append('has_file', body.has_file)
    formData.append('file_count', '1')
    formData.append('img_profile', files)
    formData.append('age', body.age)
    formData.append('gender', body.gender)
    formData.append('province', body.province)
    formData.append('region', body.region)
    formData.append('city', body.city)
    formData.append('barangay', body.barangay)
    formData.append('address', body.address)
    formData.append('remarks', body.remarks)
    return this.apiService.insertClient(addClient, formData);
  }

  insertGuest(body: any, files: any) {

    const formData: FormData = new FormData()

    formData.append('usertype_id', '2')
    formData.append('username', body.email_address)
    formData.append('email_address', body.email_address)
    formData.append('dob', body.dob)
    formData.append('password', body.password)
    formData.append('firstname', body.firstname)
    formData.append('middlename', body.middlename)
    formData.append('surename', body.surename)
    formData.append('contact_no', body.contact_no)
    formData.append('has_file', body.has_file)
    formData.append('file_count', '1')
    formData.append('img_profile', files)
    formData.append('age', body.age)
    formData.append('gender', body.gender)
    formData.append('province', body.province)
    formData.append('region', body.region)
    formData.append('city', body.city)
    formData.append('barangay', body.barangay)
    formData.append('address', body.address)
    formData.append('remarks', body.remarks)

    return this.http.post(`${HTTP_API_URL}guest`, formData);
    // return this.http.put(`${HTTP_API_URL}confirm-order/proof-of-payment/${id}`, formData);
    // return this.apiService.insertClient(addGuest, formData);
  }

  clientUpdate(body, files: any, id){

    const formData: FormData = new FormData();

    formData.append('usertype_id', body.usertype_id);
    formData.append('username', body.email_address);
    formData.append('email_address', body.email_address);
    // formData.append('password', body.password);
    formData.append('firstname', body.firstname);
    formData.append('middlename', body.middlename);
    formData.append('surename', body.surename);
    formData.append('dob', body.dob);
    formData.append('contact_no', body.contact_no);
    formData.append('age', body.age);
    formData.append('gender', body.gender);
    formData.append('has_file', "false");
    formData.append('file_count', '0');
    if(files){
      formData.append('has_file', "true")
      formData.append('file_count', '1')
      formData.append('img_profile', files)
    }
    formData.append('province', body.province)
    formData.append('region', body.region)
    formData.append('city', body.city)
    formData.append('barangay', body.barangay)
    formData.append('address', body.address)
    formData.append('remarks', body.remarks)
    // formData.append('img_profile', files)

    return this.apiService.put(`${userUrl}?id=${id}`, formData );
  }

  getUser(id: number) {
    return this.http.get(`${HTTP_API_URL}user/${id}`)
  }

  getTransaction(id) {
    return this.apiService.get(custOrder + id)
  }

  getOrderTransaction(id) {
    return this.apiService.get(getOrderID + id)
  }

  setProofPayment(files: any, id) {
    const formData: FormData = new FormData()
    formData.append('has_file', 'true')
    formData.append('proofofpayment', files)
    return this.apiService.uploadImage(proofPaymentURL + id, formData)
  }

  setProofPaymentConfirmOder(files: any, id) {
    const formData: FormData = new FormData()
    formData.append('has_file', 'true')
    formData.append('proofofpayment', files)

    return this.apiService.uploadImage(proofPaymentConfirmURL + id, formData)
    // return this.http.put(`${HTTP_API_URL}confirm-order/proof-of-payment/${id}`, formData);
  }

  uploadTestimony(body, files: Set<File>){

    const formData: FormData = new FormData()

    formData.append('item_id', body.item_id+'')
    formData.append('user_id', body.user_id+'')
    formData.append('rate', body.rate+'')
    formData.append('message', body.message)
    formData.append('order_no', body.order_no)
    formData.append('type', body.type)

    let counter = 0
    files.forEach(file => {
      counter++
      formData.append(`testimony_image_${counter}`, file)
    })

    formData.append('file_count', counter+'')
    formData.append('has_file',counter != 0 ? "true" : 'false')

    return this.http.post(`${HTTP_API_URL}testimony`, formData );

  }

  resetPassword(body: any){
    return this.apiService.post(resetPass, body)
  }

	delete(data: any) {
		console.log(data.id);
		console.log(data.email);
		console.log(data.option);
    return this.http.put(`${HTTP_API_URL}user/delete-remove`, data);
  }
}
