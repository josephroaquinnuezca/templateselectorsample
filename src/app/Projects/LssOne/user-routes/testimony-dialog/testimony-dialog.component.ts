import { UserService } from 'src/app/user-routes/shared/user.service';
import { ConfirmOrderService } from '../../_client-services'
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef , MatDialog} from '@angular/material';

@Component({
  selector: 'app-testimony-dialog',
  templateUrl: './testimony-dialog.component.html',
  styleUrls: ['./testimony-dialog.component.scss']
})
export class TestimonyDialogComponent implements OnInit {
  @ViewChild('file', { static: false }) file;
  public files: Set<File> = new Set();

  isView: boolean;
  currentImages: any[] = [];
  strUrl: any[] = [];
  rate: number = 0
  message: string
  rec: any
  isSaving: boolean = false

  constructor(@Inject(MAT_DIALOG_DATA) data, private service: UserService, public dialogRef: MatDialogRef<TestimonyDialogComponent>, private confirmOrderService: ConfirmOrderService) {
    this.rec = data
   }

  ngOnInit() {}

  addFiles() {
    this.file.nativeElement.click();
  }

  onFilesAdded() {
    const files: { [key: string]: File } = this.file.nativeElement.files
    for (let key in files) {
      if (!isNaN(parseInt(key))) {
        this.files.add(files[key])
        let reader = new FileReader()
        reader.onload = (event: any) => {
          this.strUrl.push(event.target.result)
        }
        reader.readAsDataURL(files[key])
      }
    }
  }

  saveTestimony(){
    if(this.isSaving) return
    this.isSaving = true
    let body = {
      item_id: this.rec.item_id,
      user_id: this.rec.user_id,
      rate: this.rate,
      message: this.message,
      order_no: this.rec.order_no,
      type : 1
    }
    var keepGoing = true;
    if(keepGoing){
      this.files.forEach(file => {
        if (file.size >= 6144000) {
          // console.log(file.size)
          alert("File size must not exceed 6mb. Please Try Again!");
          this.files= new Set();
          this.strUrl = []
          this.isSaving = false
          keepGoing = false;
          return;
        }
      })
    }
    if(keepGoing){
      let data = JSON.parse(sessionStorage.getItem('orderQuickView'))
      if (!data) {
        this.service.uploadTestimony(body, this.files).subscribe(res => {
          this.dialogRef.close()
          // console.log('saveTestimony', res)
          this.isSaving = false
        })
      }else{
        this.confirmOrderService.uploadTestimonyConfirmOrder(body, this.files).subscribe(res => {
          this.dialogRef.close()
          // console.log('saveTestimony', res)
          this.isSaving = false
        })
      }
    }
  }

  removeImg(i) {
    this.files.delete(i);
    this.strUrl.splice(i, 1);
  }

}
