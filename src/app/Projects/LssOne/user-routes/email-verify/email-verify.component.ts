import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { EmailVerificationService } from 'src/app/_client-services';
import { UserService } from '../shared/user.service';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';
import { MatDialog } from '@angular/material';

import * as jwt_decode from "jwt-decode";


@Component({
  selector: 'app-email-verify',
  templateUrl: './email-verify.component.html',
  styleUrls: ['./email-verify.component.scss']
})
export class EmailVerifyComponent implements OnInit {

	hasErrorValidating:boolean = false;

  constructor(public router: Router,
    public dialog: MatDialog,
    private route: ActivatedRoute, private checkEmail: EmailVerificationService, private userService: UserService) { }

  ngOnInit() {
		const token = this.route.snapshot.params['token'];
		console.log(token);
		if(!token){
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Invalid Request! Redirecting to login page.!'
      });
			// alert("Invalid Request! Redirecting to login page.");
			this.router.navigate(['user/signin']);
		}else{
			this.checkEmail.checkEmailVerification(token).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
				const r: any = res;
				console.log(r);
				this.hasErrorValidating = false;
				const data = {
					'verification_url' : token,
					'email_address' : r.data.email
				}
				console.log(data);
				// verification process starts here
					this.checkEmail.processEmailVerification(data).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
						const verifyData: any = res;
						let tokenData: any = this.tokenData(verifyData.data.token)
						console.log(verifyData);
						console.log(tokenData);
						this.hasErrorValidating = false;
						// sign in after verified
						if(!this.userService.setUser(verifyData.data.token)){
							this.hasErrorValidating = true;
						}else{
							this.hasErrorValidating = false;
							this.router.navigate(['user/email-verification/'+token]);
							// this.router.navigate(['']);
						}
					}, err => {
						this.hasErrorValidating = true;
						console.log('HTTP Error:', err)
					}, () => {
						console.log("request completed");
					});
				// verification process ends here --------------------------------
				}, err => {
					this.hasErrorValidating = true;
					console.log('HTTP Error:', err)
				}, () => {
					console.log("request completed");
				});
		}
  }
	backToHome(){
		this.router.navigate(['']);
	}
	tokenData(token: string): any {
		try {
			return jwt_decode(token);
		}
		catch (Error) {
			return null;
		}
	}
	handleError(error) {
		console.log(error);
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.statusText}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.statusText}`;
    }
    return throwError(errorMessage);
  }
}
