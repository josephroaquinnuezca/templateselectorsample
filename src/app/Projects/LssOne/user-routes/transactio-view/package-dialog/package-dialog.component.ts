import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Component({
  selector: 'app-package-dialog',
  templateUrl: './package-dialog.component.html',
  styleUrls: ['./package-dialog.component.scss']
})
export class PackageDialogComponent implements OnInit {
	details: any;
  constructor(public dialogRef: MatDialogRef<PackageDialogComponent>, @Inject(MAT_DIALOG_DATA) data) {
		this.details = data;
	}

  ngOnInit() {
		console.log(this.details)
  }

}
