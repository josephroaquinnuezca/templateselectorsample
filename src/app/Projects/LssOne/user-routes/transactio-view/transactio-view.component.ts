import { UserService } from 'src/app/user-routes/shared/user.service';
import { Component, OnInit } from '@angular/core';
import { UserProvider } from '../shared/users.provider';
import { TransactionDesc } from '../shared/transaction.model';
import { map, catchError } from 'rxjs/operators';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TestimonyDialogComponent } from '../testimony-dialog/testimony-dialog.component';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderPackageService } from '../../_client-services'
import { PackageDialogComponent } from './package-dialog/package-dialog.component';
import { SuccessMesageDialogComponent } from 'src/app/static-pages/success-mesage-dialog/success-mesage-dialog.component';
import { OrderItem } from 'src/app/ordering-routes/shared/ordering.model';

@Component({
  selector: 'app-transactio-view',
  templateUrl: './transactio-view.component.html',
  styleUrls: ['./transactio-view.component.scss']
})
export class TransactioViewComponent implements OnInit {

  deliveryMeth = ["Delivery", "Pick-up at store", "Deliver and Pick-up at LBC Branch"]
	statMess = ['Waiting for confirmation', 'Payment Confirmed', 'Ready for pick-up', 'Out for delivery',
		'Order Completed', 'Cancelled', 'Closed'];
  transDesc: TransactionDesc
  itemDesc: OrderItem
  imgFile: File[] = []
	hasFile: boolean = false;

  isUpload = false

  mRemarks: any[] = []
  can_comment: boolean = false;
	// order_package: any;

  constructor(private userProvider: UserProvider, private userService: UserService, public dialog: MatDialog, private route: ActivatedRoute, private orderPackageService: OrderPackageService) { }

  ngOnInit() {
    this.getTransDesc()
  }

  localUrl = "";
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
			this.hasFile = true;
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrl = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
      this.imgFile = event.target.files[0];
      // this.isUpload = true
    }
  }

  uploadProof(){
    if(this.isUpload) return
    this.isUpload = true
    this.userService.setProofPayment(this.imgFile, this.transDesc.id).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.Sent:
            return

          case HttpEventType.Response:
            let res: any = event.body
            this.isUpload = false
						this.hasFile = false;
            // alert('Upload finished') //
            const dialogRef = this.dialog.open(SuccessMesageDialogComponent, {
              panelClass: 'app-full-bleed-dialog-p-26',
              data: 'Thank you for uploading your payment , You will receive an email once payment is confirmed.'
            });
            this.getTransDesc();
            return
        }
      }),
      catchError((err: HttpErrorResponse) => {
        // console.log('HttpErrorResponse', err)
        this.isUpload = false
        return Observable.throw(err);
      })
    ).subscribe()
  }

  getTransDesc() {
    this.mRemarks = []


    this.userService.getOrderTransaction(this.route.snapshot.params['order_no']).subscribe(
      res => {
        console.log('getTransDesc', res)
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        for (let itm of res.data.items[0].order_items)
          this.mRemarks.push({
            item_id: itm.item_id,
            user_id: currentUser.id,
            rate: itm.rate,
            message: itm.message
          })
        this.transDesc = res.data.items[0]
        this.itemDesc = res.data.items[0].order_items
        console.log("orderInfo", this.itemDesc )
        this.localUrl = this.transDesc.proofofpayment
        this.can_comment = false
        for(let itm of this.transDesc.order_items){
          console.log('itm.can_comment: ', itm.can_comment)
          if (itm.can_comment) {
            console.log('if itm.can_comment: ', itm.can_comment)
            this.can_comment = true
          }
        }
        console.log('can_comment: ', this.can_comment)

				// if (this.deliveryMeth[this.transDesc.service_type - 1] == 'Delivery'){
				// 	var x = this.transDesc.order_no;
				// 	this.orderPackageService.fetchOrderPackage(x).subscribe(res => {
				// 		const res_data: any = res;
        //
				// 		if (res_data.status.error === false ) {
				// 			console.log(res_data);
				// 			this.order_package = res_data.data;
				// 		}
				// 	});
				// }

      }
    )
  }

  saveRemarks() {
    this.userService.inserRemarks(this.mRemarks).subscribe(
      res => {
        this.getTransDesc()
      }
    )
  }

  setTestimony(row){
    let currentUser = JSON.parse(localStorage.getItem('currentUser'))
    const dialogRef = this.dialog.open(TestimonyDialogComponent, {
      width: '80%', height: '800px',
      data: {
        user_id: currentUser.id,
        item_id: row,
        order_no: this.transDesc.order_no //this.userProvider.transData.order_no
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      this.getTransDesc()
    });
  }
	openDialog(data: any) {
		// console.log(email)
    const dialogRef = this.dialog.open(PackageDialogComponent, {
		  panelClass: 'app-full-bleed-dialog-p-10',
      data: {
				data
			}
      // disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
			// console.log(result);
      // if (result) {
      //   this.userService.signout();
			// 	this.router.navigate(['/user/signin']);
      // }
    });
  }

}
