import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutesRoutingModule } from './user-routes-routing.module';
// import { BrowserModule } from '@angular/platform-browser';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialsModule } from './../shared/materials.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MomentModule } from 'ngx-moment';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgxBarcodeModule } from 'ngx-barcode';
import { DigitOnlyModule } from '@uiowa/digit-only';
import { ToastrModule } from 'ngx-toastr';
import { AgmCoreModule, GoogleMapsAPIWrapper   } from '@agm/core';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SigninRouterComponent } from './signin-router/signin-router.component';
import { UserSigninComponent } from './user-signin/user-signin.component';
import { UserSuccessMessageComponent } from './user-success-message/user-success-message.component';
import { UserSignupComponent } from './user-signup/user-signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordVerifyComponent } from './reset-password-verify/reset-password-verify.component';
import { EmailVerifyComponent } from './email-verify/email-verify.component';

import { AgmDialogComponent } from './user-signup/agm-dialog/agm-dialog.component';
@NgModule({
  imports: [
    CommonModule,
    UserRoutesRoutingModule,
    // BrowserModule,
    // BrowserAnimationsModule,
    MaterialsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxChartsModule,
    NgxMaterialTimepickerModule,
    MomentModule,
    FlexLayoutModule,
    SlickCarouselModule,
		NgxBarcodeModule,
    DigitOnlyModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-center',
      preventDuplicates: true,
      closeButton:true,
      progressBar:true,
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDEoNWWHYCtUfBIiqeYW0K1sw63JK11tEE'
    }),
  ],
  declarations: [
    SigninRouterComponent,
    UserSigninComponent,
    UserSuccessMessageComponent,
    UserSignupComponent,
    ForgotPasswordComponent,
    ResetPasswordVerifyComponent,
    EmailVerifyComponent,
    AgmDialogComponent,
  ],
  exports:[UserSignupComponent],
  entryComponents: [
    AgmDialogComponent,
  ],
  providers: [
    GoogleMapsAPIWrapper
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})

export class UserRoutesModule { }
