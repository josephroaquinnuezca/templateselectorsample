// import { UserService } from './../shared/user.service';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { UsersService } from '../../_admin-services';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResetPasswordPrv } from '../shared/reserpassword.provider';
import { first, retry, catchError, startWith, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Component({
  selector: 'app-reset-password-verify',
  templateUrl: './reset-password-verify.component.html',
  styleUrls: ['./reset-password-verify.component.scss']
})
export class ResetPasswordVerifyComponent implements OnInit {

  constructor(public router: Router,
    private route: ActivatedRoute,
    private api: UsersService,
    private resetPasswordData: ResetPasswordPrv,
    public dialog: MatDialog,) { }

  ngOnInit() {

    const token = this.route.snapshot.params['token'];
    // this.api.passwordVerify(token)
    // .pipe(
    //   retry(1),
    //   catchError(this.handleError),
    // )
    // .subscribe(res => {
    //   const d: any = res;
    //   this.resetPasswordData.data = d.data;
    //   this.resetPasswordData.data.pwd_url = token;
    //     this.router.navigate(['reset-password/edit']);
    // });
    this.api.passwordVerify(token).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
        const d: any = res;
        this.resetPasswordData.data = d.data;
        this.resetPasswordData.data.pwd_url = token;
        this.router.navigate(['reset-password/edit']);
    }, err => {
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: err.error.status.description
      });

      dialogRef.afterClosed().subscribe(res => {
        this.router.navigate(['./user/signin']);
        return;
      });
    }, () => {
      console.log("request completed");
    });
  }
  handleError(error) {
    console.log(error);
    return throwError(error);
  }
  // handleError(error) {
  //   console.log(error)
  //   let errorMessage = '';
  //   if (error.error instanceof ErrorEvent) {
  //     // client-side error
  //     errorMessage = `Error: ${error.statusText}`;
  //   } else {
  //     // server-side error
  //     errorMessage = `Error Code: ${error.status}\nMessage: ${error.statusText}`;
  //   }
  //   // this.router.navigate(['']);
  //   window.alert(errorMessage);
  //   return throwError(errorMessage);
  // }
}
