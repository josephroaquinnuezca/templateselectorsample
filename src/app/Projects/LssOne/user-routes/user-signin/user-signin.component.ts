import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';
import { Carousel } from '../../shared/carousel.config';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../shared/user.service';
import { UserProvider } from '../shared/users.provider';
import { OverridecssService } from '../../ordering-routes/shared/overridecss.service';
import * as jwt_decode from "jwt-decode";
import { ToastrService } from 'ngx-toastr'; //may 31, 2020
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-user-signin',
  templateUrl: './user-signin.component.html',
  styleUrls: ['./user-signin.component.scss']
})
export class UserSigninComponent implements OnInit {

  chipLog: string = 'primary'
  chipReg: string = 'none'
  signinForm: FormGroup
  leftSlide: Carousel
  rightSlide: Carousel
  imgsUrl: string[] = []
  submitted: boolean = false;
	needsVerification:boolean = false;

  constructor(
    public dialog: MatDialog,
    private toastr: ToastrService, // new constructor may 31, 2020
    private formBuilder: FormBuilder, private userService: UserService, private router: Router, private userProvider: UserProvider, private route: ActivatedRoute, private overridecssService: OverridecssService) {
      this.signinForm = this.formBuilder.group({
        // username: ['', [Validators.required, Validators.email]],
        username: [ '', Validators.required ],
        password: [ '', Validators.required ]
      });
    }

  ngOnInit() {
    this.userProvider.isSignComponent = true;
    this.overridecssService.emitOverrideCssEvent(this.userProvider.isSignComponent);
    // console.log(`isSignComponent: ${this.userProvider.isSignComponent}`);
    // this.createForm();
  }
  get signinFormControl() {
    return this.signinForm.controls;
  }
  ngOnDestroy(){
    this.userProvider.isSignComponent = false;
    this.overridecssService.emitOverrideCssEvent(this.userProvider.isSignComponent);
    // console.log(`isSignComponent: ${this.userProvider.isSignComponent}`);
  }

  ngAfterViewInit() {
    if (this.userService.hasCurrentUser()) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'))
      // if(currentUser.usertype_id == 1) {
        this.router.navigate(['']);
      // }else{
      //   this.router.navigate(['./order-request']);
      // }
    }
  }

  // createForm() {
  //
  // }
  get f() { return this.signinForm.controls; }

  trimValue(formControl) {
    // console.log(formControl);
    formControl.setValue(formControl.value.trim().replace(/\s\s+/g, ' '));
  }

  onSubmit(){
    this.submitted = true;
    if(this.signinForm.invalid){
      for (let v in this.signinForm.controls) {
        this.signinForm.controls[v].markAsTouched();
      }
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Incomplete information or invalid input provided!'
    });
      return;
    }
    this.userService.signin(this.signinForm.getRawValue()).subscribe(
      res =>{
        console.log('res.status.error:', res.status.error)
        if(res.status.error){
          this.toastr.error('User not found or Password incorrect');
          // alert("User not found or Password incorrect")
          return
        }
        // console.log('onSubmit', res.status.error)

        // if(url === '/user/admin'){
        //   let tokenData: any = this.tokenData(res.data.token)
        //   if(tokenData.user_info.usertype_id != 2){
        //     alert("Your'e not allowed to signin here")
        //     return
        //   }
        //   this.submitted = false;
        //   this.userService.setUser(res.data.token)
        //   this.router.navigate(['./admin']);
        // }else{
        //   this.submitted = false;
        //   this.userService.setUser(res.data.token)
        //   this.router.navigate(['']);
        // }
        let tokenData: any = this.tokenData(res.data.token)
        if(tokenData.user_info.usertype_id == 1){
          this.submitted = false;
          // this.userService.setUser(res.data.token)
          // this.router.navigate(['./admin']);
          // alert('You will be redirected to admin login.')
          this.toastr.success('You will be redirected to admin login.');
          localStorage.removeItem('currentAdmin');
          localStorage.removeItem('lss-token');
          this.router.navigate(['/admin/login'])

        }else{
          this.submitted = false;
					// console.log(this.userService.setUser(res.data.token));
          if(!this.userService.setUser(res.data.token)){
						this.needsVerification = true;
            // this.toastr.success('You are now succesfully login');
            //     setTimeout (() => {
            //       let url = this.router.url
            //  }, 5000);
						this.router.navigate(['user/signin']);
          }else{
            this.router.navigate(['']);
            this.needsVerification = false;
            localStorage.removeItem('infocustomer');
            localStorage.removeItem('infocustomerNSA');
            localStorage.removeItem('deliveryOpts');
            this.toastr.success('You are now succesfully login');
              setTimeout (() => {
                // let url = this.router.url
            }, 5000);
					}
        }
      }
    )
  }
	backToLogin(){
		this.needsVerification = false;
	}
  tokenData(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

}
