import { ToastrService } from 'ngx-toastr';
import { SuccessMesageDialogComponent } from 'src/app/static-pages/success-mesage-dialog/success-mesage-dialog.component';
import { Component, OnInit, Inject, EventEmitter, ViewChild, Input, Output, AfterViewInit, ElementRef, OnDestroy, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher, MatDialog } from '@angular/material';
import { UserService } from '../shared/user.service';
import { Router } from '@angular/router';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import * as locations from '../../ordering-routes/shared/locations.json'
import { UserDetail } from '../shared/transaction.model';
import { UserProvider } from '../../user-routes/shared/users.provider';
import { OverridecssService } from '../../ordering-routes/shared/overridecss.service';
import { DatePipe } from '@angular/common';
import { HelperService } from '../../_helper-service';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';
import { UsersService } from '../../_admin-services';


import { AgmDialogComponent } from './agm-dialog/agm-dialog.component';
import { } from 'googlemaps';
declare var google: any;

@Component({
  selector: 'app-user-signup',
  templateUrl: './user-signup.component.html',
  styleUrls: ['./user-signup.component.scss']
})
export class UserSignupComponent implements OnInit {
  registrationForm: FormGroup;


  //
  submitted = false;


  matcher = new MyErrorStateMatcher();

	userExistError: boolean = false;
  locations = locations.default;
  mcit: any[] = [];
  mreg: any[] = [];
  cities: any[] = []
  userDetail: UserDetail

  imgFile: File
  isUpdate = false;
  isUpdatePassword: boolean = false;
  isUpdateEmail: boolean = false;
  isUpdateOrPassword: boolean = false;

	newDateOfBirth:any;

  regionName:any;
  provinceName:any;
  cityName:any;
  barangayName:any;

  // regionArray:any;
  provinceArray:any;
  cityArray:any;
  barangayArray:any;

  signinForm: FormGroup;


  customerAddressTextReferenceValue: any;
  mapDialogDataRegistrationForm: any = {lat: 14.55540163953262, lng: 121.01935339751486};
  @Input() adressType: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @ViewChild('customerAddressText', {static: false}) customerAddressText: any;

  constructor(
    private zone: NgZone,
    private toastr: ToastrService,
    public dialog: MatDialog,
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private usersService: UsersService,
    private router: Router,
    private overridecssService: OverridecssService,
    private userProvider: UserProvider,
    private helperService: HelperService) { }

  sortASC(a, b)
  {
    return a.city_name < b.city_name ? -1 : a.city_name > b.city_name ? 1 : 0;
      // return itemA.city_name - itemB.city_name;
  }
  sortDESC(a, b)
  {
    return a.city_name < b.city_name ? -1 : a.city_name > b.city_name ? 1 : 0;
      // return itemB.city_name - itemA.city_name;
  }

  getRegionValue(event) {
    this.cities = event.value.city_list.sort(this.sortASC);
    // console.log(this.cities);
  }

	getRegionValueSet(event) {
    this.cities = event.city_list.sort(this.sortASC);
    // console.log(this.cities);
  }
  getDataAttrRegion(event){
    console.log(event);
    console.log('event value:', event.value);

    let tmp:any;
    for(let location of this.locations){
      if(location.region_name === event.value) tmp = location.province_list
    }
    console.log('tmp', tmp)
    this.regionName = event.value;
    if (this.provinceArray) {
      this.provinceArray = [];
      this.cityArray = [];
      this.barangayArray = [];
    }
    // console.log(this.provinceArray)
    // console.log(this.cityArray)
    // console.log(this.barangayArray)
    this.provinceArray = tmp;
  }
  getDataAttrProvince(event){
    console.log(event);
    console.log('event value:', event.value);
    console.log('ProvinceArray: ', this.provinceArray)
    console.log('ProvinceArray: ', this.provinceArray[event.value])
    console.log('ProvinceArray: ', this.provinceArray[event.value].municipality_list)
    this.provinceName = event.value;
    if (this.cityArray) {
      this.cityArray = [];
      this.barangayArray = [];
    }
    this.cityArray = this.provinceArray[event.value].municipality_list;
  }
  getDataAttrCity(event){
    console.log(event);
    console.log('event value:', event.value);
    console.log('barangayArray: ', this.cityArray)
    console.log('barangayArray: ', this.cityArray[event.value])
    this.cityName = event.value;
    if (this.barangayArray) {
      this.barangayArray = [];
    }
    this.barangayArray = this.cityArray[event.value].barangay_list;
  }
  getDataAttrBarangay(event){
    this.barangayName = event.value;
    // console.log(event.target.dataset.barangay);
    // console.log(event.target.getAttribute('data-barangay'))
  }
  clearPCB(){
    this.provinceName = ''
    this.cityName = ''
    this.barangayName = ''

    this.registrationForm.controls['province'].setValue('');
    this.registrationForm.controls['city'].setValue('');
    this.registrationForm.controls['barangay'].setValue('');
  }
  clearCB(){
    this.cityName = ''
    this.barangayName = ''

    this.registrationForm.controls['city'].setValue('');
    this.registrationForm.controls['barangay'].setValue('');
  }
  clearB(){
    this.barangayName = ''

    this.registrationForm.controls['barangay'].setValue('');
  }
  ngOnInit() {
    console.log('this.userProvider.isFromCheckoutPersonal: ', this.userProvider.isFromCheckoutPersonal)
    // console.log(this.place);
    // console.log(this.place[2].region_list[0].city_list);
    // //Sort ASC
    // var asc = this.place[2].region_list[0].city_list.sort(this.sortASC)
    // console.log(asc);
    // //Sort DESC
    // var desc = this.place[2].region_list[0].city_list.sort(this.sortDESC)
    // console.log(desc);

    this.userProvider.isSignComponent = true;
    this.overridecssService.emitOverrideCssEvent(this.userProvider.isSignComponent);
    // console.log(`isSignComponent: ${this.userProvider.isSignComponent}`);

    this.isUpdate = this.router.url.includes('information-update');
    this.isUpdatePassword = this.router.url.includes('password-update');
    this.isUpdateEmail = this.router.url.includes('email-update');
    if(this.isUpdate){
      this.isUpdateOrPassword = false;
			this.createFormUpdate();
      this.setData();
    }else if(this.isUpdatePassword){
      this.isUpdateOrPassword = true;
      this.createFormPassword()
    }else if(this.isUpdateEmail){
      this.isUpdateOrPassword = true;
      this.createFormEmail()
    }else{
      this.isUpdateOrPassword = false;
      this.createForm();
    }

    console.log('this.isUpdate: ', this.isUpdate);
    console.log('this.isUpdatePassword: ', this.isUpdatePassword);
    console.log('this.isUpdateEmail: ', this.isUpdateEmail);
  }
  //end of onINit
    get registerFormControl() {
      return this.registrationForm.controls;
    }


  ngOnDestroy(){
    this.userProvider.isSignComponent = false;
    this.overridecssService.emitOverrideCssEvent(this.userProvider.isSignComponent);
    // console.log(`isSignComponent: ${this.userProvider.isSignComponent}`);
  }
  createFormPassword(){
    this.signinForm = this.formBuilder.group({
      password: [ '', Validators.required ]
    });
  }
  createFormEmail(){
    this.signinForm = this.formBuilder.group({
      email_address: [ '', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
    });
  }
  createForm() {
    // , Validators.email - remove add this if needed
    this.registrationForm = this.formBuilder.group({
      username: ['presetvalue', [Validators.required, Validators.minLength(6), Validators.maxLength(15), Validators.pattern('^[a-zA-Z0-9]*$')]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(15), Validators.pattern('^[A-Za-z0-9_@./#&+-]*$')]],
      password2: ['', Validators.required],
      firstname: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      middlename: ['', [Validators.pattern('^[a-zA-Z ]*$')]],
      surename: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      dob: [{value: '', disabled: false}, []],
      email_address: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      contact_no: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10),  Validators.pattern('^[0-9]*$')]],
      age: ['0', []],
      gender: ['0', []],
      province: [[], ], //Validators.required
      region: [[], ], //Validators.required
      city: [[], ], //Validators.required
      barangay: ['', ], //Validators.required
      address: ['', Validators.required],
      remarks: ['', []]
    }, { validator: this.checkPasswords })
  }

  createFormUpdate() {
    this.registrationForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      firstname: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      middlename: ['', [Validators.pattern('^[a-zA-Z ]*$')]],
      surename: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      dob: [{value: '', disabled: false}, []],
      email_address: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      contact_no: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10),  Validators.pattern('^[0-9]*$')]],
      age: ['', []],
      gender: ['', []],
      province: [[], ], //Validators.required
      region: [[], ], //Validators.required
      city: [[], ], //Validators.required
      barangay: ['', ], //Validators.required
      address: ['', Validators.required],
      remarks: ['', []]
    })
  }
  setData(){
    let user = JSON.parse(localStorage.getItem('currentUser'))
    this.userService.getUser(user.id).subscribe(
      resData => {
        const res: any = resData;
        this.userDetail = res.data.items[0];
        console.log('UserSignupComponent', this.userDetail);
        this.registrationForm.controls['username'].setValue(this.userDetail.username);
        // this.signupForm.controls['password'].setValue('******')
        // this.signupForm.controls['password2'].setValue('******')
        this.registrationForm.controls['firstname'].setValue(this.userDetail.firstname);
        this.registrationForm.controls['middlename'].setValue(this.userDetail.middlename);
        this.registrationForm.controls['surename'].setValue(this.userDetail.surename);
        this.registrationForm.controls['email_address'].setValue(this.userDetail.email_address);
				if(this.userDetail.dob){
					this.registrationForm.controls['dob'].setValue(this.datePipe.transform(new Date(this.userDetail.dob), 'yyyy-MM-dd'));
					// console.log(this.signupForm.controls['dob'].value);
					// return
					this.calculateAge(this.registrationForm.controls['dob'].value)
				}
        this.registrationForm.controls['contact_no'].setValue(this.userDetail.contact_no.replace('+63', ''));
        this.registrationForm.controls['age'].setValue(this.userDetail.age);
				this.registrationForm.controls['gender'].setValue(Number(this.userDetail.gender));
        // this.signupForm.controls['gender'].setValue(+this.userDetail.gender)
        this.registrationForm.controls['address'].setValue(this.userDetail.address);
        this.registrationForm.controls['remarks'].setValue(this.userDetail.remarks);
        //used for update if input is not touched
        // this.regionName = this.userDetail.region
        // this.provinceName = this.userDetail.province
        // this.cityName = this.userDetail.city
        // this.barangayName = this.userDetail.barangay
        //
        // this.registrationForm.controls['region'].setValue(this.regionName);
        // if (this.regionName != '') {
        //   this.getDataAttrRegion({'value': this.regionName})
        // }
        // this.registrationForm.controls['province'].setValue(this.provinceName);
        // if (this.provinceName != '') {
        //   this.getDataAttrProvince({'value': this.provinceName})
        // }
        // this.registrationForm.controls['city'].setValue(this.cityName);
        // if (this.cityName != '') {
        //   this.getDataAttrCity({'value': this.cityName})
        // }
        // this.registrationForm.controls['barangay'].setValue(this.barangayName);

        if (typeof this.userDetail.imgProfile !== 'undefined' && this.userDetail.imgProfile.length > 0) {
          this.localUrl = this.userDetail.imgProfile[0].file
        }
        else{
          this.localUrl = ''
        }
        // let tmp: any = []
        //
        // for (let myRegion in this.locations) {
        //   // console.log('myRegion:', this.locations.default[myRegion].region_name)
        //   // console.log('myRegion count:', myRegion)
        //   console.log(`myRegion Name : ${this.locations[myRegion].region_name} | myUserRegion ${this.userDetail.region}`)
        //   if (this.locations[myRegion].region_name === this.userDetail.region) {
        //     console.log('MAY TAMA KA!')
        //     let setValue = {'name': this.userDetail.region, 'location': this.locations[myRegion].province_list}
        //     this.signupForm.controls['region'].setValue(setValue);
        //     console.log('setValue:', setValue)
        //   }
        //   // for (let province in this.locations[myRegion].province_list) {
        //   //   console.log('province:', province)
        //   // }
        // }

        // for(let p of this.place){
        //   if(p.delivery_name === this.userDetail.province) tmp = p
        // }
        // this.signupForm.controls['province'].setValue(tmp)
        // for(let r of tmp.region_list){
        //   if(r.region_name === this.userDetail.region){
				// 		tmp = r
				// 		this.getRegionValueSet(tmp);
				// 	}
        // }
        // this.signupForm.controls['region'].setValue(tmp)
        // for(let c of tmp.city_list){
        //   if(c.city_name === this.userDetail.city) tmp = c
        // }
        // this.signupForm.controls['city'].setValue(tmp)
      }
    )
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.password2.value;

    return pass === confirmPass ? null : { notSame: true }
  }

  trimValue(formControl) {
    // console.log(formControl);
    formControl.setValue(formControl.value.trim().replace(/\s\s+/g, ' '));
  }
	getGenderValue(getGenderValue){
		// console.log(getGenderValue.value)
		// this.signupForm.controls['gender'].setValue(getGenderValue.value)
		//
    // console.log(this.signupForm.getRawValue());
	}

  onSubmitPasswordOrEmail(){
    console.log('QUAK!')
    let currentUser = JSON.parse(localStorage.getItem('currentUser'))
    let formData = this.signinForm.getRawValue()
    if (this.signinForm.invalid) {
      this.helperService.findInvalidControls(this.signinForm.controls);
      // alert('Incomplete information or invalid input provided!') //
        const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: 'Please input a password!'
      });
      return
    }
    if (this.isUpdatePassword) {
      var data: any = {
        id: currentUser.id,
        password: formData.password
      }
      console.log(data)
      // return
      this.usersService.changePasswordSingle(data).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
        const dataResult: any = res;
        console.log(dataResult);
        this.toastr.success('Successfully updated');
          setTimeout (() => {
        this.router.navigate(['/user-info']);
        }, 5000);
      }, err => {
        console.log('HTTP Error:', err.error);
        this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: err.error.status.description
        });
        return;
      }, () => {
        console.log("request completed");
      });
    }else if(this.isUpdateEmail){
      var data: any = {
        id: currentUser.id,
        email_address: formData.email_address
      }
      console.log(data)
      // return
      this.usersService.changeEmailSingle(data).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
        const dataResult: any = res;
        console.log(dataResult);
        this.toastr.success('Successfully updated');
          setTimeout (() => {
        this.router.navigate(['/user-info']);
        }, 5000);
      }, err => {
        console.log('HTTP Error:', err.error);
        this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: err.error.status.description
        });
        return;
      }, () => {
        console.log("request completed");
      });
    }
  }

  onSignUp() {
    if (this.registrationForm.invalid) {
      for (let v in this.registrationForm.controls) {
        this.registrationForm.controls[v].markAsTouched();
      }
			this.helperService.findInvalidControls(this.registrationForm.controls);
        const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: 'Incomplete information or invalid input provided!'
      });
      this.registrationForm.controls.untouched
      return
    }
    let data = this.registrationForm.getRawValue();
    delete data.password2
    data.usertype_id = 2;
    data.has_file = true;
    console.log('data.region: ', data.region);
    data.region = ''; //this.regionName
    data.province = ''; //this.provinceName
    data.city = ''; //this.cityName
    data.barangay = ''; //this.barangayName

    data.contact_no = '+63' + data.contact_no
		data.age = (data.age == '0' || data.age == '' || !data.age) ? 'None' : data.age
		data.gender = (data.gender == '0' || data.gender == '' || !data.gender) ? 'None' : data.gender
		data.middlename = (data.middlename == '' || !data.middlename) ? 'None' : data.middlename
		data.dob = (this.newDateOfBirth == '' || !this.newDateOfBirth) ? 'None' : this.newDateOfBirth
    console.log('CREATE ACCOUNT DATA:', data)
    // return
		// console.log(data);
		// return
		// console.log(data);
		// return
    // console.log('onSignUp', data)
    if(this.isUpdate){
      this.update(data)
    }else{
			this.userService.insertClient(data, this.imgFile).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
				const data: any = res;
				console.log(data);
			}, err => {
				console.log('HTTP Error:', err.error);
        console.log('err.error.type:', err.error.type)
				this.userExistError = true;
				console.log(this.userExistError);
        this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: err.error.status.description
        });
        return;
			}, () => {
        console.log("request completed");
				this.router.navigate(['/user/user-success-message']);
			});
		}

    // this.userService.insertClient(data, this.imgFile).pipe(
    //   map(event => {
    //     switch (event.type) {
    //       case HttpEventType.Sent:
    //         return
		//
    //       case HttpEventType.Response:
    //         let res: any = event.body
    //         this.router.navigate(['/user/signin']);
    //         return
    //     }
    //   }),
    //   catchError((err: HttpErrorResponse) => {
    //     // console.log('HttpErrorResponse', err)
    //     return Observable.throw(err);
    //   })
    // ).subscribe()
  }
  backUpdate(){
    if (this.userProvider.isFromCheckoutPersonal) {
      this.router.navigate(['/personal-info']);
      // this.userProvider.isFromCheckoutPersonal = false;
    }else{
      this.router.navigate(['/user-info']);
      // this.router.navigate(['/user-info']);
    }
  }
  update(rawData) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'))
    this.userService.clientUpdate(rawData, this.imgFile, currentUser.id)
    .pipe(first())
    .subscribe(
      data => {
        // console.log(rawData.firstname);
        // console.log(rawData.surename);
        let body = {
  	      id: currentUser.id,
  	      username: currentUser.username,
  	      email_address: currentUser.email_address,
  	      contact_no: rawData.contact_no,
  	      img_profile: currentUser.img_profile,
  	      firstname: rawData.firstname,
  	      middlename: rawData.middlename,
  	      surename: rawData.surename,
  	      usertype_id: currentUser.usertype_id,
  	      // destination: tokenData.destination,
  				dob : rawData.dob,
  				age : rawData.age
  	    }
        localStorage.setItem("currentUser", JSON.stringify(body));
        if (this.userProvider.isFromCheckoutPersonal) {
          this.router.navigate(['/personal-info']);
          this.toastr.success('Successfully updated');
            setTimeout (() => {
          }, 5000);
          // this.userProvider.isFromCheckoutPersonal = false;
        }else{
          this.router.navigate(['/user-info']);
          this.toastr.success('Successfully updated');
            setTimeout (() => {
          }, 5000);
          // this.router.navigate(['/user-info']);
        }
      },
      error => {
        if (error instanceof HttpErrorResponse) {
          console.log(error.status)
          if (error.status != 403) {
            // alert('Plese try again')//
            const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
                panelClass: 'app-full-bleed-dialog-p-26',
                disableClose: false,
                data: 'Plese try again'
            });
          }
        }
      });
  }

  localUrl = "";
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrl = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
      this.imgFile = event.target.files[0];
      // this.isUpload = true
    }
  }

	calculateAge(value){
		var newDate = this.datePipe.transform(new Date(value), 'yyyy-MM-dd HH-mm-ss')
		var newDateTime = new Date(value).getTime()
		var timeDiff = Math.abs(Date.now() - newDateTime);
		var age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
		this.registrationForm.controls['age'].setValue(age)
		this.newDateOfBirth = newDate;
		// console.log(value)
		// console.log(newDate)
	}

	handleError(error) {
		console.log(error);
    // let errorMessage = '';
    // if (error.error instanceof ErrorEvent) {
    //   // client-side error
    //   errorMessage = `Error: ${error.statusText}`;
    // } else {
    //   // server-side error
    //   errorMessage = `Error Code: ${error.status}\nMessage: ${error.statusText}`;
    // }
    // return throwError(errorMessage);
    return throwError(error);
  }
	clearAlertMessage(){
		if(this.userExistError){
			this.userExistError = false;
		}
	}

///////////////////////////////////////////
// FOR GOOGLE MAP
//////////////////////////////////////////
  onKeyUpRegistrationFormAddress(e){
    // let infocustomer = JSON.parse(localStorage.getItem('infocustomer'))
    if (this.isUpdate) {
      console.log('nagrun ito A')
      if (e.target.value === this.userDetail.address) {
        this.registrationForm.controls['address'].setErrors(null)
      }else{
        console.log(e.target.value.length)
        if (e.target.value.length == 0) {
          this.registrationForm.controls['address'].setErrors({ required: true })
        }else{
          if (this.customerAddressTextReferenceValue) {
            if (e.target.value === this.customerAddressTextReferenceValue) {
              console.log('e.target.value: ', e.target.value)
              console.log('customerAddressTextReferenceValue: ', this.customerAddressTextReferenceValue)
              this.registrationForm.controls['address'].setErrors(null)
            }else{
              this.registrationForm.controls['address'].setErrors({ isInvalidRegistrationForm: true })
            }
          }else{
            this.registrationForm.controls['address'].setErrors({ isInvalidRegistrationForm: true })
          }
        }
      }
    }else{
      console.log('nagrun ito B')
      console.log(e.target.value);
      console.log(e.target.value.length)
      if (e.target.value.length == 0) {
        this.registrationForm.controls['address'].setErrors({ required: true })
      }else{
        if (this.customerAddressTextReferenceValue) {
          if (e.target.value === this.customerAddressTextReferenceValue) {
            console.log('e.target.value: ', e.target.value)
            console.log('customerAddressTextReferenceValue: ', this.customerAddressTextReferenceValue)
            this.registrationForm.controls['address'].setErrors(null)
          }else{
            this.registrationForm.controls['address'].setErrors({ isInvalidRegistrationForm: true })
          }
        }else{
          this.registrationForm.controls['address'].setErrors({ isInvalidRegistrationForm: true })
        }
      }
    }
    this.registrationForm.controls['address'].markAsTouched();
  }

  openAgmMap(inputValue, isUpdate, formName, mapDialogData){
    // console.log(inputValue.value)
    // console.log(isUpdate)
    // console.log(formName)
    // console.log(mapDialogData)
    // return;
    const dialogData = JSON.parse(JSON.stringify(mapDialogData));
    const dialogRef = this.dialog.open(AgmDialogComponent, {
      panelClass: 'app-full-bleed-dialog-full-screen',
      disableClose: false,
      data: { inputValue: inputValue.value, isUpdate: isUpdate, form_type: formName, defaultCenter: dialogData }

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('resultFromDialogMap: ', result)
      if (result) {
        this.registrationForm.controls['address'].setValue(result.geocodeAddress);
      }

    });
  }

  ngAfterViewInit() {
      // this.getPlaceAutocomplete();
    this.getPlaceAutocompleteCustomer();

  }

  getPlaceAutocompleteCustomer() {
      const autocomplete = new google.maps.places.Autocomplete(this.customerAddressText.nativeElement,
          {
              componentRestrictions: { country: 'PH' },
              // types: ['geocode', 'establishment', 'address']  // 'establishment' / 'address' / 'geocode'
          }, (result, status) => {
            console.log('status: ', status)
            console.log('result: ', result)
          });
      autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
          const place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            // window.alert("No details available for input: '" + place.name + "'");
            this.registrationForm.controls['address'].setErrors({ isInvalidRegistrationForm: true })
            // return;
          }else{
            console.log('getPlaceAutocomplete: ', place)
            console.log('getPlaceAutocompleteLatLang: ', place.geometry.location.lat())
            console.log('getPlaceAutocompleteLatLang: ', place.geometry.location.lng())
            this.mapDialogDataRegistrationForm.lat = place.geometry.location.lat();
            this.mapDialogDataRegistrationForm.lng = place.geometry.location.lng();
            this.customerAddressTextReferenceValue = this.customerAddressText.nativeElement.value;

            console.log('log1: ', this.customerAddressText.nativeElement.value);
            this.zone.run( () => {
              this.registrationForm.controls['address'].setErrors(null);
              this.registrationForm.controls['address'].setValue(this.customerAddressText.nativeElement.value);
            });
            console.log('log2: ', this.registrationForm.controls['address']);
            // this.invokeEvent(place);
          }
      });
  }


  invokeEvent(place: Object) {
      console.log('invokeEvent: ', place)
      this.setAddress.emit(place);
  }


}
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidParent = !!(
      control
      && control.parent
      && control.parent.invalid
      && control.parent.dirty
      && control.parent.hasError('notSame'));
    return (invalidParent);
  }
}
