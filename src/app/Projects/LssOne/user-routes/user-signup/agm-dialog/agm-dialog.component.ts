import { Component, OnInit, Inject, ViewChild, NgZone } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MouseEvent } from '@agm/core';
import { DialogData, Marker } from 'src/app/_client-interface/';
import { GoogleMapsAPIWrapper, MapsAPILoader, AgmMap } from "@agm/core";
declare var google: any;

@Component({
  selector: 'app-agm-dialog',
  templateUrl: './agm-dialog.component.html',
  styleUrls: ['./agm-dialog.component.scss']
})

export class AgmDialogComponent implements OnInit {
  constructor(
    private maps: MapsAPILoader,
    private googleMapsAPIWrapper: GoogleMapsAPIWrapper,
    public dialogRefAGM: MatDialogRef<AgmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private zone: NgZone) {

      console.log('dataAGM: ', data)
      this.formType = data.form_type;
      this.defaultCenter = data.defaultCenter;
    }

  defaultCenter = {lat: 14.55540163953262, lng: 121.01935339751486};
  currentCenter = Object.assign({}, this.defaultCenter);
  zoom = 20;
  addressText:any;

  newCenterLat: number
  newCenterLng: number

  formType: any;
  dialogData: any = false;

  @ViewChild(AgmMap, {static: true}) agmMap: AgmMap;

  ngOnInit() {
    // this.maps.load().then(() => {
    //   debugger;
    //   this.googleMapsAPIWrapper.getCenter().then(place => {
    //     debugger;
    //     alert(place)
    //   });
    // });
    // this.geocodeInit();
		this.dialogRefAGM.beforeClose().subscribe(() => this.dialogRefAGM.close(this.dialogData));
  }

  closeDialog(){
    this.dialogData = false;
    this.dialogRefAGM.close(this.dialogData);
  }
  selectAddress(){
    this.dialogData = {
      form_type: this.formType,
      latlng: this.defaultCenter,
      geocodeAddress: this.addressText
    }
    this.dialogRefAGM.close(this.dialogData);
  }
  // geocodeInit(){
  //   this.agmMap.mapReady.subscribe(map => {
  //     console.log(map)
  //     this.googleMapsAPIWrapper.getCenter()
  //     map.getCenter()
  //     // this.agmMap.getCenter();
  //     // this.geocode("New York, USA").then(place => {
  //     //   this.currentCenter = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
  //     // })
  //     // .catch(err => {
  //     //   console.log(err);
  //     // });
  //   });
  // }
  geocodeAddress(){
    this.geocoderAddress("New York, USA").then(place => {
      this.currentCenter = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
    })
    .catch(err => {
      console.log(err);
    });
  }
  reverseGeocodeLatLang(){
    this.reverserGeocodeLatLang().then(place => {
      const location = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
      console.log(place);
      // setTimeout(() => {
      this.zone.run( () => {
        this.addressText = place.formatted_address;
      });
      // });
      console.log('this.addressText: ', this.addressText)
      // this.currentCenter = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
    })
    .catch(err => {
      console.log(err);
    });
  }
  geocoderAddress(address: string): Promise<any> {
    const geocoder = new google.maps.Geocoder();
    return new Promise((resolve, reject) => {
      geocoder.geocode(
        {
          address: address
        },
        (results, status) => {
          if (status === google.maps.GeocoderStatus.OK) {
            resolve(results[0]);
          } else {
            reject(new Error(status));
          }
        }
      );
    });
  }

  reverserGeocodeLatLang(): Promise<any> {
    const geocoder = new google.maps.Geocoder();
    return new Promise((resolve, reject) => {
      geocoder.geocode(
        {
          location: this.defaultCenter
        },
        (results, status) => {
          if (status === google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              console.log('results: ', results)
              resolve(results[0]);
            }else{
              reject(new Error('Something Went Wrong! No results found!'));
            }
          } else {
            reject(new Error(status));
          }
        }
      );
    });
  }

  findPlace() {
      this.geocoderAddress(this.addressText).then(place => {
        this.currentCenter = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
      })
      .catch(err => {
        console.log(err);
      });
  }

  mapReady(map) {
    this.reverseGeocodeLatLang();
    map.addListener("dragend", () => {
      //do what you want
      console.log('NEW COORDINATES END DRAG: ', this.newCenterLat,this.newCenterLng)
      this.defaultCenter.lat = this.newCenterLat;
      this.defaultCenter.lng = this.newCenterLng;
      this.reverseGeocodeLatLang();
      console.log('I am drag', name);
    });
  }

  centerChange(e) {
  console.log(e)
  this.newCenterLat = e.lat
  this.newCenterLng = e.lng
  }

  zoomChange(e){
    console.log('zoomChange: ', e)
    console.log('NEW COORDINATES zoom_changed: ', this.newCenterLat,this.newCenterLng)
    this.defaultCenter.lat = this.newCenterLat;
    this.defaultCenter.lng = this.newCenterLng;
    this.reverseGeocodeLatLang();
  }
  boundsChange(e){
    console.log('boundsChange: ', e)
  }
  // // google maps zoom level
  // zoom: number = 8;
  //
  // // initial center position for the map
  // lat: number = 51.673858;
  // lng: number = 7.815982;
  //
  // clickedMarker(label: string, index: number) {
  //   console.log(`clicked the marker: ${label || index}`)
  // }
  //
  // mapClicked($event: MouseEvent) {
  //   this.markers.push({
  //     lat: $event.coords.lat,
  //     lng: $event.coords.lng,
  //     draggable: true
  //   });
  // }
  //
  // markerDragEnd(m: marker, $event: MouseEvent) {
  //   console.log('dragEnd', m, $event);
  // }
  //
  // markers: marker[] = [
	//   {
	// 	  lat: 51.673858,
	// 	  lng: 7.815982,
	// 	  label: 'A',
	// 	  draggable: true
	//   },
	//   {
	// 	  lat: 51.373858,
	// 	  lng: 7.215982,
	// 	  label: 'B',
	// 	  draggable: false
	//   },
	//   {
	// 	  lat: 51.723858,
	// 	  lng: 7.895982,
	// 	  label: 'C',
	// 	  draggable: true
	//   }
  // ]
}

// just an interface for type safety.
interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}
