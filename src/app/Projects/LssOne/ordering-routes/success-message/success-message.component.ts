// import { DialogData } from './../../_client-interface/agm-map';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, Inject, EventEmitter, OnDestroy } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { Cookie } from 'ng2-cookies';
import * as places from '../shared/cities.json'
import * as lbc from '../shared/lbc_branches.json'
import { OrderingService } from '../shared/ordering.service.js';
import { Router, ActivatedRoute, RouterEvent, NavigationEnd } from '@angular/router';
import { Observable, Subject } from 'rxjs';
// import { startWith, map } from 'rxjs/operators';
import { TermsConditionComponent } from 'src/app/static-pages/terms-condition/terms-condition.component.js';
import { ErrorStateMatcher, MatDialog } from '@angular/material';
import { TermsDialogComponent } from '../terms-dialog/terms-dialog.component';
import { PaymentDialogComponent } from '../payment-dialog/payment-dialog.component';
//added august 2019 - AGM Dialog for LBC BRANCHES
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogData, Marker } from '../../_client-interface';

import { DatePipe } from '@angular/common';

import { PaymentFormService } from 'src/app/_admin-services/payment-form.service';

import { GiftwrapModel } from '../../_client-interface';
import { GiftwrapService } from '../../_client-services';
import { element } from 'protractor';
// import { _ } from 'underscore';
import * as _ from "underscore";
import { SettingService  } from '../../_admin-services/setting.service';
import { CartcounterService } from '../../_client-services/cartcounter.service';
// newbie
import { UserService } from 'src/app/user-routes/shared/user.service';
import { first, retry, catchError, startWith, map, filter, takeUntil } from 'rxjs/operators';
import { UserProvider } from 'src/app/user-routes/shared/users.provider';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';

@Component({
  selector: 'app-success-message',
  templateUrl: './success-message.component.html',
  styleUrls: ['./success-message.component.scss']
})
export class SuccessMessageComponent implements OnInit {
	// giftwrap variable
	reqList: GiftwrapModel[] = [];

	checkSessionStorageDeliveryOpt: any;

	giftwrap: any[] = [];
	giftwrap_reference: any;
	toNames: any[] = [];
	giftwrap_fee: number;

  currentUser: any
  items: any[] = []
  total: number = 0
  ttlFee: number = 0

  saving: boolean = false

  checked = false

  giftChecked: boolean = false
  to: string = ""
  from: string = ""
  message: string = ""

  cities: any[] = []

  //package shipping fee
  total_package_shippingfee: any = 0; // var for package template
  itemCount = 0;
  //
  currentInfofname:any;
  currentInfomname:any;
  currentInfolname:any;
  currentInfocontact:any;
  currentInfoemail:any;

  //
  opsdel:any;
  opspay:any;
  orderID:any;
  email_address:any;


  //
  currentCustomerInformation: any;

  isUpdate: boolean = false;


  noData: any;


  showdata: boolean = false;

  public destroyed = new Subject<any>();

  infocustom: any;
  custominfo: any;

  delOps: any;
  orderInfo: any;


  constructor(
     public userService: UserService,
     private userProvider: UserProvider,
     private datePipe: DatePipe,
     private service: OrderingService,
     private formBuilder: FormBuilder,
     private router: Router,
     public dialog: MatDialog,
     private paymentFormService: PaymentFormService,
     private giftwrapService: GiftwrapService,
     private settingService: SettingService,
     private cartCounterService: CartcounterService) { }

	getForm(){
		this.paymentFormService.getForm().subscribe(res => {
			let tmp: any = res
			console.log('PaymentFormComponent', tmp)
			var data = tmp.data.items[0]
			this.giftwrap_fee = data.giftwrap_fee;
			console.log(this.giftwrap_fee)
		})
	}

  shop() {
    this.router.navigate(['']);
  }

  ngOnInit() {
    if (this.checkLocalStorageHasCurrentUser()) {
      return;
    }else{
      //
      this.loadCustomerInformation();
      //
      this.checkOrderInfo();
      this.getData();
      this.checkSessionStorageDeliveryOpt = sessionStorage.getItem('deliveryOpt');

      let data = localStorage.getItem('mCart')

      if (data) {
        this.getForm()

        this.items = JSON.parse(data);

        setTimeout(() => {
          this.getTotal();
        }, 1000);
      }

      let mCarts = JSON.parse(localStorage.getItem('mCart'));
      if (mCarts) {
        for(let mCart of mCarts){
          this.itemCount = this.itemCount + mCart.qty;
        }
      }
    }
  }


  loadCustomerInformation(){
    // let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    // // let CustomerInfo =  localStorage.getItem('infocustomer')
    // this.currenuserinfo = currentUser;
    // let  datacustom = JSON.parse(localStorage.getItem('infocustomer'));
    // let  datacustomNSA = JSON.parse(localStorage.getItem('infocustomerNSA'));
    // const currentUser = JSON.parse(localStorage.getItem('currentUser'));


    // if(!datacustom) {
    //   this.noData = false;
    // }else{
    //   this.noData = true;
    //   if(!datacustomNSA){
    //     this.noData = false;
    //     this.infocustom = datacustom;
    //     console.log('data:', datacustom)
    //   }else{
    //     this.noData = true;
    //     this.infocustom = datacustomNSA;
    //     this.custominfo = datacustom;
    //   }
    // }

    // let currentUserTest = JSON.parse(localStorage.getItem('infocustomer'));

    // if(!currentUserTest) {
    //   const currentUser = JSON.parse(localStorage.getItem('currentUser'));

    //   this.userService.getUser(currentUser.id).subscribe( res => {
    //     const customer:any = res;
    //     console.log('USER:', customer.data.items[0])
    //     this.custominfo = customer.data.items[0];
    //   });
    // }else{

    //   this.noData = true;
    //   const currentUser = JSON.parse(localStorage.getItem('currentUser'));

    //   this.userService.getUser(currentUser.id).subscribe( res => {
    //     const customer:any = res;
    //     console.log('USER:', customer.data.items[0])
    //     this.custominfo = customer.data.items[0];
    //   });

    //   const  datacustom = JSON.parse(localStorage.getItem('infocustomer'));
    //   this.infocustom = datacustom;
    // }


    const data = JSON.parse(localStorage.getItem('infocustomer'));
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));

    const dataNsa = JSON.parse(localStorage.getItem('infocustomerNSA'));

    if (dataNsa) {
      this.noData =  dataNsa;
    }
    //

    console.log('data:', data)
    if (currentUser) {
      this.userService.getUser(currentUser.id).subscribe( res => {
        const data:any = res;
        console.log('USER:', data.data.items[0])
        this.infocustom = data.data.items[0];
      });
      this.noData =  data;

    }else{
      this.infocustom = data;
      this.showdata = true;
    }


    // }
  }


  checkOrderInfo(){
    this.delOps = JSON.parse(localStorage.getItem('deliveryOpts'));
    this.opsdel = this.delOps.service_method;
    this.opspay = this.delOps.payment_method;
    this.orderInfo = JSON.parse(localStorage.getItem('orderInfo'));
    this.orderID = this.orderInfo.order_no;
    // let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    // this.currentInfofname = currentUser.firstname;
    // this.currentInfomname = currentUser.middlename;
    // this.currentInfolname = currentUser.surename;
    // this.currentInfocontact = currentUser.contact_no;
    // this.currentInfoemail = currentUser.email_address;

    // let infocustomer = JSON.parse(localStorage.getItem('infocustomer'));
    // if (infocustomer) {
    //   this.email_address = infocustomer.email_address;
    // }else{
    //   this.email_address = currentUser.email_address;
    // }
  }

  ngOnDestroy(){
    localStorage.removeItem('mCart');
    localStorage.removeItem('deliveryOpts');
    localStorage.removeItem('infocustomer');
    localStorage.removeItem('infocustomerNSA');
    localStorage.removeItem('orderInfo');
    this.cartCounterService.emitCartCount(0);
  }

  checkLocalStorageHasCurrentUser(){
    let data = localStorage.getItem('mCart')
    if (data) {
      if (JSON.parse(data).length <= 0) {
        const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: 'You can not access this page!'
        });
        this.router.navigate(['']);
        return true;
      }else{
        return false;
      }
    }else{
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'You can not access this page!'
      });
      this.router.navigate(['']);
    }
    return true;
  }
  getTotal() {
		// console.log(this.giftwrap);
		// console.log(this.giftwrap_reference);
		// console.log(this.giftChecked);
		// console.log(this.giftwrap_fee);
		if(this.giftChecked){
			var total_giftwrapCount = 0;
			for (let giftwrap of this.giftwrap){
					total_giftwrapCount += giftwrap.quantity;
			}
		}
		// console.log(total_giftwrapCount);
    this.total = this.giftChecked?total_giftwrapCount*this.giftwrap_fee:0
    this.ttlFee = 0
    for (let itm of this.items)
      // this.total += itm.qty * (itm.item.price + (this.defval === 'deliver' ? this.getFee(itm) : 0))
      this.total += (itm.qty * itm.item.price);
    for (let itm of this.items)
      // this.ttlFee += itm.qty * (this.defval === 'deliver' ? this.getFee(itm) : 0)
      this.ttlFee += itm.qty;

		// console.log(this.total);
    // if(this.defval == 'deliver'){
    //   this.calculateNewShipFee();
    // }else{
      this.total_package_shippingfee = 0;
    // }
  }

	getData() {
    this.giftwrapService.getAllGiftwrap()
    .subscribe( data => {
      // this.search_loading = false;
      const d: any = data;
      if (!this.isObjectEmpty(d.data.items)) {
        this.reqList = d.data.items;
        console.log( this.reqList);
      } else {
        this.reqList = [];
      }
    });
  }

  isObjectEmpty (Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

}
