import { Component, OnInit, HostListener } from '@angular/core';
import { Carousel } from 'src/app/shared/carousel.config';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderingService } from '../shared/ordering.service';
import { OrderItem } from '../shared/ordering.model';
import { OrderID } from '../shared/ordering.prov';
import { Subscription, Observable } from 'rxjs';

//Added setptember 6, 2019
import { UserProvider } from '../../user-routes/shared/users.provider';
import { SearchService } from '../shared/search.service';
declare var $: any;

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  // june 5, 2020
  windowScrolled: boolean;

  myColor: string = "rgb(192, 192, 192, 0.1)";

  // contentHeight: number;
  view = 'grid' ;
  testItems: any;
  sub: any;
  isSearching: boolean = false;
  rightSlide: Carousel
  imgsUrl: string[] = []
  items: OrderItem[] = []
  canCarClick: boolean = true
  carPos = 1
  cItems: any[] = []
  cItems2 = [
    {
      img: '../../../assets/images/product1.jpg',
      left: -25
    },
    {
      img: '../../../assets/images/product2.jpg',
      left: 25
    },
    {
      img: '../../../assets/images/product3.jpg',
      left: 75
    },
    {
      img: '../../../assets/images/product4.jpg',
      left: 125
    }
  ]
  timer: Subscription

  constructor(private router: Router, private service: OrderingService, private orderID: OrderID, private userProvider: UserProvider, private searchService:SearchService) {}

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
        // var btn = document.querySelector('#addCartSection')
        // if(btn){
        //   this.flotingCartDialog(btn)
        // }
        this.windowScrolled = true;
        // console.log("ViewportTop: ", viewportTop)
        // console.log("viewportHeight: ", document.documentElement.clientHeight)
        // console.log("viewportBottom: ", viewportBottom)
        // console.log("bounding: ", bounding)
        // console.log("elementTop: ", elementTop)
        // console.log("elementHeight: ", bounding.height)
        // console.log("elementClientHeight: ", btn.clientHeight)
        // console.log("elementOffsetHeight: ", btn.offsetHeight)
        // console.log("elementBottom: ", elementBottom)
        // console.log(`${elementBottom} > ${viewportTop} && ${elementTop} < ${viewportBottom}`)
        // console.log(elementBottom > viewportTop && elementTop < viewportBottom)
    }
    else if (this.windowScrolled && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) {
        this.windowScrolled = false;
    }
  }


  // @HostListener('window:resize', ['$event'])
  // onResize(event) {
  //   // event.target.innerWidth;
  //   this.contentHeight = document.documentElement.clientHeight - 150//(179.5 + 64 + 20)
  // }
  selectedNavItem(item: any) {
    this.items = item;
    this.isSearching = this.userProvider.isSearching;
    // console.log(`Landing Page Emit Items: ${this.items}`);
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  ngOnInit() {
    // this.contentHeight = document.documentElement.clientHeight - 150//(179.5 + 64 + 20)
    // this.sub = this.searchService.getNavSearchEmitter().subscribe((resultData) => {
    //      console.log(resultData);
    // });
    this.sub = this.searchService.getNavSearchEmitter().subscribe(item => this.selectedNavItem(item));

    this.isSearching = false;
    if(this.userProvider.searchItemsData){
      this.items = this.userProvider.searchItemsData
      // console.log(this.userProvider.searchItemsData)
    }
    else {
      this.service.getAllItems(1, 50).subscribe(
        res => {
					console.log(res);
          if(!res.data) return
          this.items = res.data.items
        }
      )
    }

    // this.service.getCarousel().subscribe(res => {
    //   // console.log('ngOnInit', res)
    //   let start = res.data.length <= 2 ? 15 : -55 //default 25 : -75
    //   for(let itms of res.data){
    //     this.cItems.push({
    //       img: itms.file,
    //       left: start
    //     })
    //     start = start + 70 //default 50
    //   }
    // })
    // setInterval(() => {
    //   this.carClick(-70)
    // },3000)
  }

  openItem(row) {
    // return(console.log(row.item_slug))
    // this.orderID.id = id
    this.router.navigate(['./item-info/' + row.item_slug]);
  }

  getDec(dec){
    let tmp = dec.toString().split('.')
    return (tmp[1].charAt(0) * 10) + '%'
  }

  // async carClick(num: number){
  //   if(!this.canCarClick || this.cItems.length==1) return
  //   this.canCarClick = false
  //
  //   if(num==70){ // if previous default 50
  //     let tmp = this.cItems[this.cItems.length-1]
  //     let start = this.cItems.length <= 2 ? -15 : -55 //default -25 : -75
  //     tmp.left = start
  //     this.cItems.splice(0, 0, tmp)
  //     this.cItems.splice(this.cItems.length-1, 1)
  //   }
  //
  //   await this.delay(50);
  //   for(let itms of this.cItems) itms.left += num
  //   await this.delay(850);
  //
  //   if(num==-70){ // if next default -50
  //     let tmp = this.cItems[0]
  //     tmp.left = tmp.left + (70 * this.cItems.length) //default 50
  //     this.cItems.splice(0,1)
  //     await this.delay(50);
  //     this.cItems.push(tmp)
  //   }
  //   this.canCarClick = true
  // }

  // delay(ms: number){
  //   return new Promise(resolve => setTimeout(resolve, ms));
  // }

  changeView(val){
    if (val === 'grid'){
      $('#grid').css('display','flex');
      $('#list').css('display','none');
      this.view = 'grid';
    }else if(val === 'list'){
      $('#grid').css('display','none');
      $('#list').css('display','flex');
      this.view = 'list';
    }

  }
  // june 5 2020
  scrollToTop() {
    (function smoothscroll() {
        var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
        if (currentScroll > 0) {
            window.requestAnimationFrame(smoothscroll);
            window.scrollTo(0, currentScroll - (currentScroll / 8));
        }
    })();
  }

}
