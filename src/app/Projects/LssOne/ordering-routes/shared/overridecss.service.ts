import {Injectable, EventEmitter} from "@angular/core";
// import { OrderingService } from './ordering.service';

@Injectable({
  providedIn: 'root'
})
export class OverridecssService {
  overridecss: EventEmitter<any> = new EventEmitter<any>();
  constructor() {}
  emitOverrideCssEvent(number) {
    this.overridecss.emit(number);
  }
  getOverrideCssEmitter() {
    return this.overridecss;
  }
}
