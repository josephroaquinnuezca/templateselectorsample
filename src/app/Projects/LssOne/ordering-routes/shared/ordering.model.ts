export interface OrderItem {
    id: number;
    item_no: number;
    itemcateg_id: number;
    name: string;
    description: string;
    min_stock: string;
    current_stock: string;
    status: number;
    price: number;
    length: number;
    width: number;
    height: number;
    weight: number;
    images: ItemImage[];
    rating: Rating;
    testimonies: any[];
    shipping_fee_ncr: number;
    shipping_fee_luz: number;
    shipping_fee_vis: number;
    shipping_fee_min: number;
}

export interface Rating {
    1: number;
    2: number;
    3: number;
    4: number;
    5: number;
    total: number;
    count: number;
    percentage: number;
}

export interface ItemImage {
  file: string;
}
