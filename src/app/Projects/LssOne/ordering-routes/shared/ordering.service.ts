import { ApisService, saveItemURL, order, carouselUrl, carouselUrlAbout } from './../../shared/apis.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL } from './../../shared/apis.service';

@Injectable({
  providedIn: 'root'
})
export class OrderingService {

  constructor(private apiService: ApisService, private http: HttpClient) { }

  getAllItems(page: number, limit: number){
    return this.apiService.get(saveItemURL + '?page=' + page + '&limit=' + limit)
  }

  getItem(id: number){
    return this.http.get(`${HTTP_API_URL}item/${id}`)
  }

  saveOrder(data: any){
    return this.apiService.post(order, data)
  }
  saveOrderGuest(data: any){
    return this.http.post(`${HTTP_API_URL}order-record-guest`, data)
  }
  getCarousel(){
    return this.apiService.get(carouselUrl)
  }

  getCarouselAbout(){
    return this.apiService.get(carouselUrlAbout)
  }

}
