import {Injectable, EventEmitter} from "@angular/core";
// import { OrderingService } from './ordering.service';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  search: EventEmitter<any> = new EventEmitter<any>();
  constructor() {}
  emitNavSearchEvent(number) {
    this.search.emit(number);
  }
  getNavSearchEmitter() {
    return this.search;
  }
}
