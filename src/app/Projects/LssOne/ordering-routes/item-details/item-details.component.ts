import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { Carousel } from 'src/app/shared/carousel.config';
import { OrderID } from '../shared/ordering.prov';
import { OrderingService } from '../shared/ordering.service';
import { OrderItem } from '../shared/ordering.model';
import { Cookie } from 'ng2-cookies';
import { Router, ActivatedRoute } from '@angular/router';
import {CartcounterService} from '../../_client-services/cartcounter.service'
//addded import month of august 2019
import { Inject, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/common';
// import * as $ from 'jquery';
// declare var jQuery: any;
// (function ($) {})(jQuery);
//declared function from assets JS
// declare function checkElemVisibility(): any;

// declare var $: any;


@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss']
})
export class ItemDetailsComponent implements OnInit, AfterViewInit {
  // @ViewChild("addCartSection", {static:false}) addCartBTN: ElementRef;
  show = 5;
  slide: Carousel
  // imgsUrl: string[] = []
  img: string;
  defval: string = 'deliver'
  item: OrderItem
  itemCount = 0;
  items: any[] = []
  ifOnCart: boolean = false
  qnty: number = 1
  item_imgs: any[] = []
  item_instImgs: any[] = []
  windowScrolled: boolean;
  hideFloatingCart: boolean = true;
  //shoppingCart Data Stored on local storage
  data = localStorage.getItem('mCart');




  constructor(@Inject(DOCUMENT) private document: Document, private orderID: OrderID, private service: OrderingService, private router: Router, private route: ActivatedRoute,private cartCounterService: CartcounterService) { }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
        var btn = document.querySelector('#addCartSection')
        if(btn){
          this.flotingCartDialog(btn)
        }
        this.windowScrolled = true;
        // console.log("ViewportTop: ", viewportTop)
        // console.log("viewportHeight: ", document.documentElement.clientHeight)
        // console.log("viewportBottom: ", viewportBottom)
        // console.log("bounding: ", bounding)
        // console.log("elementTop: ", elementTop)
        // console.log("elementHeight: ", bounding.height)
        // console.log("elementClientHeight: ", btn.clientHeight)
        // console.log("elementOffsetHeight: ", btn.offsetHeight)
        // console.log("elementBottom: ", elementBottom)
        // console.log(`${elementBottom} > ${viewportTop} && ${elementTop} < ${viewportBottom}`)
        // console.log(elementBottom > viewportTop && elementTop < viewportBottom)
    }
    else if (this.windowScrolled && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) {

        var btn = document.querySelector('#addCartSection')
        if(btn){
          this.flotingCartDialog(btn)
        }
        this.windowScrolled = false;
    }
  }

  scrollToTop() {
      (function smoothscroll() {
          var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
          if (currentScroll > 0) {
              window.requestAnimationFrame(smoothscroll);
              window.scrollTo(0, currentScroll - (currentScroll / 8));
          }
      })();
  }

  flotingCartDialog(btn){

    var isInViewport = function (elem) {
      var viewportHeight = document.documentElement.clientHeight
      // Get it's position in the viewport
      var bounding = elem.getBoundingClientRect();

      var elementTop = bounding.top + window.scrollY;
      var elementBottom = elementTop + bounding.height; //-150

      var viewportTop = document.documentElement.scrollTop;
      var viewportBottom = viewportTop + viewportHeight;

      // return elementBottom > viewportTop && elementTop < viewportBottom
      return elementBottom > viewportTop
    }

    if (isInViewport(btn)) {
      console.log('In the viewport!!!!!!');
      this.hideFloatingCart = true;
    }
    else{
      console.log('In NOT the viewport!!!!!!');
      this.hideFloatingCart = false;
    }
  }
  swipeLeft(){
    var is_touch_device = 'ontouchstart' in window;
    console.log('is_touch_device:', is_touch_device);
    if (is_touch_device) {
      console.log('you swipe left!');
      this.slide.setPosition(-1);
    }
  }
  swipeRight(){
    var is_touch_device = 'ontouchstart' in window;
    console.log('is_touch_device:', is_touch_device);
    if (is_touch_device) {
      console.log('you swipe Right!');
      this.slide.setPosition(1);
    }
  }
  ngAfterViewInit() {
    // this.floatingCartHideAndShow()
  }



  ngOnInit() {
    console.log('A: ', this.hideFloatingCart)
    this.onWindowScroll();
    console.log('B: ', this.hideFloatingCart)
    // this.items = JSON.parse(sessionStorage.getItem('mCart'))
    this.slide = new Carousel;
    const item_slug = this.route.snapshot.params['item_slug'];

    this.service.getItem(item_slug).subscribe(
      res => {
        const res_data: any = res
        this.item = res_data.data;
				console.log(this.item);
        this.item_imgs = res_data.data.images;
        this.item_instImgs = res_data.data.instruction_images;
        // for (let img of this.item.images) {
        //   this.imgsUrl.push(img);
        // }
        this.slide.setLimit(this.item.images.length);
      }
    )

    if (this.data) {
      // console.log(`Data: ${data}`)
      this.items = JSON.parse(this.data)
      // for (let itm of this.items) {
      //   if (itm.item.id == this.orderID.id) {
      //     console.log("this IF is running")
      //     this.ifOnCart = true
      //     this.qnty = itm.qty
      //     this.defval = itm.package
      //   }
      // }
    }

    let mCarts = JSON.parse(localStorage.getItem('mCart'));
    if (mCarts) {
      for(let mCart of mCarts){
        this.itemCount = this.itemCount + mCart.qty;
      }
    }

  }

  increaseShow(){
    this.show += 5;
  }

  negativeShow(){

    var totalLength =  this.items.length;
    console.log("arraylenght", totalLength)
    this.show -= 5;
  }

  saveToCart(qty, item_id) {

		this.router.navigate(['./shopping-cart']);

    this.itemCount = this.itemCount + qty
    this.cartCounterService.emitCartCount(this.itemCount);
		// return;

    if (this.data) {
      for (let cartItem of this.items) {
        // console.log(`Item ID: ${item_id} = Item FOR ID: ${cartItem.item.id}`)
        // console.log(`QUANTITY: ${qty}`)
        if (item_id === cartItem.item.id) {
            // alert(`Existing Item on Cart`)
          cartItem.qty = cartItem.qty + qty
          cartItem.package = this.defval
          localStorage.setItem('mCart', JSON.stringify(this.items))
          Cookie.set('mCart', JSON.stringify(this.items), 0.0034);
          return
        }
      }

    }
    this.items.push({
      item: this.item,
      qty: qty,
      package: this.defval
    })
    localStorage.setItem('mCart', JSON.stringify(this.items))
    Cookie.set('mCart', JSON.stringify(this.items), 0.0034);

    // if (this.ifOnCart) {
    //   for (let itm of this.items) {
    //     if (itm.item.id == this.orderID.id) {
    //       itm.qty = this.qnty
    //       itm.package = this.defval
    //     }
    //   }
    //   localStorage.setItem('mCart', JSON.stringify(this.items))
    //   // Cookie.set('mCart', JSON.stringify(this.items), 0.0034);
    //   return
    // }
    // this.items.push({
    //   item: this.item,
    //   qty: qty,
    //   package: this.defval
    // })
    // localStorage.setItem('mCart', JSON.stringify(this.items))
    // Cookie.set('mCart', JSON.stringify(this.items), 0.0034);
  }

  getDec(dec) {
    let tmp = dec.toString().split('.')
    return (tmp[1].charAt(0) * 10) + '%'
  }
  // // SOME JQUERY CODES
  // floatingCartHideAndShow(){
  //   $(document).ready(function(){
  //     var test = "xxxx";
  //     $.fn.isInViewport = function() {
  //       var elementTop = $(this).offset().top;
  //       var elementBottom = elementTop + $(this).outerHeight();
  //
  //       var viewportTop = $(window).scrollTop();
  //       var viewportBottom = viewportTop + $(window).height();
  //
  //       return elementBottom > viewportTop && elementTop < viewportBottom;
  //     };
  //
  //     $(window).on('resize scroll', function() {
  //       // $('.color').each(function() {
  //       //     var activeColor = $(this).attr('id');
  //         if ($('#addCartSection').isInViewport()) {
  //           // $('#fixed-' + activeColor).addClass(activeColor + '-active');
  //           console.log("JQUERY ELEMENT viewportTop", $('#addCartSection').offset().top)
  //           console.log("JQUERY ELEMENT WINDOW HEIGHT", $('#addCartSection').outerHeight())
  //           console.log("JQUERY ELEMENT TOP + HEIGHT", $('#addCartSection').offset().top + $('#addCartSection').outerHeight())
  //           // console.log("JQUERY WINDOWS viewportTop", $(window).scrollTop())
  //           // console.log("JQUERY WINDOWS WINDOW HEIGHT", $(window).height())
  //           // console.log("JQUERY WINDOWS TOP + HEIGHT", $(window).scrollTop() + $(window).height())
  //           console.log("in viewport");
  //           // $('#floatingCartContainer').hide("slide", {direction: "right"}, 500)
  //           $(".floating-cart-wrapper").addClass('hideFloatingCart')
  //         } else {
  //           // $('#fixed-' + activeColor).removeClass(activeColor + '-active');
  //           console.log("JQUERY ELEMENT viewportTop", $('#addCartSection').offset().top)
  //           console.log("JQUERY ELEMENT WINDOW HEIGHT", $('#addCartSection').outerHeight())
  //           console.log("JQUERY ELEMENT TOP + HEIGHT", $('#addCartSection').offset().top + $('#addCartSection').outerHeight())
  //           // console.log("JQUERY WINDOWS viewportTop", $(window).scrollTop())
  //           // console.log("JQUERY WINDOWS WINDOW HEIGHT", $(window).height())
  //           // console.log("JQUERY WINDOWS TOP + HEIGHT", $(window).scrollTop() + $(window).height())
  //           console.log("not in viewport");
  //           // $('#floatingCartContainer').show("slide", {direction: "left"}, 500)
  //           $(".floating-cart-wrapper").removeClass('hideFloatingCart')
  //         }
  //       });
  //   });
  // }
}
