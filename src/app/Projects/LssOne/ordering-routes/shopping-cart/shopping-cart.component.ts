import { element } from 'protractor';
import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { TermsDialogComponent } from '../terms-dialog/terms-dialog.component';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Cookie } from 'ng2-cookies';
import { PaymentFormService } from 'src/app/_admin-services/payment-form.service';
import { startWith, map, first } from 'rxjs/operators';
import * as places from '../shared/cities.json'
import * as lbc from '../shared/lbc_branches.json'
import { GiftwrapModel } from '../../_client-interface';
import { GiftwrapService } from '../../_client-services';
import { CartcounterService } from '../../_client-services/cartcounter.service';
import { SettingService  } from '../../_admin-services/setting.service';
// import { _ } from 'underscore';
import * as _ from "underscore";
import { CartDialogComponent } from './cart-dialog/cart-dialog.component';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {

	// giftwrap variable
	reqList: GiftwrapModel[] = [];
  myGiftwrapOption: any;
  itemCount = 0;

	checkSessionStorageDeliveryOpt: any;
	sessionBody: any;

	giftwrap: any[] = [];
	giftwrap_reference: any;
	toNames: any[] = [];
	giftwrap_fee: number;

  defval: string = 'deliver'
  placeVal: string = 'NCR'
  selTime: string = ''
  selDate: string = ''
  items: any[] = []
  total: number = 0
  lbc_branch = lbc.default
  lbc_name: string = lbc.default[0].branch_name
  filteredOptions: Observable<string[]>;
  myControl = new FormControl();
  today = new Date();
  package_template: any[] = [];
  checked: boolean = false
  to: string = ""
  from: string = ""
  message: string = ""

  //shoppingCart Data Stored on local storage
  data = localStorage.getItem('mCart');
  //stored object
  myItemsQTY: any;
  //package shipping fee
  total_package_shippingfee: any = 0; // var for package template

  //
  show: boolean = false



  constructor(
    private router: Router,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private paymentFormService: PaymentFormService,
    private giftwrapService: GiftwrapService,
    private cartCounterService: CartcounterService,
    private settingService: SettingService) {

	}

  filterPastDate() {
      this.today.setDate(this.today.getDate() + 1)
  }

	getForm(){
		this.paymentFormService.getForm().subscribe(res => {
			let tmp: any = res
			// console.log('PaymentFormComponent', tmp)
			var data = tmp.data.items[0]
			this.giftwrap_fee = data.giftwrap_fee;
		})
  }

  ngOnInit() {

    // this.settingService.getpackageTemplate().subscribe(res => {
    //   const packageTemplate: any = res;
    //
    //   if (packageTemplate.status.error === false ) {
    //     this.package_template = packageTemplate.data;
    //   }
    // });

		this.getData();
		this.getForm();
		this.checkSessionStorageDeliveryOpt = sessionStorage.getItem('deliveryOpt');
    this.filterPastDate();
		if (this.data && JSON.parse(this.data).length > 0) {
    this.show = true;
      console.log('May laman ang cart!');
      console.log(JSON.parse(this.data));
      this.items = JSON.parse(this.data);
			if (sessionStorage.getItem('deliveryOpt')) {
        console.log('May laman ang cart! - deliveryOpt');
				var giftwrapData = JSON.parse(sessionStorage.getItem('deliveryOpt'));
				if(giftwrapData.length !== this.items.length){
          console.log('May laman ang cart! - deliveryOpt ---');
					var i = 0
					for (let itm of this.items){
						// if(this.giftwrap[i] !== undefined){
							this.giftwrap[i] = {'quantity': (giftwrapData.giftwrap_remarks[i])?giftwrapData.giftwrap_remarks[i].quantity:itm.qty,'name': itm.item.name, 'giftwrapTo': (giftwrapData.giftwrap_remarks[i])?giftwrapData.giftwrap_remarks[i].giftwrapTo:''};
							this.toNames[i] = (giftwrapData.giftwrap_remarks[i])?giftwrapData.giftwrap_remarks[i].giftwrapTo:'';
						// }
						// else{
						// 	this.giftwrap[i] = {'quantity': itm.qty,'name': itm.item.name, 'giftwrapTo': ''};
						// 	this.toNames[i] = '';
						// }
						let body = {
							'defval': giftwrapData.defval,
							'placeVal': giftwrapData.placeVal,
							'selTime': giftwrapData.selTime,
							'selDate': giftwrapData.selDate,
							'selBranch': giftwrapData.selBranch,
							'checked': giftwrapData.checked,
							'toNames': this.toNames,
							'giftwrap_remarks': this.giftwrap,
							'giftwrap_option': giftwrapData.giftwrap_option,
							'giftwrap_status': giftwrapData.checked,
							'to': giftwrapData.to,
							'from': giftwrapData.from,
							'message': giftwrapData.message
						}
						// console.log('purchase', body)
						sessionStorage.setItem('deliveryOpt', JSON.stringify(body))
						i++;
						// console.log(i);
					}
				}
			}
      console.log('May laman ang cart! - deliveryOpt ---XXX');
      this.getTotal()
    }
		// console.log('xxxxxxxxxxxxxxx1111111xxxxxxxxxxxxxxxx')
		// console.log(this.giftwrap)
		// console.log(this.giftwrap_reference)
		// console.log(this.checked)
		// console.log(this.toNames)
		// console.log('xxxxxxxxxxxxxxx1111111xxxxxxxxxxxxxxxx')
    // console.log('ShoppingCartComponent', localStorage.getItem('mCart'))
    if (sessionStorage.getItem('deliveryOpt')) {
      this.sessionBody = JSON.parse(sessionStorage.getItem('deliveryOpt'))
      this.defval = this.sessionBody.defval
      this.placeVal = this.sessionBody.placeVal
      this.selTime = this.sessionBody.selTime
      this.selDate = this.sessionBody.selDate
      this.checked = this.sessionBody.checked
			this.toNames = this.sessionBody.toNames
			this.giftwrap = this.sessionBody.giftwrap_remarks
			this.myGiftwrapOption = this.sessionBody.giftwrap_option
			this.giftwrap_reference = this.sessionBody.giftwrap_remarks
      this.to = this.sessionBody.to
      this.from = this.sessionBody.from
      this.message = this.sessionBody.message
    }

    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );

		// giftwrap load data
		// this.loadGiftWrapValue();
		// console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
		// console.log(this.giftwrap)
		// console.log(this.giftwrap_reference)
		// console.log(this.checked)
		// console.log(this.toNames)
		// console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
		this.checkGiftWrapToggle();
		// this.checked ? this.loadGiftWrapValue() : this.giftwrap = [];

    let mCarts = JSON.parse(localStorage.getItem('mCart'));
    if (mCarts) {
      for(let mCart of mCarts){
        this.itemCount = this.itemCount + mCart.qty;
      }
    }

    // this.calculateNewShipFee()
  }


  addArrow(item_id) {
     // itm.qty = itm.qty + 1

     if (this.data) {
       for (let cartItem of this.items) {
         // console.log(`Item ID: ${item_id} = Item FOR ID: ${cartItem.item.id}`)
         if (item_id === cartItem.item.id) {
             // alert(`Existing Item on Cart`)
           cartItem.qty = cartItem.qty + 1
           this.itemCount = this.itemCount + 1
           this.cartCounterService.emitCartCount(this.itemCount)
           let mCart = localStorage.setItem('mCart', JSON.stringify(this.items))
           Cookie.set('mCart', JSON.stringify(this.items), 0.0034);

           // this.calculateNewShipFee();

           return
         }

       }
     }


  }

  minusArrow(item_id) {
    // itm.qty = itm.qty < 2 ? 1 : itm.qty - 1
    if (this.data) {
      for (let cartItem of this.items) {
        // console.log(`Item ID: ${item_id} = Item FOR ID: ${cartItem.item.id}`)
        if (item_id === cartItem.item.id) {
            // alert(`Existing Item on Cart`)
            if(cartItem.qty > 1) {
              cartItem.qty = cartItem.qty - 1
              this.itemCount = this.itemCount - 1
              this.cartCounterService.emitCartCount(this.itemCount)
            }

          localStorage.setItem('mCart', JSON.stringify(this.items))
          Cookie.set('mCart', JSON.stringify(this.items), 0.0034);

          // this.calculateNewShipFee();

          return
        }
      }
    }
  }
	resetGiftWrap(){
		this.giftwrap = [];
	}
	arrowActionGiftWrapValue(i,value){
		// console.log(this.toNames);
		// console.log(value);
		var counter = 0;
		for (let itm of this.items){
			if(i == counter){
				this.giftwrap[i] = {'quantity': itm.qty,'name': itm.item.name, 'giftwrapTo': value};
			}
			counter++;
		}
	}

	checkGiftWrapToggle(){
		// console.log(this.checked);
		if(!sessionStorage.getItem('deliveryOpt')){
			// console.log("ERROR BAKIT KA NATAWAG");
			// this.checked ? this.loadGiftWrapValue() : this.loadGiftWrapValue();
			this.loadGiftWrapValue()
		}
	}
	loadGiftWrapValue(){
		if(!sessionStorage.getItem('deliveryOpt')){
			// console.log("---------------------");
			// console.log(this.items);
			var i = 0;
			for (let itm of this.items){
				if(this.toNames === undefined || this.toNames.length == 0){
					this.giftwrap[i] = {'quantity': itm.qty,'name': itm.item.name, 'giftwrapTo': ''};
				}else{
					this.giftwrap[i] = {'quantity': itm.qty,'name': itm.item.name, 'giftwrapTo': this.toNames[i]};
				}
				i++;

				// console.log(i);
			}
		}
		// console.log("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv");
		// console.log(this.giftwrap);
	}
  purchase() {
		// console.log("-----------------------------");
		// console.log(this.giftwrap);
		if(this.checked){
			if(this.reqList.length > 0){
				if(!this.myGiftwrapOption){
					alert("Please select giftwrap to use.");
					return;
				}
			}
			for (let giftwrap of this.giftwrap){
				if((giftwrap.giftwrapTo === '' || !giftwrap.giftwrapTo)){
					alert("Please input giftwrap receivers name.");
					// console.log(this.toNames);
					return;
				}
			}
		}
		// for (let giftwrap of this.giftwrap) {
		// 	console.log(giftwrap.quantity);
		// 	console.log(giftwrap.name);
		// }
		// return
    if (this.defval === 'pick') {
      if (this.selTime === '' || this.selDate === '') {
        alert('Please provide time and date of pickup')
        return
      }
    }
    if(this.defval === 'lbc'){
      if(this.myControl.value) {
        let isExist = false
        for(let b of this.lbc_branch){
          if(b.branch_name.toLowerCase() === this.myControl.value.toLowerCase()) isExist = true
        }
        if(!isExist){
          alert("Invalid LBC branch")
          return
        }
      }
      else{
        alert("Please select LBC Branch.")

        return
      }

    }
    let body = {
      'defval': this.defval,
      'placeVal': this.placeVal,
      'selTime': this.selTime,
      'selDate': this.selDate,
      'selBranch': this.myControl.value,
      'checked': this.checked,
			'toNames': this.toNames,
			'giftwrap_remarks': this.giftwrap,
			'giftwrap_option': (this.reqList.length > 0) ? this.myGiftwrapOption : null,
			'giftwrap_status': this.checked,
      'to': this.to,
      'from': this.from,
      'message': this.message
    }
    // console.log('purchase', body)
    sessionStorage.setItem('deliveryOpt', JSON.stringify(body))
    localStorage.setItem('mCart', JSON.stringify(this.items))
    this.router.navigate(['./personal-info']);
    // this.router.navigate(['./purchase-procedure']);

  }

  shop() {
    // let data = localStorage.getItem('mCart')
    // if (this.data) {
    //   let tmp = JSON.parse(this.data)
    //   // console.log(`ITEM LENGTH: ${tmp.length}`)
    //   if (tmp.length != this.items.length)
    //     if (confirm('Do you want to save the changes?'))
    //       this.saveChanges()
    //   for (let x = 0; x < tmp.length; x++)
    //     if (tmp[x].qty != this.items[x].qty)
    //       if (confirm('Do you want to save the changes?'))
    //         this.saveChanges()
    // }
    this.router.navigate(['']);
  }

  saveChanges() {
    localStorage.setItem('mCart', JSON.stringify(this.items))
    this.router.navigate(['']);
  }

  delete(i) {

    // const identity = i;
    // console.log(identity);
    const dialogRef = this.dialog.open(CartDialogComponent, {
		  panelClass: 'app-full-bleed-dialog-p-26',
      disableClose: false

    });
    dialogRef.afterClosed().subscribe(result => {
			console.log(result);
      if (result) {
        this.itemCount = this.itemCount - this.items[i].qty
        this.cartCounterService.emitCartCount(this.itemCount)
  			if(sessionStorage.getItem('deliveryOpt')){
  	      this.items.splice(i, 1)
  	      let mCart = localStorage.setItem('mCart', JSON.stringify(this.items))
  				this.giftwrap.splice(i, 1);
  				this.toNames.splice(i, 1);
  				let body = {
  		      'defval': this.defval,
  		      'placeVal': this.placeVal,
  		      'selTime': this.selTime,
  		      'selDate': this.selDate,
  		      'selBranch': this.myControl.value,
  		      'checked': this.checked,
  					'toNames': this.toNames,
  					'giftwrap_remarks': this.giftwrap,
  					'giftwrap_option': this.myGiftwrapOption,
  					'giftwrap_status': this.checked,
  		      'to': this.to,
  		      'from': this.from,
  		      'message': this.message
  		    }
  		    sessionStorage.setItem('deliveryOpt', JSON.stringify(body))

  				this.getTotal()
  				if(this.giftwrap === undefined || this.giftwrap.length == 0 && this.toNames === undefined || this.toNames.length == 0){
  			    sessionStorage.removeItem('deliveryOpt')
  				}
  			}
  			else{
  	      this.items.splice(i, 1)
  	      localStorage.setItem('mCart', JSON.stringify(this.items))
  				this.resetGiftWrap();
  				this.loadGiftWrapValue();

  				this.getTotal()
  			}
      }
    });
  }

  getTotal() {
		// console.log(this.giftwrap);
		// console.log(this.giftwrap_reference);

    this.total = 0
		var total_giftwrapcount = 0

    for (let itm of this.items) {

			if(this.checked){
				for (let giftwrap of this.giftwrap){
					if(giftwrap.name === itm.item.name){
						total_giftwrapcount = giftwrap.quantity;
						console.log(total_giftwrapcount);
					}
				}
			}

      // this.total += (itm.qty * (itm.item.price + (this.defval === 'deliver' ? this.getFee(itm) : 0))) + (this.checked? total_giftwrapcount * this.giftwrap_fee : 0 )
      this.total += (itm.qty * itm.item.price) + (this.checked? total_giftwrapcount * this.giftwrap_fee : 0 )
    }
    // if(this.defval == 'deliver'){
      // this.calculateNewShipFee();
    // }else{
      this.total_package_shippingfee = 0;
    // }
  }

  getFee(itm) {
    let x = 'ncr'
    if (this.placeVal === 'LUZON') x = 'luz'
    else if (this.placeVal === 'VISAYAS') x = 'vis'
    else if (this.placeVal === 'MINDANAO') x = 'min'
    return itm.item['shipping_fee_' + x]
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.lbc_branch.filter(option => option.branch_name.toLowerCase().includes(filterValue));
  }
	// GIFT WRAP COUNTER
	counter(i: number) {
	    return new Array(i);
	}

	getData() {
    this.giftwrapService.getAllGiftwrap()
    .subscribe( data => {
      // this.search_loading = false;
      const d: any = data;
      if (!this.isObjectEmpty(d.data.items)) {
        this.reqList = d.data.items;
      } else {
        this.reqList = [];
      }
    });
  }

  isObjectEmpty (Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

  sortASC(itemA, itemB)
  {
      return parseFloat(itemA.item.id) - parseFloat(itemB.item.id);
  }
  sortDESC(itemA, itemB)
  {
      return parseFloat(itemB.item.id) - parseFloat(itemA.item.id);
  }

  async calculateNewShipFee() {

    // if (this.placeVal === 'LUZON') x = 'luz'
    // else if (this.placeVal === 'VISAYAS') x = 'vis'
    // else if (this.placeVal === 'MINDANAO') x = 'min'
    console.log(this.itemCount);
    // let testCounter = 0;
    let testCounterOutside = 0;
    var reference_items = JSON.parse(JSON.stringify(this.items)).sort(this.sortASC);
    var res_counter = 0;
    let baseCounter = this.itemCount;
    var finalPackages: any[] = [];

    // while (testCounter < baseCounter) {
    // for (let testCounter = 0; testCounter <= 5; testCounter += 1) {
    //   console.log(testCounter);
    // }
    // return
    // , p = Promise.resolve()
    for (let testCounter = 0; testCounter < baseCounter; testCounter = testCounter + res_counter) {
      var destination;

      switch (this.placeVal) {
        case 'NCR':
          destination = 1;
          break;
        case 'LUZON':
          destination = 2;
          break;
        case 'VISAYAS':
          destination = 3;
          break;
        case 'MINDANAO':
          destination = 3;
          break;
        default:
          destination = 4;
          break;
      }
      var x = {};
      var test_x = {};
      var combineReferenceID = [];
      var combineReferenceQTY = [];
      var test_combineReferenceID = [];
      var test_combineReferenceQTY = [];

      reference_items.forEach(function(val, index, array) {

        let itemsInPackage = [];
        let test_itemsInPackage = [];

        itemsInPackage.push([{item_id: val.item.id, qty: val.qty}])
        test_itemsInPackage.push(val.qty)

        x[index] = { data: val.item, itemsInPackage: itemsInPackage };
        test_x[index] = { data: val.item, itemsInPackage: test_itemsInPackage };

      });
      console.log('xxxxxx: ', x);
      console.log('test_xtest_xtest_x: ', test_x);
      _.map(x, function(val, key) {
        console.log('map function', val);
        // x[key]['itemsInPackage'] = val['itemsInPackage'].reverse();
        //
        // combineReferenceID.push([{
        //   item_id: x[key]['data'].id,
        //   item_name: x[key]['data'].id,
        //   package: x[key]['itemsInPackage']
        // }]);
        combineReferenceQTY.push(x[key]['itemsInPackage']);
        // console.log(val['packages'].reverse());
      });

      _.map(test_x, function(val, key) {
        console.log('map function', val);
        // test_x[key]['itemsInPackage'] = val['itemsInPackage'].reverse();
        //
        // test_combineReferenceID.push([{
        //   item_id: test_x[key]['data'].id,
        //   item_name: test_x[key]['data'].id,
        //   package: test_x[key]['itemsInPackage']
        // }]);
        test_combineReferenceQTY.push(test_x[key]['itemsInPackage']);
        // console.log(val['packages'].reverse());
      });

      console.log(combineReferenceQTY);
      console.log(test_combineReferenceQTY);

      // var result = [];
      // for (var i = 0; i < combineReferenceQTY[0].length; i++) {
      //   result[i] = new Array(combineReferenceQTY[0].length).fill();
      //
      //   for (var j = 0; j < combineReferenceQTY.length; j++) {
      //     result[i][j] = combineReferenceQTY[j][i]; // Here is the fixed column access using the outter index i.
      //   }
      // }
      let getIndexOfLongestArray = combineReferenceQTY.reduce(function(maxI,el,i,arr) {return el.length>arr[maxI].length ? i : maxI;}, 0);
      console.log(getIndexOfLongestArray);
      var packageResults = combineReferenceQTY[getIndexOfLongestArray].map((row, index) => {
        return combineReferenceQTY.map((column) => {
          return column[index]
        })
      })

      let test_getIndexOfLongestArray = test_combineReferenceQTY.reduce(function(maxI,el,i,arr) {return el.length>arr[maxI].length ? i : maxI;}, 0);
      console.log(test_getIndexOfLongestArray);
      var test_packageResults = test_combineReferenceQTY[test_getIndexOfLongestArray].map((row, index) => {
        return test_combineReferenceQTY.map((column) => {
          return column[index]
        })
      })


      console.log(packageResults);
      console.log(test_packageResults);

      let z = packageResults[0];
      let queryPackage = '';
      console.log(z);
      if (z.length > 1) {
        for(var index in z){
          // z[index][0].item_id = 'xxxx';
          if(+index+1 == z.length){
            queryPackage += '( item_id = ' + z[index][0].item_id + ' AND itemqty_count <= ' + z[index][0].qty + '))';
            console.log(test_packageResults[0][index]);
            console.log('queryPackage-MULTIPLE: ', queryPackage);
            console.log('queryPackage-MULTIPLE: z.length', z.length);
            console.log('queryPackage-MULTIPLE: index+1', (+index+1));
            // test_packageResults[0][index] = test_packageResults[0][index] - z[index][0].qty;
          }
          else if(+index+1 == 1){
            queryPackage += '((item_id = ' + z[index][0].item_id + ' AND itemqty_count <= ' + z[index][0].qty + ') OR ';
            console.log(test_packageResults[0][index]);
            console.log('queryPackage-MULTIPLE: ', queryPackage);
            console.log('queryPackage-MULTIPLE: z.length', z.length);
            console.log('queryPackage-MULTIPLE: index+1', +index+1);
          }
          else{ if(+index+1 >= 1)
            queryPackage += '(item_id = ' + z[index][0].item_id + ' AND itemqty_count <= ' + z[index][0].qty + ') OR ';
            console.log(test_packageResults[0][index]);
            console.log('queryPackage-MULTIPLE: ', queryPackage);
            console.log('queryPackage-MULTIPLE: z.length', z.length);
            console.log('queryPackage-MULTIPLE: index+1', +index+1);
            // test_packageResults[0][index] = test_packageResults[0][index] - z[index][0].qty;
          }
        }
        console.log('queryPackage-MULTIPLE: ', queryPackage);

      }else{
        console.log('ELSE: reference items:', reference_items)
        console.log('ELSE: z:', z)
        queryPackage = 'item_id = ' + z[0][0].item_id + ' AND itemqty_count <= ' + z[0][0].qty + '';
        // test_packageResults[0][0] = test_packageResults[0][0] - z[0][0].qty;
        console.log('queryPackage-SINGLE: ', queryPackage);
      }

      var data_package: any = {
        data: queryPackage,
        itemType_count: z.length,
        destination: destination
      }
      console.log('data_package', data_package);

      var testVar = 0
      var recursive_counter_itemCount = 0;
      var recursive_boolean: boolean;
      var validate_recursive: boolean;

      await this.settingService.generatePackage(data_package).toPromise().then(res => {
        const res_data: any = res;

        if (res_data.data != "NO PACKAGGE FOUND") {
          console.log('NORMAL SUBSCRIBE:', res_data)
          console.log('Reference Items', reference_items);

          for(var res_index = +res_data.data.items.length-1; res_index >= 0; res_index--){
            console.log('index', res_index);
            console.log('index qty', res_data.data.items[res_index].itemqty_count)
            console.log('reference index ITEM ID', reference_items[res_index].item.id)
            console.log('recursive index ITEM ID', res_data.data.items[res_index].item_id)
            // if (reference_items.length != 0 && reference_items[res_index].item.id == res_data.data.items[res_index].item_id) {
            //   console.log('reference quantity: ', reference_items[res_index].qty = reference_items[res_index].qty - res_data.data.items[res_index].itemqty_count)
            //   if(reference_items[res_index].qty == 0){
            //     // if (res_index == 0) {
            //       reference_items.splice(res_index, 1)
            //     // }else{
            //       // reference_items.splice(+res_index-1, 1)
            //     // }
            //   }
            // }
            for(var index_ref_itm in reference_items){
              if (reference_items.length != 0 && reference_items[index_ref_itm].item.id == res_data.data.items[res_index].item_id) {
                console.log('reference index ITEM ID', reference_items[index_ref_itm].item.id)
                console.log('reference quantity: ', reference_items[index_ref_itm].qty = reference_items[index_ref_itm].qty - res_data.data.items[res_index].itemqty_count)
                if(reference_items[index_ref_itm].qty == 0){
                  // if (res_index == 0) {
                    reference_items.splice(index_ref_itm, 1)
                  // }else{
                  //   reference_items.splice(+recursive_res_index-1, 1)
                  // }
                }
              }
            }
          }
          res_counter = res_data.data.totalQty_of_items[0].total_qty
          res_data.data.check_package[0]['shipping_price'] = res_data.data.items[0].shipping_price;
          console.log('res_datares_data: ', res_data.data.check_package[0]);
          let final_package = {
            package_detail: res_data.data.check_package[0],
            package_items: res_data.data.items
          }
          finalPackages.push(final_package);

          testVar = res_data.data.totalQty_of_items;
          console.log('testVar: ', testVar);
            validate_recursive = false;
        }
        else if(res_data.data == "NO PACKAGGE FOUND"){
          validate_recursive = true;
        }
        // end if else ----
      });
      // ---- RECURSIVE PART -----------------
      if (validate_recursive) {
        while (validate_recursive) {
          data_package.itemType_count = data_package.itemType_count-1;
          console.log("HULILA KA! WALANG PACKAGE!");
          await this.settingService.generatePackage(data_package).toPromise().then(res => {
            const recursive_res_data: any = res;
            if(recursive_res_data.data != "NO PACKAGGE FOUND"){
              console.log('NESTED SUBSCRIBE: ', recursive_res_data)
              console.log('Reference Items', reference_items);
              console.log('itemType_count: ', data_package.itemType_count);

              for(var recursive_res_index = +recursive_res_data.data.items.length-1; recursive_res_index >= 0; recursive_res_index--){
                console.log('index', recursive_res_index);
                console.log('index qty', recursive_res_data.data.items[recursive_res_index].itemqty_count)
                console.log('recursive index ITEM ID', recursive_res_data.data.items[recursive_res_index].item_id)
                for(var index_ref_itm in reference_items){
                  if (reference_items.length != 0 && reference_items[index_ref_itm].item.id == recursive_res_data.data.items[recursive_res_index].item_id) {
                    console.log('reference index ITEM ID', reference_items[index_ref_itm].item.id)
                    console.log('reference quantity: ', reference_items[index_ref_itm].qty = reference_items[index_ref_itm].qty - recursive_res_data.data.items[recursive_res_index].itemqty_count)
                    if(reference_items[index_ref_itm].qty == 0){
                      // if (recursive_res_index == 0) {
                        reference_items.splice(index_ref_itm, 1)
                      // }else{
                      //   reference_items.splice(+recursive_res_index-1, 1)
                      // }
                    }
                  }
                }
              }
              res_counter = recursive_res_data.data.totalQty_of_items[0].total_qty
              recursive_res_data.data.check_package[0]['shipping_price'] = recursive_res_data.data.items[0].shipping_price;
              console.log('recursive_res_datarecursive_res_data: ', recursive_res_data.data.check_package[0]);
              let final_package = {
                package_detail: recursive_res_data.data.check_package[0],
                package_items: recursive_res_data.data.items
              }
              finalPackages.push(final_package);

              testVar = recursive_res_data.data.totalQty_of_items;
              console.log('testVar: ', testVar);
              validate_recursive = false;
            }
            else if(recursive_res_data.data == "NO PACKAGGE FOUND"){
              validate_recursive = true;
            }
          });
        }
      }
      // ---- END RECURSIVE PART -----------------


      console.log('testVar Outside: ', testVar);
      console.log('xxxx')

      console.log(x);
      console.log(test_packageResults);

      console.log('testCounter-OUTSIDE: ', testCounter)

      testCounterOutside = testCounter;

      console.log('finalPackages: ', finalPackages);

    }
    var total = 0;
    for(let fees of finalPackages){
      total = total + fees.package_detail.shipping_price;
      console.log(fees.package_detail.shipping_price);
    }
    this.total_package_shippingfee = total;
    console.log('total ', total);
    console.log('res_counter ', res_counter);
    console.log('SUPER-OUTSIDE-testCounter ', testCounterOutside);
    console.log('baseCounter ', baseCounter);
  }


  array_map (callback) { // eslint-disable-line camelcase
    //  discuss at: https://locutus.io/php/array_map/
    // original by: Andrea Giammarchi (https://webreflection.blogspot.com)
    // improved by: Kevin van Zonneveld (https://kvz.io)
    // improved by: Brett Zamir (https://brett-zamir.me)
    //    input by: thekid
    //      note 1: If the callback is a string (or object, if an array is supplied),
    //      note 1: it can only work if the function name is in the global context
    //   example 1: array_map( function (a){return (a * a * a)}, [1, 2, 3, 4, 5] )
    //   returns 1: [ 1, 8, 27, 64, 125 ]

    var argc = arguments.length
    var argv = arguments
    var obj = null
    var cb = callback
    var j = argv[1].length
    var i = 0
    var k = 1
    var m = 0
    var tmp = []
    var tmpArr = []

    var $global = (typeof window !== 'undefined' ? window : global)

    while (i < j) {
      while (k < argc) {
        tmp[m++] = argv[k++][i]
      }

      m = 0
      k = 1

      if (callback) {
        if (typeof callback === 'string') {
          cb = $global[callback]
        } else if (typeof callback === 'object' && callback.length) {
          obj = typeof callback[0] === 'string' ? $global[callback[0]] : callback[0]
          if (typeof obj === 'undefined') {
            throw new Error('Object not found: ' + callback[0])
          }
          cb = typeof callback[1] === 'string' ? obj[callback[1]] : callback[1]
        }
        tmpArr[i++] = cb.apply(obj, tmp)
      } else {
        tmpArr[i++] = tmp
      }

      tmp = []
    }

    return tmpArr
  }

}
