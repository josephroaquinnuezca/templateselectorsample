import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Component({
  selector: 'app-cart-dialog',
  templateUrl: './cart-dialog.component.html',
  styleUrls: ['./cart-dialog.component.css']
})
export class CartDialogComponent implements OnInit {

  myVariable: boolean = false;

  constructor(
     public dialogRef: MatDialogRef<CartDialogComponent>,
     @Inject(MAT_DIALOG_DATA) public getdata: any
  ) { }

  ngOnInit() {
    this.dialogRef.beforeClose().subscribe(() => this.dialogRef.close(this.myVariable));
  }
  okayBTN(){
    this.myVariable = true;
    this.dialogRef.close(this.myVariable);
  }
  closeBTN(){
    this.myVariable = false;
    this.dialogRef.close(this.myVariable);
  }


}
