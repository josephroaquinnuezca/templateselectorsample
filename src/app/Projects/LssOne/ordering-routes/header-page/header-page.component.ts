import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { UserService } from 'src/app/user-routes/shared/user.service';
import { ActivatedRoute, RouterEvent, NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
//Added September 6, 2019
import { MatDialog } from '@angular/material';
import { ItemNoResultComponent } from './modals/no-result/no-result.component';
import { ItemsService } from '../../_admin-services';
import { UserProvider } from '../../user-routes/shared/users.provider';
import { OrderRequest } from '../../admin-routes/shared/admin.model';
import { SearchService } from '../shared/search.service';
import { OverridecssService } from '../shared/overridecss.service';
import { CartcounterService } from '../../_client-services/cartcounter.service';
declare var $: any;
import * as $ from 'jquery';
import { ViewportScroller } from '@angular/common';

@Component({
  selector: 'app-header-page',
  templateUrl: './header-page.component.html',
  styleUrls: ['./header-page.component.scss']
})
export class HeaderPageComponent implements OnInit {
  //fix the background heigh'
  contentHeight: number;
  isOpen = false;

  cart_item_count = 0;
  //change background trigger event
  sub: any;
  isSignComponent: boolean;
  // END change background trigger event
  reqList: OrderRequest[] = [];
  paginator = {
    page: 1,
    limit: '20',
    key: '_all',
    type: '2'
  };

  pageItem = {
    first: 1,
    total: 0,
    pages: 0,
    has_next: 0,
    has_previous: 0,
    next_page: 0,
    previous_page: 0,
    offset: 0,
    current: 0
  };
  searchItems: any[] = [];

  showPopover: boolean = false;
  popoverFocus: boolean = false;

  currentUser: any
  windowScrolled: boolean;
  public destroyed = new Subject<any>();

  testmargin: number;

  // may 29, 2020

  constructor(private itemService: ItemsService,
    private viewportScroller: ViewportScroller,
    public userService: UserService,
    private router: Router, public service: UserService,
    public dialog: MatDialog, private userProvider: UserProvider, private searchService: SearchService, private overridecssService: OverridecssService,private cartCounterService: CartcounterService) { }
  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
        this.windowScrolled = true;
        this.showPopover =false;
    }
    else if (this.windowScrolled && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) {
        this.windowScrolled = false;
        this.showPopover =false;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // event.target.innerWidth;
    // let margintesting = document.getElementById("nav-testing"); //june 23, 2020
    this.contentHeight = document.documentElement.clientHeight - 180//(179.5 + 64 + 20)


    // this.testmargin =  margintesting.offsetHeight;
    // console.log(this.contentHeight);
  }

  // onActivate(event) {
  //     console.log('this is running')
  //     let scrollToTop = window.setInterval(() => {
  //         let pos = window.pageYOffset;
  //         if (pos > 0) {
  //             window.scrollTo(0, pos - 20); // how far to scroll on each step
  //         } else {
  //             window.clearInterval(scrollToTop);
  //         }
  //     }, 16);
  // }


  getData(pgn: any) {

    const str = $.param(pgn);
    this.itemService.getAll(str)
      .subscribe(data => {
        // this.search_loading = false;
        const d: any = data;
        if (!this.isObjectEmpty(d.data.items)) {
          this.pageItem = d.data.pagination;
          this.reqList = d.data.items;
          // console.log(this.reqList)
          this.searchItems = this.reqList;
          this.userProvider.searchItemsData = this.searchItems;

          this.searchService.emitNavSearchEvent(this.searchItems);
          // console.log(this.userProvider.searchItemsData)
        } else {
          this.openDialog();
        }
      });
  }

  applyFilter(key: string) {
    const emptyArray = [];
    this.router.navigate(['']);
    if (key) {
      this.userProvider.isSearching = true;
      this.paginator.page = 1;
      this.paginator.key = key;
      this.getData(this.paginator);
    } else {
      // this.searchService.emitNavSearchEvent(emptyArray);
      // if(emptyArray.length){
      //   console.log('Not empty')
      // }else{
      //   console.log('Is empty')
      // }
      // return
      this.userProvider.isSearching = false;
      this.paginator.page = 1;
      this.paginator.key = '';
      this.getData(this.paginator);
    }
  }
  isObjectEmpty(Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(ItemNoResultComponent, {
      panelClass: 'app-full-bleed-dialog-p-26',
      disableClose: false
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  goToShoppingCart() {
    this.router.navigate(['/shopping-cart']);
  }

  //change background trigger event
  triggerChangeCss(data: any) {
    // this.items = item;
    // this.isSearching = this.userProvider.isSearching;
    setTimeout(() => {
      this.isSignComponent = data;
      // console.log(`SWERSWERTE GUMANA ANIMAL: ${data}`);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  // END change background trigger event

  ngOnInit() {

    // let margintesting = document.getElementById("nav-testing"); newly deleted
    // this.testmargin =  margintesting.offsetHeight; /new deleted

    this.cartCounterService.getCartItemCount().subscribe(count =>{
      this.cart_item_count = count
			// console.log(this.cart_item_count);
    })

    this.contentHeight = document.documentElement.clientHeight - 180//(179.5 + 64 + 20)
    //change background trigger event
    this.isSignComponent = false;
    this.sub = this.overridecssService.getOverrideCssEmitter().subscribe(data => this.triggerChangeCss(data));
    // console.log(`isSignComponent: ${this.userProvider.isSignComponent}`);
    // END change background trigger event

    let mCarts = JSON.parse(localStorage.getItem('mCart'));
    if (mCarts) {
      for(let mCart of mCarts){
        this.cart_item_count = this.cart_item_count + mCart.qty;
      }
    }

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationEnd),
      takeUntil(this.destroyed)
    ).subscribe((res) => {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    });
		// if token expire redirect & signout
		// if(this.currentUser){
		// 	if (this.service.isTokenExpired()) {
		// 		// console.log('Token Exp');
		// 		this.service.signout();
		// 		this.router.navigate(['']);
		// 	}
		// }
  }

  mouseover() {
    this.showPopover = true;
  }

  onMouseOut() {
    this.showPopover = true;
    setTimeout(() => {
      this.showPopover = false;
    }, 100);
  }


  toggle(){
    if(!this.showPopover){
      this.showPopover = true;
    }else{
      this.showPopover =false;
    }
  }


  opendropdown(){
    $('.dropdown-toggle').css('display', 'block');
  }


  openNav(){
    $('#active-menu').css('background', '#4db6ac');
    $('#howayNav').css('display', 'block');
    $('#closenav').css('display', 'block');
    $('#opennav').css('display', 'none');
  }

  closeNav(){
    $('#active-menu').css('background', '#009688');
    $('#opennav').css('display', 'block');
    $('#closenav').css('display', 'none');
    $('#howayNav').css('display', 'none');

  }


  //scroll to top

  scrollToTopNew() {
    (function smoothscroll() {
        var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
        if (currentScroll > 0) {
            window.requestAnimationFrame(smoothscroll);
            window.scrollTo(0, currentScroll - (currentScroll / 8));
        }
    })();
  }


  scrollTo(elementID){
    let element = document.getElementById(elementID);
    console.log(element);
    if(element) {
      setTimeout (() => {
        console.log('natawag ako!')
        element.scrollIntoView({ block: 'center',  behavior: 'smooth' }); // scroll to a particular element
      });
    }
    // jQ('body,html').animate({
		// 	scrollTop: 1500 - 144
		// }, 750);
  }

  shipping(){
    this.router.navigate(['/guides']);
  }

  payment(){
    this.router.navigate(['/guides']);
  }

  return(){
    this.router.navigate(['/guides']);
  }

}
