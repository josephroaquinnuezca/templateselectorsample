import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemNoResultComponent } from './no-result.component';

describe('ItemNoResultComponent', () => {
  let component: ItemNoResultComponent;
  let fixture: ComponentFixture<ItemNoResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemNoResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemNoResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
