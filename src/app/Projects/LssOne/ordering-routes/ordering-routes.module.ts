import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OrderingRoutesRoutingModule } from './ordering-routes-routing.module';// import { BrowserModule } from '@angular/platform-browser';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialsModule } from './../shared/materials.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MomentModule } from 'ngx-moment';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgxBarcodeModule } from 'ngx-barcode';
import { DigitOnlyModule } from '@uiowa/digit-only';
import { ToastrModule } from 'ngx-toastr';
import { AgmCoreModule, GoogleMapsAPIWrapper   } from '@agm/core';

import { ConfirmOrderComponent } from './confirm-order/confirm-order.component';
import { PurchaseProcComponent } from './purchase-proc/purchase-proc.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { ItemDetailsComponent } from './item-details/item-details.component';
import { SuccessMessageComponent } from './success-message/success-message.component';
import { PersonalInformationComponent } from './personal-information/personal-information.component';
// STATIC PAGES
import { AboutUsComponent } from './../static-pages/about-us/about-us.component';
import { ContactUsComponent } from './../static-pages/contact-us/contact-us.component';
import { ClientGuideComponent } from './../static-pages/client-guide/client-guide.component';
import { PrivacyPolicyComponent } from './../static-pages/privacy-policy/privacy-policy.component';
import { RetexPolicyComponent } from './../static-pages/retex-policy/retex-policy.component';
import { TermsConditionComponent } from './../static-pages/terms-condition/terms-condition.component';
// USER IMPORTED
import { UserRoutesModule } from './../user-routes/user-routes.module';

import { ForgotPasswordResetComponent } from './../user-routes/forgot-password-reset/forgot-password-reset.component';
import { TransactionListComponent } from './../user-routes/transaction-list/transaction-list.component';
import { TransactioViewComponent } from './../user-routes/transactio-view/transactio-view.component';
import { TransactionQuickViewComponent } from './../user-routes/transaction-quick-view/transaction-quick-view.component';
import { UserInfoComponent } from './../user-routes/user-info/user-info.component';
import { ShortNumberpipePipe } from './item-details/short-numberpipe.pipe';
import { CustomPipesModule } from  'src/app/_custom-pipes/custom-pipes.module';
import { NgxGalleryModule } from 'ngx-gallery'; //newly added

// DIALOGS
import { AgmDialogComponent } from './personal-information/agm-dialog/agm-dialog.component';
import { ConfirmDialogComponent } from './personal-information/confirm-dialog/confirm-dialog.component';
import { CartDialogComponent } from './shopping-cart/cart-dialog/cart-dialog.component';
@NgModule({
  imports: [
    NgxGalleryModule,
    CommonModule,
    OrderingRoutesRoutingModule,
    UserRoutesModule, //from other module
    CustomPipesModule, //from other module
    // BrowserModule,
    // BrowserAnimationsModule,
    MaterialsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxChartsModule,
    NgxMaterialTimepickerModule,
    MomentModule,
    FlexLayoutModule,
    SlickCarouselModule,
		NgxBarcodeModule,
    DigitOnlyModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDEoNWWHYCtUfBIiqeYW0K1sw63JK11tEE'
    }),
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-center',
      preventDuplicates: true,
      closeButton:true,
      progressBar:true,
    }),

  ],
  declarations: [
    ConfirmOrderComponent,
    ItemDetailsComponent,
    ShoppingCartComponent,
    SuccessMessageComponent,
    PurchaseProcComponent,
    AboutUsComponent,
    ContactUsComponent,
    ClientGuideComponent,
    PrivacyPolicyComponent,
    RetexPolicyComponent,
    TermsConditionComponent,
    PersonalInformationComponent,
    // UserSignupComponent,
    ForgotPasswordResetComponent,
    TransactionListComponent,
    TransactioViewComponent,
    TransactionQuickViewComponent,
    UserInfoComponent,
    ShortNumberpipePipe,
    AgmDialogComponent,
    ConfirmDialogComponent,
    CartDialogComponent
  ],
  entryComponents: [
    AgmDialogComponent,
    ConfirmDialogComponent,
    CartDialogComponent
  ],
  providers: [
    GoogleMapsAPIWrapper
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})

export class OrderingRoutesModule { }
