import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {



  constructor(private router: Router,
    private dialogRef:MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public getdata: any) { }

  ngOnInit() {
  }

  yes(): void{
    //  this.router.navigate(['/purchase-procedure']);
    console.log('test', this.router.url)
     if (this.router.url === '/personal-info'){
      this.dialogRef.close();
     }
  }

}
