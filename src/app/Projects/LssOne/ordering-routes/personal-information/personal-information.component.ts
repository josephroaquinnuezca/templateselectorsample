import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { AgmDialogComponent } from './agm-dialog/agm-dialog.component';
import { SuccessMesageDialogComponent } from './../../static-pages/success-mesage-dialog/success-mesage-dialog.component';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';
import { first, retry, catchError, startWith, map, filter, takeUntil } from 'rxjs/operators';
import { Observable, throwError, Subject } from 'rxjs';
// import { DialogData } from './../../_client-interface/agm-map';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, Inject, EventEmitter, ViewChild, Input, Output, AfterViewInit, ElementRef, NgZone } from '@angular/core';
import { Cookie } from 'ng2-cookies';
import * as places from '../shared/cities.json'
import * as locations from '../shared/locations.json'
import { Router, ActivatedRoute, RouterEvent, NavigationEnd } from '@angular/router';
import { TermsConditionComponent } from 'src/app/static-pages/terms-condition/terms-condition.component.js';
import { ErrorStateMatcher, MatDialog } from '@angular/material';
//added august 2019 - AGM Dialog for LBC BRANCHES
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { PaymentFormService } from 'src/app/_admin-services/payment-form.service';
import { GiftwrapModel } from '../../_client-interface';
import { GiftwrapService } from '../../_client-services';
import { element } from 'protractor';
// import { _ } from 'underscore';
import * as _ from "underscore";

//june 5, 2020
import { UserService } from 'src/app/user-routes/shared/user.service';
import { UserProvider } from 'src/app/user-routes/shared/users.provider';
import { ToastrService } from 'ngx-toastr';


import { HelperService } from '../../_helper-service';
import * as jwt_decode from "jwt-decode";
import { } from 'googlemaps';
declare var google: any;
//june 3 2020



// export class MyErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//     const isSubmitted = form && form.submitted;
//     return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
//   }
// }
@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit {
  locations = locations.default;
  //june 7, 2020
  Customerform: FormGroup;
  // june 9 , 2020
  CustomerDiff: FormGroup;

  CustomerNSAForm: FormGroup;

  signinForm: FormGroup;
  submitted: boolean = false;
  imgFile: File

  optionPass: any;
  newDateOfBirth:any;
  userExistError: boolean = false;
  needsVerification: boolean = false;
  // barangay = barangay;
  optionRegister: Number = 0;
  // optionRegisterNSA: Number = 0;
  optionRegisterNSA: boolean = false;
  optionRegisterGuestNSA: boolean = false;
  // june 3 2020
  hide = true;

  //matcher = new MyErrorStateMatcher();
  //uncomment if your going to use matcher validation on matInput
  // matcher = new MyErrorStateMatcher();

  defval: string = 'deliver'
  placeVal: string = 'ncr'
  place = places.default

  mcit: any[] = []
  mreg: any[] = []
  total: number = 0
  ttlFee: number = 0

  checked = false

  giftChecked: boolean = false
  to: string = ""
  from: string = ""
  message: string = ""

  cities: any[] = []

  //package shipping fee
  total_package_shippingfee: any = 0; // var for package template
  itemCount = 0;

  //newly added
  deliveryOpts: string = '';
  selTime: string = ''
  selDate: string = ''
  today = new Date();
  items: any[] = []

  // giftwrap variable
	giftwrap_items: GiftwrapModel[] = [];
  myGiftwrapOption: any;
	checkSessionStorageDeliveryOpt: any;
	sessionBody: any;
	giftwrap: any[] = [];
	giftwrap_reference: any;
	toNames: any[] = [];
	giftwrap_fee: number;

  regionName:any;
  provinceName:any;
  cityName:any;
  barangayName:any;

  // regionArray:any;
  provinceArray:any;
  cityArray:any;
  barangayArray:any;

  regionNameNSA:any;
  provinceNameNSA:any;
  cityNameNSA:any;
  barangayNameNSA:any;

  // regionArray:any;
  provinceArrayNSA:any;
  cityArrayNSA:any;
  barangayArrayNSA:any;

  // locationVisibleNSA: boolean = false;
  locationVisible: boolean = false;

  currentCustomerInformation: any;
  isUpdate: boolean = false;
  userDetail: any;
  public destroyed = new Subject<any>();

  optionIfAlreadyMember: boolean = false;
  isBtnBack:boolean = false;

  // isValidCustomerAddressText: boolean = false;
  // isValidCustomerAddressTextDiff: boolean = false;
  // isValidCustomerAddressTextNSA: boolean = false;
  private customerAddressTextNSAElRef:ElementRef;
  private customerAddressTextDiffElRef:ElementRef;
  mapDialogDataCustomerform: any = {lat: 14.55540163953262, lng: 121.01935339751486};
  mapDialogDataCustomerNSAForm: any = {lat: 14.55540163953262, lng: 121.01935339751486};
  mapDialogDataCustomerDiff: any = {lat: 14.55540163953262, lng: 121.01935339751486};

  // isValidCustomerform: boolean = false;
  // isValidCustomerNSAForm: boolean = false;
  // isValidCustomerDiff: boolean = false;
  customerAddressTextReferenceValue: any;
  customerAddressTextNSAReferenceValue: any;
  customerAddressTextDiffReferenceValue: any;

  @Input() adressType: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @ViewChild('customerAddressText', {static: false}) customerAddressText: any;

  @ViewChild('customerAddressTextNSA', {static: false}) set customerAddressTextNSA(elRef: ElementRef) {
      this.customerAddressTextNSAElRef = elRef;
  }

  @ViewChild('customerAddressTextDiff', {static: false}) set customerAddressTextDiff(elRef: ElementRef) {
      this.customerAddressTextDiffElRef = elRef;
  }

  // autocompleteInput: string;
  // queryWait: boolean;

  // componentForm = {
  //   street_number: "short_name",
  //   route: "long_name",
  //   locality: "long_name",
  //   administrative_area_level_1: "short_name",
  //   country: "long_name",
  //   postal_code: "short_name",
  // };


  constructor(
    private zone: NgZone,
    private toastr: ToastrService, // new constructor may 31, 2020
    private helperService: HelperService,
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
    public errdialog: MatDialog,
    public sccdialog: MatDialog,
    public userService: UserService,
    private userProvider: UserProvider,
    private paymentFormService: PaymentFormService,
    private giftwrapService: GiftwrapService,
    public activatedRoute:ActivatedRoute) {

      // activatedRoute.params.subscribe(val => {
      //   // put the code from `ngOnInit` here
      // });

    }
  // end
  scrollToTop() {
    // (function smoothscroll() {
    //     var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
    //     if (currentScroll > 0) {
    //         window.requestAnimationFrame(smoothscroll);
    //         window.scrollTo(0, currentScroll - (currentScroll / 8));
    //     }
    // })();
    // window.scrollTo(0, 0);
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth' //smooth or auto
    });
  }

  filterPastDate() {
      //add or substract if u want to select past date or adjust today date into future date
      this.today.setDate(this.today.getDate())
  }

  sortASC(a, b)
  {
    return a.city_name < b.city_name ? -1 : a.city_name > b.city_name ? 1 : 0;
      // return itemA.city_name - itemB.city_name;
  }
  sortDESC(a, b)
  {
    return a.city_name < b.city_name ? -1 : a.city_name > b.city_name ? 1 : 0;
      // return itemB.city_name - itemA.city_name;
  }

  getRegionValue(event) {
    this.cities = event.value.city_list.sort(this.sortASC);
    // console.log(this.cities);
  }
    // june 14, 2020
  backshoppingcart(){
      this.router.navigate(['./shopping-cart']);
  }
  getDataAttrRegion(event){
    console.log(event);
    console.log('event value:', event.value);

    let tmp:any;
    for(let location of this.locations){
      if(location.region_name === event.value) tmp = location.province_list
    }

    this.regionName = event.value;
    if (this.provinceArray) {
      this.provinceArray = [];
      this.cityArray = [];
      this.barangayArray = [];
    }
    // console.log(this.provinceArray)
    // console.log(this.cityArray)
    // console.log(this.barangayArray)
    this.provinceArray = tmp;
  }
  getDataAttrProvince(event){
    console.log(event);
    console.log('event value:', event.value);
    console.log('ProvinceArray: ', this.provinceArray)
    console.log('ProvinceArray: ', this.provinceArray[event.value])
    console.log('ProvinceArray: ', this.provinceArray[event.value].municipality_list)
    this.provinceName = event.value;
    if (this.cityArray) {
      this.cityArray = [];
      this.barangayArray = [];
    }
    this.cityArray = this.provinceArray[event.value].municipality_list;
  }
  getDataAttrCity(event){
    console.log(event);
    console.log('event value:', event.value);
    console.log('barangayArray: ', this.cityArray)
    console.log('barangayArray: ', this.cityArray[event.value])
    this.cityName = event.value;
    if (this.barangayArray) {
      this.barangayArray = [];
    }
    this.barangayArray = this.cityArray[event.value].barangay_list;
  }
  getDataAttrBarangay(event){
    this.barangayName = event.value;
    // console.log(event.target.dataset.barangay);
    // console.log(event.target.getAttribute('data-barangay'))
  }
  clearPCB(){
    this.provinceName = ''
    this.cityName = ''
    this.barangayName = ''


    if (this.userService.hasCurrentUser()) {
      if (this.optionRegisterNSA) {
        this.CustomerDiff.controls['province'].setValue('');
        this.CustomerDiff.controls['city'].setValue('');
        this.CustomerDiff.controls['barangay'].setValue('');
      }
    }else{
      this.Customerform.controls['province'].setValue('');
      this.Customerform.controls['city'].setValue('');
      this.Customerform.controls['barangay'].setValue('');
    }
  }
  clearCB(){
    this.cityName = ''
    this.barangayName = ''

    if (this.userService.hasCurrentUser()) {
      if (this.optionRegisterNSA) {
        this.CustomerDiff.controls['city'].setValue('');
        this.CustomerDiff.controls['barangay'].setValue('');
      }
    }else{
      this.Customerform.controls['city'].setValue('');
      this.Customerform.controls['barangay'].setValue('');
    }
  }
  clearB(){
    this.barangayName = ''

    if (this.userService.hasCurrentUser()) {
      if (this.optionRegisterNSA) {
        this.CustomerDiff.controls['barangay'].setValue('');
      }
    }else{
      this.Customerform.controls['barangay'].setValue('');
    }
  }

  getDataAttrRegionNSA(event){
    console.log(event);
    console.log('event value:', event.value);

    let tmp:any;
    for(let location of this.locations){
      if(location.region_name === event.value) tmp = location.province_list
    }

    this.regionNameNSA = event.value;
    if (this.provinceArrayNSA) {
      this.provinceArrayNSA = [];
      this.cityArrayNSA = [];
      this.barangayArrayNSA = [];
    }
    this.provinceArrayNSA = tmp;
  }
  getDataAttrProvinceNSA(event){
    console.log(event);
    console.log('event value:', event.value);
    console.log('ProvinceArrayNSA: ', this.provinceArrayNSA)
    console.log('ProvinceArrayNSA: ', this.provinceArrayNSA[event.value])
    console.log('ProvinceArrayNSA: ', this.provinceArrayNSA[event.value].municipality_list)
    this.provinceNameNSA = event.value;
    if (this.cityArrayNSA) {
      this.cityArrayNSA = [];
      this.barangayArrayNSA = [];
    }
    this.cityArrayNSA = this.provinceArrayNSA[event.value].municipality_list;
  }
  getDataAttrCityNSA(event){
    console.log(event);
    console.log('event value:', event.value);
    console.log('barangayArray: ', this.cityArrayNSA)
    console.log('barangayArray: ', this.cityArrayNSA[event.value])
    this.cityNameNSA = event.value;
    if (this.barangayArrayNSA) {
      this.barangayArrayNSA = [];
    }
    this.barangayArrayNSA = this.cityArrayNSA[event.value].barangay_list;
  }
  getDataAttrBarangayNSA(event){
    this.barangayNameNSA = event.value;
    // console.log(event.target.dataset.barangay);
    // console.log(event.target.getAttribute('data-barangay'))
  }
  clearPCBNSA(){
    this.provinceNameNSA = ''
    this.cityNameNSA = ''
    this.barangayNameNSA = ''


    if (this.optionRegisterGuestNSA) {
      this.CustomerNSAForm.controls['province'].setValue('');
      this.CustomerNSAForm.controls['city'].setValue('');
      this.CustomerNSAForm.controls['barangay'].setValue('');
    }
  }
  clearCBNSA(){
    this.cityNameNSA = ''
    this.barangayNameNSA = ''

    if (this.optionRegisterGuestNSA) {
      this.CustomerNSAForm.controls['city'].setValue('');
      this.CustomerNSAForm.controls['barangay'].setValue('');
    }
  }
  clearBNSA(){
    this.barangayNameNSA = ''

    if (this.optionRegisterGuestNSA) {
      this.CustomerNSAForm.controls['barangay'].setValue('');
    }
  }
  ngOnInit() {
    // this.scrollToTop();
    this.createFormSignIn();
    this.createFormsAndValidate();
    this.setFormData();
		this.getAllGiftwrap();
    this.loadCustomerInformation();
		this.checkSessionStorageDeliveryOpt = sessionStorage.getItem('deliveryOpt');
		// console.log(JSON.parse(this.checkSessionStorageDeliveryOpt.giftwrap_remarks[0].quantity));
    this.filterPastDate()

    // console.log(this.lbc_branch);
    let data = localStorage.getItem('mCart')
    console.log('data: ', data)
    if (data && JSON.parse(data).length > 0) {
			this.getGiftwrapPrice()

      this.items = JSON.parse(data);

	    setTimeout(() => {
	      this.getTotal();
			}, 1000);
    }else{
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Please add items to cart first!'
      });
      this.router.navigate(['']);
    }

    let mCarts = JSON.parse(localStorage.getItem('mCart'));
    console.log('mCarts: ', mCarts)
    if (mCarts) {
      for(let mCart of mCarts){
        this.itemCount = this.itemCount + mCart.qty;
      }
    }
    // this.childModal.onShown.subscribe((reason: string) => {
    // this.pickMap.triggerResize();
    // });
  }

  loadCustomerInformation(){
    // let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    // // let CustomerInfo =  localStorage.getItem('infocustomer')
    // this.currenuserinfo = currentUser;
    if (this.userService.hasCurrentUser()) {
      this.userProvider.isFromCheckoutPersonal = true;
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));

      this.userService.getUser(currentUser.id).subscribe( res => {
        const data:any = res;
        console.log('USER:', data.data.items[0])
        this.currentCustomerInformation = data.data.items[0];
      });

      this.isUpdate = this.router.url.includes('information-update');

      this.router.events.pipe(
        filter((event: RouterEvent) => event instanceof NavigationEnd),
        takeUntil(this.destroyed)
      ).subscribe((res) => {
        let tmp: any = res;
        this.isUpdate = tmp.urlAfterRedirects.includes('information-update');
        this.userService.getUser(currentUser.id).subscribe( res => {
          const data:any = res;
          console.log('USER:', data.data.items[0])
          this.currentCustomerInformation = data.data.items[0];
        });
      });
    }
  }

  btnUpdateProfile(){
    this.isUpdate = true;
  }

  createFormsAndValidate(){
    if (this.userService.hasCurrentUser()) {
      console.log('Form pang nakalogin!')
      this.createFormCustomerDifferentShipping();
    }else{
      console.log('Form pang hindi nakalogin!')
      this.createFormCustomerForm();
    }
  }
  // redirectSamePage(){
  //   this.router.routeReuseStrategy.shouldReuseRoute = function () {
  //   return false;
  //   }
  //   this.router.onSameUrlNavigation = 'reload';
  //   this.router.navigate('/personal-info', { queryParams: { index: 1 } });
  // }
  //june 7 . 2020

  setFormData(){
    let deliveryOpts = JSON.parse(localStorage.getItem('deliveryOpts'))
    console.log('deliveryOpts', deliveryOpts)
    let infocustomer = JSON.parse(localStorage.getItem('infocustomer'))
    let infocustomerNSA = JSON.parse(localStorage.getItem('infocustomerNSA'))
    if (deliveryOpts) {
      if (deliveryOpts.service_method === 'delivery') {
        console.log('deliveryOpts == ', deliveryOpts)
        this.deliveryOpts = deliveryOpts.service_method;
      }else{
        this.deliveryOpts = deliveryOpts.service_method;
        this.selDate = deliveryOpts.pickup_date;
        this.selTime = deliveryOpts.pickup_time;
      }
    }
    if (infocustomer) {
      this.locationVisible = true;
      if (infocustomer.info_type === 'membership_nsa') {
        console.log('NEW ADDRESS!');
        // this.optionRegisterNSA = 3;
        this.optionRegisterNSA = true;
        this.CustomerDiff.controls['firstname'].setValue(infocustomer.firstname);
        this.CustomerDiff.controls['middlename'].setValue(infocustomer.middlename);
        this.CustomerDiff.controls['surename'].setValue(infocustomer.surename);
        this.CustomerDiff.controls['email_address'].setValue(infocustomer.email_address);
        if(infocustomer.dob){
          this.CustomerDiff.controls['dob'].setValue(this.datePipe.transform(new Date(infocustomer.dob), 'yyyy-MM-dd'));
          this.calculateAge(this.CustomerDiff.controls['dob'].value)
        }
        this.CustomerDiff.controls['contact_no'].setValue(infocustomer.contact_no.replace('+63', ''));
        this.CustomerDiff.controls['age'].setValue(infocustomer.age);
        this.CustomerDiff.controls['gender'].setValue(Number(infocustomer.gender));
        this.CustomerDiff.controls['address'].setValue(infocustomer.address);
        this.CustomerDiff.controls['remarks'].setValue(infocustomer.remarks);

        //used for update if input is not touched
        // this.regionName = infocustomer.region
        // this.provinceName = infocustomer.province
        // this.cityName = infocustomer.city
        // this.barangayName = infocustomer.barangay
        // /END
        // this.CustomerDiff.controls['region'].setValue(this.regionName);
        // if (this.regionName != '') {
        //   this.getDataAttrRegion({'value': this.regionName})
        // }
        // this.CustomerDiff.controls['province'].setValue(this.provinceName);
        // if (this.provinceName != '') {
        //   this.getDataAttrProvince({'value': this.provinceName})
        // }
        // this.CustomerDiff.controls['city'].setValue(this.cityName);
        // if (this.cityName != '') {
        //   this.getDataAttrCity({'value': this.cityName})
        // }
        // this.CustomerDiff.controls['barangay'].setValue(this.barangayName);

      }else if(infocustomer.info_type === 'guest'){
        this.Customerform.controls['firstname'].setValue(infocustomer.firstname);
        this.Customerform.controls['middlename'].setValue(infocustomer.middlename);
        this.Customerform.controls['surename'].setValue(infocustomer.surename);
        this.Customerform.controls['email_address'].setValue(infocustomer.email_address);
        if(infocustomer.dob){
          this.Customerform.controls['dob'].setValue(this.datePipe.transform(new Date(infocustomer.dob), 'yyyy-MM-dd'));
          this.calculateAge(this.Customerform.controls['dob'].value)
        }
        this.Customerform.controls['contact_no'].setValue(infocustomer.contact_no.replace('+63', ''));
        this.Customerform.controls['age'].setValue(infocustomer.age);
        this.Customerform.controls['gender'].setValue(Number(infocustomer.gender));
        this.Customerform.controls['address'].setValue(infocustomer.address);
        this.Customerform.controls['remarks'].setValue(infocustomer.remarks);

        //used for update if input is not touched
        // this.regionName = infocustomer.region
        // this.provinceName = infocustomer.province
        // this.cityName = infocustomer.city
        // this.barangayName = infocustomer.barangay
        // // /END
        // console.log('xxxxxxxxxxxxxxx')
        // console.log(this.regionName)
        // console.log(this.provinceName)
        // console.log(this.cityName)
        // console.log(this.barangayName)
        // console.log(infocustomer.region)
        // console.log(infocustomer.province)
        // console.log(infocustomer.city)
        // console.log(infocustomer.barangay)
        //
        //
        // this.Customerform.controls['region'].setValue(this.regionName);
        // if (this.regionName != '') {
        //   this.getDataAttrRegion({'value': this.regionName})
        // }
        // this.Customerform.controls['province'].setValue(this.provinceName);
        // if (this.provinceName != '') {
        //   this.getDataAttrProvince({'value': this.provinceName})
        // }
        // this.Customerform.controls['city'].setValue(this.cityName);
        // if (this.cityName != '') {
        //   this.getDataAttrCity({'value': this.cityName})
        // }
        // this.Customerform.controls['barangay'].setValue(this.barangayName);
        // FOR GUEST NEW SHIPPING DIFFERENT ADDRESS
        if (infocustomerNSA) {
          this.optionRegisterGuestNSA = true;
          this.CustomerNSAForm.controls['firstname'].setValue(infocustomerNSA.firstname);
          this.CustomerNSAForm.controls['middlename'].setValue(infocustomerNSA.middlename);
          this.CustomerNSAForm.controls['surename'].setValue(infocustomerNSA.surename);
          this.CustomerNSAForm.controls['email_address'].setValue(infocustomerNSA.email_address);
          if(infocustomerNSA.dob){
            this.CustomerNSAForm.controls['dob'].setValue(this.datePipe.transform(new Date(infocustomerNSA.dob), 'yyyy-MM-dd'));
            this.calculateAge(this.CustomerNSAForm.controls['dob'].value)
          }
          this.CustomerNSAForm.controls['contact_no'].setValue(infocustomerNSA.contact_no.replace('+63', ''));
          this.CustomerNSAForm.controls['age'].setValue(infocustomerNSA.age);
          this.CustomerNSAForm.controls['gender'].setValue(Number(infocustomerNSA.gender));
          this.CustomerNSAForm.controls['address'].setValue(infocustomerNSA.address);
          this.CustomerNSAForm.controls['remarks'].setValue(infocustomerNSA.remarks);

          //used for update if input is not touched
          // this.regionNameNSA = infocustomerNSA.region
          // this.provinceNameNSA = infocustomerNSA.province
          // this.cityNameNSA = infocustomerNSA.city
          // this.barangayNameNSA = infocustomerNSA.barangay
          // // /END
          //
          // this.CustomerNSAForm.controls['region'].setValue(this.regionNameNSA);
          // if (this.regionNameNSA != '') {
          //   this.getDataAttrRegionNSA({'value': this.regionNameNSA})
          // }
          // this.CustomerNSAForm.controls['province'].setValue(this.provinceNameNSA);
          // if (this.provinceNameNSA != '') {
          //   this.getDataAttrProvinceNSA({'value': this.provinceNameNSA})
          // }
          // this.CustomerNSAForm.controls['city'].setValue(this.cityNameNSA);
          // if (this.cityNameNSA != '') {
          //   this.getDataAttrCityNSA({'value': this.cityNameNSA})
          // }
          // this.CustomerNSAForm.controls['barangay'].setValue(this.barangayNameNSA);
        }
      }
    }
    // console.log('optionRegisterNSA: ', this.optionRegisterNSA);
  }
  // objectComparisonFunction( option, value ){
  //   console.log('option: ', option)
  //   console.log('value: ', value)
  //   return option.name === value.name;
  // }
  createFormCustomerForm(){
    this.Customerform = this.formBuilder.group({
      firstname: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      middlename: ['',[]],
      surename: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      age: ['', []],
      dob: [{value: '', disabled: false}, []],
      email_address: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      gender: ['0', []],
      contact_no: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^[0-9]*$')]],
      region: ['', ], //[Validators.required]
      province: ['', ], //[Validators.required]
      city: ['', ], //[Validators.required]
      barangay: ['', ], //[Validators.required]
      address: ['', [Validators.required]],
      remarks: ['', []]
    });

    this.CustomerNSAForm = this.formBuilder.group({
      firstname: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      middlename: ['',[]],
      surename: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      age: ['', []],
      dob: [{value: '', disabled: false}, []],
      email_address: [''],
      gender: ['0', []],
      contact_no: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^[0-9]*$')]],
      region: ['', ],//[Validators.required]
      province: ['', ],//[Validators.required]
      city: ['', ],//[Validators.required]
      barangay: ['', ],//[Validators.required]
      address: ['', [Validators.required]],
      remarks: ['', []]
    });
  }

  createFormCustomerDifferentShipping(){
    this.CustomerDiff = this.formBuilder.group({
      firstname: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      middlename: ['',],
      surename: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      age: ['', []],
      dob: [{value: '', disabled: false}, []],
      email_address: [''],
      gender: ['0', []],
      contact_no: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^[0-9]*$')]],
      region: ['', ], //[Validators.required]
      province: ['', ], //[Validators.required]
      city: ['', ], //[Validators.required]
      barangay: ['', ], //[Validators.required]
      address: ['', [Validators.required]],
      remarks: ['', []]
    });
  }

  createFormSignIn(){
    this.signinForm = this.formBuilder.group({
      username: [ '', Validators.required ],
      password: [ '', Validators.required ]
    });
  }

  onSignInSubmit(){
    this.submitted = true;
    if(this.signinForm.invalid){
      // alert('Please complete the signin form')

      return
    }
    // console.log(this.signinForm.getRawValue());
    // return;
    this.userService.signin(this.signinForm.getRawValue()).subscribe(
      res =>{
        console.log(res);
        if(res.status.error){
          this.toastr.error('User not found or Password incorrect');
          return
        }
        this.toastr.success('You are now succesfully login');
            setTimeout (() => {
              let url = this.router.url
         }, 5000);

        let tokenData: any = this.tokenData(res.data.token)
        if(tokenData.user_info.usertype_id == 1){
          this.submitted = false;
          this.toastr.success('You will be redirected to admin login.');
          localStorage.removeItem('currentAdmin');
          localStorage.removeItem('lss-token');
          this.router.navigate(['/admin/login'])

        }else{
          this.submitted = false;
          if(!this.userService.setUser(res.data.token)){
            // this.needsVerification = true;
            this.router.navigate(['personal-info']);
            this.createFormsAndValidate();
            // this.redirectSamePage();
          }else{
            this.router.navigate(['personal-info']);
            this.createFormsAndValidate();
            this.loadCustomerInformation();
            localStorage.removeItem('infocustomer');
            localStorage.removeItem('infocustomerNSA');
            localStorage.removeItem('deliveryOpts');
            this.isBtnBack = false;
            this.optionIfAlreadyMember = false;
            // this.redirectSamePage();
          }
        }
      }
    )
  }

  SubmitOn(){
    if (this.Customerform.invalid) {
      for (let v in this.Customerform.controls) {
        this.Customerform.controls[v].markAsTouched();
      }
			this.helperService.findInvalidControls(this.Customerform.controls);
      // alert('Incomplete information or invalid input provided!') //

        const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: 'Incomplete information or invalid input provided!'
        });
        return
    }
    if (!this.optionPass) {
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Please input a password!'
      });
      return;
    }
    let data = this.Customerform.getRawValue();
    data.usertype_id = 2
    data.password = this.optionPass
    data.has_file = true
    // data.region = this.regionName;
    // data.province = this.provinceName;
    // data.city = this.cityName;

    data.region = ''; //this.regionName
    data.province = ''; //this.provinceName
    data.city = ''; //this.cityName
    data.barangay = ''; //this.barangayName

    data.contact_no = '+63' + data.contact_no;
    data.age = (data.age == '0' || data.age == '' || !data.age) ? 'None' : data.age
    data.gender = (data.gender == '0' || data.gender == '' || !data.gender) ? 'None' : data.gender
    data.dob = (this.newDateOfBirth == '' || !this.newDateOfBirth) ? 'None' : this.newDateOfBirth

    this.userService.insertGuest(data, this.imgFile).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
      const data: any = res;
      console.log('data:', data);
      if(!this.userService.setUser(data.data.token)){
        // this.needsVerification = true;
        this.router.navigate(['/personal-info']);
        this.createFormsAndValidate();
        // this.redirectSamePage();
      }else{
        // this.needsVerification = false;
        this.setDeliveryOptions();
        this.router.navigate(['/purchase-procedure']);
      }
    }, err => {
      console.log('HTTP Error:', err.error);
      console.log(err.error.status.error);
      console.log('err.error:', err.error);
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: err.error.status.description
      });
      return;
      // if(err.error.type == 'error'){
      //   console.log('err.error.type:', err.error.type)
      //   // this.userExistError = true;
      //   // console.log(this.userExistError);
      // }
      // else{
      //   // this.userExistError = true;
      //   // console.log(this.userExistError);
      // }
    }, () => {
      console.log("request completed");
      // this.router.navigate(['/purchase-procedure']);
    });

  }
  guestNSA(){
    if (this.optionRegisterGuestNSA) {
      if (!this.isBtnBack) {
        for (let v in this.CustomerNSAForm.controls) {
          this.CustomerNSAForm.controls[v].markAsTouched();
        }
        if(this.CustomerNSAForm.invalid){
          this.helperService.findInvalidControls(this.CustomerNSAForm.controls);
          this.errdialog.open(ErrorMesageDialogComponent, {
            panelClass: 'app-full-bleed-dialog-p-26',
            disableClose: false,
            data: 'Incomplete information or invalid input provided for new different address!'
          });
          return false;
        }else{
          console.log('GUEST NSA')
          let infocustomer = this.CustomerNSAForm.getRawValue();
          infocustomer.info_type = "guest_nsa";
          infocustomer.region = '';//this.regionNameNSA
          infocustomer.province = '';//this.provinceNameNSA
          infocustomer.city = '';//this.cityNameNSA
          infocustomer.barangay = '';//this.barangayNameNSA
          // infocustomer.dob = (this.newDateOfBirth == '' || !this.newDateOfBirth) ? 'None' : this.newDateOfBirth
          localStorage.removeItem('infocustomerNSA');
          localStorage.setItem('infocustomerNSA', JSON.stringify(infocustomer));
          return true;
        }
      }else{
        console.log('GUEST NSA BACK')
        let infocustomer = this.CustomerNSAForm.getRawValue();
        infocustomer.info_type = "guest_nsa";
        infocustomer.region = (this.regionNameNSA) ? this.regionNameNSA : '';
        infocustomer.province = (this.provinceNameNSA) ? this.provinceNameNSA : '';
        infocustomer.city = (this.cityNameNSA) ? this.cityNameNSA : '';
        infocustomer.barangay = (this.barangayNameNSA) ? this.barangayNameNSA : '';
        infocustomer.address = '';
        // infocustomer.dob = (this.newDateOfBirth == '' || !this.newDateOfBirth) ? 'None' : this.newDateOfBirth
        localStorage.removeItem('infocustomerNSA');
        localStorage.setItem('infocustomerNSA', JSON.stringify(infocustomer));
        return true;
      }
      // end if isBtnBack
    }else{
      console.log('GUEST NSA CLEAR/REMOVE')
      console.log('NATAWAGGGGSSSS')
      localStorage.removeItem('infocustomerNSA');
      return true;
    }
    // END IF optionRegisterGuestNSA
  }
  //june 7, 2020
  checkoutConfirmBTN(isBackPressed){
    this.isBtnBack = isBackPressed;
    console.log('isBackPressed: ', isBackPressed)
    if (!this.isBtnBack) {
      if (this.optionIfAlreadyMember) {
        const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: 'Please sign in to continue.'
        });
        return
      }
      if(!this.deliveryOpts){
        // this.helperService.findInvalidControls(this.CustomerDiff.controls);
        this.errdialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: 'Please select service method!'
        });
        return;
      }
      if (this.deliveryOpts) {
        if (this.deliveryOpts==='pickup') {
          if (this.selDate == '' || this.selTime == '') {
            this.errdialog.open(ErrorMesageDialogComponent, {
              panelClass: 'app-full-bleed-dialog-p-26',
              disableClose: false,
              data: 'Please select a time you will visit the store!'
            });
            return;
          }
        }
      }
      if(!this.userService.hasCurrentUser()){
        if(!this.optionRegister){
          // this.helperService.findInvalidControls(this.CustomerDiff.controls);
          this.errdialog.open(ErrorMesageDialogComponent, {
            panelClass: 'app-full-bleed-dialog-p-26',
            disableClose: false,
            data: 'Select an shop member option to proceed!'
          });
          return;
        }
      }
    }
    // if(this.userService.hasCurrentUser()){
    //   if(!this.optionRegisterNSA){
    //     // this.helperService.findInvalidControls(this.CustomerDiff.controls);
    //     this.errdialog.open(ErrorMesageDialogComponent, {
    //       height: '250px',
    //       width: '450px',
    //       data: 'Select an shop member option to proceed!'
    //     });
    //     return;
    //   }
    // }
    if (this.optionRegister == 1) { // FOR AUTOMATIC LOGIN UPON CREATIN GUEST ACCOUNT
      // this.SubmitOn();
      if (!this.isBtnBack) {
        if (!this.optionPass) {
          this.errdialog.open(ErrorMesageDialogComponent, {
            panelClass: 'app-full-bleed-dialog-p-26',
            disableClose: false,
            data: 'Please input a password!'
          });
          return;
        }
        for (let v in this.Customerform.controls) {
          this.Customerform.controls[v].markAsTouched();
        }
        if(this.Customerform.invalid){

          this.errdialog.open(ErrorMesageDialogComponent, {
            panelClass: 'app-full-bleed-dialog-p-26',
            disableClose: false,
            data: 'Incomplete information or invalid input provided!'
          });
          return;
        }else{
          console.log('natawag A');
          let infocustomer = this.Customerform.getRawValue();
          infocustomer.info_type = "guest";
          infocustomer.guest_type = "membership";
          infocustomer.password = this.optionPass;
          infocustomer.region = ''; //this.regionName
          infocustomer.province = ''; //this.provinceName
          infocustomer.city = ''; //this.cityName
          infocustomer.barangay = ''; //this.barangayName
          // infocustomer.dob = (this.newDateOfBirth == '' || !this.newDateOfBirth) ? 'None' : this.newDateOfBirth
          localStorage.removeItem('infocustomer');
          localStorage.setItem('infocustomer', JSON.stringify(infocustomer));
          this.setDeliveryOptions();
          if (!this.guestNSA()) {
            return
          }
          this.router.navigate(['/purchase-procedure']);
        }
      }else{
        console.log('natawag A BACK BUTTON');
        let infocustomer = this.Customerform.getRawValue();
        infocustomer.info_type = "guest";
        infocustomer.guest_type = "membership";
        infocustomer.password = this.optionPass;
        infocustomer.region = (this.regionName) ? this.regionName : '';
        infocustomer.province = (this.provinceName) ? this.provinceName : '';
        infocustomer.city = (this.cityName) ? this.cityName : '';
        infocustomer.barangay = (this.barangayName) ? this.barangayName : '';
        infocustomer.address = '';
        // infocustomer.dob = (this.newDateOfBirth == '' || !this.newDateOfBirth) ? 'None' : this.newDateOfBirth
        localStorage.removeItem('infocustomer');
        localStorage.setItem('infocustomer', JSON.stringify(infocustomer));
        this.setDeliveryOptions();
        this.guestNSA();
        this.router.navigate(['./shopping-cart']);
      }

    } else if(this.optionRegister == 2) { // FOR GUEST INFORMATION
      this.helperService.findInvalidControls(this.Customerform.controls);

      if (!this.isBtnBack) {
        for (let v in this.Customerform.controls) {
          this.Customerform.controls[v].markAsTouched();
        }
        if(this.Customerform.invalid){

          this.errdialog.open(ErrorMesageDialogComponent, {
            panelClass: 'app-full-bleed-dialog-p-26',
            disableClose: false,
            data: 'Incomplete information or invalid input provided!'
          });
          return;
        }else{
          console.log('natawag B');
          // console.log('isValidCustomerform: ', this.isValidCustomerform)
          // if (!this.isValidCustomerform) {
          //   this.Customerform.controls['address'].setErrors({ isInvalidCustomerform: true })
          //   for (let v in this.Customerform.controls) {
          //     this.Customerform.controls[v].markAsTouched();
          //   }
          //   this.errdialog.open(ErrorMesageDialogComponent, {
          //     panelClass: 'app-full-bleed-dialog-p-26',
          //     disableClose: false,
          //     data: 'Invalid Address. Please check your address input.'
          //   });
          //   return;
          // }
          // else{
          //   this.Customerform.controls['address'].setErrors(null)
          // }
          let infocustomer = this.Customerform.getRawValue();

          console.log(infocustomer);
          // return;
          infocustomer.info_type = "guest";
          infocustomer.guest_type = "none_membership";
          infocustomer.region = ''; //this.regionName
          infocustomer.province = ''; //this.provinceName
          infocustomer.city = ''; //this.cityName
          infocustomer.barangay = ''; //this.barangayName
          // infocustomer.dob = (this.newDateOfBirth == '' || !this.newDateOfBirth) ? 'None' : this.newDateOfBirth
          localStorage.removeItem('infocustomer');
          localStorage.setItem('infocustomer', JSON.stringify(infocustomer));
          this.setDeliveryOptions();
          if (!this.guestNSA()) {
            return
          }
          this.router.navigate(['/purchase-procedure']);
        }
      }else{
        console.log('natawag B BACK BUTTON');
        let infocustomer = this.Customerform.getRawValue();
        infocustomer.info_type = "guest";
        infocustomer.guest_type = "none_membership";
        infocustomer.region = (this.regionName) ? this.regionName : '';
        infocustomer.province = (this.provinceName) ? this.provinceName : '';
        infocustomer.city = (this.cityName) ? this.cityName : '';
        infocustomer.barangay = (this.barangayName) ? this.barangayName : '';
        infocustomer.address = '';
        // infocustomer.dob = (this.newDateOfBirth == '' || !this.newDateOfBirth) ? 'None' : this.newDateOfBirth
        localStorage.removeItem('infocustomer');
        localStorage.setItem('infocustomer', JSON.stringify(infocustomer));
        this.setDeliveryOptions();
        this.guestNSA();
        this.router.navigate(['./shopping-cart']);
      }

    // } else if(this.optionRegisterNSA == 3) { // FOR DIFFERENT SHIPPING ADDRESS
    } else if(this.optionRegisterNSA) { // FOR DIFFERENT SHIPPING ADDRESS

      if (!this.isBtnBack) {
        if(this.CustomerDiff.invalid){
          for (let v in this.CustomerDiff.controls) {
            this.CustomerDiff.controls[v].markAsTouched();
          }
          this.helperService.findInvalidControls(this.CustomerDiff.controls);
          this.errdialog.open(ErrorMesageDialogComponent, {
            panelClass: 'app-full-bleed-dialog-p-26',
            disableClose: false,
            data: 'Incomplete information or invalid input provided!'
          });
          return;
        }else{
          console.log('natawag C');
          this.router.navigate(['./purchase-procedure']);
          let infocustomer = this.CustomerDiff.getRawValue();

          infocustomer.info_type = "membership_nsa";
          infocustomer.region = ''; //this.regionName
          infocustomer.province = ''; //this.provinceName
          infocustomer.city = ''; //this.cityName
          infocustomer.barangay = ''; //this.barangayName
          localStorage.removeItem('infocustomer');
          localStorage.setItem('infocustomer', JSON.stringify(infocustomer));
          this.setDeliveryOptions();
          this.router.navigate(['/purchase-procedure']);
        }
      }else{
        console.log('natawag C BACK BUTTON');
        // this.router.navigate(['./purchase-procedure']);
        let infocustomer = this.CustomerDiff.getRawValue();
        infocustomer.info_type = "membership_nsa";
        infocustomer.region = (this.regionName) ? this.regionName : '';
        infocustomer.province = (this.provinceName) ? this.provinceName : '';
        infocustomer.city = (this.cityName) ? this.cityName : '';
        infocustomer.barangay = (this.barangayName) ? this.barangayName : '';
        infocustomer.address = '';
        localStorage.removeItem('infocustomer');
        localStorage.setItem('infocustomer', JSON.stringify(infocustomer));
        this.setDeliveryOptions();
        this.router.navigate(['./shopping-cart']);
      }

    }else{ // MOVE TO NEXT PAGE USE GETUSER SINGLE NEXT PAGE IF NO "infocustomer" is set

      if (!this.isBtnBack) {
        console.log('natawag D');
        localStorage.removeItem('infocustomer');
        this.setDeliveryOptions();
        this.router.navigate(['/purchase-procedure']);
      }else{
        console.log('natawag D BACK BUTTON');
        if(!this.userService.hasCurrentUser()){
          // this.router.navigate(['./purchase-procedure']);
          let infocustomer = this.Customerform.getRawValue();
          infocustomer.info_type = "guest";
          infocustomer.region = (this.regionName) ? this.regionName : '';
          infocustomer.province = (this.provinceName) ? this.provinceName : '';
          infocustomer.city = (this.cityName) ? this.cityName : '';
          infocustomer.barangay = (this.barangayName) ? this.barangayName : '';
          infocustomer.address = '';
          // infocustomer.barangay = (this.barangayName) ? this.barangayName : 'Select Barangay';
          localStorage.removeItem('infocustomer');
          localStorage.setItem('infocustomer', JSON.stringify(infocustomer));
          this.setDeliveryOptions();
          this.guestNSA();
          this.router.navigate(['./shopping-cart']);
        }else{
          console.log('natawag D');
          localStorage.removeItem('infocustomer');
          this.setDeliveryOptions();
          this.router.navigate(['./shopping-cart']);
        }
      }
    }
  }

  setDeliveryOptions(){
    let data;
    if (this.deliveryOpts === 'delivery') {
      data = {
        service_method: this.deliveryOpts
      }
    }else{
      data = {
        service_method: this.deliveryOpts,
        pickup_date: this.selDate,
        pickup_time: this.selTime
      }
    }

    localStorage.removeItem('deliveryOpts');
    localStorage.setItem('deliveryOpts', JSON.stringify(data));
  }
  // june 9, 2020
  // diffcustombtn(){
  //   if(this.optionDiffaddress == 1){
  //
  //   }else{
  //     const dialogRef = this.sccdialog.open(ConfirmDialogComponent, {
  //       height: '250px',
  //       width: '450px',
  //       data: 'Are you sure want to proceed?'
  //     });
  //
  //     // this.router.navigate(['/purchase-procedure']);
  //   }
  //
  // }
  //USE FOR TESTING
  onRadioChangeServiceMethod(event){
    if (event.value === 'pickup') {
      this.optionRegisterNSA = false;
      this.optionRegisterGuestNSA = false;
    }
    console.log('event value:', event.value);
    console.log('this.deliveryOpts:', this.deliveryOpts);
  }
  //USE FOR TESTING
  onOptionRegisterChange(event){
    console.log('event value:', event.value);
    console.log('this.optionRegister:', this.optionRegister);
  }

  clearAlertMessage(){
		if(this.userExistError){
			this.userExistError = false;
		}
	}

  getTotal() {
		// console.log(this.giftwrap);
		// console.log(this.giftwrap_reference);
		// console.log(this.giftChecked);
		// console.log(this.giftwrap_fee);
		if(this.giftChecked){
			var total_giftwrapCount = 0;
			for (let giftwrap of this.giftwrap){
					total_giftwrapCount += giftwrap.quantity;
			}
		}
		// console.log(total_giftwrapCount);
    this.total = this.giftChecked?total_giftwrapCount*this.giftwrap_fee:0
    this.ttlFee = 0
    for (let itm of this.items)
      // this.total += itm.qty * (itm.item.price + (this.defval === 'deliver' ? this.getFee(itm) : 0))
      this.total += (itm.qty * itm.item.price);
    for (let itm of this.items)
      // this.ttlFee += itm.qty * (this.defval === 'deliver' ? this.getFee(itm) : 0)
      this.ttlFee += itm.qty;

    this.total_package_shippingfee = 0;
  }

	getGiftwrapPrice(){
		this.paymentFormService.getForm().subscribe(res => {
			let tmp: any = res
			console.log('PaymentFormComponent', tmp)
			var data = tmp.data.items[0]
			this.giftwrap_fee = data.giftwrap_fee;
			console.log(this.giftwrap_fee)
		})
  }
  getAllGiftwrap() {
    this.giftwrapService.getAllGiftwrap()
    .subscribe( data => {
      // this.search_loading = false;
      const d: any = data;
      if (!this.isObjectEmpty(d.data.items)) {
        this.giftwrap_items = d.data.items;
        console.log( this.giftwrap_items);
      } else {
        this.giftwrap_items = [];
      }
    });
  }

	calculateAge(value){
		var newDate = this.datePipe.transform(new Date(value), 'yyyy-MM-dd HH-mm-ss')
		var newDateTime = new Date(value).getTime()
		var timeDiff = Math.abs(Date.now() - newDateTime);
		var age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);

    if (this.userService.hasCurrentUser()) {
  		this.CustomerDiff.controls['age'].setValue(age)
    }else{
  		this.Customerform.controls['age'].setValue(age)
    }
		this.newDateOfBirth = newDate;
		console.log(this.newDateOfBirth)
		// console.log(newDate)
	}

	jsonReturn(value){
		return JSON.parse(value);
	}
  changeDestinationOpt(selectedValue) {
    this.placeVal = selectedValue.delivery_name;
    let ordOpt = JSON.parse(sessionStorage.getItem('deliveryOpt'))
    let body = {
      'defval': ordOpt.defval,
      'placeVal': selectedValue.delivery_name,
      'selTime': ordOpt.selTime,
      'selDate': ordOpt.selDate,
      'selBranch': ordOpt.selBranch,
      'checked': ordOpt.checked,
			'toNames': ordOpt.toNames,
			'giftwrap_remarks': ordOpt.giftwrap_remarks,
			'giftwrap_option': ordOpt.giftwrap_option,
			'giftwrap_status': ordOpt.checked,
      'to': ordOpt.to,
      'from': ordOpt.from,
      'message': ordOpt.message
    }
    sessionStorage.setItem('deliveryOpt', JSON.stringify(body))

    this.getTotal()
  }

  clearSelection(selectedValue) {
    let ordOpt = JSON.parse(sessionStorage.getItem('deliveryOpt'))

    let body = {
      'defval': selectedValue,
      'placeVal': ordOpt.placeVal,
      'selTime': ordOpt.selTime,
      'selDate': ordOpt.selDate,
      'selBranch': ordOpt.selBranch,
      'checked': ordOpt.checked,
			'toNames': ordOpt.toNames,
			'giftwrap_remarks': ordOpt.giftwrap_remarks,
			'giftwrap_option': ordOpt.giftwrap_option,
			'giftwrap_status': ordOpt.checked,
      'to': ordOpt.to,
      'from': ordOpt.from,
      'message': ordOpt.message
    }

    sessionStorage.setItem('deliveryOpt', JSON.stringify(body))
  }

  onTypeValidate(formControl) {
    formControl.markAsTouched();
  }

  trimValue(formControl) {
    console.log(formControl);
    console.log(formControl.value.trim().replace(/\s\s+/g, ' '));
    formControl.setValue(formControl.value.trim().replace(/\s\s+/g, ' '));
  }

  get f() { return this.signinForm.controls; }
  // june 7 2020
  get firstname(){ return this.Customerform.get('firstname');}
  // get middlename(){ return this.Customerform.get('middlename');}
  get surename(){ return this.Customerform.get('surename');}
  // get dob(){ return this.Customerform.get('dob');}
  get email_address(){ return this.Customerform.get('email_address');}
  get gender(){ return this.Customerform.get('gender');}
  get contact_no(){ return this.Customerform.get('contact_no');}
  get region(){ return this.Customerform.get('region');}
  get province(){ return this.Customerform.get('province');}
  get city(){ return this.Customerform.get('city');}
  get barangay(){ return this.Customerform.get('barangay');}
  get address(){ return this.Customerform.get('address');}
  // get remarks(){ return this.Customerform.get('remarks');}


  get cusfirstname(){ return this.CustomerDiff.get('firstname');}
  // get cusmiddlename(){ return this.CustomerDiff.get('middlename');}
  get cuslastname(){ return this.CustomerDiff.get('surename');}
  get cusemail_address(){ return this.CustomerDiff.get('email_address');}
  get cusgender(){ return this.CustomerDiff.get('gender');}
  get cuscontact_no(){ return this.CustomerDiff.get('contact_no');}
  get cusregion(){ return this.CustomerDiff.get('region');}
  get cusprovince(){ return this.CustomerDiff.get('province');}
  get cuscity(){ return this.CustomerDiff.get('city');}
  get cusbarangay(){ return this.CustomerDiff.get('barangay');}
  get cusaddress(){ return this.CustomerDiff.get('address');}
  // get cusremarks(){ return this.CustomerDiff.get('remarks');}


  handleError(error) {
    console.log(error);
    return throwError(error);
  }

  isObjectEmpty (Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

  tokenData(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

///////////////////////////////////////////////
// GOOGLE  MAPS
///////////////////////////////////////////////
  onKeyUpCustomerFormAddress(e){
    let infocustomer = JSON.parse(localStorage.getItem('infocustomer'))
    if (infocustomer) {
      console.log('nagrun ito A')
      if (e.target.value === infocustomer.address) {
        this.Customerform.controls['address'].setErrors(null)
      }else{
        console.log(e.target.value.length)
        if (e.target.value.length == 0) {
          this.Customerform.controls['address'].setErrors({ required: true })
        }else{
          if (this.customerAddressTextReferenceValue) {
            if (e.target.value === this.customerAddressTextReferenceValue) {
              this.Customerform.controls['address'].setErrors(null)
            }else{
              this.Customerform.controls['address'].setErrors({ isInvalidCustomerform: true })
            }
          }else{
            this.Customerform.controls['address'].setErrors({ isInvalidCustomerform: true })
          }
        }
      }
    }else{
      console.log('nagrun ito B')
      console.log(e.target.value);
      console.log(e.target.value.length)
      if (e.target.value.length == 0) {
        this.Customerform.controls['address'].setErrors({ required: true })
      }else{
        if (this.customerAddressTextReferenceValue) {
          if (e.target.value === this.customerAddressTextReferenceValue) {
            this.Customerform.controls['address'].setErrors(null)
          }else{
            this.Customerform.controls['address'].setErrors({ isInvalidCustomerform: true })
          }
        }else{
          this.Customerform.controls['address'].setErrors({ isInvalidCustomerform: true })
        }
      }
    }
    this.Customerform.controls['address'].markAsTouched();
  }

  onKeyUpCustomerFormAddressNSA(e){
    let infocustomerNSA = JSON.parse(localStorage.getItem('infocustomerNSA'))
    if (infocustomerNSA) {
      console.log('nagrun ito A')
      if (e.target.value === infocustomerNSA.address) {
        this.CustomerNSAForm.controls['address'].setErrors(null)
      }else{
        console.log(e.target.value.length)
        if (e.target.value.length == 0) {
          this.CustomerNSAForm.controls['address'].setErrors({ required: true })
        }else{
          if (this.customerAddressTextNSAReferenceValue) {
            if (e.target.value === this.customerAddressTextNSAReferenceValue) {
              this.CustomerNSAForm.controls['address'].setErrors(null)
            }else{
              this.CustomerNSAForm.controls['address'].setErrors({ isInvalidCustomerNSAForm: true })
            }
          }else{
            this.CustomerNSAForm.controls['address'].setErrors({ isInvalidCustomerNSAForm: true })
          }
        }
      }
    }else{
      console.log('nagrun ito B')
      console.log(e.target.value);
      console.log(e.target.value.length)
      if (e.target.value.length == 0) {
        this.CustomerNSAForm.controls['address'].setErrors({ required: true })
      }else{
        if (this.customerAddressTextNSAReferenceValue) {
          if (e.target.value === this.customerAddressTextNSAReferenceValue) {
            this.CustomerNSAForm.controls['address'].setErrors(null)
          }else{
            this.CustomerNSAForm.controls['address'].setErrors({ isInvalidCustomerNSAForm: true })
          }
        }else{
          this.CustomerNSAForm.controls['address'].setErrors({ isInvalidCustomerNSAForm: true })
        }
      }
    }
    this.CustomerNSAForm.controls['address'].markAsTouched();
  }

  onKeyUpCustomerFormAddressDiff(e){
    let infocustomer = JSON.parse(localStorage.getItem('infocustomer'))
    if (infocustomer) {
      console.log('nagrun ito A')
      if (e.target.value === infocustomer.address) {
        this.CustomerDiff.controls['address'].setErrors(null)
      }else{
        console.log(e.target.value.length)
        if (e.target.value.length == 0) {
          this.CustomerDiff.controls['address'].setErrors({ required: true })
        }else{
          if (this.customerAddressTextDiffReferenceValue) {
            if (e.target.value === this.customerAddressTextDiffReferenceValue) {
              this.CustomerDiff.controls['address'].setErrors(null)
            }else{
              this.CustomerDiff.controls['address'].setErrors({ isInvalidCustomerDiff: true })
            }
          }else{
            this.CustomerDiff.controls['address'].setErrors({ isInvalidCustomerDiff: true })
          }
        }
      }
    }else{
      console.log('nagrun ito B')
      console.log(e.target.value);
      console.log(e.target.value.length)
      if (e.target.value.length == 0) {
        this.CustomerDiff.controls['address'].setErrors({ required: true })
      }else{
        if (this.customerAddressTextDiffReferenceValue) {
          if (e.target.value === this.customerAddressTextDiffReferenceValue) {
            this.CustomerDiff.controls['address'].setErrors(null)
          }else{
            this.CustomerDiff.controls['address'].setErrors({ isInvalidCustomerDiff: true })
          }
        }else{
          this.CustomerDiff.controls['address'].setErrors({ isInvalidCustomerDiff: true })
        }
      }
    }
    this.CustomerDiff.controls['address'].markAsTouched();
  }

  openAgmMap(value, mapDialogData){
    const dialogData = JSON.parse(JSON.stringify(mapDialogData));
    const dialogRef = this.dialog.open(AgmDialogComponent, {
      panelClass: 'app-full-bleed-dialog-full-screen',
      disableClose: false,
      data: { form_type: value, defaultCenter: dialogData }

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('resultFromDialogMap: ', result)
      if (result) {
        if (result.form_type == "Customerform") {
          this.Customerform.controls['address'].setValue(result.geocodeAddress);
          console.log('mapDialogDataCustomerform: ', this.mapDialogDataCustomerform)
        }
        if (result.form_type == "CustomerNSAForm") {
          this.CustomerNSAForm.controls['address'].setValue(result.geocodeAddress);
          console.log('mapDialogDataCustomerNSAForm: ', this.mapDialogDataCustomerNSAForm)

        }
        if (result.form_type == "CustomerDiff") {
          this.CustomerDiff.controls['address'].setValue(result.geocodeAddress);
          console.log('mapDialogDataCustomerDiff: ', this.mapDialogDataCustomerDiff)
        }
      }

    });
  }

  onRadioChangeCustomerNSA(e){
    if (e.checked) {
      console.log(e);
      this.getCustomerAddressTextNSAElRef().then(() =>
        this.getPlaceAutocompleteCustomerNSA()
      )

    }
  }

  onRadioChangeCustomerDiff(e){
    if (e.checked) {
      console.log(e);
      this.getCustomerAddressTextDiffElRef().then(() =>
        this.getPlaceAutocompleteCustomerDiff()
      )

    }
  }

  getCustomerAddressTextNSAElRef(){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
          console.log(this.customerAddressTextNSAElRef.nativeElement);
          resolve();
        }, 1000);
    });
  }

  getCustomerAddressTextDiffElRef(){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
          console.log(this.customerAddressTextDiffElRef.nativeElement);
          resolve();
        }, 1000);
    });
  }

  ngAfterViewInit() {
    if (!this.userService.hasCurrentUser()) {
      this.getPlaceAutocompleteCustomer();
    }
    if (this.optionRegisterNSA) {
      this.getCustomerAddressTextDiffElRef().then(() =>
        this.getPlaceAutocompleteCustomerDiff()
      )
    }
    if (this.optionRegisterGuestNSA) {
      this.getCustomerAddressTextNSAElRef().then(() =>
      this.getPlaceAutocompleteCustomerNSA()
      )
    }

  }

  getPlaceAutocompleteCustomer() {
      const autocomplete = new google.maps.places.Autocomplete(this.customerAddressText.nativeElement,
          {
              componentRestrictions: { country: 'PH' },
              // types: ['geocode']  // 'establishment' / 'address' / 'geocode'
          });
      // autocomplete.setFields(['address_components', 'geometry', 'formatted_address']);

      autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
          const place = autocomplete.getPlace();
          console.log('HAHAHAHAHAHAHAHAHAHAHA')
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            // window.alert("No details available for input: '" + place.name + "'");
            this.Customerform.controls['address'].setErrors({ isInvalidCustomerform: true })
            // return;
          }else{
            console.log('getPlaceAutocomplete: ', place)
            console.log('getPlaceAutocompleteLatLang: ', place.geometry.location.lat())
            console.log('getPlaceAutocompleteLatLang: ', place.geometry.location.lng())
            this.mapDialogDataCustomerform.lat = place.geometry.location.lat();
            this.mapDialogDataCustomerform.lng = place.geometry.location.lng();
            this.customerAddressTextReferenceValue = this.customerAddressText.nativeElement.value;
            console.log('log1: ', this.customerAddressText.nativeElement.value);
            this.zone.run( () => {
              this.Customerform.controls['address'].setErrors(null);
              this.Customerform.controls['address'].setValue(this.customerAddressText.nativeElement.value);
            });
            console.log('log2: ', this.Customerform.controls['address']);
            // this.invokeEvent(place);
          }
      });
  }

  getPlaceAutocompleteCustomerNSA() {
      const autocomplete = new google.maps.places.Autocomplete(this.customerAddressTextNSAElRef.nativeElement,
          {
              componentRestrictions: { country: 'PH' },
              // types: ['geocode']  // 'establishment' / 'address' / 'geocode'
          });
      // autocomplete.setFields(['address_components', 'geometry', 'formatted_address']);

      autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
          const place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            // window.alert("No details available for input: '" + place.name + "'");
            this.CustomerNSAForm.controls['address'].setErrors({ isInvalidCustomerNSAForm: true })
            // return;
          }else{
            console.log('getPlaceAutocomplete: ', place)
            console.log('getPlaceAutocompleteLatLang: ', place.geometry.location.lat())
            console.log('getPlaceAutocompleteLatLang: ', place.geometry.location.lng())
            this.mapDialogDataCustomerNSAForm.lat = place.geometry.location.lat();
            this.mapDialogDataCustomerNSAForm.lng = place.geometry.location.lng();
            this.customerAddressTextNSAReferenceValue = this.customerAddressTextNSAElRef.nativeElement.value;
            console.log('log1: ', this.customerAddressTextNSAElRef.nativeElement.value);
            this.zone.run( () => {
              this.CustomerNSAForm.controls['address'].setErrors(null);
              this.CustomerNSAForm.controls['address'].setValue(this.customerAddressTextNSAElRef.nativeElement.value);
            });
            console.log('log2: ', this.CustomerNSAForm.controls['address']);

          }
      });
  }

  getPlaceAutocompleteCustomerDiff() {
      const autocomplete = new google.maps.places.Autocomplete(this.customerAddressTextDiffElRef.nativeElement,
          {
              componentRestrictions: { country: 'PH' },
              // types: ['geocode']  // 'establishment' / 'address' / 'geocode'
          });
      // autocomplete.setFields(['address_components', 'geometry', 'formatted_address']);

      autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
          const place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            // window.alert("No details available for input: '" + place.name + "'");
            this.CustomerDiff.controls['address'].setErrors({ isInvalidCustomerDiff: true })
            // return;
          }else{
            // console.log('log: ', this.customerAddressTextDiff.nativeElement.value);
            console.log('getPlaceAutocomplete: ', place)
            console.log('getPlaceAutocompleteLatLang: ', place.geometry.location.lat())
            console.log('getPlaceAutocompleteLatLang: ', place.geometry.location.lng())
            this.mapDialogDataCustomerDiff.lat = place.geometry.location.lat();
            this.mapDialogDataCustomerDiff.lng = place.geometry.location.lng();
            this.customerAddressTextDiffReferenceValue = this.customerAddressTextDiffElRef.nativeElement.value;
            console.log('log1: ', this.customerAddressTextDiffElRef.nativeElement.value);
            this.zone.run( () => {
              this.CustomerDiff.controls['address'].setErrors(null);
              this.CustomerDiff.controls['address'].setValue(this.customerAddressTextDiffElRef.nativeElement.value);
            });
            console.log('log2: ', this.CustomerDiff.controls['address']);
          }
      });
  }

  invokeEvent(place: Object) {
      console.log('invokeEvent: ', place)
      this.setAddress.emit(place);
  }

}

// @Component({
//   selector: 'dialog-agm-map',
//   templateUrl: 'dialog-agm-map.html',
//   // styleUrls: 'dialog-agm-map.scss',
// })
// export class DialogAgmMap {
//   onSelect = new EventEmitter<{branch_name: string, lat: number, lng: number}>();
//
//   constructor(
//     public dialogRefAGM: MatDialogRef<DialogAgmMap>,
//     @Inject(MAT_DIALOG_DATA) public data: DialogData) {}
//     // ,@Inject(MAT_DIALOG_DATA) public xData: PurchaseProcComponent
//
//   onSelectClick(branch_name: string, lat: number, lng: number) {
//     // console.log(`onSelectClick: ${branch_name} | ${lat} | ${lng}`)
//     this.onSelect.emit({branch_name, lat, lng});
//   }
//
//   onNoClick(): void {
//     this.dialogRefAGM.close();
//     this.data.animal = "OKININAM"
//   }
//   clickedMarker(label: string, index: number, lat: number, lng: number) {
//     // console.log(`clicked the marker: ${label || index}`)
//     this.data.animal = label;
//     this.data.selectedBranch_name = label;
//     this.data.selectedBranch_lat = lat;
//     this.data.selectedBranch_lng = lng;
//     this.onSelectClick(label, lat, lng)
//     // this.xData.animal = label;
//   }
//
//   // mapClicked($event: MouseEvent) {
//   //   this.dialogData.markers.push({
//   //     lat: $event.coords.lat,
//   //     lng: $event.coords.lng,
//   //     draggable: true
//   //   });
//   // }
//
//   // markerDragEnd(m: marker, $event: MouseEvent) {
//   //   console.log('dragEnd', m, $event);
//   // }
// 	// GIFT WRAP COUNTER
// }

//uncomment if your going to use matcher validation on matInput
//usage of this class is to put [errorStateMatcher]="matcher" on <input "here">
// export class MyErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//     return !!(control && control.invalid && (control.dirty || control.touched));
//   }
// }
