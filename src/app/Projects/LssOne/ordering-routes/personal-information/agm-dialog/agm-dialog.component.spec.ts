import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgmDialogComponent } from './confirm-dialog.component';

describe('AgmDialogComponent', () => {
  let component: AgmDialogComponent;
  let fixture: ComponentFixture<AgmDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgmDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
