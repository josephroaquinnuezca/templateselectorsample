import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfirmOrderService } from '../../_client-services';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { UserService } from '../../user-routes/shared/user.service';
import { UserProvider } from '../../user-routes/shared/users.provider';
import * as jwt_decode from "jwt-decode";
import { ToastrService } from 'ngx-toastr'; //may 31, 2020
import { SuccessMesageDialogComponent } from './../../static-pages/success-mesage-dialog/success-mesage-dialog.component';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-confirm-order',
  templateUrl: './confirm-order.component.html',
  styleUrls: ['./confirm-order.component.scss']
})
export class ConfirmOrderComponent implements OnInit {

  signinForm: FormGroup;
  confirmOrderForm: FormGroup;
  submittedSigninForm: boolean = false;
  submittedConfirmOrderForm: boolean = false;
  // order_no: any = '4DP-000001';
  // email: any = 'erwin.vicarme@easycom-intl.com';

  submitted = false;
  show: boolean = false;

  default: boolean = false;

  needsVerification:boolean = false;

  constructor(private confirmOrderService: ConfirmOrderService,
    private toastr: ToastrService, // new constructor may 31, 2020
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
    public dialog: MatDialog,
    private userProvider: UserProvider,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.createFormSignIn();
    this.createFormConfirmOrder();
  }

  // 1st step
  get signinFormControl() {
    return this.signinForm.controls;
  }

    // 2nd step
 get orderFormControl() {
      return this.confirmOrderForm.controls;
    }


  ngAfterViewInit() {
    if (this.userService.hasCurrentUser()) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'))
      // if(currentUser.usertype_id == 1) {
        this.router.navigate(['transactions']);
      // }else{
      //   this.router.navigate(['./order-request']);
      // }
    }
  }

  createFormSignIn(){
    this.signinForm = this.formBuilder.group({
      username: [ '', [ Validators.required, Validators.email] ],
      password: [ '', Validators.required ]
    });
  }

  createFormConfirmOrder(){
    this.confirmOrderForm = this.formBuilder.group({
      email: [ '', [ Validators.required, Validators.email] ],
      order_id: [ '', Validators.required ]
    });
  }

  getOrderRecord(){
    this.submittedConfirmOrderForm = true;
    if(this.confirmOrderForm.invalid){
      for (let v in this.confirmOrderForm.controls) {
        this.confirmOrderForm.controls[v].markAsTouched();
      }
      this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: 'Incomplete information or invalid input provided!'
      });
      return;
    }
    let data = this.confirmOrderForm.getRawValue()
    const formData: FormData = new FormData();

    // formData.append('email', data.email);
    // formData.append('order_id', data.order_id);
    // console.log(data);
    // console.log(formData.get('order_id'));
    // return
    this.confirmOrderService.getOrderTransaction(data.order_id, data.email).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
      const dataResult: any = res;
      console.log('dataResult:', dataResult);
      // let body = {
      //   'defval': ordOpt.defval,
      //   'placeVal': selectedValue.delivery_name,
      //   'selTime': ordOpt.selTime,
      //   'selDate': ordOpt.selDate,
      //   'selBranch': ordOpt.selBranch,
      //   'checked': ordOpt.checked,
      //   'toNames': ordOpt.toNames,
      //   'giftwrap_remarks': ordOpt.giftwrap_remarks,
      //   'giftwrap_option': ordOpt.giftwrap_option,
      //   'giftwrap_status': ordOpt.checked,
      //   'to': ordOpt.to,
      //   'from': ordOpt.from,
      //   'message': ordOpt.message
      // }
      sessionStorage.setItem('orderQuickView', JSON.stringify(dataResult))

      this.router.navigate(['transaction-quick-view']);

    }, err => {
      if (err.error.status.description) {
        const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: err.error.status.description
        });
      }
      // console.log('HTTP Error:', err.error);
      // if(err.error.type == 'error'){
      //   console.log('HTTP Error Type:', err.error.type);
      // }
      // else{
      // }
    }, () => {
      this.submittedConfirmOrderForm = true;
      console.log('Done Fetching...');
    });
  }

  get g() { return this.confirmOrderForm.controls; }
  get f() { return this.signinForm.controls; }

  trimValue(formControl) {
    // console.log(formControl);
    formControl.setValue(formControl.value.trim().replace(/\s\s+/g, ' '));
  }
  onSubmit(){
    this.submittedSigninForm = true;
    if(this.signinForm.invalid){
      for (let v in this.signinForm.controls) {
        this.signinForm.controls[v].markAsTouched();
      }
      this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: 'Incomplete information or invalid input provided!'
      });
      return;
    }
    // console.log(this.signinForm.getRawValue());
    // return;
    this.userService.signin(this.signinForm.getRawValue()).subscribe(
      res =>{
        if(res.status.error){
          this.toastr.error('User not found or Password incorrect');
          // alert("User not found or Password incorrect")
          return
        }
        // console.log('onSubmit', res.status.error)
        // this.toastr.success('You are now succesfully login');
            setTimeout (() => {
              let url = this.router.url
         }, 5000);

        // if(url === '/user/admin'){
        //   let tokenData: any = this.tokenData(res.data.token)
        //   if(tokenData.user_info.usertype_id != 2){
        //     alert("Your'e not allowed to signin here")
        //     return
        //   }
        //   this.submitted = false;
        //   this.userService.setUser(res.data.token)
        //   this.router.navigate(['./admin']);
        // }else{
        //   this.submitted = false;
        //   this.userService.setUser(res.data.token)
        //   this.router.navigate(['']);
        // }
        let tokenData: any = this.tokenData(res.data.token)
        if(tokenData.user_info.usertype_id == 1){
          this.submittedSigninForm = false;
          // this.userService.setUser(res.data.token)
          // this.router.navigate(['./admin']);
          // alert('You will be redirected to admin login.')
          this.toastr.success('You will be redirected to admin login.');
          localStorage.removeItem('currentAdmin');
          localStorage.removeItem('lss-token');
          this.router.navigate(['/admin/login'])

        }else{
          this.submittedSigninForm = false;
					// console.log(this.userService.setUser(res.data.token));
          if(!this.userService.setUser(res.data.token)){
            this.needsVerification = true;
            this.router.navigate(['confirm-order']);
					}else{
						this.needsVerification = false;
            this.router.navigate(['transactions']);
            this.toastr.success('You are now succesfully login');
					}
        }
      }
    )
  }

  backToLogin(){
		this.needsVerification = false;
	}

  tokenData(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

  handleError(error) {
    console.log(error);
    return throwError(error);
  }
}
