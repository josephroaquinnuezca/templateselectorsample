import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { RouterModule } from  '@angular/router';
import { ConfirmOrderComponent } from './confirm-order/confirm-order.component';
import { PurchaseProcComponent } from './purchase-proc/purchase-proc.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { ItemDetailsComponent } from './item-details/item-details.component';
import { PersonalInformationComponent } from './personal-information/personal-information.component';

// STATIC PAGES
import { AboutUsComponent } from './../static-pages/about-us/about-us.component';
import { ContactUsComponent } from './../static-pages/contact-us/contact-us.component';
import { ClientGuideComponent } from './../static-pages/client-guide/client-guide.component';
import { PrivacyPolicyComponent } from './../static-pages/privacy-policy/privacy-policy.component';
import { RetexPolicyComponent } from './../static-pages/retex-policy/retex-policy.component';
import { TermsConditionComponent } from './../static-pages/terms-condition/terms-condition.component';

// USER IMPORTED
import { UsertypeGuard } from './../shared/usertype.guard';
import { UserSignupComponent } from './../user-routes/user-signup/user-signup.component';

import { ForgotPasswordResetComponent } from './../user-routes/forgot-password-reset/forgot-password-reset.component';
import { TransactionListComponent } from './../user-routes/transaction-list/transaction-list.component';
import { TransactioViewComponent } from './../user-routes/transactio-view/transactio-view.component';
import { TransactionQuickViewComponent } from './../user-routes/transaction-quick-view/transaction-quick-view.component';
import { UserInfoComponent } from './../user-routes/user-info/user-info.component';
import { SuccessMessageComponent } from './success-message/success-message.component';
// @NgModule({
//   declarations: [],
//   imports: [
//     CommonModule
//   ]
// })
const routes: Routes = [
    { path: 'confirm-order', component: ConfirmOrderComponent },
    { path: 'item-info/:item_slug', component: ItemDetailsComponent },
    { path: 'shopping-cart', component: ShoppingCartComponent }, //currently going this atask hard
    { path: 'success-message', component: SuccessMessageComponent }, //june 3 2020 /added
    { path: 'purchase-procedure', component: PurchaseProcComponent }, //canActivate: [UsertypeGuard]
    { path: 'aboutus', component: AboutUsComponent },
    { path: 'contactus', component: ContactUsComponent },
    { path: 'guides', component: ClientGuideComponent},
    { path: 'privacy-policy', component: PrivacyPolicyComponent },
    { path: 'return-exchange-policy', component: RetexPolicyComponent },
    { path: 'terms-condition', component: TermsConditionComponent },
    { path: 'personal-info', component: PersonalInformationComponent,
      children: [
        { path: 'information-update', component: UserSignupComponent }
    ]},
    //additionals but user route
    { path: 'reset-password/edit', component: ForgotPasswordResetComponent },
    { path: 'transactions', component: TransactionListComponent, canActivate: [UsertypeGuard]},  //canActivate: [UsertypeGuard]
    { path: 'transaction-view/:order_no', component: TransactioViewComponent, canActivate: [UsertypeGuard] },
    { path: 'transaction-quick-view', component: TransactionQuickViewComponent},
    { path: 'user-info', component: UserInfoComponent, canActivate: [UsertypeGuard],
      children: [
        { path: 'information-update', component: UserSignupComponent },
        { path: 'password-update', component: UserSignupComponent },
        { path: 'email-update', component: UserSignupComponent }
      ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class OrderingRoutesRoutingModule { }
