import { UserService } from './../../user-routes/shared/user.service';
import { SuccessMesageDialogComponent } from './../../static-pages/success-mesage-dialog/success-mesage-dialog.component';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';
// import { DialogData } from './../../_client-interface/agm-map';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { Cookie } from 'ng2-cookies';
import * as places from '../shared/cities.json'
import * as lbc from '../shared/lbc_branches.json'
import { OrderingService } from '../shared/ordering.service.js';
import { Router } from '@angular/router';
import { first, retry, catchError, startWith, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { TermsConditionComponent } from 'src/app/static-pages/terms-condition/terms-condition.component.js';
import { ErrorStateMatcher, MatDialog } from '@angular/material';
import { TermsDialogComponent } from '../terms-dialog/terms-dialog.component';
import { PaymentDialogComponent } from '../payment-dialog/payment-dialog.component';
//added august 2019 - AGM Dialog for LBC BRANCHES
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// import { DialogData, Marker } from '../../_client-interface';

import { DatePipe } from '@angular/common';

import { GiftwrapModel } from '../../_client-interface';
import { GiftwrapService } from '../../_client-services';
import { element } from 'protractor';
// import { _ } from 'underscore';
import * as _ from "underscore";
import { PaymentFormService, SettingService } from 'src/app/_admin-services';
import { CartcounterService, PaymentMethodService, MrSpeedyService, CourierReferenceService } from 'src/app/_client-services';


@Component({
  selector: 'app-purchase-proc',
  templateUrl: './purchase-proc.component.html',
  styleUrls: ['./purchase-proc.component.scss'],
})

export class PurchaseProcComponent implements OnInit {
	// giftwrap variable
	giftwrap_items: GiftwrapModel[] = [];
  myGiftwrapOption: any;
	checkSessionStorageDeliveryOpt: any;
	giftwrap: any[] = [];
	giftwrap_reference: any;
	toNames: any[] = [];
	giftwrap_fee: number;
  //uncomment if your going to use matcher validation on matInput
  // matcher = new MyErrorStateMatcher();

  isNewAddress = 2
  defval: string = 'deliver'
  placeVal: string = 'ncr'
  selTime: string = '9/25/2019'
  selDate: string = '9/25/2019'
  today = new Date();
  items: any[] = []
  currentUser: any
  selPayment = 0

  place = places.default
  lbc_branch = lbc.default
  lbc_name: string = lbc.default[0].branch_name
  filteredOptions: Observable<string[]>;
  myControl = new FormControl();

  mcit: any[] = []
  mreg: any[] = []
  total: number = 0
  ttlFee: number = 0

  saving: boolean = false

  noData: any;

  checked = false

  giftChecked: boolean = false
  to: string = ""
  from: string = ""
  message: string = ""

  cities: any[] = []

  //package shipping fee
  total_package_shippingfee: any = 0; // var for package template
  itemCount = 0;

  // june 8 2020 joseph nuezca //here
  infocustom: any;
  custominfo: any;
  // currenuserinfo: any[] = []
  // diffcustominfo: any[] = []

  lssfname: string;
  paymentMenthodOption: any;

  deliveryOpts: any;
  //JUNE 21, 2020
  // localStoreOne: boolean = false;
  // localStoreTwo: boolean = false;
  // end joseph nuezca


  showdata: boolean = false;

  mrSpeedyPayload: any;
  checkoutNotAllowed: boolean = false;
  shippingFee: Number = 0;

  constructor(
    private paymentMethodService: PaymentMethodService,
    private datePipe: DatePipe,
    private service: OrderingService,
    private formBuilder: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
    public userService: UserService,
    private paymentFormService: PaymentFormService,
    private giftwrapService: GiftwrapService,
    private settingService: SettingService,
    private cartCounterService: CartcounterService,
    private mrSpeedyService: MrSpeedyService,
    private courierReferenceService: CourierReferenceService) { }

  ngOnInit() {
    // this.scrollToTop();
    //june 8 2020  joseph nuezca
		this.getAllGiftwrap();
		this.checkSessionStorageDeliveryOpt = sessionStorage.getItem('deliveryOpt');
		// console.log(JSON.parse(this.checkSessionStorageDeliveryOpt.giftwrap_remarks[0].quantity));

    // console.log(this.lbc_branch);
    let data = localStorage.getItem('mCart')
    if (data && JSON.parse(data).length > 0) {
			this.getGiftwrapPrice()

      this.items = JSON.parse(data);

	    // setTimeout(() => {
	    //   this.getTotal();
			// }, 1000);
      this.getTotal();
    }else{
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Please add items to cart first!'
      });
      this.router.navigate(['']);
    }

    this.loadPaymentMethod();
    this.loadDeliveryOptions();
    this.loadCustomerInformation().then(() =>
      this.calculateOrderMrSpeedy()
    )
    let mCarts = JSON.parse(localStorage.getItem('mCart'));
    if (mCarts) {
      for(let mCart of mCarts){
        this.itemCount = this.itemCount + mCart.qty;
      }
    }
  }
  scrollToTop() {
    // (function smoothscroll() {
    //     var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
    //     if (currentScroll > 0) {
    //         window.requestAnimationFrame(smoothscroll);
    //         window.scrollTo(0, currentScroll - (currentScroll / 8));
    //     }
    // })();
    // window.scrollTo(0, 0);
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'auto' //smooth or auto
    });
  }
  selectPaymentOption(){
    console.log('Payment Method:', this.selPayment)
    let service = JSON.parse(localStorage.getItem('deliveryOpts'));
    console.log(service)
    service.payment_method = this.selPayment;
    // var myService: any = {
    //   payment_method: this.selPayment
    // };
    // if (service.service_method === 'delivery') {
    //   myService.service_method = service.service_method;
    // }else{
    //   myService.service_method = service.service_method;
    //   myService.pickup_date = service.pickup_date;
    //   myService.pickup_time = service.pickup_time;
    // }

    localStorage.removeItem('deliveryOpts');
    localStorage.setItem('deliveryOpts', JSON.stringify(service));
  }

  loadPaymentMethod(){
    this.paymentMethodService.getAll().subscribe( res => {
      const data:any = res;
      console.log(data);
      this.paymentMenthodOption = data.data.payload;
      console.log(this.paymentMenthodOption);
      // console.log('USER:', data.data.items[0])
      // this.infocustom = data.data.items[0];
    });
  }
  loadDeliveryOptions(){
    let service = JSON.parse(localStorage.getItem('deliveryOpts'));
    this.deliveryOpts = service;
  }
  async loadCustomerInformation(){
    // let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    // // let CustomerInfo =  localStorage.getItem('infocustomer')
    // this.currenuserinfo = currentUser;

    // let  datacustom = JSON.parse(localStorage.getItem('infocustomer'));
    // let  datacustomNSA = JSON.parse(localStorage.getItem('infocustomerNSA'));



    //  if(!datacustomNSA){
    //     this.noData = false;
    //     this.custominfo = datacustom;
    //     console.log('data:', datacustom)
    //   }else{
    //     this.noData = true;
    //     this.infocustom = datacustomNSA;
    //     this.custominfo = datacustom;
    //   }

    // let currentUserTest = JSON.parse(localStorage.getItem('infocustomer'));

    // if(!currentUserTest) {
    //   this.noData = false;
    //   const currentUser = JSON.parse(localStorage.getItem('currentUser'));

    //   this.userService.getUser(currentUser.id).subscribe( res => {
    //     const customer:any = res;
    //     console.log('USER:', customer.data.items[0])
    //     this.custominfo = customer.data.items[0];
    //   });
    // }else{

    //   this.noData = true;
    //   const currentUser = JSON.parse(localStorage.getItem('currentUser'));

    //   this.userService.getUser(currentUser.id).subscribe( res => {
    //     const customer:any = res;
    //     console.log('USER:', customer.data.items[0])
    //     this.custominfo = customer.data.items[0];
    //   });

    //   const  datacustom = JSON.parse(localStorage.getItem('infocustomer'));
    //   this.infocustom = datacustom;

    // }

    const data = JSON.parse(localStorage.getItem('infocustomer'));
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));

    const dataNsa = JSON.parse(localStorage.getItem('infocustomerNSA'));

    if (dataNsa) {
      this.noData =  dataNsa;
    }
    //

    console.log('data:', data)
    if (currentUser) {
      await this.userService.getUser(currentUser.id).toPromise().then( res => {
        const data:any = res;
        console.log('USER:', data.data.items[0])
        this.infocustom = data.data.items[0];
      });
      this.noData =  data;
    }else{
      this.infocustom = data;
      this.showdata = true;
    }

    // if (!data) {
      // const currentUser = JSON.parse(localStorage.getItem('currentUser'));


    // }else{
      // this.infocustom = data;
    // }
  }

  // june 20 2020
  backtocart(){
    this.router.navigate(['/shopping-cart']);
  }

  previousPage(){
    this.router.navigate(['/personal-info']);
  }

  getTotal() {
		// console.log(this.giftwrap);
		// console.log(this.giftwrap_reference);
		// console.log(this.giftChecked);
		// console.log(this.giftwrap_fee);
		if(this.giftChecked){
			var total_giftwrapCount = 0;
			for (let giftwrap of this.giftwrap){
					total_giftwrapCount += giftwrap.quantity;
			}
		}
		// console.log(total_giftwrapCount);
    this.total = this.giftChecked?total_giftwrapCount*this.giftwrap_fee:0
    this.ttlFee = 0
    for (let itm of this.items)
      // this.total += itm.qty * (itm.item.price + (this.defval === 'deliver' ? this.getFee(itm) : 0))
      this.total += (itm.qty * itm.item.price);
    for (let itm of this.items)
      // this.ttlFee += itm.qty * (this.defval === 'deliver' ? this.getFee(itm) : 0)
      this.ttlFee += itm.qty;

		// console.log(this.total);
    // if(this.defval == 'deliver'){
    //   this.calculateNewShipFee();
    // }else{
    this.total_package_shippingfee = 0;
    // }
  }

  confirmOrder() {
    //Payment validation
    if(this.selPayment == 0){
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Please select mode of payment!'
      });
      return
    }
    //Terms and condition validation
    if (!this.checked) {
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Please agree to our terms and condition!'
      });
      return
    }

    let service = JSON.parse(localStorage.getItem('deliveryOpts'));
    let customerInformation = JSON.parse(localStorage.getItem('infocustomer'));
    let customerInformationNSA = JSON.parse(localStorage.getItem('infocustomerNSA'));
    let user_id = '';
    let firstname = '';
    let middlename = '';
    let surename = '';
    let dob = '';
    let age = '';
    let email_address = '';
    let gender = '';
    let contact_no = '';
    let region = '';
    let province = '';
    let city = '';
    let barangay = '';
    let address = '';
    let remarks = '';

    let arr: any[] = []
    for (let itm of this.items)
      arr.push({
        "item_id": itm.item.id,
        "quantity": itm.qty
      })
    let order_item = arr;
    //IF GUEST CREATE/INSERT ACCOUNT
    if(this.userService.hasCurrentUser()) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      user_id = this.currentUser.id;
    }
    if(customerInformation){
      firstname = customerInformation.firstname;
      middlename = customerInformation.middlename;
      surename = customerInformation.surename;
      dob = customerInformation.dob;
      age = customerInformation.age;
      email_address = customerInformation.email_address;
      gender = customerInformation.gender;
      contact_no =  '+63' + customerInformation.contact_no;
      region = customerInformation.region;
      province = customerInformation.province;
      city = customerInformation.city;
      barangay = customerInformation.barangay;
      address = customerInformation.address;
      remarks = customerInformation.remarks;
    }
    let service_type = service.service_method === 'delivery' ? 1 : 2;
    // let mop = ['CEBUANA', 'LBC', 'PNB', 'BDO', 'BPI'];
    let modeofpayment = this.selPayment;
    let tracking_no = 0;
    var dateFormat = service.service_method === 'pickup' ? new Date(service.pickup_date) : '';
    let pickup_date = service.service_method === 'pickup' ? this.datePipe.transform(dateFormat, 'yyyy-MM-dd HH-mm-ss') : '';
    let pickup_time = service.service_method === 'pickup' ? service.pickup_time : ''; //`${this.selDate} ${this.selTime}`
    // let ship_fee = (service.service_method === 'delivery' && this.isNewAddress == 2) ? this.ttlFee : 0;

    let total_shipping_fee = this.total_package_shippingfee;
    let pickup_location = 'STORE';//this.defval === 'lbc' ? this.myControl.value.toUpperCase() : '';

    let courier = 1; //FUTURE TYPE OF COURRIER DEFAULT IS 1( Mr Speedy )
    let total_amt = this.total;
    let receiver_type = (customerInformation) ? (customerInformation.info_type === 'membership_nsa') ? 2 : 3 : 1
    let greeting_msg = "greeting";

		let giftwrap_status = '';
		// let giftwrap_option = '';
    // let gift_to = this.to;
    // let gift_from = this.from;
    // let gift_message = this.message;
    // let giftwrap_fee = 0;
		// let giftwrap_remarks = 0;
    var data: any = {
      guest_type: '',
      order_item: order_item,
      user_id: user_id,
      firstname: firstname,
      middlename: middlename,
      lastname: surename,
      dob: (dob == '' || !dob) ? 'None' : this.datePipe.transform(dob, 'yyyy-MM-dd HH-mm-ss'),
      age: (age == '0' || age == '' || !age) ? 'None' : age,
      email_address: email_address,
      gender: gender,
      contact_no: contact_no,
      region: region,
      province: province,
      city: city,
      barangay: barangay,
      address: address,
      remarks: remarks,
      service_type: service_type,
      modeofpayment: modeofpayment,
      tracking_no: tracking_no,
      dateFormat: dateFormat,
      pickup_date: pickup_date,
      pickup_time: pickup_time,
      total_shipping_fee: this.shippingFee ? this.shippingFee : total_shipping_fee,
      pickup_location: pickup_location,
      courier: courier,
      courier_details: (service.courier) ? service.courier : [],
      total_amt: total_amt,
      receiver_type: receiver_type,
      greeting_msg: greeting_msg,
      giftwrap_status: giftwrap_status
    }
    console.log('ORDER REQUEST DATA:', data)
    // return;
    // delete data.destination
    // console.log('SAVEORDER', data)
    if (receiver_type == 3) {
      data.guest_type = customerInformation.guest_type;
      data.receiver_kind = 'guest_default';
      data.receiver_type = 1;
      if (data.guest_type == 'membership') {
        data.password = customerInformation.password;
      }
      if (customerInformationNSA) {
        data.receiver_kind = customerInformationNSA.info_type;
        data.receiver_type = 2;
        data.firstnameNSA = customerInformationNSA.firstname
        data.middlenameNSA = customerInformationNSA.middlename
        data.lastnameNSA = customerInformationNSA.surename
        data.dobNSA = customerInformationNSA.dob
        data.ageNSA = customerInformationNSA.age
        data.email_addressNSA = customerInformationNSA.email_address
        data.genderNSA = customerInformationNSA.gender
        data.contact_noNSA =  '+63' + customerInformationNSA.contact_no
        data.regionNSA = customerInformationNSA.region
        data.provinceNSA = customerInformationNSA.province
        data.cityNSA = customerInformationNSA.city
        data.barangayNSA = customerInformationNSA.barangay
        data.addressNSA = customerInformationNSA.address
        data.remarksNSA = customerInformationNSA.remarks
      }
      console.log(data);
      // return;
      // this.service.saveOrderGuest(data).subscribe(
      //   res => {
      //     const data:any = res.data.items[0];
      //     // alert('Item waiting for approval');
      //     // localStorage.removeItem('mCart');
      //     // localStorage.removeItem('deliveryOpts');
      //     // localStorage.removeItem('infocustomer');
      //     this.cartCounterService.emitCartCount(0);
      //     let order_no = {
      //       order_no: data.data.items[0]
      //     }
      //     return
      //     localStorage.setItem('orderInfo', JSON.stringify(order_no));
      //     this.router.navigate(['./success-message']);;
      //   }
      // )
      this.service.saveOrderGuest(data).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
        const data: any = res;
        console.log('data:', data);

        this.cartCounterService.emitCartCount(0);
        let order_no = {
          order_no: data.data.items[0].order_no
        }
        localStorage.setItem('orderInfo', JSON.stringify(order_no));
        this.router.navigate(['./success-message']);
        // this.createReferenceBooking(data.data.items[0].order_no).then(
        //   res => {
        //     this.router.navigate(['./success-message']);
        //   },
        //   err => {
        //     this.dialog.open(ErrorMesageDialogComponent, {
        //       panelClass: 'app-full-bleed-dialog-p-26',
        //       disableClose: false,
        //       data: err.error.status.description
        //     });
        //   }
        // );
      }, err => {
        console.log('HTTP Error:', err.error);
        console.log(err.error.status.error);
        console.log('err.error:', err.error);
        this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: err.error.status.description
        });
        return;
      }, () => {
        console.log("request completed");
      });
    }else{
      this.service.saveOrder(data).subscribe(
        res => {
          const data:any = res;
          // alert('Item waiting for approval');
          // localStorage.removeItem('mCart');
          // localStorage.removeItem('deliveryOpts');
          // localStorage.removeItem('infocustomer');
          this.cartCounterService.emitCartCount(0);
          let order_no = {
            order_no: data.data.items[0].order_no
          }
          localStorage.setItem('orderInfo', JSON.stringify(order_no));
          this.router.navigate(['./success-message']);
          // this.createReferenceBooking(data.data.items[0].order_no).then(
          //   res => {
          //     this.router.navigate(['./success-message']);
          //   },
          //   err => {
          //     this.dialog.open(ErrorMesageDialogComponent, {
          //       panelClass: 'app-full-bleed-dialog-p-26',
          //       disableClose: false,
          //       data: err.error.status.description
          //     });
          //   }
          // );
        }
      )
    }

  }

  trimValue(formControl) {
    // console.log(formControl);
    formControl.setValue(formControl.value.trim().replace(/\s\s+/g, ' '));
  }

  viewTerm(){

    const dialogRef = this.dialog.open(TermsDialogComponent, {
      width: '80%', height: '80%',
      data: ''
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        // this.orderPagination(this.paginator);
      }
    });
  }

  viewPayment(p){
    const dialogRef = this.dialog.open(PaymentDialogComponent, {
      width: '90vw', height: '90%', minWidth: '90vw',
      data: {
        ind: p,
        total: this.total
      }
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        // this.orderPagination(this.paginator);
      }
    });
  }

  getGiftwrapPrice(){
		this.paymentFormService.getForm().subscribe(res => {
			let tmp: any = res
			console.log('PaymentFormComponent', tmp)
			var data = tmp.data.items[0]
			this.giftwrap_fee = data.giftwrap_fee;
			console.log(this.giftwrap_fee)
		})
  }

  getAllGiftwrap() {
    this.giftwrapService.getAllGiftwrap()
    .subscribe( data => {
      // this.search_loading = false;
      const d: any = data;
      if (!this.isObjectEmpty(d.data.items)) {
        this.giftwrap_items = d.data.items;
        console.log( this.giftwrap_items);
      } else {
        this.giftwrap_items = [];
      }
    });
  }

  onTypeValidate(formControl) {
    formControl.markAsTouched();
  }

  handleError(error) {
    console.log(error);
    return throwError(error);
  }

  isObjectEmpty (Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

  async createReferenceBooking(order_no: any){
    let data: any = {
      order_no: order_no,
      fee: this.shippingFee
    }
    this.courierReferenceService.createReference(data).pipe(retry(1),catchError(this.handleError)).subscribe( resData => {
      const res: any = resData;
      console.log('resData:', res)
    }, err => {
      this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Something Went Wrong! Please try again.'
      });
    }, () => {
      console.log("request completed");
    });
  }

  calculateOrderMrSpeedy(){
    let service = JSON.parse(localStorage.getItem('deliveryOpts'));
    let customerInformation = JSON.parse(localStorage.getItem('infocustomer'));
    let customerInformationNSA = JSON.parse(localStorage.getItem('infocustomerNSA'));
    let customer_address = ''
    let customer_contact_person = ''
    let customer_contact_no = ''
    let total_weight_kg = '20'
    let insurance_amount = (this.total + this.total_package_shippingfee)
    console.log('this.total:', this.total)
    insurance_amount.toString();
    if (service.service_method == 'delivery') {
      if(this.userService.hasCurrentUser()) {
        if (customerInformation) { //FOR LOGGED IN WITH NSA ADDRESS
          let data = this.generateShippingInfo(customerInformation);
          customer_address = data.address
          customer_contact_person = data.name
          customer_contact_no = data.contact_no
          console.log(data)
        }else{ // FOR LOGGED IN BUT NO NSA
          let data = this.generateShippingInfo(this.infocustom);
          customer_address = data.address
          customer_contact_person = data.name
          customer_contact_no = data.contact_no
          console.log(data)
        }
      }else{ //FOR GUEST
        if (customerInformationNSA) { // FOR GUEST WITH NSA ADDRESS
          let data = this.generateShippingInfo(customerInformationNSA);
          customer_address = data.address
          customer_contact_person = data.name
          customer_contact_no = data.contact_no
          console.log(data)
        }else{ // FOR GIEST WITH NO NSA
          let data = this.generateShippingInfo(customerInformation);
          customer_address = data.address
          customer_contact_person = data.name
          customer_contact_no = data.contact_no
          console.log(data)
        }
      }

      let mrSpeedyData: any = {
        total_weight_kg: total_weight_kg,
        insurance_amount: insurance_amount,
        customer_address: customer_address,
        customer_contact_person: customer_contact_person,
        customer_contact_no: customer_contact_no,
      }
      //
      this.mrSpeedyService.calculateOrder(mrSpeedyData).pipe(retry(1),catchError(this.handleError)).subscribe( resData => {
        const res: any = resData;
        this.mrSpeedyPayload = res.data;
        this.shippingFee = this.mrSpeedyPayload.order.payment_amount;
        console.log(res)
        mrSpeedyData.shipping_fee = this.mrSpeedyPayload.order.payment_amount;
        mrSpeedyData.courier_name = 'Mr Speedy'
        console.log(mrSpeedyData);

        service.courier = mrSpeedyData;
        localStorage.removeItem('deliveryOpts');
        localStorage.setItem('deliveryOpts', JSON.stringify(service));

        if (res.data.warnings.length) {
          this.dialog.open(ErrorMesageDialogComponent, {
            panelClass: 'app-full-bleed-dialog-p-26',
            disableClose: false,
            data: 'Please check your contact number or address may not be available.'
          });

          this.checkoutNotAllowed = true;
        }
      }, err => {
        this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: 'Something Went Wrong! Please try another delivery service.'
        });

        this.checkoutNotAllowed = true;
      }, () => {
        console.log("request completed");
      });
    }
  }

  generateShippingInfo(data){
    let name = data.firstname + ' ' + data.middlename + ' ' + data.surename;
    let address = data.address;
    let contact_no =  data.contact_no;

    let returnData: any = {
      name: name,
      address: address.replace(/(\r\n|\n|\r)/gm, " "),
      contact_no: contact_no
    }
    console.log('returnData: ', returnData);
    return returnData;
  }

}
