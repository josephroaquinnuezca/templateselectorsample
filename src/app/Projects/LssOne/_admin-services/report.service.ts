import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL, getOrderID } from '../shared/apis.service';

@Injectable({ providedIn: 'root' })
export class ReportService {

  constructor(private http: HttpClient) { }

  getProductCountByFilter(data: any) {
    return this.http.get(`${HTTP_API_URL}report-prod-count?${data}`);
  }
  getTransactionCountByFilter(data: any) {
    return this.http.get(`${HTTP_API_URL}report-transaction-count?${data}`);
  }
  getInventory(data: any) {
    return this.http.get(`${HTTP_API_URL}report-inventory-item?${data}`);
  }

}
