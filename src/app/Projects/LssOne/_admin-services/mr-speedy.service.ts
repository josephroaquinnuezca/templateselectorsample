import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL } from '../shared/apis.service';
import * as jwt_decode from "jwt-decode";
import { ActivatedRoute, Router} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class MrSpeedyService {

  constructor(private http: HttpClient, public router: Router, private route: ActivatedRoute) {}

	createBooking(body) {
    const formData: FormData = new FormData();

    formData.append('vehicle_type_id', '8'); //set in API: only declared here for testing
    formData.append('loaders_count', '0'); //set in API: only declared here for testing
    formData.append('matter', 'Food'); //set in API: only declared here for testing
    formData.append('total_weight_kg', body.total_weight_kg);
    formData.append('insurance_amount', body.insurance_amount);
    formData.append('store_is_order_payment_here', 'false'); //set in API: only declared here for testing
    formData.append('store_address', '104 Don Carlos Palanca, Legazpi Village, Makati, Kalakhang Maynila, Philippines'); //set in API: only declared here for testing
    formData.append('store_contact_person', 'Fatima Atog'); //set in API: only declared here for
    formData.append('store_contact_no', '9158616678'); //set in API: only declared here for testing
    formData.append('customer_is_order_payment_here', 'true'); //set in API: only declared here for testing
    formData.append('customer_address', body.customer_address);
    formData.append('customer_contact_person', body.customer_contact_person);
    formData.append('customer_contact_no', body.customer_contact_no);
    formData.append('payment_method', 'cash'); //set in API: only declared here for testing
    formData.append('is_motobox_required', 'true'); //set in API: only declared here for testing
    formData.append('has_file', 'true');

    return this.http.post(`${HTTP_API_URL}mr-speedy/`, formData);
  }
}
