import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL } from '../shared/apis.service';
import * as jwt_decode from "jwt-decode";
import { ActivatedRoute, Router} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class CourierReferenceService {

  constructor(private http: HttpClient, public router: Router, private route: ActivatedRoute) {}

  getAll(data: any) {
    return this.http.get(`${HTTP_API_URL}courier-reference?${data}`);
  }

  getSingle(id) {
    return this.http.get(`${HTTP_API_URL}courier-reference/${id}`);
  }
}
