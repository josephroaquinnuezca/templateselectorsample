import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL } from '../shared/apis.service';

@Injectable({
  providedIn: 'root'
})
export class TestimonyService {

  constructor(private http: HttpClient) { }

  getTestimonyByFilter(data: any) {
    return this.http.get(`${HTTP_API_URL}testimony?${data}`);
  }

  updateTestimonyReply(id: any, body: any){
    console.log(`FROM TESTIMONY SERVICE: ${id.id}`)
    const testimonyID = id.id
    return this.http.put(`${HTTP_API_URL}testimony?id=${testimonyID}`, body);
  }

  createTestimonyReply(data: any){
    console.log(`FROM TESTIMONY SERVICE: ${data}`)
    const formData: FormData = new FormData()

    formData.append('item_id', data.item_id+'')
    formData.append('user_id', data.user_id+'')
    formData.append('rate', data.rate+'')
    formData.append('message', data.message)
    formData.append('order_no', data.order_no)
    formData.append('type', data.type)

    let counter = 0

    formData.append('file_count', counter+'')
    formData.append('has_file',counter != 0 ? "true" : 'false')

    return this.http.post(`${HTTP_API_URL}testimony`, formData );

  }

}
