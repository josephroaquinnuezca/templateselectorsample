export * from './order.service';
export * from './users.service';
export * from './items.service';
export * from './item-categ.service';
export * from './user-type.service';
export * from './report.service';
export * from './setting.service';
export * from './returnEx-itemLost.service';
export * from './testimony.service';
export * from './payment-form.service';

// june 10, 2020
// export * from './payment-method.service';

// added on June 2020
export * from './payment-method.service';
export * from './order-format.service';
// >>>>>>> lss-dev-beta-06022020
//OCtober
export * from './courier-reference.service';
export * from './courier.service';
export * from './mr-speedy.service';
