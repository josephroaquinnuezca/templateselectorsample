import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL } from '../shared/apis.service';

@Injectable({ providedIn: 'root' })
export class ReturnExItemLost {

  constructor(private http: HttpClient) {}

  historyItemLost() {
    return this.http.get(`${HTTP_API_URL}lost-items/history`);
  }

  getAllItems() {
    return this.http.get(`${HTTP_API_URL}lost-items`);
  }

  additemLost(data: any, id: string) {
    return this.http.put(`${HTTP_API_URL}lost-items?item_id=${id}`, data);
  }

}
