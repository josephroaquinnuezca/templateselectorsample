import { ApisService } from './../shared/apis.service';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL } from '../shared/apis.service';

@Injectable({ providedIn: 'root' })
export class SettingService {

  constructor(private http: HttpClient, private service: ApisService) {}

  getAllGuide() {
    return this.http.get(`${HTTP_API_URL}setting/guide`);
  }

  deleteGuide(id: any) {
    return this.http.delete(`${HTTP_API_URL}setting/guide?id=${id}`);
  }

  getGuide(id: any) {
    return this.http.get(`${HTTP_API_URL}setting/guide?id=${id}`);
  }

  createGuide(data: any, files: Set<File>) {

		const formData: FormData = new FormData();
    formData.append('title', data.title);
    formData.append('description', data.description);
    formData.append('has_file', data.has_file);
    formData.append('file_type', data.file_type);

		let counter = 0;
		files.forEach(file => {
			counter ++;
			formData.append(`image_${counter}`, file);
		});

		data.file_count = counter;

		formData.append('file_count', data.file_count);

    return this.http.post(`${HTTP_API_URL}setting/guide`, formData);
  }

  updateGuide(data: any, files: Set<File>, id: string) {
		const formData: FormData = new FormData();
    formData.append('title', data.title);
    formData.append('description', data.description);
    formData.append('has_file', data.has_file);
    formData.append('file_type', data.file_type);
    formData.append('new_id_image', data.new_id_image);

		let counter = 0;
		files.forEach(file => {
			counter ++;
			formData.append(`image_${counter}`, file);
		});

		data.file_count = counter;

		formData.append('file_count', data.file_count);

    return this.http.put(`${HTTP_API_URL}setting/guide?id=${id}`, formData);
  }

  //
  uploadFiles(body, description, files: Set<File>) {

    const formData: FormData = new FormData();
    formData.append('has_file', body.has_file);
    formData.append('file_type', body.file_type);

    let counter = 0;
    files.forEach(file => {
      counter ++;
      formData.append(`image_${counter}`, file);
    });

    let x = 0;
    description.forEach(d => {
      x ++;
      formData.append(`description_${counter}`, d);
    });

    body.file_count = counter;
    formData.append('file_count', body.file_count);
    return this.http.post(`${HTTP_API_URL}setting/image-upload`, formData );

  }

  getUploadedFiles(homePage) {
    return this.service.get(`${HTTP_API_URL}setting/image-upload?file_type=${homePage}`);
  }

  deleteImage(id) {
    return this.http.delete(`${HTTP_API_URL}setting/image-upload?id=${id}`);
  }

  updateImageDesc(id, data) {
    return this.http.put(`${HTTP_API_URL}setting/image-upload?id=${id}`, data);
  }

	// gift wrap settings
	getAllGiftwrap() {
		return this.http.get(`${HTTP_API_URL}setting/giftwrap`);
	}

	deleteGiftwrap(id: any) {
		return this.http.delete(`${HTTP_API_URL}setting/giftwrap?id=${id}`);
	}

	getGiftwrap(id: any) {
		return this.http.get(`${HTTP_API_URL}setting/giftwrap?id=${id}`);
	}

	createGiftwrap(data: any, files: Set<File>) {

		const formData: FormData = new FormData();
		formData.append('name', data.name);
		formData.append('description', data.description);
		formData.append('has_file', data.has_file);
		formData.append('file_type', data.file_type);

		let counter = 0;
		files.forEach(file => {
			counter ++;
			formData.append(`image_${counter}`, file);
		});

		data.file_count = counter;

		formData.append('file_count', data.file_count);

		return this.http.post(`${HTTP_API_URL}setting/giftwrap`, formData);
	}

	updateGiftwrap(data: any, files: Set<File>, id: string) {
		const formData: FormData = new FormData();
		formData.append('name', data.name);
		formData.append('description', data.description);
		formData.append('has_file', data.has_file);
		formData.append('file_type', data.file_type);
		formData.append('new_id_image', data.new_id_image);

		let counter = 0;
		files.forEach(file => {
			counter ++;
			formData.append(`image_${counter}`, file);
		});

		data.file_count = counter;

		formData.append('file_count', data.file_count);

		return this.http.put(`${HTTP_API_URL}setting/giftwrap?id=${id}`, formData);
	}
  // package settings
  getAllItems() {
    return this.http.get(`${HTTP_API_URL}item/all`);
  }
  getpackageTemplate() {
    return this.http.get(`${HTTP_API_URL}setting/package-template`);
  }
  createPackageTemplate(data: any) {
    return this.http.post(`${HTTP_API_URL}setting/package-template`, data);
  }

  generatePackage(data: any) {
    return this.http.post(`${HTTP_API_URL}setting/package-template/check-max`, data);
  }
}
