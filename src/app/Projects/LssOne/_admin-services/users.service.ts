import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL } from '../shared/apis.service';
import * as jwt_decode from "jwt-decode";
import { ActivatedRoute, Router} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class UsersService {

	isSignedIn = false;
  currentUser: any;

  constructor(private http: HttpClient, public router: Router, private route: ActivatedRoute) {}

	signin(body: any) {
		return this.http.post(`${HTTP_API_URL}admin/login`, body);
  }
	setAdmin(token: any) {
    let tokenData: any = this.tokenData(token);
    console.log('TOKEN', tokenData);
    let body = {
      id: tokenData.user_info.id,
      username: tokenData.user_info.username,
      email_address: tokenData.user_info.email_address,
      contact_no: tokenData.user_info.contact_no,
      img_profile: tokenData.user_info.img_profile,
      firstname: tokenData.user_info.firstname,
      middlename: tokenData.user_info.middlename,
      surename: tokenData.user_info.surename,
      usertype_id: tokenData.user_info.usertype_id,
      // destination: tokenData.user_info.destination
    }
    this.currentUser = body
    //remove current session
    localStorage.removeItem('currentUser');
    localStorage.removeItem('lss-token');
    //replace the current session with this.
    localStorage.setItem('currentAdmin', JSON.stringify(body))
    localStorage.setItem('lss-token', token)
  }

	tokenData(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

  getAll(data: any) {
    return this.http.get(`${HTTP_API_URL}user?${data}`);
  }
	getAllAccounts(data: any) {
    return this.http.get(`${HTTP_API_URL}accounts?${data}`);
  }
  get(id) {
    return this.http.get(`${HTTP_API_URL}user/${id}`);
  }

  delete(id: any) {
    return this.http.delete(`${HTTP_API_URL}user?id=${id}`);
  }

  update(body, files: any, id) {

		const formData: FormData = new FormData();

    formData.append('usertype_id', body.usertype_id);
    formData.append('username', body.email_address);
    formData.append('email_address', body.email_address);
    // formData.append('password', body.password);
    formData.append('firstname', body.firstname);
    formData.append('middlename', body.middlename);
    formData.append('surename', body.surename);
    formData.append('dob', body.dob);
    formData.append('contact_no', body.contact_no);
    formData.append('age', body.age);
    formData.append('gender', body.gender);
    formData.append('has_file', "false");
    formData.append('file_count', '0');
    if(files){
      formData.append('has_file', "true")
      formData.append('file_count', '1')
      formData.append('img_profile', files)
    }
		// if(body.usertype_id == '2'){
			formData.append('region', body.region);
			formData.append('province', body.province);
			formData.append('city', body.city);
			formData.append('barangay', body.barangay);
	    formData.append('address', body.address);
	    formData.append('remarks', body.remarks);
		// }

    // const formData: FormData = new FormData();
		//
    // formData.append('usertype_id', body.usertype_id);
    // formData.append('username', body.username);
    // formData.append('email_address', body.email_address);
    // // formData.append('password', body.password);
    // formData.append('firstname', body.firstname);
    // formData.append('middlename', body.middlename);
    // formData.append('surename', body.surename);
    // formData.append('contact_no', body.contact_no);
    // formData.append('age', body.age);
    // formData.append('gender', body.gender);
    // formData.append('address', body.address);
    // formData.append('has_file', body.has_file);
		//
    // let counter = 0;
    // files.forEach(file => {
    //   counter ++;
    //   formData.append(`img_profile`, file);
    // });
		//
    // body.file_count = counter;
		//
    // formData.append('file_count', body.file_count);

    return this.http.put(`${HTTP_API_URL}user?id=${id}`, formData );

  }
	changePasswordSingle(data){
		  return this.http.put(`${HTTP_API_URL}user/change-user-password`, data );
	}
	changeEmailSingle(data){
		  return this.http.put(`${HTTP_API_URL}user/change-user-email`, data );
	}
  create(body, files: any)  {

		const formData: FormData = new FormData();

    formData.append('usertype_id', body.usertype_id);
    formData.append('username', body.email_address);
    formData.append('email_address', body.email_address);
    formData.append('dob', body.dob);
    formData.append('password', body.password);
    formData.append('firstname', body.firstname);
    formData.append('middlename', body.middlename);
    formData.append('surename', body.surename);
    formData.append('contact_no', body.contact_no);
    formData.append('has_file', body.has_file);
    formData.append('file_count', '1');
    formData.append('img_profile', files);
    formData.append('age', body.age);
    formData.append('gender', body.gender);
		formData.append('region', body.region);
		formData.append('province', body.province);
		formData.append('city', body.city);
		formData.append('barangay', body.barangay);
    formData.append('address', body.address);
    formData.append('remarks', body.remarks);

    // formData.append('usertype_id', body.usertype_id);
    // formData.append('username', body.username);
    // formData.append('email_address', body.email_address);
    // formData.append('password', body.password);
    // formData.append('firstname', body.firstname);
    // formData.append('middlename', body.middlename);
    // formData.append('surename', body.surename);
    // formData.append('contact_no', body.contact_no);
    // formData.append('age', body.age);
    // formData.append('gender', body.gender);
    // formData.append('address', body.address);
    // formData.append('has_file', body.has_file);
		//
    // let counter = 0;
    // files.forEach(file => {
    //   counter ++;
    //   formData.append(`img_profile`, file);
    // });
		//
    // body.file_count = counter;
		//
    // formData.append('file_count', body.file_count);

    return this.http.post(`${HTTP_API_URL}user`, formData );

  }

  forgotPassword(email_address: string) {
    return this.http.post(`${HTTP_API_URL}user/reset-password`, {email_address : email_address });
  }

  passwordVerify(token: string) {
    return this.http.get(`${HTTP_API_URL}user/reset-password-verify/${token}`);
  }

  passwordReset(data: any) {
    return this.http.put(`${HTTP_API_URL}user/reset-password`, data);
  }
}
