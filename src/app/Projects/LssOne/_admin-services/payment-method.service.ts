import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL } from '../shared/apis.service';
import * as jwt_decode from "jwt-decode";
import { ActivatedRoute, Router} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class PaymentMethodService {

	isSignedIn = false;
  currentUser: any;

  constructor(private http: HttpClient, public router: Router, private route: ActivatedRoute) {}

  getAll(data: any) {
    return this.http.get(`${HTTP_API_URL}payment-method?page=1&limit=1000`);
  }

  getSingle(id) {
    return this.http.get(`${HTTP_API_URL}payment-method/${id}`);
  }

  togglePaymentMethod(id: any) {
		let data = {}
    return this.http.put(`${HTTP_API_URL}payment-method/toggle?id=${id}`, data);
  }

  update(body, files: any, id) {

		const formData: FormData = new FormData();

    formData.append('name', body.name);
    formData.append('account_number', body.account_number);
    formData.append('has_file', "false");
    formData.append('file_count', '0');
    if(files){
      formData.append('has_file', "true")
      formData.append('file_count', '1')
      formData.append('img_pm', files)
    }

    return this.http.put(`${HTTP_API_URL}payment-method?id=${id}`, formData );

  }
  create(body, files: any)  {

		const formData: FormData = new FormData();

    formData.append('name', body.name);
    formData.append('account_number', body.account_number);
    formData.append('has_file', body.has_file);
    // formData.append('file_count', '1');
    formData.append('img_pm', files);

    return this.http.post(`${HTTP_API_URL}payment-method`, formData );

  }
}
