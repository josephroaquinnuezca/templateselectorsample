import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL } from '../shared/apis.service';
import * as jwt_decode from "jwt-decode";
import { ActivatedRoute, Router} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class OrderFormatService {

	isSignedIn = false;
  currentUser: any;

  constructor(private http: HttpClient, public router: Router, private route: ActivatedRoute) {}

  getAll() {
    return this.http.get(`${HTTP_API_URL}order-format`);
  }


  update(body, id) {

		const formData: FormData = new FormData();

    formData.append('order_id', body.order_id);
    formData.append('order_year', body.order_year);
    formData.append('has_file', "true");

    return this.http.put(`${HTTP_API_URL}order-format/${id}`, formData );

  }
  create(body)  {

		const formData: FormData = new FormData();

    formData.append('order_id', body.order_id);
    formData.append('order_year', body.order_year);
    formData.append('has_file', "true");

    return this.http.post(`${HTTP_API_URL}order-format`, formData );

  }
	delete(id: any) {
    return this.http.delete(`${HTTP_API_URL}order-format/${id}`);
  }
}
