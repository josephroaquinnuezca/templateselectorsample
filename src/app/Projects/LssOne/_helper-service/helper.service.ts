import { Injectable, Inject } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
   constructor(private http: HttpClient) {}

	 public findInvalidControls(control) {
	 		const invalid = [];
 	 		const invalidValue = [];
 	 		// const errorValue = [];
	 		const controls = control;
	 		for (const name in controls) {
	 				if (controls[name].invalid) {
							// errorValue.push(controls[name].errors)
	 						invalid.push(name);
	 						invalidValue.push(controls[name].value);
	 				}
	 		}
	 		console.log(invalid);
	 		console.log(invalidValue);
	 		// console.log(errorValue);
			// return invalid;
	 }

}
