import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'uppercaseRoman'
})
export class UppercaseRomanPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    // var capitalized = string.replace(/\w\S*/g, function(match){
    //   return match.charAt(0).toUpperCase() + match.substr(1).toLowerCase();
    // });
    if (!value) return value;
    // var string = value;
    var string = value;
    // console.log('value: ', string)
    // regex = /\b(?![LXIVCDM]+\b)([A-Z]+)\b/g;
    let regex = /\b(?![LXIVCDMNCRAB]+\b)([A-Za-z]+)\b/g;

    let modified = string.replace(regex, function(match) {
      // return match.toLowerCase();
      // return match
      // console.log('match: ', match)
      return match.replace(/\w\S*/g, (word) => {
          word = word.toLowerCase();
          // console.log('Per word: ', word)
          // console.log('Word length: ', word.length)
          return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
          // word = word.split(' ');
          // for (var i = 0; i < word.length; i++) {
          //   console.log('word[ ' + i + ']: ', word[i]);
          //   word[i] = word[i].charAt(0).toUpperCase() + word[i].substr(1).toLowerCase();
          // }
          // return word.join(' ');
        }
      );
    });
    console.log(modified);
    return modified;
    // return modified.charAt(0).toUpperCase() + modified.substr(1);
  }

}
