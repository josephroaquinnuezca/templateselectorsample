import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UppercaseRomanPipe } from './uppercase-roman.pipe';


@NgModule({
  declarations: [
    UppercaseRomanPipe
  ],
  exports: [
    UppercaseRomanPipe
  ],
  imports: [
    CommonModule
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})

export class CustomPipesModule { }
