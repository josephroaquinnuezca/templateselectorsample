import { TokenInterceptor } from './shared/token.interceptor';
import { NgxGalleryModule } from 'ngx-gallery';
import { AgmCoreModule } from '@agm/core';
import { ToastrModule } from 'ngx-toastr';
import { DigitOnlyModule } from '@uiowa/digit-only';
import { NgxBarcodeModule } from 'ngx-barcode';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MomentModule } from 'ngx-moment';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { TemplateOneRoutingModule } from './template-one-routing.module';
import { HeaderPageComponent } from './ordering-routes/header-page/header-page.component';
import { LandingPageComponent } from './ordering-routes/landing-page/landing-page.component';
import { ItemNoResultComponent } from './ordering-routes/header-page/modals/no-result/no-result.component';
import { NoResultComponent } from './admin-routes/items/lists/modals/no-result/no-result.component';
import { PrintOrComponent } from './admin-routes/print-or/print-or.component';
import { TermsDialogComponent } from './ordering-routes/terms-dialog/terms-dialog.component';
import { PaymentDialogComponent } from './ordering-routes/payment-dialog/payment-dialog.component';
import { TestimonyDialogComponent } from './user-routes/testimony-dialog/testimony-dialog.component';
import { ShippingprintBulkComponent } from './admin-routes/shippingprint_bulk/shippingprint-bulk.component';
import { RemoveDeleteDialogComponent } from './user-routes/user-info/remove-delete-dialog/remove-delete-dialog.component';
import { PackageDialogComponent } from './user-routes/transactio-view/package-dialog/package-dialog.component';
import { SuccessMesageDialogComponent } from './static-pages/success-mesage-dialog/success-mesage-dialog.component';
import { ErrorMesageDialogComponent } from './static-pages/error-mesage-dialog/error-mesage-dialog.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialsModule } from './shared/materials.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OrderID } from './ordering-routes/shared/ordering.prov';
import { UserProvider } from './user-routes/shared/users.provider';
import { OrderPrintProvider } from './_admin-provider';
import { ResetPasswordPrv } from './user-routes/shared/reserpassword.provider';
import { AppComponent } from 'src/app/app.component';
import { NgHttpLoaderModule } from 'ng-http-loader/lib/ng-http-loader.module';


@NgModule({
  declarations: [
    AppComponent,
    HeaderPageComponent,
    LandingPageComponent,
    ItemNoResultComponent,
    NoResultComponent,
    // AccountsAdminFormComponent,
    // AccountsCustomerFormComponent,
    // AccountsGuestFormComponent,
    // ReturnExchangeDialogComponent,
    // SettingImgModalComponent,
    // SettingImgModalAddComponent,
    // DialogReply,
		// DialogGiftwrap,
    // ItemModalComponent,
    // GuideFormComponent,
    // PaymentAddDialogComponent,
    // PaymentEditDialogComponent,
    // PackageSettingDialogComponent,
    // ItemCategFormComponent,
    // ItemsFormComponent,
    // UserFormComponent,
    // OrderDialogComponent,
    // PaymentMethodFormComponent,
    PrintOrComponent,
    TermsDialogComponent,
    PaymentDialogComponent,
    TestimonyDialogComponent,
    // DialogAgmMap,
    ShippingprintBulkComponent,
    RemoveDeleteDialogComponent,
    PackageDialogComponent,
    SuccessMesageDialogComponent,
    ErrorMesageDialogComponent
  ],
  imports: [
    CommonModule,
    TemplateOneRoutingModule,
    // AppRoutingModule, //exiteng
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxChartsModule,
    NgxMaterialTimepickerModule,
    MomentModule,
    FlexLayoutModule,
    SlickCarouselModule,
		NgxBarcodeModule,
    DigitOnlyModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-center',
      preventDuplicates: true,
      closeButton:true,
      progressBar:true,

    }), // ToastrModule added may 31, 2020
    NgHttpLoaderModule.forRoot(),
    // MDBBootstrapModule.forRoot(),
		// SweetAlert2Module.forRoot(),
    // free api - AIzaSyAvcDy5ZYc2ujCS6TTtI3RYX5QmuoV8Ffw ( not secured )
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDEoNWWHYCtUfBIiqeYW0K1sw63JK11tEE'
    }),

    NgxGalleryModule,

    // NgbModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    OrderID,
    UserProvider,
    OrderPrintProvider,
    ResetPasswordPrv,
    DatePipe
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    PaymentDialogComponent,
    TermsDialogComponent,
    NoResultComponent,
    ItemNoResultComponent,
    // June 2020 -erwin
    // AccountsAdminFormComponent,
    // AccountsCustomerFormComponent,
    // AccountsGuestFormComponent,
    // ReturnExchangeDialogComponent,
    // SettingImgModalComponent,
    // SettingImgModalAddComponent,
    // DialogReply,
		// DialogGiftwrap,
    // ItemModalComponent,
    // GuideFormComponent,
    // PaymentAddDialogComponent,
    // PaymentEditDialogComponent,
    // PackageSettingDialogComponent,
    // ItemCategFormComponent,
    // ItemsFormComponent,
    // UserFormComponent,
    // OrderDialogComponent,
    // PaymentMethodFormComponent,
    // /END
    TestimonyDialogComponent,
    // DialogAgmMap,
    RemoveDeleteDialogComponent,
    PackageDialogComponent,
    //new only today
    ErrorMesageDialogComponent,
    SuccessMesageDialogComponent
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class TemplateOneModule { }
