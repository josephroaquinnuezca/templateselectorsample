export interface GiftwrapModel {
    id: number;
    name: string;
    description: string;
    status: number;
    images: string;
    created_date: string;
    updated_date: string;
}
