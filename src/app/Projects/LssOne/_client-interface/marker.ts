export interface Marker {
  branch_name: string;
  branch_address?: string;
  branch_contactno?: number;
  branch_latitude: number;
  branch_longitude: number;
}
