import { Marker } from './index';

export interface DialogData {
  animal: string;
  // google maps zoom level
  zoom: number;
  // initial center position for the map
  // selectedBranch_lat: number;
  // selectedBranch_lng: number;
  // selectedBranch_name: String;
  // oldSelectedBranch_lat: number;
  // oldSelectedBranch_lng: number;
  // oldSelectedBranch_name: String;
  markers: Marker[]
}
