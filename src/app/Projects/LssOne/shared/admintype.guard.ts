import { id } from '@swimlane/ngx-charts/release/utils';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { ErrorMesageDialogComponent } from '../static-pages/error-mesage-dialog/error-mesage-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class UsertypeGuard implements CanActivate {

  // canActivateChild(
  //   next: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  //
  //   let currentAdmin = JSON.parse(localStorage.getItem('currentAdmin'))
  //   if (currentAdmin != null) {
  //     if (currentAdmin.usertype_id != 1) {
  //       alert("You can't access this page")
  //       this.router.navigate([''])
  //     }
  //   }
  //
  //   return this.canActivateAdmin(next, state);
  // }

  constructor(public router: Router, public dialog: MatDialog) { }

  // canActivateAdmin( next: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  //
  //   let currentAdmin = JSON.parse(localStorage.getItem('currentAdmin'))
  //   // console.log('canActivate', next.routeConfig.path)
  //   if (currentAdmin != null) {
  //     return true
  //   }else{
  //     alert('Please signin first')
  //     this.router.navigate(['/admin/login'])
  //     return false
  //   }
  // }

  canActivate( next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    let currentAdmin = JSON.parse(localStorage.getItem('currentAdmin'))
    if(currentAdmin.user_type == 1){
      return true
    }else{
      // alert('You do not have administrative privelage to view this section.') //
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        height: '250px',
        width: '650px',
        data: 'You do not have administrative privelage to view this section.'
    });
      this.router.navigate(['/admin'])
      return false
    }
  }
}
