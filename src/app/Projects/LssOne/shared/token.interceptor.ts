import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse,
  HttpErrorResponse,
  HttpInterceptor
} from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';
import { UserService } from 'src/app/user-routes/shared/user.service';



@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private router: Router, private dialogRef: MatDialog, private userService: UserService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        Authorization: '' + localStorage.getItem('lss-token')
      }
    });

    console.log('INTERCEPT!')
    console.log('INTERCEPT!: ', localStorage.getItem('lss-token'))

    return next.handle(request).pipe(catchError((error, caught) => {
      console.log('err: ', error);
      console.log('caught: ', caught);
      if (error instanceof HttpErrorResponse) {
        console.log(error.status)
        if (error.status == 403) {
          const dialogRef = this.dialogRef.open(ErrorMesageDialogComponent, {
              panelClass: 'app-full-bleed-dialog-p-26',
              disableClose: false,
              data: 'Your session is expired. Please signin again.'
          });
          dialogRef.afterClosed().subscribe(result => {
            if (localStorage.getItem('currentUser')) {
              this.dialogRef.closeAll();
              this.userService.signout();
              this.router.navigate(['/user/signin']);
            }
            if (localStorage.getItem('currentAdmin')) {
              this.dialogRef.closeAll();
              this.userService.signoutAdmin();
              this.router.navigate(['/admin/login']);
            }
          });
        }

      }
      return Observable.throwError(error);
    })) as any;
  }
}
