import { id } from '@swimlane/ngx-charts/release/utils';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class UsertypeGuard implements CanActivate {

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    let currentAdmin = JSON.parse(localStorage.getItem('currentAdmin'))
    if (currentAdmin != null) {
      if (currentAdmin.usertype_id == 2) {
        alert("You can't access this page");
        this.router.navigate(['./user/signin']);
				if(currentAdmin){
		      setTimeout(() => {
						localStorage.removeItem('currentAdmin');
						localStorage.removeItem('lss-token');
		      });
				}
      }else{
				return this.canActivateAdmin(next, state);
			}
    }else{
			return this.canActivateAdmin(next, state);
		}
  }

  constructor(public router: Router,
              private toastr: ToastrService, // new constructor may 31, 2020
    ) { }

  canActivateAdmin( next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    let currentAdmin = JSON.parse(localStorage.getItem('currentAdmin'))
    // console.log('canActivate', next.routeConfig.path)
    if (currentAdmin != null) {
      return true
    }else{
			let currentUser = JSON.parse(localStorage.getItem('currentUser'))
			if(currentUser){
        // alert("You can't access this page"); //
        this.toastr.error("You can't access this page");
				this.router.navigate(['./user/signin']);
	      return false
			}else{
        this.toastr.error('Please signin first');
        // alert('Please signin first')

	      this.router.navigate(['/admin/login'])
	      return false
			}
    }
  }

  canActivate( next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    let currentUser = JSON.parse(localStorage.getItem('currentUser'))
    // let currentAdmin = JSON.parse(localStorage.getItem('currentAdmin'))
    // console.log('canActivate', next.routeConfig.path)
    if (currentUser != null) { // || currentAdmin != null
      if(currentUser){
        if(next.routeConfig.path === 'purchase-procedure'){
          let items = localStorage.getItem('mCart')
          if(items){
            if(JSON.parse(items).length == 0){
              alert('You have no items yet to purchase')
							sessionStorage.removeItem('deliveryOpt')
              this.router.navigate([''])
              return false
            }else{
              return true
            }
          }else{
            return false
          }
        }else{
          return true
        }
      }
      // else{
      //   alert('You will be redirected to admin login.')
      //   localStorage.removeItem('currentAdmin');
      //   localStorage.removeItem('lss-token');
      //   this.router.navigate(['/admin/login'])
      // }

    }else{
      this.toastr.error('Please signin first');
      // alert('Please signin first')
      this.router.navigate(['/user/signin'])
      return false
    }
  }
}
