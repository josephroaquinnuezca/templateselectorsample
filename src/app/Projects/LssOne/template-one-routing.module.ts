import { LandingPageComponent } from './ordering-routes/landing-page/landing-page.component';
import { ShippingprintBulkComponent } from './admin-routes/shippingprint_bulk/shippingprint-bulk.component';
import { PrintOrComponent } from './admin-routes/print-or/print-or.component';
import { HeaderPageComponent } from './ordering-routes/header-page/header-page.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: HeaderPageComponent,
    children: [
      // { path: '', redirectTo: '..', pathMatch: 'full' },
         { path: '', component: LandingPageComponent },
      // { path: '..', component: LandingPageComponent },
        { path: '', loadChildren: () => import('../LssOne/ordering-routes/ordering-routes.module').then(m => m.OrderingRoutesModule) },
        { path: 'user', loadChildren: () => import('../LssOne/user-routes/user-routes.module').then(m => m.UserRoutesModule) }
    ]
  },
  { path: 'admin', loadChildren: () => import('../LssOne/admin-routes/admin-routes.module').then(m => m.AdminRoutesModule) },
  { path: 'print-or/:ord_no', component: PrintOrComponent },
  { path: 'shipping-slip-bulk', component: ShippingprintBulkComponent },
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      onSameUrlNavigation: 'reload' ,
      enableTracing: false,
      scrollPositionRestoration: 'top'
    })
  ],
  exports: [RouterModule],
  declarations: []
})
export class TemplateOneRoutingModule { }
