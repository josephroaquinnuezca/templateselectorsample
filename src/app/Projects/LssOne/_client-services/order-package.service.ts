import { ApisService } from './../shared/apis.service';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL, getOrderID } from '../shared/apis.service';


@Injectable({ providedIn: 'root' })
export class OrderPackageService {

  constructor(private http: HttpClient, private service: ApisService) { }

	fetchOrderPackage(order_no: any) {
		return this.http.get(`${HTTP_API_URL}order-record/fetch-package/${order_no}`);
	}
}
