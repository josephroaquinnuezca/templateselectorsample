import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL } from '../shared/apis.service';
import * as jwt_decode from "jwt-decode";
import { ActivatedRoute, Router} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class PaymentMethodService {

	isSignedIn = false;
  currentUser: any;

  constructor(private http: HttpClient, public router: Router, private route: ActivatedRoute) {}

  getAll() {
    return this.http.get(`${HTTP_API_URL}payment-method?page=1&limit=1000`);
  }
  getSingle(id) {
    return this.http.get(`${HTTP_API_URL}payment-method/${id}`);
  }
}
