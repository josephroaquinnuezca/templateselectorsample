import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL } from '../shared/apis.service';

@Injectable({ providedIn: 'root' })
export class EmailVerificationService {

  constructor(private http: HttpClient) {}

  checkEmailVerification(data: any) {
    return this.http.get(`${HTTP_API_URL}/client/email-verify/`+data);
  }
	processEmailVerification(data: any) {
		return this.http.put(`${HTTP_API_URL}/client/email-verify/`, data);
	}
}
