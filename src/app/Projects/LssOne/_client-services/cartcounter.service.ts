import { Injectable , EventEmitter} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartcounterService {
  cartCount: EventEmitter<any> = new EventEmitter<any>();
  constructor() {}
  emitCartCount(string) {
    this.cartCount.emit(string);
  }

  getCartItemCount() {
    return this.cartCount;
  }
}
