import { ApisService } from './../shared/apis.service';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL, getOrderID } from '../shared/apis.service';


@Injectable({ providedIn: 'root' })
export class ConfirmOrderService {

  constructor(private http: HttpClient, private service: ApisService) { }

	getOrderTransaction(order_no: any, email: any) {
		return this.http.get(`${HTTP_API_URL}confirm-order/${order_no}/${email}`);
	}
  uploadTestimonyConfirmOrder(body, files: Set<File>){

    const formData: FormData = new FormData()

    formData.append('item_id', body.item_id+'')
    formData.append('user_id', body.user_id+'')
    formData.append('rate', body.rate+'')
    formData.append('message', body.message)
    formData.append('order_no', body.order_no)
    formData.append('type', body.type)

    let counter = 0
    files.forEach(file => {
      counter++
      formData.append(`testimony_image_${counter}`, file)
    })

    formData.append('file_count', counter+'')
    formData.append('has_file',counter != 0 ? "true" : 'false')

    return this.http.post(`${HTTP_API_URL}confirm-order/testimony`, formData );

  }
}
