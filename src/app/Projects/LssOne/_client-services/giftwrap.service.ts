import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL } from '../shared/apis.service';

@Injectable({ providedIn: 'root' })
export class GiftwrapService {

  constructor(private http: HttpClient) {}

	getAllGiftwrap() {
		return this.http.get(`${HTTP_API_URL}setting/giftwrap`);
	}
	getGiftwrap(id: any) {
		return this.http.get(`${HTTP_API_URL}setting/giftwrap?id=${id}`);
	}
	
}
