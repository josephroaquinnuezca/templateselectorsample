import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_API_URL } from '../shared/apis.service';
import * as jwt_decode from "jwt-decode";
import { ActivatedRoute, Router} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class CourierReferenceService {

  constructor(private http: HttpClient, public router: Router, private route: ActivatedRoute) {}

  createReference(body)  {

		const formData: FormData = new FormData();

    formData.append('order_no', body.order_no);
    formData.append('fee', body.fee);
    formData.append('courier_id', '1');
    formData.append('courier_name', 'Mr Speedy');
    formData.append('has_file', "false");

    return this.http.post(`${HTTP_API_URL}courier`, formData );

  }
}
