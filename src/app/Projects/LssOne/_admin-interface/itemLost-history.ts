export interface ItemLostHistory {
	id: number;
	item_name: string;
	quantity: number;
	type: number;
	created_date: string;
}
