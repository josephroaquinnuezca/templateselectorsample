export interface PaymentServiceInterface {
  id: number
  pm_no: number
  name: string
  account_number: string
}
