import { guides } from './../../shared/apis.service';
import { Component, OnInit } from '@angular/core';

import { ItemCategService, SettingService } from '../../_admin-services';
import { PaymentMethodService } from '../../_client-services/payment-method.service';

// import { OrderingService } from '';


@Component({
  selector: 'app-client-guide',
  templateUrl: './client-guide.component.html',
  styleUrls: ['./client-guide.component.scss']
})
export class ClientGuideComponent implements OnInit {

  constructor(private settingService: SettingService, private paymentMethodService: PaymentMethodService) { }

  guides: any[] = [];
  paymentMenthodOption:any;
  ngOnInit() {
    this.getPaymentMethod();
    this.getData();
  }

  getPaymentMethod(){
    this.paymentMethodService.getAll().subscribe( res => {
      const data:any = res;
      console.log(data);
      this.paymentMenthodOption = data.data.payload;
      console.log(this.paymentMenthodOption);
      // console.log('USER:', data.data.items[0])
      // this.infocustom = data.data.items[0];
    });
  }

  getData() {

    this.settingService.getAllGuide()
    .subscribe( data => {
      const d: any = data;
      this.guides = d.data.items;
      // console.log(this.guides)
    });
  }
}

//SAMPL+E SCROLL HTML
// <hello name="{{ name }}"></hello>
//
// <button (click)="track.scroll()">Click to Scroll</button>
//
// <div class="box">
//     BOX 1
// </div>
//
// <div class="box">
//     BOX 2
// </div>
//
// <div class="box">
//     BOX 3
// </div>
//
// <div class="box">
//     BOX 4
// </div>
//
//
// <div class="box" [track-scroll] #track="trackScroll">
//     BOX 5
// </div>

//SAMPLE CODE SMOOTH SCROLL TS
// constructor(public el: ElementRef, private renderer: Renderer2) {
//
// }
//
// @Input("track-scroll")
// private name;
//
// @HostListener('click', ['$event'])
// checkScroll() {
// 	 this.scroll();
// }
//
// scroll(){
// 	 console.log("scoll started");
// 	 let anchor = this.el.nativeElement.top = 100;
//
// 	 //style.top =100;
//
// 	// this.el.nativeElement.style.top = 100;
// 	 window.scrollTo({top: anchor, left: 0, behavior: 'smooth'});
// }
