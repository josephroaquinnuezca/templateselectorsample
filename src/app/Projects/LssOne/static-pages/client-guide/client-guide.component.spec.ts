import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientGuideComponent } from './client-guide.component';

describe('ClientGuideComponent', () => {
  let component: ClientGuideComponent;
  let fixture: ComponentFixture<ClientGuideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientGuideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
