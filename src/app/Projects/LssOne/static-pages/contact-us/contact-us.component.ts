import { ApisService, contactUs } from './../../shared/apis.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr'; //may 31, 2020
import { ErrorMesageDialogComponent } from '../error-mesage-dialog/error-mesage-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { SuccessMesageDialogComponent } from '../success-mesage-dialog/success-mesage-dialog.component';
import { UserService } from 'src/app/user-routes/shared/user.service';
import { HelperService } from '../../_helper-service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  //
  form: FormGroup
  //
  submitted = false;

  title: string = 'My first AGM project';
  lat: number = 14.576059;
  lng: number = 121.042404;
  public zoom = 15;
  isSending: boolean = false

  constructor(
    private toastr: ToastrService, // new constructor may 31, 2020
    public dialog: MatDialog,
    public  userService: UserService,
    private formBuilder: FormBuilder, private api: ApisService,
    private helperService: HelperService) { }

  ngOnInit() {
    this.createForm();
    this.loadCurrentUser();
  }
  loadCurrentUser(){
    if (this.userService.hasCurrentUser()) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.form.controls['fullname'].setValue(`${currentUser.firstname} ${currentUser.surename}`)
      this.form.controls['company_name'].setValue('None')
      this.form.controls['contact_number'].setValue(currentUser.contact_no)
      this.form.controls['email_address'].setValue(currentUser.email_address)
      this.form.controls['subject'].setValue('')
    }else{
      this.form.reset();
      this.form.controls['company_name'].setValue('None');
    }
  }
  createForm() {
    this.form = this.formBuilder.group({
      fullname: ['', Validators.required],
      company_name: ['', Validators.required],
      contact_number: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      email_address: ['', [Validators.required, Validators.email]],
      subject: ['', Validators.required]
    })
  }


  get contactFormControl() {
    return this.form.controls;
  }

  trimValue(formControl) {
    formControl.setValue(formControl.value.trim().replace(/\s\s+/g, ' '));
    console.log('test-trim', formControl);
  }
  onSubmit(){
    if (this.form.invalid) {
			this.helperService.findInvalidControls(this.form.controls);
      // alert('Incomplete information or invalid input provided!') //
      // this.toastr.error('Please complete the registration form');
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: 'Incomplete information or invalid input provided!'
      });
      // alert('Please complete the registration form')
      return
    }
    if(this.isSending) return
    this.isSending = true
    let body = this.form.getRawValue()
    // console.log('ContactUsComponent', body)
    this.api.post(contactUs, body).subscribe(res => {
      // console.log('ContactUsComponent', res)
      this.isSending = false;
      this.loadCurrentUser();
      // alert('Email Sent!')
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Email Sent!'
    });
    })
  }

}
