import { Component, OnInit } from '@angular/core';

import { OrderingService } from '../../ordering-routes/shared/ordering.service';


@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {

  // slideConfig = {
  //           dots: true,
  //           infinite: true,
  //           speed: 300,
  //           slidesToShow: 4,
  //           slidesToScroll: 4,
  //           responsive: [
  //             {
  //               breakpoint: 1024,
  //               settings: {
  //                 slidesToShow: 2,
  //                 slidesToScroll: 1,
  //                 infinite: true,
  //                 dots: true,
  //               },
  //             },
  //             {
  //               breakpoint: 600,
  //               settings: {
  //                 slidesToShow: 1,
  //                 slidesToScroll: 1,
  //               },
  //             },
  //             {
  //               breakpoint: 480,
  //               settings: {
  //                 slidesToShow: 1,
  //                 slidesToScroll: 1,
  //               },
  //             },
  //           ]
  //         }

  // addSlide() {
  //   this.slides.push({img: "http://placehold.it/350x150/777777"})
  // }

  // removeSlide() {
  //   this.slides.length = this.slides.length - 1;
  // }
  //
  // slickInit(e) {
  //   console.log('slick initialized');
  // }
  //
  // breakpoint(e) {
  //   console.log('breakpoint');
  // }
  //
  // afterChange(e) {
  //   console.log('afterChange');
  // }
  //
  // beforeChange(e) {
  //   console.log('beforeChange');
  // }

  title = 'ngSlick';
  slideConfig = {
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3500,
    dots: true,
    infinite: true,
    nextArrow: "<div class='nav-btn next-slide'></div>",
    prevArrow: "<div class='nav-btn prev-slide'></div>",
    responsive: [
      {
        'breakpoint': 1600,
        'settings': {
          'slidesToShow': 3,
          'slidesToScroll': 1,
        }
      },
      {
        'breakpoint': 1000,
        'settings': {
          'slidesToShow': 2,
          'slidesToScroll': 1,
        }
      },
      {
        'breakpoint': 600,
        'settings': {
          'slidesToShow': 1,
          'slidesToScroll': 1,
        }
      }
     ]
   };


  // slideConfig = {
  //   slidesToShow: 3,
  //   slidesToScroll: 1,
  //   dots: true,
  //   autoplay: true,
  //   nextArrow: "<div class='nav-btn next-slide'></div>",
  //   prevArrow: "<div class='nav-btn prev-slide'></div>",
  // };

  slides: any[] = [];

  constructor(private service: OrderingService) { }

  ngOnInit() {

    this.service.getCarouselAbout().subscribe( res => {
      const d: any = res.data;
      d.forEach(element => {
        this.slides.push({
          img: element.file
        });
      });
    });
  }

}
