import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-success-mesage-dialog',
  templateUrl: './success-mesage-dialog.component.html',
  styleUrls: ['./success-mesage-dialog.component.css']
})
export class SuccessMesageDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public getdata: any) {
  }

 ngOnInit() {


   console.log('testnaten to', this.getdata);
 }

}


