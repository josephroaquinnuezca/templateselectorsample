import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessMesageDialogComponent } from './success-mesage-dialog.component';

describe('SuccessMesageDialogComponent', () => {
  let component: SuccessMesageDialogComponent;
  let fixture: ComponentFixture<SuccessMesageDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessMesageDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessMesageDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
