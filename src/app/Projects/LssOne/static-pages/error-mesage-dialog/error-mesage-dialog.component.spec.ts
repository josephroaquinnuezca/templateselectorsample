import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorMesageDialogComponent } from './error-mesage-dialog.component';

describe('ErrorMesageDialogComponent', () => {
  let component: ErrorMesageDialogComponent;
  let fixture: ComponentFixture<ErrorMesageDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorMesageDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorMesageDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
