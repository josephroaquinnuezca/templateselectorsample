import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
// import { FormGroup, FormBuilder, Validators } from '@angular/forms'; //
import { PaymentMethodService } from 'src/app/_admin-services/payment-method.service';
import { PaymentMethod } from '../../shared/admin.model';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { id } from '@swimlane/ngx-charts/release/utils';
import { forkJoin } from 'rxjs';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payment-edit-dialog',
  templateUrl: './payment-edit-dialog.component.html',
  styleUrls: ['./payment-edit-dialog.component.css']
})
export class PaymentEditDialogComponent implements OnInit {

  paymentForm: FormGroup;
  paymentData: PaymentMethod;
  data: any;
  paymentInfo: any = [];
  imgFile: File;
  imgData: any;
  localUrl: any;
  isUpdate: boolean = false;


  constructor( private formBuilder: FormBuilder,
               private router: Router,
               private toastr: ToastrService,
               private paymentMethodService: PaymentMethodService,
               public dialogRef: MatDialogRef<PaymentEditDialogComponent>,
               @Inject(MAT_DIALOG_DATA) data
    ) {
      this.data = data.id
    }

  ngOnInit() {
    this.fetchData();
    this.createForm()
  }

  createForm(){
    this.paymentForm = this.formBuilder.group({
      name: ['', Validators.required],
      account_number: ['', Validators.required]
    });
    // console.log(this.paymentForm);
  }

  setData(){
    // console.log(this.paymentData)
    this.paymentForm.controls['name'].setValue(this.paymentData.name);
    this.paymentForm.controls['account_number'].setValue(this.paymentData.account_number);
  }

  fetchData(){
    this.paymentMethodService.getSingle(this.data).subscribe(data => {
        console.log(data);
        // this.paymentinfo = data;
        const d: any = data;

        console.log(d);
        if (!this.isObjectEmpty(d.data.payload)) {
          //this.pageItem = d.data.pagination;
          this.paymentData = d.data.payload[0];
          this.imgData = this.paymentData.img_pm[0].file;
          console.log('hellojosep', this.paymentData.img_pm[0].file);
          this.setData();
        }
    });
  }



  get name(){ return this.paymentForm.get('name');}
  get account_number(){ return this.paymentForm.get('account_number');}

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.isUpdate = true;
        this.localUrl = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
      this.imgFile = event.target.files[0];
      // this.isUpload = true
    }
  }

  get f() { return this.paymentForm.controls; }

  // console.log(f)

  updatebtn(){

     const dataUpdate = {
      name : this.f.name.value,
      account_number : this.f.account_number.value,
      file_type: 'payment-images',
			has_file: true,
    }

    console.log('potekconsole', dataUpdate)
    this.paymentMethodService.update(dataUpdate, this.imgFile, this.data).pipe(first()).subscribe(
      data =>{
        this.dialogRef.close(this.router.navigate['/payment-setting'])
        this.toastr.success("Successfully updated")
        console.log(data)
      }
    )



  }

  isObjectEmpty (Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)){
        return false;
      }
    }
    return true;
  }
}
