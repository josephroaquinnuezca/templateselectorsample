import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatSort, MatSlideToggleChange, MatSlideToggle } from '@angular/material';
// import { AddPaymentDialogComponent } from '../add-payment-dialog/add-payment-dialog.component';
import { PaymentAddDialogComponent } from './payment-add-dialog/payment-add-dialog.component';
import { PaymentEditDialogComponent } from './payment-edit-dialog/payment-edit-dialog.component';
import { PaymentMethodService } from 'src/app/_admin-services/payment-method.service';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { NoResultComponent } from '../items/lists/modals/no-result/no-result.component';
// import { PaymentService } from '../shared/admin.model';
// import { PaymentServiceInterface } from 'src/app/_admin-interface/payment-interface';
import * as $ from 'jquery';
import * as moment from 'moment';
import { PaymentMethod } from '../shared/admin.model';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-payment-setting',
  templateUrl: './payment-setting.component.html',
  styleUrls: ['./payment-setting.component.scss']
})
export class PaymentSettingComponent implements OnInit {

  // dataSource: MatTableDataSource<PaymentService>;
  // paymentinfo: any = [];

  // datainfo: any;

  paymentinfo: any = [];

  @ViewChild('editTemplate', { read: true, static: false }) sort: MatSort;
  reqList: PaymentMethod[] = [];
  dataSource: MatTableDataSource<PaymentMethod>;
  search_loading = false;
  imgany: any;
  idCurrent:any;
  testchecked =  false;
  testNotchecked =  false;

  testVariable: any;

  // new
  isChecked = true;
  formGroup: FormGroup;

  isTest: boolean = false;

  displayedColumns: string[] = [ 'pm_no', 'img_pm',  'name', 'account_number', 'viewAction'];

  constructor(
    public dialog: MatDialog,
    private paymentmethodservice: PaymentMethodService,
    ) { }



  paginator = {
    page : 1,
    limit : '20',
    key : '_all',
    type : '0'
  };

  pageItem = {
    first         : 1,
    total         : 0,
    pages         : 0,
    has_next      : 0,
    has_previous  : 0,
    next_page     : 0,
    previous_page : 0,
    offset        : 0,
    current       : 0
  };

  isObjectEmpty (Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)){
        return false;
      }
    }
    return true;
  }


  ngOnInit() {
    // this.reloaddata(this.datainfo);
    this.getData(this.paginator);
    this.dataSource = new MatTableDataSource(this.reqList);
    this.dataSource.sort = this.sort;



  }

  addpayment(){
    // alert('hello joseph nuezca');
    // this.file.nativeElement.click();
    const dialogRef = this.dialog.open(PaymentAddDialogComponent, {
      panelClass: 'app-full-bleed-dialog-p-10',
      data: 'home-page',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
     console.log('The dialog was closed');
     this.getData(this.paginator);
     // this.getImages();
   });
  }



  // reloaddata(datainfo){

  //    this.paymentmethodservice.getAll(datainfo).subscribe(data => {
  //     this.datainfo = data;
  //     this.dataSource = new MatTableDataSource<PaymentService>(this.datainfo);
  //     this.dataSource.data = this.datainfo.payload;
  //   });


  // }

  // getImages() {
  //   this.paymentmethodservice.getAll()
  //     .subscribe(data => {
  //       const d: any = data;
  //       this.details = d.data;
  //       this.currentImages = this.details.images;
  //       this.inst_currentImages = this.details.instruction_images;
  //     });
  // }


  getData(pgn: any) {

    const str = $.param(pgn);
    this.paymentmethodservice.getAll(str)
    .subscribe( data => {
      this.search_loading = false;
      const d: any = data;
      if (!this.isObjectEmpty(d.data.payload)) {
        this.pageItem = d.data.pagination;
        this.reqList = d.data.payload;
        console.log('hello joseph', this.reqList[0].status);
        // if (this.reqList[0].status === 1) {
        //   this.testchecked = true;
        //   this.isTest = false;
        // } else {
        //   this.testNotchecked = false;
        //   this.isTest = true;
        // }
        this.dataSource.data = this.reqList;
      } else {
        this.openDialog();
      }
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NoResultComponent, {
      panelClass: 'app-full-bleed-dialog-p-26',
      disableClose: false
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  editpayment(id: number): void{
    const dialogRef = this.dialog.open(PaymentEditDialogComponent, {
      panelClass: 'app-full-bleed-dialog-p-10',
      data: {id},
      disableClose: true
    });
    //close
      dialogRef.afterClosed().subscribe(res => {
        this.getData(this.paginator);
      });
  }






  // testFunc(event){
  //   // this.myVariable = event.checked;
  //   // // console.log('event:', event);
  //   // // console.log(this.myVariable)
  //   // console.log('event value:', (event.checked) ? 'ACTIVE' : 'INACTIVE' );



  // }

  // matToggle(id: number){
  //   console.log(id);
  // }

  // activate = new FormControl();

    // this.paymentmethodservice.togglePaymentMethod(id).subscribe(data => {

    //   this.idCurrent = data;

    //   this.idCurrent.status;
    //   console.log(this.idCurrent)

    // })
  // fetch na muna naten yung id at status niya
  // if === 1 cheked / if !not doncheckked




    // console.log(ob.checked);
    // this.a = ob.checked;
    // this.idCurrent = id;

    // this.idCurrent = this.a;


    // console.log(this.idCurrent);



    // let matSlideToggle: MatSlideToggle = ob.source;
    // console.log(matSlideToggle.color);
    // console.log(matSlideToggle.required);

    toggleSlide(id: any){
      this.paymentmethodservice.togglePaymentMethod(id).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
        const dataResult: any = res;
        console.log('dataResult:', dataResult);
      }, err => {
        console.log('HTTP Error:', err);
        if (err.error) {
          console.log('HTTP Error:', err.error);
          if(err.error.type == 'error'){
            console.log('HTTP Error Type:', err.error.type);
          }
          else{
          }
        }
      }, () => {
        console.log('Done Fetching...');
        this.getData(this.paginator);
      });
    }

    handleError(error) {
      console.log(error);
      return throwError(error);
    }

}
