import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentAddDialogComponent } from './payment-add-dialog.component';

describe('PaymentAddDialogComponent', () => {
  let component: PaymentAddDialogComponent;
  let fixture: ComponentFixture<PaymentAddDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentAddDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
