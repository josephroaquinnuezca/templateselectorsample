import { OnInit, Component, ViewChild, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from "@angular/material";
import { PaymentMethodService } from "src/app/_admin-services/payment-method.service";
import { ToastrService } from "ngx-toastr";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { first } from "rxjs/operators";
import { ErrorMesageDialogComponent } from "src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component";

@Component({
  selector: 'app-payment-add-dialog',
  templateUrl: './payment-add-dialog.component.html',
  styleUrls: ['./payment-add-dialog.component.scss']
})
export class PaymentAddDialogComponent implements OnInit {

  paymentform: FormGroup;
  postData: any;

  imgFile: File;

  submitted = false;
  details: any;
	// image variables
  @ViewChild('file', { static: false }) file;
  public files: Set<File> = new Set();
  strUrl: any[] = [];
	hasFiles:boolean = false;
  currentImages: any[] = [];
  option: any;

  // isUpdate: boolean = false;
  localUrl: any;

  constructor(
    public dialogRef: MatDialogRef<PaymentAddDialogComponent>,
    private paymentmethodservice: PaymentMethodService,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    this.paymentform = this.formBuilder.group({
      img: ['', [Validators.required]],
      name: ['', [Validators.required]],
      account_number: ['', [Validators.required]]
    });

  }


  // get name(){ return this.paymentform.get('name');}
  // get account_number(){ return this.paymentform.get('account_number');}

  get f() { return this.paymentform.controls; }

  onSubmit() {

    this.postData = {
      name: this.f.name.value,
      account_number: this.f.account_number.value,
			file_type: 'payment-images',
			has_file: true,
    };
		console.log('joseph nuezca postdata', this.postData)
    if(this.paymentform.invalid){
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Incomplete information or invalid input provided!'
    });
    return
    }else{

    this.paymentmethodservice.create(this.paymentform.value, this.imgFile)
    .pipe(first())
    .subscribe(
      data => {
        this.submitted = false;
        this.dialogRef.close();
        console.log('Jospeh nuezca', data)
        this.toastr.success("Successfully added");
      },
      error => {
        this.submitted = false;
        // console.log();
      });

    }




  }

  // 	this.userSerive.create(data, this.imgFile).pipe(retry(1),catchError(this.handleError)).subscribe( res => {


  // if (typeof this.details.imgProfile !== 'undefined' && this.details.imgProfile.length > 0) {
  //   this.localUrl = this.details.imgProfile[0].file
  // }else{
  //   this.localUrl = ''
  // }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrl = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
      this.imgFile = event.target.files[0];
      // this.isUpload = true
    }
  }






}
