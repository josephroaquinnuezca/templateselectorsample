import { first } from 'rxjs/operators';
import { SharedDataService } from '.././../../shared/shared-data.service';
import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { Transaction } from 'src/app/user-routes/shared/transaction.model';
import { OrderRequest } from '../../shared/admin.model';
// import { OrderDialogComponent } from '../../order-dialog/order-dialog.component';

import { UsersService, ItemsService } from '../../../_admin-services';

import { FormControl } from '@angular/forms';

import * as $ from 'jquery';
import * as moment from 'moment';
import { AccountsGuestFormComponent } from '../form/form.component';
import { NoResultComponent } from '../../items/lists/modals/no-result/no-result.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class AccountsGuestListComponent implements OnInit {
  contentHeight: number;
  date = new FormControl(new Date());
  date_To = new FormControl('');
  date_From = new FormControl('');

  @ViewChild('editTemplate', { read: true, static: false }) sort: MatSort;
  reqList: OrderRequest[] = [];
  dataSource: MatTableDataSource<OrderRequest>;
  search_loading = false;

  displayedColumns = ['id', 'user_no', 'name', 'type', 'viewAction'];
  titlebranch: String = "Guest Membership";

  userdetails: any;

  constructor(private sharedData: SharedDataService, public dialog: MatDialog, private service: UsersService) { }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // event.target.innerWidth;
    this.contentHeight = document.documentElement.clientHeight - 277.5
  }

  paginator = {
    page : 1,
    limit : '20',
    key : '_all',
    type : '1',
    account_type : '4',
  };

  pageItem = {
    first         : 1,
    total         : 0,
    pages         : 0,
    has_next      : 0,
    has_previous  : 0,
    next_page     : 0,
    previous_page : 0,
    offset        : 0,
    current       : 0
  };

  isObjectEmpty (Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)){
        return false;
      }
    }
    return true;
  }

  ngOnInit() {
    this.userdetails = JSON.parse(localStorage.getItem('currentAdmin'));

    this.contentHeight = document.documentElement.clientHeight - 277.5
    this.getData(this.paginator);
    this.dataSource = new MatTableDataSource(this.reqList);
    this.dataSource.sort = this.sort;
  }

  getData(pgn: any) {

    const str = $.param(pgn);
    this.service.getAllAccounts(str)
    .subscribe( data => {
      this.search_loading = false;
      const d: any = data;
      if (!this.isObjectEmpty(d.data.items)) {
        this.pageItem = d.data.pagination;
        this.reqList = d.data.items;
        // console.log( this.reqList);
        this.dataSource.data = this.reqList;
      } else {
        this.openDialog();
      }
    });
  }

  selectLimit(value: any) {
    this.paginator.limit = value;
    this.getData(this.paginator);
  }

  selectType(event: any) {
    this.paginator.type = event;
  }
  pageAction (action: string) {
    switch (action) {
      case 'first' : {
        this.paginator.page = 1;
        break;
      }
      case 'prev' : {
        this.paginator.page = this.pageItem.previous_page;
        break;
      }
      case 'next' : {
        this.paginator.page = this.pageItem.next_page;
        break;
      }
      case 'last' : {
        this.paginator.page = this.pageItem.pages;
        break;
      }
    }

    this.getData(this.paginator);
  }

  applyFilter(key: string) {
    if (key) {
      this.paginator.page = 1;
      this.paginator.key = key;
      this.getData(this.paginator);
    } else {
      this.paginator.page = 1 ;
      this.paginator.key = '';
      this.getData(this.paginator);
    }
  }

  additem(option) {
    const dialogRef = this.dialog.open(AccountsGuestFormComponent, {
		  panelClass: 'app-full-bleed-dialog-p-10',
      data: {option}
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
      this.getData(this.paginator);
    });
  }

  userDialog(data, option) {
    const dialogRef = this.dialog.open(AccountsGuestFormComponent, {
		  panelClass: 'app-full-bleed-dialog-p-10',
      data: {
				data,
				option
			}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
      this.getData(this.paginator);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NoResultComponent, {
      panelClass: 'app-full-bleed-dialog-p-26',
      disableClose: false
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  date_FromChange(value: any) {
    this.paginator['date_from'] = moment(new Date(value)).format('YYYY-MM-DD');
    // console.log(this.paginator);
  }

  date_ToChange(value: any) {
    this.paginator['date_to'] = moment(new Date(value)).format('YYYY-MM-DD');
    // console.log(this.paginator);
  }

  delete(id) {
    this.service.delete(id).subscribe(data => {
      const d: any = data;
      this.getData(this.paginator);
    });
  }
}
