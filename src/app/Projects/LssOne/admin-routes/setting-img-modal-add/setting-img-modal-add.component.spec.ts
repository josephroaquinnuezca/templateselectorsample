import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingImgModalAddComponent } from './setting-img-modal-add.component';

describe('SettingImgModalAddComponent', () => {
  let component: SettingImgModalAddComponent;
  let fixture: ComponentFixture<SettingImgModalAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingImgModalAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingImgModalAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
