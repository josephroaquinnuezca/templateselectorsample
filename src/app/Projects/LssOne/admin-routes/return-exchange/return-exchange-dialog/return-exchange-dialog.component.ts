import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-return-exchange-dialog',
  templateUrl: './return-exchange-dialog.component.html',
  styleUrls: ['./return-exchange-dialog.component.scss']
})
export class ReturnExchangeDialogComponent implements OnInit {

	details: any;
	option: any;
  constructor(public dialogRef: MatDialogRef<ReturnExchangeDialogComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.details = data.data;
    this.option = data.option;
	}

  ngOnInit() {
		console.log(this.details);
		console.log(this.option);
		//manually trigger
		// this.dialogRefAGM.close();
		//listen to back drop & close
		this.dialogRef.beforeClose().subscribe(() => this.dialogRef.close(this.details));
  }
	trimValue(value){
		return (value) ? value.trim().replace(/\s\s+/g, ' ') : '';
	}
}
