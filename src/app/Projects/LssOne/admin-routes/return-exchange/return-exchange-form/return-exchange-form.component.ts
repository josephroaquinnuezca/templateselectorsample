import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { OrderService, ReturnExItemLost } from 'src/app/_admin-services';


import { MatTableDataSource, MatSort, MatDialog, MatSortModule } from '@angular/material';
import { OrderRequest } from '../../shared/admin.model';
import { ORHistory } from '../../shared/or-history';
import { ItemLostHistory } from '../../../_admin-interface';
import { ReturnExchangeDialogComponent } from '../return-exchange-dialog/return-exchange-dialog.component';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';

@Component({
  selector: 'app-return-exchange-form',
  templateUrl: './return-exchange-form.component.html',
  styleUrls: ['./return-exchange-form.component.scss']
})
export class ReturnExchangeFormComponent implements OnInit {

  @ViewChild('date', { read: MatSort, static: true }) sort: MatSort;
  @ViewChild(MatSort, {static: true}) sort_2: MatSort;
  //for mat-table
  dataSource: MatTableDataSource<ORHistory>;
  orHistory: ORHistory[] = [];
  //for bootstrap
  orHistoryBS: any[] = [];
  //for mat-table
  itemLostDataSource: MatTableDataSource<ItemLostHistory>;
  itemLostHistory: ItemLostHistory[] = [];
  //for bootstrap
  itemLostHistoryBS: any[] = [];

  displayedColumns = ['id', 'created_date', 'order_record', 'item_name', 'type', 'quantity', 'viewAction'];
  ILdisplayedColumns = ['id', 'created_date', 'item_name', 'type', 'quantity'];

  titlebranch: String = "Return And Exchange";

  items: any[] = [];
  type: number = 1;
  selItem: any = null;
  resp: any;

  orStat: number = 0;
  // 1 = Searched
  // 2 = Loading
  // 3 = Failed
  or: string = '';
  qty: number = null;
  remark: string = '';
  btnDisabled = false;
  isSaving: boolean;
  search_loading: boolean;
	contentHeight: number;
  constructor(private service: OrderService, public dialog: MatDialog, private returnExItemLost: ReturnExItemLost) { }

	@HostListener('window:resize', ['$event'])
  onResize(event) {
    // event.target.innerWidth;
    this.contentHeight = document.documentElement.clientHeight - 277.5
  }

  ngOnInit() {
    this.contentHeight = document.documentElement.clientHeight - 277.5
    this.dataSource = new MatTableDataSource(this.orHistory);
    this.dataSource.sort = this.sort_2;
		console.log(this.dataSource.sort);
    this.getHistory();
    this.itemLostDataSource = new MatTableDataSource(this.itemLostHistory);
    this.itemLostDataSource.sort = this.sort;
    this.getItemLostHistory()
  }

  getItemLostHistory() {
		this.search_loading = true;
    this.returnExItemLost.historyItemLost().subscribe( res => {
      const r: any = res;
      this.itemLostHistory = r.data.items;
      this.itemLostDataSource.data = this.itemLostHistory;
			this.search_loading = false;
      console.log(this.itemLostDataSource);
    });
  }

  getHistory() {
		this.search_loading = true;
    this.service.getAllOR().subscribe(res => {
      console.log(res);
      this.orHistory = res.data.items;
      this.dataSource.data = this.orHistory;
			console.log(this.orHistory);
      console.log(this.dataSource);
			this.search_loading = false;
    });
  }

  searchOR(str: string) {
    this.resp = null
    if(str.length == 0){
      this.orStat = 0
      return
    }
    this.items = []
    this.selItem = null
    this.orStat = 2
    this.service.getOR(str).subscribe(res => {
      this.resp = res
      this.orStat = 1
      this.items = this.resp.data.items
    },err => {
      this.orStat = 3
    })
  }

  clsOR() {
    this.or = '';
    this.orStat = 0;
    this.items = [];
    this.selItem = null;
    this.qty = null;
    this.resp = null;

    if (this.type === 0) {
      this.getItems_forItemLost();
    }
  }

  getItems_forItemLost() {
    this.returnExItemLost.getAllItems().subscribe(res => {
      const d: any = res;
			console.log(d);
      this.items = d.data.items;
    });

  }

  tabChange(event) {
    if (event.index === 2) {
      this.itemLostDataSource = new MatTableDataSource(this.itemLostHistory);
      this.itemLostDataSource.sort = this.sort;
      this.getItemLostHistory();
    }
  }

  setRE() {
    if(this.selItem == null){
      // alert("Select an item")
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        data: 'Select an item'
    });
      return
    }
    if(this.qty == null || (this.qty <= 0 || this.qty > this.selItem.quantity)){
      // alert("Invalid item quantity")
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        data: 'Invalid item quantity'
    });
      return
    }

    if( this.type === 1 ) {

      if(this.orStat != 1){
        // alert("Invalid OR number")
        const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          data: 'Invalid OR number'
       });
        return
      }

      this.btnDisabled = true

      let body = {
        or_no: this.or,
        quantity: this.qty,
        remarks: this.remark,
        type: this.type,
        item_id: this.selItem.id
      }

      this.service.setRE(body).subscribe(res => {
        // console.log(res);
        this.btnDisabled = false;
        this.remark = '';
        this.clsOR();
        this.getHistory();
      });

    } else {

      this.btnDisabled = true;

      const body = {
        quantity: this.qty,
        remarks: this.remark,
        type: this.type,
        item_id: this.selItem.id
      };

      this.returnExItemLost.additemLost(body, this.selItem.id).subscribe(res => {
        this.btnDisabled = false;
        this.remark = '';
        this.clsOR();
      });

    }

  }

  openView(data, option){
    const dialogRef = this.dialog.open(ReturnExchangeDialogComponent, {
      panelClass: 'app-full-bleed-dialog-p-10',
      data: {
        data,
        option
      }
    });

		dialogRef.afterClosed().subscribe(result => {
			console.log(result);
			this.getHistory();
		});
  }

}
