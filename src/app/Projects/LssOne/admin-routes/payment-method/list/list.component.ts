import { first } from 'rxjs/operators';
import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { PaymentMethod, OrderRequest } from '../../shared/admin.model';
import { PaymentMethodService, UsersService, ItemsService } from '../../../_admin-services';
import { FormControl } from '@angular/forms';

import * as $ from 'jquery';
import * as moment from 'moment';
import { PaymentMethodFormComponent } from '../form/form.component';
import { NoResultComponent } from '../../items/lists/modals/no-result/no-result.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class PaymentMethodListComponent implements OnInit {
  contentHeight: number;
  date = new FormControl(new Date());
  date_To = new FormControl('');
  date_From = new FormControl('');

  @ViewChild('editTemplate', { read: true, static: false }) sort: MatSort;
  reqList: PaymentMethod[] = [];
  dataSource: MatTableDataSource<PaymentMethod>;
  search_loading = false;

  displayedColumns = ['id', 'pm_no', 'name', 'account_number', 'viewAction'];
  titlebranch: String = "Payment Method";

  userdetails: any;

  constructor(public dialog: MatDialog, private paymentMethodService: PaymentMethodService) { }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // event.target.innerWidth;
    this.contentHeight = document.documentElement.clientHeight - 277.5
  }

  paginator = {
    page : 1,
    limit : '20',
    key : '_all',
    type : '0'
  };

  pageItem = {
    first         : 1,
    total         : 0,
    pages         : 0,
    has_next      : 0,
    has_previous  : 0,
    next_page     : 0,
    previous_page : 0,
    offset        : 0,
    current       : 0
  };

  isObjectEmpty (Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)){
        return false;
      }
    }
    return true;
  }

  ngOnInit() {
    this.userdetails = JSON.parse(localStorage.getItem('currentAdmin'));

    this.contentHeight = document.documentElement.clientHeight - 277.5
    this.getData(this.paginator);
    this.dataSource = new MatTableDataSource(this.reqList);
    this.dataSource.sort = this.sort;
  }

  getData(pgn: any) {

    const str = $.param(pgn);
    this.paymentMethodService.getAll(str)
    .subscribe( data => {
      console.log('getData Function:', data)
      this.search_loading = false;
      const d: any = data;
      if (!this.isObjectEmpty(d.data.payload)) {
        this.pageItem = d.data.pagination;
        this.reqList = d.data.payload;
        // console.log( this.reqList);
        this.dataSource.data = this.reqList;
      } else {
        this.openDialog();
      }
    });
  }

  selectLimit(value: any) {
    this.paginator.limit = value;
    this.getData(this.paginator);
  }

  selectType(event: any) {
    this.paginator.type = event;
  }
  pageAction (action: string) {
    switch (action) {
      case 'first' : {
        this.paginator.page = 1;
        break;
      }
      case 'prev' : {
        this.paginator.page = this.pageItem.previous_page;
        break;
      }
      case 'next' : {
        this.paginator.page = this.pageItem.next_page;
        break;
      }
      case 'last' : {
        this.paginator.page = this.pageItem.pages;
        break;
      }
    }

    this.getData(this.paginator);
  }

  applyFilter(key: string) {
    if (key) {
      this.paginator.page = 1;
      this.paginator.key = key;
      this.getData(this.paginator);
    } else {
      this.paginator.page = 1 ;
      this.paginator.key = '';
      this.getData(this.paginator);
    }
  }

  additem(option) {
    const dialogRef = this.dialog.open(PaymentMethodFormComponent, {
		  panelClass: 'app-full-bleed-dialog-p-10',
      data: {option}
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
      this.getData(this.paginator);
    });
  }

  userDialog(data, option) {
    const dialogRef = this.dialog.open(PaymentMethodFormComponent, {
		  panelClass: 'app-full-bleed-dialog-p-10',
      data: {
				data,
				option
			}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
      this.getData(this.paginator);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NoResultComponent, {
      panelClass: 'app-full-bleed-dialog-p-26',
      disableClose: false
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  date_FromChange(value: any) {
    this.paginator['date_from'] = moment(new Date(value)).format('YYYY-MM-DD');
    // console.log(this.paginator);
  }

  date_ToChange(value: any) {
    this.paginator['date_to'] = moment(new Date(value)).format('YYYY-MM-DD');
    // console.log(this.paginator);
  }

  // delete(id) {
  //   this.service.delete(id).subscribe(data => {
  //     const d: any = data;
  //     this.getData(this.paginator);
  //   });
  // }
}
