import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DatePipe } from '@angular/common';
import { HelperService } from '../../../_helper-service';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemCategService, UsersService, UserTypeService } from '../../../_admin-services';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { id } from '@swimlane/ngx-charts/release/utils';

import * as places from '../../../ordering-routes/shared/cities.json'
import * as locations from '../../../ordering-routes/shared/locations.json'
import * as $ from 'jquery';
import { forkJoin } from 'rxjs';



@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class AccountsCustomerFormComponent implements OnInit {
  @ViewChild('file', { static: false }) file;
  public files: Set<File> = new Set();

  locations = locations.default;
  cities: any[] = []

  mform: FormGroup;

  userType = [];
  primaryId: any;
  strUrl: any[] = [];

  progress;
  canBeClosed = true;
  primaryButtonText: any;
  showCancelButton = true;
  uploading = false;
  uploadSuccessful = false;

	//revamp
	@ViewChild('btnChangePassword', {static: false}) btnChangePassword: ElementRef;
	details: any;
	option: any;

  imgFile: File;
	localUrl: any;
	matcher = new MyErrorStateMatcher();
  // details: any;
	newDateOfBirth:any;
	isUpdate: boolean = false;
	isSpinner: boolean = false;
	userExistError: boolean = false;
	errorRequiredFields: boolean = false
	isSuccessProcess: boolean = false;
	disableConfirmBtn: boolean = false;
	conaddText = 'Contact & Location Details';
	destText = 'Region';
	//pw change
	changePasswordText = 'CHANGE PASSWORD';
	errorChangePasswordText: any;
	errorChanngePassword: boolean = false;
	successChangePasswordText: any;
	successChanngePassword:  boolean = false;

  regionName:any;
  provinceName:any;
  cityName:any;
  barangayName:any;

  // regionArray:any;
  provinceArray:any;
  cityArray:any;
  barangayArray:any;


  constructor(
		private router: Router,
		private route: ActivatedRoute,
		private datePipe: DatePipe,
		private helperService: HelperService,
    public dialogRef: MatDialogRef<AccountsCustomerFormComponent>,
    private userSerive: UsersService,
    private userTypeService: UserTypeService,
    private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) data) {

		this.option = data.option;
		this.primaryButtonText = (data.option == 'edit') ? 'Save Changes' : 'Add User';
    if(data.option == 'edit' || data.option == 'view'){
      this.primaryId = data.data.id;
    }
  }

	sortASC(a, b)
  {
    return a.city_name < b.city_name ? -1 : a.city_name > b.city_name ? 1 : 0;
      // return itemA.city_name - itemB.city_name;
  }
  sortDESC(a, b)
  {
    return a.city_name < b.city_name ? -1 : a.city_name > b.city_name ? 1 : 0;
      // return itemB.city_name - itemA.city_name;
  }

  getRegionValue(event) {
    this.cities = event.value.city_list.sort(this.sortASC);
    // console.log(this.cities);
  }

	getRegionValueSet(event) {
    this.cities = event.city_list.sort(this.sortASC);
    // console.log(this.cities);
  }
  getDataAttrRegion(event){
    console.log(event);
    console.log('event value:', event.value);

    let tmp:any;
    for(let location of this.locations){
      if(location.region_name === event.value) tmp = location.province_list
    }
    console.log('tmp', tmp)
    this.regionName = event.value;
    if (this.provinceArray) {
      this.provinceArray = [];
      this.cityArray = [];
      this.barangayArray = [];
    }
    // console.log(this.provinceArray)
    // console.log(this.cityArray)
    // console.log(this.barangayArray)
    this.provinceArray = tmp;
  }
  getDataAttrProvince(event){
    console.log(event);
    console.log('event value:', event.value);
    console.log('ProvinceArray: ', this.provinceArray)
    console.log('ProvinceArray: ', this.provinceArray[event.value])
    console.log('ProvinceArray: ', this.provinceArray[event.value].municipality_list)
    this.provinceName = event.value;
    if (this.cityArray) {
      this.cityArray = [];
      this.barangayArray = [];
    }
    this.cityArray = this.provinceArray[event.value].municipality_list;
  }
  getDataAttrCity(event){
    console.log(event);
    console.log('event value:', event.value);
    console.log('barangayArray: ', this.cityArray)
    console.log('barangayArray: ', this.cityArray[event.value])
    this.cityName = event.value;
    if (this.barangayArray) {
      this.barangayArray = [];
    }
    this.barangayArray = this.cityArray[event.value].barangay_list;
  }
  getDataAttrBarangay(event){
    this.barangayName = event.value;
    // console.log(event.target.dataset.barangay);
    // console.log(event.target.getAttribute('data-barangay'))
  }
  clearPCB(){
    this.provinceName = ''
    this.cityName = ''
    this.barangayName = ''

    this.mform.controls['province'].setValue('');
    this.mform.controls['city'].setValue('');
    this.mform.controls['barangay'].setValue('');
  }
  clearCB(){
    this.cityName = ''
    this.barangayName = ''

    this.mform.controls['city'].setValue('');
    this.mform.controls['barangay'].setValue('');
  }
  clearB(){
    this.barangayName = ''

    this.mform.controls['barangay'].setValue('');
  }
  ngOnInit() {
		// this.dialogRefAGM.close();
		// this.dialogRef.beforeClose().subscribe(() => this.dialogRef.close(this.isSuccessProcess));
		this.getuserTypeService();
    if (this.option == 'edit') {
			this.isUpdate = true;
      // this.getUser();
			this.createFormUpdate();
			this.setData();
    }else if (this.option == 'add'){
			this.createForm();
		}else{
			this.getDataForViewing();
		}
  }
  // onFilesAdded() {
  //   const files: { [key: string]: File } = this.file.nativeElement.files;
  //   for (let key in files) {
  //     if (!isNaN(parseInt(key))) {
  //       this.files.add(files[key]);
  //       let reader = new FileReader();
  //       reader.onload = (event: any) => {
  //         this.strUrl.push(event.target.result);
  //       };
  //       reader.readAsDataURL(files[key]);
  //     }
  //   }
  // }

  update(data) {
		this.userSerive.update(data, this.imgFile, this.primaryId).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
			const dataResult: any = res;
			console.log(dataResult);
			// for ui visual only
			this.userExistError = false;
			this.isSuccessProcess = true;
			this.disableConfirmBtn = false;
			this.isSpinner = false;
			setTimeout(() => {
				this.isSuccessProcess = false;
			}, 3000);
		}, err => {
			console.log('HTTP Error:', err.error);
			// console.log(err.error.status.error);
			console.log(err.error.type);
			if(err.error.type == 'error'){
				// for ui visual only
				this.userExistError = true;
				setTimeout(() => {
					this.userExistError = false;
				}, 3000);
				this.disableConfirmBtn = false;
				this.isSpinner = false;
				console.log(this.userExistError);
			}
			else{
				// for ui visual only
				this.userExistError = true;
				setTimeout(() => {
					this.userExistError = false;
				}, 3000);
				this.disableConfirmBtn = false;
				this.isSpinner = false;
				console.log(this.userExistError);
			}
		}, () => {
			// for ui visual only
			this.disableConfirmBtn = false;
			this.isSpinner = false;
			console.log("request completed");
			// this.router.navigate(['/admin/users']);
		});
  }
	create(data){
		this.userSerive.create(data, this.imgFile).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
			const dataResult: any = res;
			console.log(dataResult);
			// for ui visual only
			this.userExistError = false;
			this.isSuccessProcess = true;
			this.disableConfirmBtn = false;
			this.isSpinner = false;
			setTimeout(() => {
				this.isSuccessProcess = false;
			}, 3000);
		}, err => {
			console.log('HTTP Error:', err.error);
			// console.log(err.error.status.error);
			if(err.error.type == 'error'){
				// for ui visual only
				this.userExistError = true;
				setTimeout(() => {
					this.userExistError = false;
				}, 3000);
				this.disableConfirmBtn = false;
				this.isSpinner = false;
				console.log(this.userExistError);
			}
			else{
				// for ui visual only
				this.userExistError = true;
				setTimeout(() => {
					this.userExistError = false;
				}, 3000);
				this.disableConfirmBtn = false;
				this.isSpinner = false;
				console.log(this.userExistError);
			}
		}, () => {
			// for ui visual only
			this.disableConfirmBtn = false;
			this.isSpinner = false;
			console.log("request completed");
			// this.router.navigate(['/admin/users']);
		});
	}

	//revamp
	onSubmit() {
		if(this.isUpdate){
			this.disableChangePassword();
		}
		for (let v in this.mform.controls) {
			this.mform.controls[v].markAsTouched();
		}
    if (this.mform.invalid) {
			// console.log(this.mform.controls)
			this.helperService.findInvalidControls(this.mform.controls);
			this.errorRequiredFields = true;
			setTimeout(() => {
				this.errorRequiredFields = false;
			}, 4000);
      // alert('Please fill out all required fields!')
      return
    }
		this.disableConfirmBtn = true;
		this.isSpinner = true;
    let data = this.mform.getRawValue();
    delete data.password2
    // data.usertype_id = 2
    data.has_file = true
    // data.destination = (data.usertype_id == 2) ? data.destination.delivery_name : 'None';
    // data.city = (data.usertype_id == 2) ? data.city.city_name ? data.city.city_name : '' : 'None';
    // data.region = (data.usertype_id == 2) ? data.region.region_name : 'None';

    data.region = this.regionName;
    data.province = this.provinceName;
    data.city = this.cityName;
    data.barangay = this.barangayName;

    data.remarks = data.remarks
    data.contact_no = '+63' + data.contact_no
		data.age = (data.age == '0' || data.age == '' || !data.age) ? 'None' : data.age
		data.gender = (data.gender == '0' || data.gender == '' || !data.gender) ? 'None' : data.gender
		data.middlename = (data.middlename == '' || !data.middlename) ? 'None' : data.middlename
		data.dob = (this.newDateOfBirth == '' || !this.newDateOfBirth) ? 'None' : this.newDateOfBirth
		console.log(data);
		// return
		// console.log(data);
		// return
    // console.log('onSignUp', data)
    if(this.isUpdate){
      this.update(data);
    }else{
			this.create(data);
		}
  }

	createForm() {
    this.mform = this.formBuilder.group({
			usertype_id : ['', Validators.required],
      username: ['presetvalue', [Validators.required, Validators.minLength(6), Validators.maxLength(15)]], //, Validators.pattern('^[a-zA-Z0-9]*$')]
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(15), Validators.pattern('^[A-Za-z0-9_@./#&+-]*$')]],
      password2: ['', Validators.required],
      firstname: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      middlename: ['', [Validators.pattern('^[a-zA-Z ]*$')]],
      surename: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      dob: [{value: '', disabled: false}, []],
      email_address: ['', [Validators.required, Validators.email]],
      contact_no: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10),  Validators.pattern('^[0-9]*$')]],
      age: ['0', []],
      gender: ['0', []],
      province: [[], Validators.required],
      region: [[], Validators.required],
      city: [[], Validators.required],
      barangay: ['', Validators.required],
      address: ['', Validators.required],
      remarks: ['',]
    }, { validator: this.checkPasswords })
  }

	createFormUpdate() {
    this.mform = this.formBuilder.group({
			usertype_id : [{value:'', disabled: true}, Validators.required],
			username: ['presetvalue', [Validators.required, Validators.minLength(6), Validators.maxLength(15)]], //Validators.pattern('^[a-zA-Z0-9]*$')]],
      password: [{value:'defaultpw', disabled: true}, [Validators.required, Validators.minLength(6), Validators.maxLength(15), Validators.pattern('^[A-Za-z0-9_@./#&+-]*$')]],
      firstname: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      middlename: ['', [Validators.pattern('^[a-zA-Z ]*$')]],
      surename: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      dob: [{value: '', disabled: false}, []],
      email_address: [{value:'', disabled: true}, [Validators.required, Validators.email]],
      contact_no: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10),  Validators.pattern('^[0-9]*$')]],
      age: ['', []],
      gender: ['', []],
      province: [[], Validators.required],
      region: [[], Validators.required],
      city: [[], Validators.required],
      barangay: ['', Validators.required],
      address: ['', Validators.required],
      remarks: ['',]
    })
  }

	getDataForViewing(){
		this.userSerive.get(this.primaryId).subscribe(
			(res: any) => {
				console.log(res);
				this.details = res.data.items[0];
				if (typeof this.details.imgProfile !== 'undefined' && this.details.imgProfile.length > 0) {
					this.localUrl = this.details.imgProfile[0].file
				}
				else{
					this.localUrl = ''
				}
			});
	}

  setData(){
    this.userSerive.get(this.primaryId).subscribe(
      (res: any) => {
				console.log(res);
        this.details = res.data.items[0];
        console.log('UserSignupComponent', this.details);
				if(this.details.usertype_id == '2'){
					this.conaddText = 'Contact & Shipping Location Details';
					this.destText = 'Region';
				}
				else{
					this.conaddText = 'Contact & Personal Location Details';
					this.destText = 'Area';
				}
				this.mform.controls['usertype_id'].setValue(this.details.usertype_id);
        // this.mform.controls['username'].setValue(this.details.username);
        // this.mform.controls['password'].setValue('******')
        // this.mform.controls['password2'].setValue('******')
        this.mform.controls['firstname'].setValue(this.details.firstname);
        this.mform.controls['middlename'].setValue(this.details.middlename);
        this.mform.controls['surename'].setValue(this.details.surename);
        this.mform.controls['email_address'].setValue(this.details.email_address);
				if(this.details.dob){
					this.mform.controls['dob'].setValue(this.datePipe.transform(new Date(this.details.dob), 'yyyy-MM-dd'));
					// console.log(this.mform.controls['dob'].value);
					// return
					this.calculateAge(this.mform.controls['dob'].value)
				}
        this.mform.controls['contact_no'].setValue(this.details.contact_no.replace('+63', ''));
        this.mform.controls['age'].setValue(this.details.age);
				this.mform.controls['gender'].setValue(Number(this.details.gender));
        // this.mform.controls['gender'].setValue(+this.details.gender)
        this.mform.controls['address'].setValue(this.details.address);
        this.mform.controls['remarks'].setValue(this.details.remarks);

        //used for update if input is not touched
        this.regionName = this.details.region
        this.provinceName = this.details.province
        this.cityName = this.details.city
        this.barangayName = this.details.barangay

        this.mform.controls['region'].setValue(this.regionName);
        if (this.regionName != '') {
          this.getDataAttrRegion({'value': this.regionName})
        }
        this.mform.controls['province'].setValue(this.provinceName);
        if (this.provinceName != '') {
          this.getDataAttrProvince({'value': this.provinceName})
        }
        this.mform.controls['city'].setValue(this.cityName);
        if (this.cityName != '') {
          this.getDataAttrCity({'value': this.cityName})
        }
        this.mform.controls['barangay'].setValue(this.barangayName);

				if (typeof this.details.imgProfile !== 'undefined' && this.details.imgProfile.length > 0) {
					this.localUrl = this.details.imgProfile[0].file
				}
				else{
					this.localUrl = ''
				}
				// if (this.details.usertype_id == 2){
        // let tmp: any = []
        // for(let p of this.place){
        //   if(p.delivery_name === this.details.region) tmp = p
        // }
        // this.mform.controls['region'].setValue(tmp)
        // for(let r of tmp.region_list){
        //   if(r.region_name === this.details.province){
				// 		tmp = r
				// 		this.getRegionValueSet(tmp);
				// 	}
        // }
        // this.mform.controls['province'].setValue(tmp)
        // for(let c of tmp.city_list){
        //   if(c.city_name === this.details.city) tmp = c
        // }
        // this.mform.controls['city'].setValue(tmp)
				// }
      }
    )
  }

  getuserTypeService() {
    this.userTypeService.getAll().subscribe(data => {
      const d: any = data;
      this.userType = d.data.items;
    });
  }

  get f() { return this.mform.controls; }

  // getUser() {
  //   this.userSerive.get(this.primaryId)
  //     .subscribe(data => {
  //       const d: any = data;
  //       this.details = d.data.items[0];
	// 			console.log(this.details);
  //       // this.details.items[0].imgProfile.forEach(d => {
  //       //   // console.log(d.file)
  //       //   this.strUrl.push(d.file);
  //       // });
	//
  //     });
  // }

	onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrl = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
      this.imgFile = event.target.files[0];
      // this.isUpload = true
    }
  }
	checkPasswords(group: FormGroup) { // here we have the 'passwords' group
		let pass = group.controls.password.value;
		let confirmPass = group.controls.password2.value;

		return pass === confirmPass ? null : { notSame: true }
	}

	trimValue(formControl) {
    console.log(formControl);
    formControl.setValue(formControl.value.trim().replace(/\s\s+/g, ' '));
  }

	getGenderValue(getGenderValue){
		console.log(getGenderValue.value)
		// this.mform.controls['gender'].setValue(getGenderValue.value)
		//
    // console.log(this.mform.getRawValue());
	}
	getUserTypeValue(getUserTypeValue){
		console.log(getUserTypeValue.value)
		if(getUserTypeValue.value == '2'){
			this.conaddText = 'Contact & Shipping Location Details';
			this.destText = 'Region';
		}
		else{
			this.conaddText = 'Contact & Personal Location Details';
			this.destText = 'Area';
		}
		// this.mform.controls['gender'].setValue(getGenderValue.value)
		//
    // console.log(this.mform.getRawValue());
	}
	disableChangePassword(){
		let control = this.mform.controls['password'];

		var elementRef = <HTMLInputElement>this.btnChangePassword.nativeElement;
		control.setValue('defaultpw');
		control.disable();
		this.changePasswordText = 'CHANGE PASSWORD';
		elementRef.classList.remove('btn-warning');
		elementRef.classList.add('btn-outline-danger');
	}
	changePassword(){
		let control = this.mform.controls['password'];
		console.log(control);
		console.log(this.btnChangePassword);
		console.log(<HTMLInputElement>this.btnChangePassword.nativeElement);
		var elementRef = <HTMLInputElement>this.btnChangePassword.nativeElement;
		// control.disabled ? () : ();
		if(control.disabled){
			control.enable()
			control.setValue('');
			this.changePasswordText = 'SAVE PASSWORD';
			elementRef.classList.remove('btn-outline-danger');
			elementRef.classList.add('btn-warning');
		}else{
			this.isSpinner = true;
			var data = {
				id: this.details.id,
				password: control.value
			}
			console.log(data)
			// return
			this.userSerive.changePasswordSingle(data).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
				const dataResult: any = res;
				console.log(dataResult);
				// for ui visual only
				control.disable();
				this.changePasswordText = 'CHANGE PASSWORD';
				elementRef.classList.remove('btn-warning');
				elementRef.classList.add('btn-outline-danger');
				this.successChanngePassword = true;
				this.successChangePasswordText = 'Successfully updated user password.';
				setTimeout(() => {
					this.successChanngePassword = false;
				}, 3000);
				control.setValue('defaultpw');
				this.isSpinner = false;
				this.errorChanngePassword = false;
			}, err => {
				console.log('HTTP Error:', err.error);
				// console.log(err.error.status.error);
				if (err.error.status){
					this.errorChanngePassword = true;
					this.errorChangePasswordText = err.error.status.description;
					setTimeout(() => {
						this.errorChanngePassword = false;
					}, 3000);
				}else{
					this.errorChanngePassword = true;
					this.errorChangePasswordText = 'Something went wrong. Failed to change user password.';
					setTimeout(() => {
						this.errorChanngePassword = false;
					}, 3000);
				}
				this.isSpinner = false;
			}, () => {
				// for ui visual only

				this.isSpinner = false;
				console.log("request completed");
				// this.router.navigate(['/admin/users']);
			});
		}
	}
	resetRegionCity(){
		this.mform.controls['region'].reset();
		this.mform.controls['city'].reset();
	}
	calculateAge(value){
		var newDate = this.datePipe.transform(new Date(value), 'yyyy-MM-dd HH-mm-ss')
		var newDateTime = new Date(value).getTime()
		var timeDiff = Math.abs(Date.now() - newDateTime);
		var age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
		this.mform.controls['age'].setValue(age)
		this.newDateOfBirth = newDate;
		console.log(value)
		console.log(newDate)
	}
	clearAlertMessage(){
		if(this.userExistError){
			this.userExistError = false;
			this.isSpinner = false;
		}
	}
	handleError(error) {
		console.log(error);
    return throwError(error);
  }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
	const invalidParent = !!(
		control
		&& control.parent
		&& control.parent.invalid
		&& control.parent.dirty
		&& control.parent.hasError('notSame'));
	return (invalidParent);
}
}
