import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutesRoutingModule } from './admin-routes-routing.module';
// import { BrowserModule } from '@angular/platform-browser';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialsModule } from './../shared/materials.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MomentModule } from 'ngx-moment';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgxBarcodeModule } from 'ngx-barcode';
import { DigitOnlyModule } from '@uiowa/digit-only';
import { ToastrModule } from 'ngx-toastr';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AnalyticsComponent } from './analytics/analytics.component';
import { AdminRoutesComponent } from './admin-routes.component';
import { OrderRequestComponent } from './order-request/order-request.component';
import { TransactionComponent } from './transaction/transaction.component';
import { UserListComponent } from './users/list/list.component';
import { AccountsAdminListComponent } from './accounts-admin/list/list.component';
import { AccountsCustomerListComponent } from './accounts-customer/list/list.component';
import { AccountsGuestListComponent } from './accounts-guest/list/list.component';
import { PaymentMethodListComponent } from './payment-method/list/list.component';
import { OrderFormatComponent } from './order-format/order-format.component';
// import { UserFormComponent } from './users/form/form.component';
import { ItemsListsComponent } from './items/lists/lists.component';
import { ItemCategListComponent } from './item-category/item-categ-list/item-categ-list.component';
import { PaymentFormComponent } from './payment-form/payment-form.component';
import { AboutComponent } from './about/about.component';
import { GuideComponent } from './guide/guide.component';
import { AdminLandingPageComponent } from './admin-landing-page/admin-landing-page.component';
import { ReturnExchangeFormComponent } from './return-exchange/return-exchange-form/return-exchange-form.component';
import { TestimonyComponent } from './testimony/testimony.component';
import { GiftwrapComponent } from './giftwrap/giftwrap.component';
import { PackageSettingComponent } from './package-setting/package-setting.component';
import { PaymentSettingComponent } from './payment-setting/payment-setting.component';
import { CourierManagementComponent } from './courier-management/courier-management.component';
import { CourierViewComponent } from './courier-management/courier-view/courier-view.component';
import { HomeAdminComponent } from './home-admin/home-admin.component';

// added june 8, 2020 - erwin
//LIST
//FORM
import { AccountsAdminFormComponent } from './accounts-admin/form/form.component';
import { AccountsCustomerFormComponent } from './accounts-customer/form/form.component';
import { AccountsGuestFormComponent } from './accounts-guest/form/form.component';
// end

import { CustomPipesModule } from  'src/app/_custom-pipes/custom-pipes.module';
import { ReturnExchangeDialogComponent } from './return-exchange/return-exchange-dialog/return-exchange-dialog.component';
import { SettingImgModalComponent } from './setting-img-modal/setting-img-modal.component';
import { SettingImgModalAddComponent } from './setting-img-modal-add/setting-img-modal-add.component';
import { DialogReply } from './testimony/testimony.component';
import { DialogGiftwrap } from './giftwrap/giftwrap.component';
import { ItemModalComponent } from './items/lists/modals/item-modal/item-modal.component';
import { GuideFormComponent } from './guide/guide-form/guide-form.component';
import { PaymentAddDialogComponent } from './payment-setting/payment-add-dialog/payment-add-dialog.component';
import { PaymentEditDialogComponent } from './payment-setting/payment-edit-dialog/payment-edit-dialog.component';
import { CourierAddDialogComponent } from './courier-management/courier-add-dialog/courier-add-dialog.component';
import { CourierEditDialogComponent } from './courier-management/courier-edit-dialog/courier-edit-dialog.component';
import { CourierViewDialogComponent } from './courier-management/courier-view/courier-view-dialog/courier-view-dialog.component';
import { PackageSettingDialogComponent } from './package-setting/package-setting-dialog/package-setting-dialog.component';
import { ItemCategFormComponent } from './item-category/item-categ-form/item-categ-form.component';
import { ItemsFormComponent } from './items/form/form.component';
import { UserFormComponent } from './users/form/form.component';
import { OrderDialogComponent } from './order-dialog/order-dialog.component';
import { PaymentMethodFormComponent } from './payment-method/form/form.component';


import { AdminLoginComponent } from './admin-login/admin-login.component';

@NgModule({
  imports: [
    CustomPipesModule, //from other module
    CommonModule,
    AdminRoutesRoutingModule,
    // BrowserModule,
    // BrowserAnimationsModule,
    MaterialsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxChartsModule,
    NgxMaterialTimepickerModule,
    MomentModule,
    FlexLayoutModule,
    SlickCarouselModule,
		NgxBarcodeModule,
    DigitOnlyModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-center',
      preventDuplicates: true,
      closeButton:true,
      progressBar:true,
    }),
  ],
  declarations: [
    AnalyticsComponent,
    AdminRoutesComponent,
    OrderRequestComponent,
    AdminLoginComponent,
    TransactionComponent,
    UserListComponent,
    AccountsAdminListComponent,
    AccountsCustomerListComponent,
    AccountsGuestListComponent,
    PaymentMethodListComponent,
    OrderFormatComponent,
    ItemsListsComponent,
    ItemCategListComponent,
    PaymentFormComponent,
    AboutComponent,
    GuideComponent,
    AdminLandingPageComponent,
    ReturnExchangeFormComponent,
    TestimonyComponent,
    GiftwrapComponent,
    PackageSettingComponent,
    PaymentSettingComponent,
    CourierManagementComponent,
    CourierViewComponent,
    HomeAdminComponent,
    AccountsAdminFormComponent,
    AccountsCustomerFormComponent,
    AccountsGuestFormComponent,
    ReturnExchangeDialogComponent,
    SettingImgModalComponent,
    SettingImgModalAddComponent,
    DialogReply,
		DialogGiftwrap,
    ItemModalComponent,
    GuideFormComponent,
    PaymentAddDialogComponent,
    PaymentEditDialogComponent,
    CourierAddDialogComponent,
    CourierEditDialogComponent,
    CourierViewDialogComponent,
    PackageSettingDialogComponent,
    ItemCategFormComponent,
    ItemsFormComponent,
    UserFormComponent,
    OrderDialogComponent,
    PaymentMethodFormComponent
  ],
  entryComponents: [
    AccountsAdminFormComponent,
    AccountsCustomerFormComponent,
    AccountsGuestFormComponent,
    ReturnExchangeDialogComponent,
    SettingImgModalComponent,
    SettingImgModalAddComponent,
    DialogReply,
		DialogGiftwrap,
    ItemModalComponent,
    GuideFormComponent,
    PaymentAddDialogComponent,
    PaymentEditDialogComponent,
    CourierAddDialogComponent,
    CourierEditDialogComponent,
    CourierViewDialogComponent,
    PackageSettingDialogComponent,
    ItemCategFormComponent,
    ItemsFormComponent,
    UserFormComponent,
    OrderDialogComponent,
    PaymentMethodFormComponent
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})

export class AdminRoutesModule { }
