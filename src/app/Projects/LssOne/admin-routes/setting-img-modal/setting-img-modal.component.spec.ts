import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingImgModalComponent } from './setting-img-modal.component';

describe('SettingImgModalComponent', () => {
  let component: SettingImgModalComponent;
  let fixture: ComponentFixture<SettingImgModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingImgModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingImgModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
