import { Component, OnInit } from '@angular/core';
import { SharedDataService } from '../shared/shared-data.service';
import { UserService } from '../user-routes/shared/user.service';

@Component({
  selector: 'app-admin-routes',
  templateUrl: './admin-routes.component.html',
  styleUrls: ['./admin-routes.component.scss']
})
export class AdminRoutesComponent implements OnInit {

  drawerShow: boolean = false;
  drawerOpen: boolean = false;
  showSettingMenu = false;
  showAccountsMenu = false;
  userdetails: any;
  btnId:boolean = false;
  btnIdAccounts:boolean = false;

  constructor(public userService: UserService) {
  }

  ngOnInit() {
    this.userdetails = JSON.parse(localStorage.getItem('currentAdmin'));
    // console.log(this.userdetails);
  }

  toggleSettingMenu() {
    this.showSettingMenu = !this.showSettingMenu;
  }

  toggleAccountsMenu() {
    this.showAccountsMenu = !this.showAccountsMenu;
  }

  drawerToggle() {
    this.drawerShow = !this.drawerShow
    this.drawerOpen = !this.drawerOpen
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.drawerOpen = true;
      this.drawerShow = true;
    }, 500);
  }

}
