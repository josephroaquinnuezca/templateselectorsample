import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserProvider } from '../../user-routes/shared/users.provider';
import { UsersService } from '../../_admin-services';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import * as jwt_decode from "jwt-decode";
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';
import { MatDialog } from '@angular/material';



@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {

  signinForm: FormGroup

  constructor(private formBuilder: FormBuilder,
              public dialog: MatDialog,
							private usersService: UsersService,
              private router: Router,
              private userProvider: UserProvider,
              private route: ActivatedRoute) {
                        this.signinForm = this.formBuilder.group({
                          username: [ '', Validators.required ],
                          password: [ '', Validators.required ]
                        });
                       }

  ngOnInit() {
    let currentUser = JSON.parse(localStorage.getItem('currentAdmin'))
    if (currentUser){
      this.router.navigate(['./admin/order-request']);
    }
  }
  ngAfterViewInit() {
    // if (this.userService.hasCurrentUser()) {
    //   let currentUser = JSON.parse(localStorage.getItem('currentUser'))
    //   if(currentUser.usertype_id == 1) {
    //     this.router.navigate(['']);
    //   }else{
    //     this.router.navigate(['./order-request']);
    //   }
    // }
  }

  // createForm() {
  //
  // }
  get f() { return this.signinForm.controls; }

  onSubmit(){
    if(this.signinForm.invalid){
      // alert('Please complete the signin form')
      return
    }
    this.usersService.signin(this.signinForm.getRawValue()).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
			const data: any = res;
			console.log(data.data);
			console.log(data.data.token);
			// return
			this.usersService.setAdmin(data.data.token);
			}, err => {
				console.log('HTTP Error:', err.error);
				console.log(err.error.status.error);
				if(err.error.status.error){
					// hasError
          const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
            panelClass: 'app-full-bleed-dialog-p-26',
            disableClose: false,
            data: 'User not found!'
          });
					// alert("User not found!");
				}
				else{
					//else do something here
				}
			}, () => {
				console.log("request completed");
				this.router.navigate(['./admin/order-request']);
			});
		// .subscribe(
    //   res =>{
    //     if(res.status.error){
    //       alert("User not found or Password incorrect")
    //       return
    //     }

        // this.userService.setAdmin(res.data.token)
        // this.router.navigate(['./admin/order-request']);
        // let url = this.router.url
        //
        // let tokenData: any = this.tokenData(res.data.token)
        // if(tokenData.user_info.usertype_id == 1){
        //   this.userService.setUser(res.data.token)
        //   this.router.navigate(['./admin/order-request']);
        // }else{
        //   this.userService.setUser(res.data.token)
        //   this.router.navigate(['']);
        // }
    //   }
    // )
  }

	handleError(error) {
		console.log(error);
    // let errorMessage = '';
    // if (error.error instanceof ErrorEvent) {
    //   // client-side error
    //   errorMessage = `Error: ${error.statusText}`;
    // } else {
    //   // server-side error
    //   errorMessage = `Error Code: ${error.status}\nMessage: ${error.statusText}`;
    // }
    // return throwError(errorMessage);
    return throwError(error);
  }

  tokenData(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }
}
