import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { RouterModule } from  '@angular/router';
import { UsertypeGuard } from './../shared/usertype.guard';

import { AnalyticsComponent } from './analytics/analytics.component';
import { AdminRoutesComponent } from './admin-routes.component';
import { OrderRequestComponent } from './order-request/order-request.component';
import { TransactionComponent } from './transaction/transaction.component';
import { UserListComponent } from './users/list/list.component';
import { AccountsAdminListComponent } from './accounts-admin/list/list.component';
import { AccountsCustomerListComponent } from './accounts-customer/list/list.component';
import { AccountsGuestListComponent } from './accounts-guest/list/list.component';
import { PaymentMethodListComponent } from './payment-method/list/list.component';
import { OrderFormatComponent } from './order-format/order-format.component';
// import { UserFormComponent } from './users/form/form.component';
import { ItemsListsComponent } from './items/lists/lists.component';
import { ItemCategListComponent } from './item-category/item-categ-list/item-categ-list.component';
import { PaymentFormComponent } from './payment-form/payment-form.component';
import { AboutComponent } from './about/about.component';
import { GuideComponent } from './guide/guide.component';
import { AdminLandingPageComponent } from './admin-landing-page/admin-landing-page.component';
import { ReturnExchangeFormComponent } from './return-exchange/return-exchange-form/return-exchange-form.component';
import { TestimonyComponent } from './testimony/testimony.component';
import { GiftwrapComponent } from './giftwrap/giftwrap.component';
import { PackageSettingComponent } from './package-setting/package-setting.component';
import { PaymentSettingComponent } from './payment-setting/payment-setting.component';
import { CourierManagementComponent } from './courier-management/courier-management.component';
import { CourierViewComponent } from './courier-management/courier-view/courier-view.component';
import { HomeAdminComponent } from './home-admin/home-admin.component';


import { AdminLoginComponent } from './admin-login/admin-login.component';

// @NgModule({
//   declarations: [],
//   imports: [
//     CommonModule
//   ]
// })

const routes: Routes = [
  {
    path: '', component: AdminRoutesComponent, canActivateChild: [UsertypeGuard],
    children: [
      { path: '', redirectTo: 'order-request', pathMatch: 'full', },
      { path: 'home-admin', component: HomeAdminComponent },
      { path: 'order-request', component: OrderRequestComponent },
      { path: 'analytics', component: AnalyticsComponent },
      { path: 'transaction', component: TransactionComponent },
      { path: 'users', component: UserListComponent },
      { path: 'accounts-admin', component: AccountsAdminListComponent },
      { path: 'accounts-customer', component: AccountsCustomerListComponent },
      { path: 'accounts-guest', component: AccountsGuestListComponent },
      { path: 'items', component: ItemsListsComponent },
      { path: 'itemCategory', component: ItemCategListComponent },
      { path: 'payment-form', component: PaymentFormComponent },
      { path: 'landing-page', component: AdminLandingPageComponent },
      { path: 'about', component: AboutComponent },
      { path: 'guide', component: GuideComponent },
      { path: 'return-exchange', component: ReturnExchangeFormComponent },
      { path: 'testimony', component: TestimonyComponent },
      { path: 'giftwrap', component: GiftwrapComponent },
      { path: 'package-setting', component: PackageSettingComponent },
      { path: 'payment-method', component: PaymentMethodListComponent },
      { path: 'payment-setting', component: PaymentSettingComponent },
      { path: 'courier-management', component: CourierManagementComponent },
      { path: 'courier-view/:id/:name', component: CourierViewComponent },
      { path: 'order-format', component: OrderFormatComponent },

    ]
  },
  { path: 'login', component: AdminLoginComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AdminRoutesRoutingModule { }
