import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrderFormatService } from 'src/app/_admin-services/order-format.service';
import { MatDatepicker } from '@angular/material/datepicker';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-order-format',
  templateUrl: './order-format.component.html',
  styleUrls: ['./order-format.component.scss']
})
export class OrderFormatComponent implements OnInit {

  orderFormatForm: FormGroup;
  orderFormatDataArray: any;
  isSaving: boolean = false;
  year_list: any = [];
  output_id: any;
  hasData: boolean = false

  constructor(public dialog: MatDialog,
    private formBuilder: FormBuilder, private orderFormatService: OrderFormatService) {}

  ngOnInit() {
    this.createForm();
    this.getCurrentYear();
    this.getData();
  }

  createForm(){
    this.orderFormatForm = this.formBuilder.group({
      order_id: ['', Validators.required],
      order_year: ['', Validators.required]
    })
  }

  getKeyInput(){
    let data = this.orderFormatForm.getRawValue();
    this.output_id = data.order_id + '-' + data.order_year;
  }

  onChangeDate(){
    let data = this.orderFormatForm.getRawValue();
    this.output_id = data.order_id + '-' + data.order_year;
  }

  getCurrentYear(){
    var date = new Date();
    var year = date.getFullYear();

    console.log('date:', date)
    console.log('year:', year)
    var currentYear = year;
    var previousYear  = year - 110;
    console.log('currentYear:', currentYear)
    for(var i = currentYear; i >=previousYear ; i--){
        this.year_list.push({value: i});
    }
    console.log(this.year_list);
    if (!this.hasData) {
      console.log('currentYear:', currentYear)

			// setTimeout(() => {
        this.orderFormatForm.controls['order_year'].setValue(String(currentYear));
			// }, 3000);
    }
  }
  getData(){
    this.orderFormatService.getAll().subscribe(res => {
      let tmp: any = res
      console.log('res:', res)
      // console.log('PaymentFormComponent', tmp)
      if (tmp.data.payload.length != 0){
        this.hasData = true;
        this.orderFormatDataArray = tmp.data.payload[0];
        this.output_id = this.orderFormatDataArray.order_id + '-' + this.orderFormatDataArray.order_year;
        this.orderFormatForm.controls['order_id'].setValue(this.orderFormatDataArray.order_id);
        console.log(typeof Number(this.orderFormatDataArray.order_year));
        this.orderFormatForm.controls['order_year'].setValue(this.orderFormatDataArray.order_year);
      }
    });
  }

  onSubmit(){
    if(this.isSaving) return
    if (this.orderFormatForm.invalid) {
      // alert('Please complete the payment information form')
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        data: 'Please complete the payment information form'
       });
      return
    }
    let data = this.orderFormatForm.getRawValue()
    console.log('data', data)
    // return
    this.isSaving = true

    if (!this.hasData) {
      this.orderFormatService.create(data).subscribe(res => {
        this.getData()
        this.isSaving = false
      })
    }else{
      console.log(this.orderFormatDataArray.id);
      // return
      this.orderFormatService.update(data, this.orderFormatDataArray.id).subscribe(res => {
        this.getData()
        this.isSaving = false
      })
    }
  }
  // delete(id) {
  //   this.orderFormatService.delete(id).subscribe(data => {
  //     const d: any = data;
  //     this.getData();
  //   });
  // }
}
