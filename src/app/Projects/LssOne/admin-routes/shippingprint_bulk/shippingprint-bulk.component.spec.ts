import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippingprintBulkComponent } from './shippingprint-bulk.component';

describe('ShippingprintBulkComponent', () => {
  let component: ShippingprintBulkComponent;
  let fixture: ComponentFixture<ShippingprintBulkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShippingprintBulkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippingprintBulkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
