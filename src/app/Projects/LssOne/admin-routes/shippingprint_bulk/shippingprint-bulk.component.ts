import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PrintService } from '../shared/print.service';
import { OrderService } from '../../_admin-services';
import { OrderRequest } from '../shared/admin.model';
import { OrderPrintProvider } from '../../_admin-provider';
import { OrderPackageService } from '../../_client-services';

import * as $ from 'jquery';

@Component({
  selector: 'app-shippingprint-bulk',
  templateUrl: './shippingprint-bulk.component.html',
  styleUrls: ['./shippingprint-bulk.component.scss']
})
export class ShippingprintBulkComponent implements OnInit {
	order_package: any;

	reqList: any[] = []

	elementType = 'svg';
  // value = 'someValue12340987';
  format = 'CODE128';
  lineColor = '#000000';
  width = .50;
  width_2nd = 1.3;
  height = 45;
  displayValue = true;
  fontOptions = '';
  // font = 'monospace';
  textAlign = 'center';
  textPosition = 'bottom';
  textPosition_2nd = 'top';
  textMargin = 2;
  fontSize = 7;
  fontSize_2nd = 9;
  background = '#ffffff';
  margin = 0;
  marginTop = 0;
  marginBottom = 0;
  marginLeft = 0;
  marginRight = 0;

	selectedPrintData: any;
	selectedPrintActive: Boolean = false;

	values(value): string[] {
    return value.split('\n');
  }

  constructor(private orderPackageService: OrderPackageService, private router: Router, private route: ActivatedRoute, private printService: PrintService, private orderService: OrderService, private orderPrintProvider: OrderPrintProvider) {
		// this.selectedPrintData
	}

  ngOnInit() {
		// this.getData();
		console.log(this.orderPrintProvider.printData);
		if(this.orderPrintProvider.printData){
			console.log("this.selectedPrintActive");
			this.reqList = this.orderPrintProvider.printData;
			for(let data of this.reqList){
				if(data){
					this.selectedPrintActive = true
				}
			}
			if(this.selectedPrintActive){
				console.log("this.selectedPrintActive");
				for (let item of this.reqList){
					if(item){
						this.getPackagesByOrderNo(item);
					}
				}
				this.printService.onDataReady_shipslip()
			}else{
				console.log("this.getData()");
				this.getData();
			}
		}
		else{
			console.log("this.getData()");
			this.getData();
		}
		// return
		// for(var property in this.reqList) {
		//   //do nothing..
  	// 	console.log(`${property}: ${this.reqList[property].print}`);
		// }
  }

	getPackagesByOrderNo(item){
		console.log(item);
		var x = item.order_no;
		this.orderPackageService.fetchOrderPackage(x).toPromise().then(res => {
			const res_data: any = res;

			if (res_data.status.error === false ) {
				console.log(res_data);
				this.order_package = res_data.data;
				item.package_detail = this.order_package;
				console.log("WITH Package_Detail: ", item);
			}
		});
	}

	ngOnDestroy(){
		if(this.orderPrintProvider.printData){
			delete this.orderPrintProvider.printData;
		}
	}
  getData() {
  this.orderService.getORShippingSlipReadyAll()
    .subscribe(data => {
      const d: any = data;
      if (!this.isObjectEmpty(d.data.items)) {
        this.reqList = d.data.items;
				console.log(this.reqList);

				for (let item of this.reqList){
					this.getPackagesByOrderNo(item);
				}

        this.printService.onDataReady_shipslip()
      } else {
        alert("nothing to print!")
				this.router.navigate(['./admin']);
        // this.printService.onDataReady_shipslip()
      }
    });
  }

	isObjectEmpty(Obj) {
		for (const key in Obj) {
			if (Obj.hasOwnProperty(key)) {
				return false;
			}
		}
		return true;
	}
}
