import { first } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Component, OnInit, ViewChild, ContentChild, HostListener, Inject, EventEmitter } from '@angular/core';
import { TestimonyService } from '../../_admin-services';
import { Router, ActivatedRoute } from '@angular/router';
import { Testimony } from '../shared/testimony.model';
import { MatSort, MatTableDataSource, MatDialog } from '@angular/material';

import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
// import { DialogData, Marker } from '../../_client-interface';

import * as $ from 'jquery';
import * as moment from 'moment';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';

@Component({
  selector: 'app-testimony',
  templateUrl: './testimony.component.html',
  styleUrls: ['./testimony.component.scss']
})
export class TestimonyComponent implements OnInit {
  titlebranch: String = 'Testimony'
  contentHeight: number;
  @ViewChild('editTemplate', { read: true, static: false }) sort: MatSort;
  reqList: Testimony[] = []
  dataSource: MatTableDataSource<Testimony>;
  displayedColumns = ['id', 'order_no', 'rate', 'message', 'status', 'viewAction'];

  search_loading = false;
  paginator = {
    page: 1,
    limit: '20',
  }

  pageItem = {
    first: 1,
    total: 0,
    pages: 0,
    has_next: 0,
    has_previous: 0,
    next_page: 0,
    previous_page: 0,
    offset: 0,
    current: 0
  }

  constructor(
     private testimonyService: TestimonyService,
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder, public dialog: MatDialog) { }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // event.target.innerWidth;
    this.contentHeight = document.documentElement.clientHeight - (179.5 + 64 + 20)
  }

  ngOnInit() {
    this.search_loading = true;
    this.testimonyPagination(this.paginator);
    this.dataSource = new MatTableDataSource(this.reqList);
    this.dataSource.sort = this.sort;
    // console.log(this.dataSource.sort)
  }

  isObjectEmpty(Obj) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

  testimonyPagination(pgn: any) {
    const str = $.param(pgn);
    console.log(str)
    this.testimonyService.getTestimonyByFilter(str)
      .subscribe(data => {
        this.search_loading = false;
        const d: any = data;
        if (!this.isObjectEmpty(d.data.items)) {
          this.pageItem = d.data.pagination;
          this.reqList = d.data.items;
          console.log(this.reqList)
          this.dataSource.data = this.reqList;
          console.log(this.dataSource.data)
        } else {
          //this.openDialog();
          this.dialog.open(ErrorMesageDialogComponent, {
            panelClass: 'app-full-bleed-dialog-p-26',
            disableClose: false,
            data: 'No data found'
        });
        }
      });
  }

  selectLimit(value: any) {
    this.paginator.limit = value;
    this.testimonyPagination(this.paginator);
  }

  // selectType(event: any) {
  //   this.paginator.type = event;
  // }

  pageAction(action: string) {
    switch (action) {
      case 'first': {
        this.paginator.page = 1;
        break;
      }
      case 'prev': {
        this.paginator.page = this.pageItem.previous_page;
        break;
      }
      case 'next': {
        this.paginator.page = this.pageItem.next_page;
        break;
      }
      case 'last': {
        this.paginator.page = this.pageItem.pages;
        break;
      }
    }

    this.testimonyPagination(this.paginator);
  }

  applyFilter(key: string) {
    if (key) {
      this.paginator.page = 1;
      // this.paginator.key = key;
      this.testimonyPagination(this.paginator);
    } else {
      this.paginator.page = 1;
      // this.paginator.key = '';
      this.testimonyPagination(this.paginator);
    }
  }


  openDialog(row: any, option: any): void {
    const dialogRef = this.dialog.open(DialogReply, {
      panelClass: 'app-full-bleed-dialog-p-10',
      data: {
        row,
        option
      }
      //data: {zoom: this.zoom, selectedBranch_name: this.selectedBranch_name, selectedBranch_lat: this.selectedBranch_lat, selectedBranch_lng: this.selectedBranch_lng, markers: this.markers, animal: this.animal}
    });

    // const sub = dialogRefAGM.componentInstance.onSelect.subscribe((resultData) => {
    //   // do something
    //   // console.log(`Resut Data: ${resultData.branch_name} | ${resultData.lat} | ${resultData.lng}`)
    //   // alert(`It's Working Yeah!! ${resultData.branch_name} | ${resultData.lat} | ${resultData.lng}`)
    //   //
    //   this.myControl.setValue(resultData.branch_name)
    //   this.selectedBranch_name = resultData.branch_name;
    //   this.selectedBranch_lat = resultData.lat;
    //   this.selectedBranch_lng = resultData.lng;
    // });

    dialogRef.afterClosed().subscribe(dataResult => {
      console.log(dataResult)
      if (dataResult) {
        this.testimonyPagination(this.paginator);
      }
      // if(!dataResult){
      //   // console.log('The dialog was closed');
      //   // console.log(`RESULT: ${dataResult}`);
      //   // console.log('User cancelled changing location: reverting back to old selection');
      //
      //   this.myControl.setValue(this.oldSelectedBranch_name)
      //   this.selectedBranch_name = this.oldSelectedBranch_name;
      //   this.selectedBranch_lat = this.oldSelectedBranch_lat;
      //   this.selectedBranch_lng = this.oldSelectedBranch_lng;
      //
      //   sub.unsubscribe();
      // }
      // else {
      //   // console.log('The dialog was closed');
      //   // console.log(`RESULT: ${dataResult.animal}`);
      //   this.animal = dataResult.animal;
      //   this.myControl.setValue(this.selectedBranch_name)
      //   this.oldSelectedBranch_name = this.selectedBranch_name;
      //   this.oldSelectedBranch_lat = this.selectedBranch_lat;
      //   this.oldSelectedBranch_lng = this.selectedBranch_lng;
      //   sub.unsubscribe();
      // }
    });
  }
}


@Component({
  selector: 'dialog-reply',
  templateUrl: 'dialog-reply.html',
  styleUrls: ['./testimony.component.scss']
})
export class DialogReply {
  // onSelect = new EventEmitter<{branch_name: string, lat: number, lng: number}>();


  adminReplyForm: FormGroup;
  details: any;
  // adminReply = new FormControl();

  constructor(
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private testimonyService: TestimonyService,
    public dialogRef: MatDialogRef<DialogReply>, @Inject(MAT_DIALOG_DATA) data){

  this.details = data;
  console.log(data);
  console.log(this.details);
  this.createForm()
  }
    // @Inject(MAT_DIALOG_DATA) public data: DialogData
    // ,@Inject(MAT_DIALOG_DATA) public xData: PurchaseProcComponent

  // onSelectClick(branch_name: string, lat: number, lng: number) {
  //   // console.log(`onSelectClick: ${branch_name} | ${lat} | ${lng}`)
  //   this.onSelect.emit({branch_name, lat, lng});
  // }
  //
  // onNoClick(): void {
  //   this.dialogRefAGM.close();
  //   this.data.animal = "OKININAM"
  // }
  // clickedMarker(label: string, index: number, lat: number, lng: number) {
  //   // console.log(`clicked the marker: ${label || index}`)
  //   this.data.animal = label;
  //   this.data.selectedBranch_name = label;
  //   this.data.selectedBranch_lat = lat;
  //   this.data.selectedBranch_lng = lng;
  //   this.onSelectClick(label, lat, lng)
  //   // this.xData.animal = label;
  // }

  get f() { return this.adminReplyForm.controls; }
  createForm() {
    this.adminReplyForm = this.formBuilder.group({
      adminreply: ['', Validators.required],
    })
  }

  onSubmit(){
    for (let v in this.adminReplyForm.controls) {
      this.adminReplyForm.controls[v].markAsTouched();
    }
    if (this.adminReplyForm.invalid) {
      this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: true,
        data: 'Incomplete information or invalid input provided!'
    });
    return;
    // alert('Please complete the registration form')


    }
    // console.log(this.f.adminreply.value.trim().replace(/\s\s+/g, ' '));
    let id = {
      id: this.details.row.id
    }
    let body = {
    }
    let data = {
      item_id: this.details.row.item_id,
      user_id: this.details.row.user_id,
      rate: this.details.row.rate,
      message: this.f.adminreply.value.trim().replace(/\s\s+/g, ' '),
      order_no: this.details.row.order_no,
      type : 2
    }

    this.testimonyService.updateTestimonyReply(id, body).subscribe(res => {
      console.log('updateStatusTestimony', res)
      // this.isSaving = false
    })
    this.testimonyService.createTestimonyReply(data).subscribe(res => {
      console.log('saveTestimony', res)
      // this.isSaving = false
    })
    this.dialogRef.close(this.details)
  }

}
