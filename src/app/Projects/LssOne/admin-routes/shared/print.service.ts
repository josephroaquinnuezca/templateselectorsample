import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PrintService {
  isPrinting = false;

  constructor(private router: Router, private route: ActivatedRoute) { }

  printDocument(no) {
    this.isPrinting = true;
    this.router.navigate(['./print-or', no])
  }
	printDocumentShippingBulk() {
    this.isPrinting = true;
    this.router.navigate(['./shipping-slip-bulk'])
  }
  onDataReady() {
    setTimeout(() => {

      window.print();
      this.isPrinting = false;
      this.router.navigate(['./admin']);
    });
  }

	onDataReady_shipslip() {
    setTimeout(() => {

			var css = '@page { size: A4 landscape; } @media print { .pagebreak { clear: both; page-break-after: always; }}',
			    head = document.head || document.getElementsByTagName('head')[0],
			    style = document.createElement('style');

			style.type = 'text/css';
			style.media = 'print';
			// if (style.stylesheet){
			//   style.stylesheet.cssText = css;
			// } else {
			//   style.appendChild(document.createTextNode(css));
			// }
			style.appendChild(document.createTextNode(css));
			// console.log(style);
			// return
			head.appendChild(style);

	    setTimeout(() => {
				window.print();
				this.isPrinting = false;
				this.router.navigate(['./admin']);
	    }, 1500);
    });
  }
}
