export interface ORHistory {
	id: number;
	item_name: string;
	order_record: string;
	quantity: number;
	or_no?: any;
	type: number;
	created_date: string;
}