export interface OrderRequest{
    id: number
    name: string
    service: string
    status: string
}
export interface PackageDetail{
    package_no: number
    itemtype_count: number
    type: string
}

export interface PaymentMethod{
    id: number
    pm_no: number
    name: string
    account_number: string
    img_pm: any
    status: number
}

export interface Courier{
    id: number
    name: string
    contact_no: number
    email_address: string
    img: any
    status: number
}
