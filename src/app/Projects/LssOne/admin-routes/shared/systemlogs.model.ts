export interface SystemLogs{
    name: string
		user_id: number
    module: string
    activity_type: string
    activity_details: string
    activity_date: string
}
