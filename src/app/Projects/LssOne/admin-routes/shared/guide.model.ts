export interface GuideModel {
    id: number;
    title: string;
    description: string;
    status: string;
}
