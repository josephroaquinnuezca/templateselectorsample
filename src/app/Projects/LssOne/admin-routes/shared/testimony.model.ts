export interface Testimony{
    id: number
    item_id: number
    user_id: number
    rate: number
    message: string
    status: string
    created_by: number
    order_no: number
    type: number
    imageList: any[]
    admin_reply: any[]
}
