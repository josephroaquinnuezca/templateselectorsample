import { first } from 'rxjs/operators';
import { SharedDataService } from './../../shared/shared-data.service';
import { Component, OnInit, ViewChild, ContentChild, HostListener, OnDestroy } from '@angular/core';
import { MatSort, MatTableDataSource, MatDialog, MatSortModule } from '@angular/material';
import { Transaction } from 'src/app/user-routes/shared/transaction.model';
import { OrderRequest } from '../shared/admin.model';
import { OrderDialogComponent } from '../order-dialog/order-dialog.component';
import { OrderService } from '../../_admin-services';
import { NoResultComponent } from '../items/lists/modals/no-result/no-result.component';
import * as $ from 'jquery';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';
import {TooltipPosition} from '@angular/material/tooltip';
import { PrintService } from '../shared/print.service';
import { OrderPrintProvider } from '../../_admin-provider';
import { OrderPackageService } from '../../_client-services';

import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-order-request',
  templateUrl: './order-request.component.html',
  styleUrls: ['./order-request.component.scss']
})
export class OrderRequestComponent implements OnInit {
	// order_package: any;

  // @ViewChild('editTemplate', { read: true, static: false }) sort: MatSort;
  @ViewChild('dataSort', { read: MatSort, static: true }) sort: MatSort;
  // @ViewChild(MatSort, {static: true}) sort: MatSort;
  reqList: OrderRequest[] = []
  reqListGenerate: OrderRequest[] = []
	selectedPrintActive: Boolean = false;

  dataSource: MatTableDataSource<OrderRequest>;
  // displayedColumns = ['shipping_status', 'id', 'order_no', 'receiver_name', 'concat_item', 'service', 'status', 'viewAction']; //username
  // displayedColumns = ['id', 'order_no', 'receiver_name', 'concat_item', 'service', 'status', 'viewAction']; //username
  displayedColumns = ['id', 'order_no', 'receiver_name', 'service', 'status', 'viewAction']; //username
  titlebranch: String = "Order Request"
	printValues: any[] = [];

  constructor(private orderPackageService: OrderPackageService,private datePipe: DatePipe, public printService: PrintService, private sharedData: SharedDataService, public dialog: MatDialog, private orderService: OrderService,
    private router: Router, private route: ActivatedRoute, private orderPrintProvider: OrderPrintProvider) { }
  contentHeight: number;
  samp: string;
  search_loading = false;
  paginator = {
    page: 1,
    limit: '20',
    key: '_all',
    module: 'request',
    type: '1'
  }

  pageItem = {
    first: 1,
    total: 0,
    pages: 0,
    has_next: 0,
    has_previous: 0,
    next_page: 0,
    previous_page: 0,
    offset: 0,
    current: 0
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // event.target.innerWidth;
    this.contentHeight = document.documentElement.clientHeight - 277.5
  }
  isObjectEmpty(Obj) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NoResultComponent, {
      panelClass: 'app-full-bleed-dialog-p-26',
      disableClose: false
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
    });
  }


  ngOnInit() {
		if(this.orderPrintProvider.printData){
			delete this.orderPrintProvider.printData;
		}
    this.contentHeight = document.documentElement.clientHeight - 277.5 //21 for the space below
    // console.log(this.contentHeight)
    this.search_loading = true;
    this.dataSource = new MatTableDataSource(this.reqList);
    this.dataSource.sort = this.sort;
    this.orderPagination(this.paginator);
    // this.dataSource = new MatTableDataSource(this.reqList);
    // this.dataSource.sort = this.sort;
  }

	ngOnDestroy(){
		// if(this.orderPrintProvider.printData){
		// 	delete this.orderPrintProvider.printData;
		// }
	}

  orderPagination(pgn: any) {
    const str = $.param(pgn);
    this.orderService.getAll(str)
      .subscribe(data => {
        this.search_loading = false;
        const d: any = data;
        if (!this.isObjectEmpty(d.data.items)) {
          this.pageItem = d.data.pagination;
          this.reqList = d.data.items;

					// this.dataSource = new MatTableDataSource(this.reqList);
          this.dataSource.data = this.reqList;
					// this.dataSource.sort = this.sort;

          console.log(this.dataSource.data)
        } else {
          // this.dataSource.data = [];
          this.openDialog();
        }
      });
  }

  selectLimit(value: any) {
    this.paginator.limit = value;
    this.orderPagination(this.paginator);
  }

  selectType(event: any) {
    this.paginator.type = event;
  }

  pageAction(action: string) {
    switch (action) {
      case 'first': {
        this.paginator.page = 1;
        break;
      }
      case 'prev': {
        this.paginator.page = this.pageItem.previous_page;
        break;
      }
      case 'next': {
        this.paginator.page = this.pageItem.next_page;
        break;
      }
      case 'last': {
        this.paginator.page = this.pageItem.pages;
        break;
      }
    }

    this.orderPagination(this.paginator);
  }

  applyFilter(key: string) {
    if (key) {
      this.paginator.page = 1;
      this.paginator.key = key;
      this.orderPagination(this.paginator);
    } else {
      this.paginator.page = 1;
      this.paginator.key = '';
      this.orderPagination(this.paginator);
    }
  }

  openView(row: any, option: string) {
    const dialogRef = this.dialog.open(OrderDialogComponent, {
		  panelClass: 'app-full-bleed-dialog-p-10',
      data: {
        option,
        row
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(res => {
      // if (res) {
        this.orderPagination(this.paginator);
      // }
    });
  }
	printShippingSlipBulk() {
    this.printService.printDocumentShippingBulk()
  }
	addToPrintSlip(event, row, i){
		if(event.checked){
			this.printValues[i] = row;
		}else{
			this.printValues.splice(i, 1);
		}
		console.log(event.checked);
		console.log(row);
		console.log(i);
		console.log(this.printValues);
		this.orderPrintProvider.printData = this.printValues;
		console.log("print provider: ", this.orderPrintProvider.printData)
	}
	// GENERATE REPORT DOWNLOAD
	downloadJSON2CSV(){
		console.log("Pinaka taas", this.reqListGenerate)
		// return
		if(this.printValues){
			for(let data of this.printValues){
				if(data){
					this.selectedPrintActive = true
				}
			}
			if(this.selectedPrintActive){
				console.log("taas-middle", this.printValues)
				for (let item of this.printValues){
					if(item){
						// this.getPackagesByOrderNo(item);
					}
				}
				this.generateCSVShippingSlip(this.printValues);
				this.selectedPrintActive = false
			}else{
				console.log("baba-middle", this.reqListGenerate)
				this.getData()
				this.selectedPrintActive = false
				// if(!this.getData()){
				// 	alert("nothing to generate!")
				// 	return
				// }
			}
		}
		else{
			console.log("Pinaka baba", this.reqListGenerate)
			this.getData()
			this.selectedPrintActive = false
			// if(!this.getData()){
			// 	alert("nothing to generate!")
			// 	return
			// }
		}
  }

	generateCSVShippingSlip(value) {
		console.log("This animal content should not be called!");
		let fileName = ''
		let csv = '';
		// let counter = 0;
		let item_content = '';
		let date = this.datePipe.transform(new Date(), 'yyyy-MM-dd-HH-mm-ss')
		fileName = `ShippingSlipCSV_${date}.csv`

	  let columnNames = ["Order #", "Customer", "Item", "Items Value", "Shipping Value"];
	  let header = columnNames.join(',');

	  csv = header;
	  // csv += '\r\n';
	  let total_value = 0;
		let total_ship_fee = 0;
		csv += '\r\n';
	  // csv += [``, ``, ``, ``, ``].join(',');
	  // csv += '\r\n';
	  for (let c of value) {
			if(c){
				total_value = total_value + (c.total_amt - c.ship_fee);
				total_ship_fee = total_ship_fee + c.ship_fee;
				// console.log('--------------------------------------------------------');
				for (let item_count in c.order_items){
					let itemCounter: number = parseInt(item_count, 10);
					if(c.order_items.length > 1){
						if(itemCounter == 0){
							item_content = "\"" + c.order_items[item_count].item_name + " - " + c.order_items[item_count].quantity + "x";
						}
						if(itemCounter == (c.order_items.length - 1)){
							item_content += "\r" + c.order_items[item_count].item_name + " - " + c.order_items[item_count].quantity + "x\"";
						}
						if(itemCounter != 0 && itemCounter != (c.order_items.length - 1)){
							item_content += "\r" + c.order_items[item_count].item_name + " - " + c.order_items[item_count].quantity + "x";
						}
					}
					else{
						item_content = "" + c.order_items[item_count].item_name + " - " + c.order_items[item_count].quantity + "x";
					}


					// console.log('order_items', c.order_items.length);
					// console.log('itemCounter', itemCounter);
					// console.log('item_count', item_count);
					// counter++;
				}
				csv += [c.order_no, c.name, item_content, `₱ `+`${c.total_amt - c.ship_fee}.00`, `₱ `+`${c.ship_fee}.00`].join(',');
				csv += '\r\n';
			}
	  }
	  csv += [``, ``, ``, ``, ``].join(',');
	  csv += '\r\n';
	  csv += '\r\n';
	  csv += [`SUB TOTAL AMOUNT`, ``, ``, `₱ `+`${total_value}.00`,`₱ `+`${total_ship_fee}.00`].join(',');
	  csv += '\r\n';
	  csv += '\r\n';
	  csv += [`TOTAL SALES AMOUNT`, ``, ``, ``,`₱ `+`${total_value + total_ship_fee}.00`].join(',');
	  csv += '\r\n';

		// this.reportTabSelectedOption == 'salesReport' ? fileName = `MonthlySalesReport_${this.defaultDateSearch}.csv` : fileName = `InventoryReport_${this.defaultDateSearch}.csv`;

		// let monthTitle = this.datePipe.transform(this.searchSalesReport.where_from, 'YYYYY MM, dd')+` to `+ this.datePipe.transform(this.searchSalesReport.where_to, 'YYYYY MM, dd')

		// if(this.reportTabSelectedOption == 'salesReport'){
		//   let columnNames = ["Order #", "Transaction Date", "Amount"];
		//   let header = columnNames.join(',');
		//
		//   csv = header;
		//   csv += '\r\n';
		//   let total_value = 0;
		//   csv += [``, ``, ``].join(',');
		//   csv += '\r\n';
		//   for (let c of this.reqListGenerate) {
		//     total_value = total_value + c.value
		//     csv += [c.order_no, c.name, `₱ `+`${c.value}.00`].join(',');
		//     csv += '\r\n';
		//   }
		//   csv += [``, ``, ``].join(',');
		//   csv += '\r\n';
		//   csv += [`TOTAL SALES AMOUNT`, ``, `₱ `+`${total_value}.00`].join(',');
		//   csv += '\r\n';
		// }else {
		//   let columnNames = ["Item Name", "Item Price", "Current Stock", "Production Amount"];
		//   let header = columnNames.join(',');
		//
		//   csv = header;
		//   csv += '\r\n';
		//   let total_value = 0;
		//   csv += [``, ``, ``].join(',');
		//   csv += '\r\n';
		//   for (let c of this.reqListGenerate) {
		//     total_value = total_value + c.product
		//     csv += [c.name, `₱ `+`${c.item_price}.00`, c.current_stock, `₱ `+`${c.product}.00`].join(',');
		//     csv += '\r\n';
		//   }
		//   csv += [``, ``, ``].join(',');
		//   csv += '\r\n';
		//   csv += [`TOTAL PRODUCTION AMOUNT`, ``, ``, `₱ `+`${total_value}.00`].join(',');
		//   csv += '\r\n';
		// }
		// this.this.reqListGenerate.map(c => {
		//   console.log(c.ordernumber)
		//   // csv += [c["ordernumber"], c["name"], c["value"]].join(',');
		//   // csv += '\r\n';
		// })

		var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });

		var link = document.createElement("a");
		if (link.download !== undefined) {
			var url = URL.createObjectURL(blob);
			link.setAttribute("href", url);
			link.setAttribute("download", fileName);
			document.body.appendChild(link);
			link.click();
			document.body.removeChild(link);
		}
	}

	getData() {
	this.reqListGenerate = [];
	this.orderService.getORShippingSlipReadyAll()
		.subscribe(data => {
			const d: any = data;
			if (!this.isObjectEmpty(d.data.items)) {
				this.reqListGenerate = d.data.items;
				console.log(this.reqListGenerate);

				for (let item of this.reqListGenerate){
					// this.getPackagesByOrderNo(item);
				}

				this.generateCSVShippingSlip(this.reqListGenerate);
			} else {
				alert("nothing to generate!");
				// this.router.navigate(['./admin']);
			}
		});
	}

	// getPackagesByOrderNo(item){
	// 	console.log(item);
	// 	var x = item.order_no;
	// 	this.orderPackageService.fetchOrderPackage(x).toPromise().then(res => {
	// 		const res_data: any = res;
  //
	// 		if (res_data.status.error === false ) {
	// 			console.log(res_data);
	// 			this.order_package = res_data.data;
	// 			item.package_detail = this.order_package;
	// 			console.log("WITH Package_Detail: ", item);
	// 		}
	// 	});
	// }

}
