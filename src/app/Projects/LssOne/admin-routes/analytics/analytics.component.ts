import { Component, OnInit } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { single } from './data';

import { ReportService } from '../../_admin-services';

import {FormControl} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';

import * as $ from 'jquery';
import * as moment from 'moment';

import {default as _rollupMoment, Moment} from 'moment';

const momentDate = _rollupMoment || moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY/MM/DD',
  },
  display: {
    dateInput: 'YYYY/MM/DD',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class AnalyticsComponent implements OnInit {
  dateToday = new Date();
  //use in naming the reportfile & setting default date for "Sales Tab": salesCount & transactionCount
  defaultDateSearch = this.datePipe.transform(this.dateToday, 'yyyy-MM-dd')
  //defailt date of "CSV TAB":csv report
  reportDefaultDateSearchFrom = this.datePipe.transform(this.dateToday, 'yyyy-MM-dd')
  reportDdefaultDateSearchTo = this.datePipe.transform(this.dateToday, 'yyyy-MM-dd')
  //setting default date for "Sales Tab": productCount
  defaultYearSearch = this.datePipe.transform(this.dateToday, 'yyyy')
  defaultMonthSearch = this.datePipe.transform(this.dateToday, 'MM')

  //selection default value
  salesTabSelectedOption = "salesCount";
  inventoryTabSelectedOption = "inventoryCount";
  reportTabSelectedOption = "salesReport";

  date = new FormControl(new Date());
  date_To = new FormControl('');
  date_From = new FormControl('');
  reportDate_To = new FormControl(momentDate());
  reportDate_From = new FormControl(momentDate());

  search = {
    type: '1',
    where_from: this.defaultDateSearch,
    where_to: this.defaultDateSearch,
    event : 'salesCount'
  };

  searchInventory = {
    event : 'inventoryCount'
  }

  searchProduct = {
    type: '1',
    param_year: this.defaultYearSearch,
    param_month: this.defaultMonthSearch
  }

  searchSalesReport = {
    type: '1',
    where_from: moment(this.reportDefaultDateSearchFrom).startOf('month').format('YYYY-MM-DD').toString(),
    where_to: moment(this.reportDdefaultDateSearchTo).endOf('month').format('YYYY-MM-DD').toString(),
    event : 'salesReport'
  };

  searchInventoryReport = {
    event : 'inventoryReport'
  }
  //Data Storage property
  //inventory
  inventoryData: any[];
  //sales
  single: any[];
  productCountData: any[];
  //report
  salesReportData: any[]
  inventoryReportData: any[]

  //title
  titlebranch: string = "Sale Analytics";

  // options
  view: any[] = [900, 400];
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Items';
  xAxisLabeldate = 'Date';
  showYAxisLabel = true;

  yAxisLabel = 'Item count';
  //for inventory
  yAxisLabelInventoryCount = 'Total Item count';
  yAxisLabelProductCost = 'Total Product Cost';
  //for sales
  yAxisLabelSalesCount = 'Total Sale Amount'
  yAxisLabelTransactionCount = 'Total Transaction Count'
  yAxisLabelProductCount = 'Total Product Count'

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  // end option

  constructor(private datePipe: DatePipe, private reportService: ReportService) { }

  ngOnInit() {
    // console.log(single);
    // Object.assign(this, { single
    //Inventory Tab
    this.getInventoryCount();
  }
  // SELECTED OPTION HERE INVENTORY , SALES, CSV --------------------------------------------------
  selectSalesTabSelectedOption(event: any) {
    this.salesTabSelectedOption = event;
    (event === 'salesCount') ? this.search.event = 'salesCount' : this.search.event = event;
    // console.log(this.salesTabSelectedOption);
    (event === 'productCount') ? this.getProductCount() : this.getTransactionCount();
  }
  selectInventoryTabSelectedOption(event: any) {
    this.inventoryTabSelectedOption = event;
    (event === 'inventoryCount') ? this.searchInventory.event = 'inventoryCount' : this.searchInventory.event = event;
    // console.log(this.inventoryTabSelectedOption);
    this.getInventoryCount();
  }
  selectReportTabSelectedOption(event: any) {
    this.reportTabSelectedOption = event;
    (event === 'salesReport') ? this.searchSalesReport.event = 'salesReport' : this.searchInventory.event = event;
    (event === 'inventoryReport') ? this.getInventoryReportMonthly() : this.getSalesReportMonthly();
    // console.log(this.reportTabSelectedOption);
  }
  // END SELECTED OPTION -----------------------------------------------------------------------

  // SEARCH CONTROLS PERIOD OF TIME, DATE ----------------------------------------------------------
  // FOR SALES TAB ONLY
  selectType(event: any) {
    (event === '0') ? this.search.type = '1' : this.search.type = event;
    (event === '0') ? this.searchProduct.type = '1' : this.searchProduct.type = event;
  }
  date_FromChange(value: any) {
    this.search['where_from'] = moment(new Date(value)).format('YYYY-MM-DD');
    this.searchProduct['param_year'] = moment(new Date(value)).format('YYYY');
    // console.log(this.search);
  }

  date_ToChange(value: any) {
    this.search['where_to'] = moment(new Date(value)).format('YYYY-MM-DD');
    this.searchProduct['param_month'] = moment(new Date(value)).format('MM');
    // console.log(this.search);
  }
  searchSelectType(){
    (this.salesTabSelectedOption === 'productCount') ? this.getProductCount() : this.getTransactionCount();
  }
  // For CSV TAB ONLY
  //this select option is hidden using "Display: none"
  selectTypeReport(event: any) {
    (event === '0') ? this.searchSalesReport.type = '1' : this.searchSalesReport.type = event;
  }
  reportDate_FromChange(value: any) {
    this.searchSalesReport['where_from'] = moment(new Date(value)).format('YYYY-MM-DD');
    // console.log(this.searchSalesReport);
  }
  reportDate_ToChange(value: any) {
    this.searchSalesReport['where_to'] = moment(new Date(value)).format('YYYY-MM-DD');
    // console.log(this.searchSalesReport);
  }
  //Month - Year Only datepicker
  chosenYearHandlerReportDate_FromChange(normalizedYear: Moment) {
    const ctrlValue = this.reportDate_From.value;
    ctrlValue.year(normalizedYear.year());
    this.reportDate_From.setValue(ctrlValue);
    // console.log(ctrlValue)
  }
  chosenMonthHandlerReportDate_FromChange(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.reportDate_From.value;
    ctrlValue.month(normalizedMonth.month());
    this.reportDate_From.setValue(moment(ctrlValue).startOf('month'));
    // console.log(ctrlValue)
    this.searchSalesReport['where_from'] = moment(ctrlValue).startOf('month').format('YYYY-MM-DD').toString()
    // console.log(moment(ctrlValue).startOf('month').format('YYYY-MM-DD').toString())
    this.getSalesReportMonthly();
    datepicker.close();
  }
  chosenYearHandlerReportDate_ToChange(normalizedYear: Moment) {
    const ctrlValue_2 = this.reportDate_To.value;
    ctrlValue_2.year(normalizedYear.year());
    this.reportDate_To.setValue(ctrlValue_2);
    // console.log(ctrlValue_2)
  }
  chosenMonthHandlerReportDate_ToChange(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue_2 = this.reportDate_To.value;
    ctrlValue_2.month(normalizedMonth.month());
    this.reportDate_To.setValue(moment(ctrlValue_2).endOf('month'));
    // console.log(ctrlValue_2)
    this.searchSalesReport['where_to'] = moment(ctrlValue_2).endOf('month').format('YYYY-MM-DD').toString()
    // console.log(moment(ctrlValue_2).endOf('month').format('YYYY-MM-DD').toString())
    this.getSalesReportMonthly();
    datepicker.close();
  }
  // END SEARCH CONTROLS ---------------------------------------------------------------------------

  // MAIN FUNCTION TO CALL ON API -----------------------------------------------------------------
  getTransactionCount() {
    const str = $.param(this.search);
    this.reportService.getTransactionCountByFilter(str).subscribe( res => {
      const d: any = res;
      this.single = d.data;
      // console.log(this.single);
    });
  }
  getProductCount() {
    const str = $.param(this.searchProduct);
    this.reportService.getProductCountByFilter(str).subscribe( res => {
      const d: any = res;
      this.productCountData = d.data.items;
      // console.log(this.productCountData);
    });
  }
  getInventoryCount(){
    const str = $.param(this.searchInventory);
    this.reportService.getInventory(str).subscribe( res => {
      const d: any = res;
      this.inventoryData = d.data;
      // console.log(this.inventoryData);
    });
  }
  // CSV REPORT SECTION -----------------------
  //SALES
  getSalesReportMonthly(){
    const str = $.param(this.searchSalesReport);
    this.reportService.getTransactionCountByFilter(str).subscribe( res => {
      const d: any = res;
      this.salesReportData = d.data;
      // console.log(this.salesReportData);
    });
  }
  //SALES
  getInventoryReportMonthly(){
    const str = $.param(this.searchInventoryReport);
    this.reportService.getInventory(str).subscribe( res => {
      const d: any = res;
      this.inventoryReportData = d.data;
      // console.log(this.inventoryReportData);
    });
  }
  // END MAIN FUNCTION TO CALL ON API -----------------------------------------------------------------


  // REPORT GENERATION FUNCTIONS
  generateCSVReport() {
  (this.reportTabSelectedOption === 'salesReport') ? this.downloadJSON2CSV(this.salesReportData) : this.downloadJSON2CSV(this.inventoryReportData);
  }
  downloadJSON2CSV(objArray){
    let fileName = ''
    let csv = '';
    this.reportTabSelectedOption == 'salesReport' ? fileName = `MonthlySalesReport_${this.defaultDateSearch}.csv` : fileName = `InventoryReport_${this.defaultDateSearch}.csv`;
    // let monthTitle = this.datePipe.transform(this.searchSalesReport.where_from, 'YYYYY MM, dd')+` to `+ this.datePipe.transform(this.searchSalesReport.where_to, 'YYYYY MM, dd')
    if(this.reportTabSelectedOption == 'salesReport'){
      let columnNames = ["Order #", "Transaction Date", "Amount"];
      let header = columnNames.join(',');

      csv = header;
      csv += '\r\n';
      let total_value = 0;
      csv += [``, ``, ``].join(',');
      csv += '\r\n';
      for (let c of objArray) {
        total_value = total_value + c.value
        csv += [c.order_no, c.name, `₱ `+`${c.value}.00`].join(',');
        csv += '\r\n';
      }
      csv += [``, ``, ``].join(',');
      csv += '\r\n';
      csv += [`TOTAL SALES AMOUNT`, ``, `₱ `+`${total_value}.00`].join(',');
      csv += '\r\n';
    }else {
      let columnNames = ["Item Name", "Item Price", "Current Stock", "Production Amount"];
      let header = columnNames.join(',');

      csv = header;
      csv += '\r\n';
      let total_value = 0;
      csv += [``, ``, ``].join(',');
      csv += '\r\n';
      for (let c of objArray) {
        total_value = total_value + c.product
        csv += [c.name, `₱ `+`${c.item_price}.00`, c.current_stock, `₱ `+`${c.product}.00`].join(',');
        csv += '\r\n';
      }
      csv += [``, ``, ``].join(',');
      csv += '\r\n';
      csv += [`TOTAL PRODUCTION AMOUNT`, ``, ``, `₱ `+`${total_value}.00`].join(',');
      csv += '\r\n';
    }
    // this.objArray.map(c => {
    //   console.log(c.ordernumber)
    //   // csv += [c["ordernumber"], c["name"], c["value"]].join(',');
    //   // csv += '\r\n';
    // })

    var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });

    var link = document.createElement("a");
    if (link.download !== undefined) {
      var url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", fileName);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
  //----------------------------
  onSelect(event) {
    console.log(event);
  }

  getData() {

  }

  tabChange(event) {
    // console.log(event);
    if (event.index === 1) {
      setTimeout(() => {
        this.getTransactionCount();
      });
    }
    else if (event.index === 2) {
      setTimeout(() => {
        // console.log(this.searchSalesReport.where_from);
        // console.log(this.searchSalesReport.where_to);
        this.reportDate_From.setValue(moment(this.reportDate_From.value).startOf('month'));
        this.reportDate_To.setValue(moment(this.reportDate_To.value).endOf('month'));
        this.getSalesReportMonthly();
      });
    }
    else {
      //Inventory Tab
      setTimeout(() => {
        this.getInventoryCount();
      });
    }
  }
}
