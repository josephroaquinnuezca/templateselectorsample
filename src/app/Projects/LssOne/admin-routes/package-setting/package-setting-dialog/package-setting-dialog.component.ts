import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material/snack-bar';
// import { UserService } from '../../shared/user.service';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { SettingService } from '../../../_admin-services';
import {ErrorStateMatcher} from '@angular/material/core';
import { SuccessMesageDialogComponent } from 'src/app/static-pages/success-mesage-dialog/success-mesage-dialog.component';


@Component({
  selector: 'app-package-setting-dialog',
  templateUrl: './package-setting-dialog.component.html',
  styleUrls: ['./package-setting-dialog.component.scss']
})

export class PackageSettingDialogComponent implements OnInit {

  selectDestination: any[] = ['NCR', 'LUZON', 'VIS / MIN', 'ISLANDS'];

  packageForm: FormGroup;

  itemList: any[] = [];
	packageItems: any[] = [];
  itemsArray: any[] = [];
  destination: any[] = [];
  itemsObject: any[] = [];

  matcher = new MyErrorStateMatcher();
  viewValue: any;
  itemsID: any[] = [];

	isSpinner: boolean = false;
	isSuccessProcess: boolean = false;

	hasError: boolean = false;
	hasErrorMessage: string = '';

  option: any;
  details: any;

  constructor(public dialog: MatDialog , private settingService: SettingService, private formBuilder: FormBuilder, public dialogRef: MatDialogRef<PackageSettingDialogComponent>, @Inject(MAT_DIALOG_DATA) data, private _snackBar: MatSnackBar) {
    this.option = data.option
    if (this.option != 'add') {
      console.log('CONSTRUCTOR: ', data);
      this.details = data.details;
    }
  }
  ngOnInit() {
    if (this.option != 'add') {
      console.log('ONINT: ', this.details);
    }else{
      this.createForm();
      this.getItems();
    }
  }
  createForm(){
    this.packageForm = this.formBuilder.group({
      items : ['',[Validators.required, Validators.pattern('valid')]],
    });
  }
  getItems(){
    this.settingService.getAllItems()
    .pipe(first())
    .subscribe(
      res => {
        const data: any = res;
        console.log(data);
        for(let item of data.data.items){
          this.itemList.push({id: item.id, item_name: item.name});
        }
        console.log(data);
        console.log(this.itemList);
      },
      error => {
        console.log(error);
        // this.submitted = false;
        // console.log();
      });
  }
  getItemSelected(eventValue){
    this.itemsArray = [];
    console.log(eventValue);
    let e = eventValue.selected;
    if(e.length !=0){
      this.viewValue = e[0].viewValue;
      this.itemsID = e;
      console.log(this.itemsArray);
      // console.log('id', e.value);
      // console.log('id', e[0].value);
      // console.log('item name', viewValue);
    }
    else{
      this.itemsID = e; //to clear the existing;
      console.log('Please select items.');
    }
  }
  onSubmit(){
    console.log(this.itemsArray);
    console.log(this.destination);
		console.log(this.itemsID);
    // itemID.value, itemID.viewValue
    if(this.itemsID.length == 0){
			console.log('Check itemsID fields.');
			this.hasError = true;
			this.hasErrorMessage = 'Please select items to add on package.';
			return
		}
    if(this.destination.length != 4){
			if (this.destination.length > 0) {
				console.log('Check destination fields.');
				this.hasError = true;
				this.hasErrorMessage = 'Please complete shipping destination fee.';
				return
			}else{
				console.log('Check destination fields.');
				this.hasError = true;
				this.hasErrorMessage = 'Please input shipping destination fee.';
				return
			}
		}
    if(this.itemsArray.length != 0){
			this.hasError = false;
      this.itemsObject = [];
      var i = 0
      for (let itemID of this.itemsID){
        this.itemsObject[i] = {item_id: itemID.value, itemqty_count: this.itemsArray[i] , shipping_price_per_item: 1 }
        i++;
      }
      console.log(this.itemsObject);
      console.log(this.destination);
			for (let itemObject of this.itemsObject){
        if (!itemObject.itemqty_count || itemObject.itemqty_count == '') {
					this.hasError = true;
					this.hasErrorMessage = 'Please complete details for number of items';
					return;
        }
      }

      let data: any = {
        data: this.itemsObject,
        ncr: this.destination[0],
        luzon: this.destination[1],
        vis_min: this.destination[2],
        island: this.destination[3]
      }

      console.log(data);
      this.settingService.createPackageTemplate(data).subscribe(res => {
        // alert('Successfully Created Template!');
        const dialogRef = this.dialog.open(SuccessMesageDialogComponent, {
          height: '250px',
          width: '350px',
          data: 'Successfully Created Template!'
         });
        // this.pass1 = '';
        // this.pass2 = '';
        // this.router.navigate(['user/signin']);
        return;
      });
    }else{
			this.hasError = true;
			this.hasErrorMessage = 'Please input number of items to add on package.';
      console.log('Check Items fields.');
    }
  }
}
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
