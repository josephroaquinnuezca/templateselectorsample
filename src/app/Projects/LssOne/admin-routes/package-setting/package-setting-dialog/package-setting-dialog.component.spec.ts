import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackageSettingDialogComponent } from './package-setting-dialog.component';

describe('PackageSettingDialogComponent', () => {
  let component: PackageSettingDialogComponent;
  let fixture: ComponentFixture<PackageSettingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackageSettingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageSettingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
