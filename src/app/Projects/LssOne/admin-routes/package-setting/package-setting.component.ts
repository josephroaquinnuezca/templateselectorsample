import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatSort, MatDialog, MatSortModule } from '@angular/material';
import { PackageSettingDialogComponent } from './package-setting-dialog/package-setting-dialog.component';
import { SettingService } from '../../_admin-services';
import { PackageDetail } from '../shared/admin.model';


@Component({
  selector: 'app-package-setting',
  templateUrl: './package-setting.component.html',
  styleUrls: ['./package-setting.component.scss']
})
export class PackageSettingComponent implements OnInit {

  // @ViewChild('editTemplate', { read: true, static: false }) sort: MatSort;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
	package_template: any;
  dataSource: MatTableDataSource<PackageDetail>;
  displayedColumns = ['package_no', 'itemtype_count', 'type', 'viewAction']; //username
	contentHeight: number;

  constructor(private settingService: SettingService, private router: Router, public dialog: MatDialog, private route: ActivatedRoute) { }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // event.target.innerWidth;
    this.contentHeight = document.documentElement.clientHeight - (179.5 + 64 + 20)
  }
  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.package_template);
    this.dataSource.sort = this.sort;
    this.getData()
  }

  getData(){
    this.settingService.getpackageTemplate().subscribe(res => {
      const packageTemplate: any = res;
      console.log(res);
      if (packageTemplate.status.error === false ) {
        this.package_template = packageTemplate.data;
        this.dataSource.data = this.package_template.check_package;
        console.log(this.package_template);
      }
    });
  }

  openDialog(option: any, details: any) {
		// console.log(email)
    const dialogRef = this.dialog.open(PackageSettingDialogComponent, {
		  panelClass: 'app-full-bleed-dialog-p-10',
      data: {
				option,
				details
			}
      // disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
			// console.log(result);
      if (result) {
        // this.userService.signout();
				// this.router.navigate(['/user/signin']);
      }
    });
  }

}
