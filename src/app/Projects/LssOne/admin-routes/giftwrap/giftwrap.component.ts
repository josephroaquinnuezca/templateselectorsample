import { first } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Component, OnInit, ViewChild, ContentChild, HostListener, Inject, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GiftwrapModel } from '../shared/giftwrap.model';
import { SettingService } from '../../_admin-services';


import { MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';

@Component({
  selector: 'app-giftwrap',
  templateUrl: './giftwrap.component.html',
  styleUrls: ['./giftwrap.component.scss']
})
export class GiftwrapComponent implements OnInit {

	@ViewChild('editTemplate', { read: true, static: false }) sort: MatSort;
  contentHeight: number;
  reqList: GiftwrapModel[] = [];
  dataSource: MatTableDataSource<GiftwrapModel>;
  displayedColumns = ['id', 'name', 'description', 'viewAction'];
  search_loading = false;

  constructor(private settingService: SettingService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, public dialog: MatDialog) { }

	@HostListener('window:resize', ['$event'])
  onResize(event) {
    // event.target.innerWidth;
    this.contentHeight = document.documentElement.clientHeight - 277.5
  }

  ngOnInit() {
		this.contentHeight = document.documentElement.clientHeight - 277.5
		this.getData();
		this.dataSource = new MatTableDataSource(this.reqList);
		this.dataSource.sort = this.sort;
  }


  getData() {
    this.settingService.getAllGiftwrap()
    .subscribe( data => {
      this.search_loading = false;
      const d: any = data;
      if (!this.isObjectEmpty(d.data.items)) {
        this.reqList = d.data.items;
        // console.log( this.reqList);
        this.dataSource.data = this.reqList;
      } else {
        // alert("No Data Found!");
        const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
          data: {
            Content: 'No data found'
          }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result: ${result}`);
        });

        this.reqList = [];
        this.dataSource.data = this.reqList;
      }
    });
  }

	addWrap(option) {
    const dialogRef = this.dialog.open(DialogGiftwrap, {
			panelClass: 'app-full-bleed-dialog',
      data : {
				option
			}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      this.getData();
    });
  }

  editWrap(data, option) {
    const dialogRef = this.dialog.open(DialogGiftwrap, {
		  panelClass: 'app-full-bleed-dialog',
      data : {
				data,
				option
			}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
			console.log(result);
      this.getData();
    });
  }

	viewWrap(data: any, option: any): void {
    const dialogRef = this.dialog.open(DialogGiftwrap, {
		  panelClass: 'app-full-bleed-dialog',
      data: {
        data,
        option
      }
    });
  }

	delete(id: any) {
    if (confirm('Are you sure you want to delete this item?')) {
      this.settingService.deleteGiftwrap(id).pipe(first()).subscribe(data => {
				console.log(data)
        const d: any = data;
        this.getData();
      });
    }
  }

  isObjectEmpty (Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

}


@Component({
  selector: 'giftwrap-dialog',
  templateUrl: 'giftwrap-dialog.html',
})
export class DialogGiftwrap {

  mform: FormGroup;
	postData: any;

  submitted = false;
  details: any;
	// image variables
  @ViewChild('file', { static: false }) file;
  public files: Set<File> = new Set();
  strUrl: any[] = [];
	hasFiles:boolean = false;
  currentImages: any[] = [];
	option: any;

  constructor(private formBuilder: FormBuilder, private settingService: SettingService, public dialogRef: MatDialogRef<DialogGiftwrap>, @Inject(MAT_DIALOG_DATA) data) {
		this.details = data.data;
		this.option = data.option;
  }

	ngOnInit() {
		if(this.option == 'edit' || this.option == 'view'){
			this.currentImages = this.details.images;
		}
    console.log(this.details);
		console.log(this.currentImages);
    this.mform = this.formBuilder.group({
      name: [(this.details) ? this.details.name : '', Validators.required],
      description: [(this.details) ? this.details.description : '']
    });
  }

  get f() { return this.mform.controls; }

  onSubmit(){
    for (let v in this.mform.controls) {
      this.mform.controls[v].markAsTouched();
    }
    if (this.mform.invalid) {
      alert('Incomplete information or invalid input provided!')
      return
    }
		this.postData = {
      name: this.f.name.value,
      description: this.f.description.value,
			file_type: 'giftwrap_images',
			has_file: true,
    };

    if (this.details) {
			if(this.details.images){
				let Ids = this.currentImages.map(function (data) {
					return data.id;
				});

				let str = Ids.join(', ');
				console.log("current images tobe save", str);
				this.postData['new_id_image'] = str;
			}
			// return
      this.edit(this.postData);
    } else {
      this.create(this.postData);
    }
  }
  edit(details) {
    this.settingService.updateGiftwrap(details, this.files, this.details.id)
    .pipe(first())
    .subscribe(
      data => {
				// console.log(data);
        this.submitted = false;
				var result = {
					data: data,
					details: details
				}
        this.dialogRef.close(result);
      },
      error => {
        this.submitted = false;
        // console.log();
      });
  }
  create(details) {
    this.settingService.createGiftwrap(details, this.files)
    .pipe(first())
    .subscribe(
      data => {
				this.submitted = false;
				var result = {
					data: data,
					details: details
				}
        this.dialogRef.close(result);
      },
      error => {
        this.submitted = false;
        // console.log();
      });
  }
	addFiles() {
    this.file.nativeElement.click();
  }
	onFilesAdded(event) {
		let files = event.target.files;
    if (files) {
      for (let file of files) {
				console.log(file);
        let reader = new FileReader();
        reader.onload = (e: any) => {
					//called files here to fix the indexing issue. Must be applied to other function similar to this one
					this.files.add(file);
          this.strUrl.push({'name': file.name, 'file': e.target.result});
					this.hasFiles = true;
        }
        reader.readAsDataURL(file);
      }
    }
	}
	checkFiles(){
		console.log("strFiles", this.files);
		console.log("strURL", this.strUrl);
	}
	removeaddesImg(i) {
		//Use forEach + delete instead of splice. Splice is not working for SET() array objects.
		//Must be applied to other function similar to this one
		this.file.nativeElement.value = null;
		var counter = 0
		this.files.forEach(point => {
			if (i === counter) {
				console.log(point);
			  this.files.delete(point);
			}
			console.log(counter);
			counter++
		});
		// delete this.file.nativeElement.files[i];
		this.strUrl.splice(i, 1);
	}
	removeCurrentImg(i) {
    this.currentImages.splice(i, 1);
		console.log(this.currentImages);
  }
}
