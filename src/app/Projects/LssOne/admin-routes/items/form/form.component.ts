import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Inject, ViewChild, NgZone } from '@angular/core';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

import { ItemCategService, ItemsService, UserTypeService } from '../../../_admin-services';
import { first, take } from 'rxjs/operators';
import { id } from '@swimlane/ngx-charts/release/utils';

import { HttpEventType, HttpErrorResponse } from '@angular/common/http';

import * as $ from 'jquery';
import { forkJoin } from 'rxjs';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})

export class ItemsFormComponent implements OnInit {

  @ViewChild('file', { static: false }) file;
  public files: Set<File> = new Set();

  @ViewChild('inst_file', { static: false }) inst_file;
  public inst_files: Set<File> = new Set();

  mform: FormGroup;
  itemCategory = [];
  details: any;
  strUrl: any[] = [];
  inst_strUrl: any[] = [];
  currentImages: any[] = [];
  inst_currentImages: any[] = [];
  isView = false;
  isSubmit = false;
  itemSlug = '';
  inst_isView: boolean;
  constructor(
    private _ngZone: NgZone,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ItemsFormComponent>,
    private itemCategService: ItemCategService,
    private itemsService: ItemsService,
    private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) data) {
    this.details = data;

    if (data) {

      this.itemSlug = data.item_slug;

      if (data.action === 'view') {
        this.isView = true;
				this.inst_isView = true;
      }
			else{
        this.isView = false;
				this.inst_isView = false;
			}
    }
  }
  @ViewChild('autosizeDescription', { static: false }) autosizeDescription: CdkTextareaAutosize;
  @ViewChild('autosizeRemarks', { static: false }) autosizeRemarks: CdkTextareaAutosize;

  progress: any;
  canBeClosed = true;
  primaryButtonText = '';
  showCancelButton = true;
  uploading = false;
  uploadSuccessful = false;
  submitted = false;
  inst_uploading = false;
  inst_uploadSuccessful = false;

  ngOnInit() {
    if (this.details) {
      this.getItem(this.details.id);
    }

    this.form();
    this.getItemCateg();
    this.primaryButtonText = (this.details) ? 'Edit Item' : 'Add Item';

  }
  triggerResizeDescription() {
    // Wait for changes to be applied, then trigger textarea resize.
    this._ngZone.onStable.pipe(take(1))
        .subscribe(() => this.autosizeDescription.resizeToFitContent(true));
  }
  triggerResizeRemarks() {
    // Wait for changes to be applied, then trigger textarea resize.
    this._ngZone.onStable.pipe(take(1))
        .subscribe(() => this.autosizeRemarks.resizeToFitContent(true));
  }

  form() {
    // console.log('form', this.details);
    this.mform = this.formBuilder.group({
      itemcateg_id: [(this.details) ? this.details.itemcateg_id : 1, Validators.required],
      item_no: [(this.details) ? this.details.item_no : '', Validators.required],
      name: [(this.details) ? this.details.name : '', Validators.required],
      description: [(this.details) ? this.details.description : '', Validators.required],
      min_stock: [(this.details) ? this.details.min_stock : '', Validators.required],
      current_stock: [(this.details) ? this.details.current_stock : '', Validators.required],
      price: [(this.details) ? this.details.price : '', Validators.required],
      length: [(this.details) ? this.details.length : '', Validators.required],
      width: [(this.details) ? this.details.width : '', Validators.required],
      height: [(this.details) ? this.details.height : '', Validators.required],
      weight: [(this.details) ? this.details.weight : '', Validators.required],
      remarks: [(this.details) ? this.details.remarks : '', []],
      // item_slug: [(this.details) ? this.details.item_slug : '', Validators.required],
      // (this.details) ? this.details.remarks : '', Validators.required for remarks
      shipping_fee_ncr: ['', []],
      shipping_fee_luz: ['', []],
      shipping_fee_vis: ['', []],
      shipping_fee_min: ['', []],
      has_file: [true]
    });
  }

  get f() { return this.mform.controls; }

  onFilesAdded() {
    // console.log('SHiy')
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (let key in files) {
      if (!isNaN(parseInt(key))) {
        this.files.add(files[key]);
        let reader = new FileReader();
        reader.onload = (event: any) => {
          this.strUrl.push(event.target.result);
        };
        reader.readAsDataURL(files[key]);
      }
    }
  }

  make_slug(event) {
    function s(d) {
      return d.replace(/\s+/g, '-').toLowerCase();
    }
    const name   = (this.f.name.value) ? s(this.f.name.value) : '';
    const item_no   = (this.f.item_no.value) ? s(this.f.item_no.value.toString()) : '';
    const length = (this.f.length.value) ? s(this.f.length.value.toString()) : '';
    const width  = (this.f.width.value) ? s(this.f.width.value.toString()) : '';
    const height = (this.f.height.value) ? s(this.f.height.value.toString()) : '';
    const weight = (this.f.weight.value) ? s(this.f.weight.value.toString()) : '';
    this.itemSlug = s(name + 'x' + 'Lx' + length + 'Wx' + width + 'HTx' + height + 'WTx' + weight);
    // this.mform.setValue({
    //   item_slug : itemSlug
    // });
  }

  inst_onFilesAdded() {
    const inst_files: { [key: string]: File } = this.inst_file.nativeElement.files;
    for (let key in inst_files) {
      if (!isNaN(parseInt(key))) {
        this.inst_files.add(inst_files[key]);
        let reader = new FileReader();
        reader.onload = (event: any) => {
          this.inst_strUrl.push(event.target.result);
        };
        reader.readAsDataURL(inst_files[key]);
      }
    }

  }

  addFiles() {
    this.file.nativeElement.click();
  }

  inst_addFiles() {
    this.inst_file.nativeElement.click();
  }

  onSubmit() {

    // if (this.isSubmit) { return; }
    // console.log(this.mform)
    if (this.mform.invalid) {
      // alert('Pls fillup all required files');
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Please fillup all required files'
    });
      return;
    }

    if (this.strUrl.length === 0 && this.currentImages.length === 0) {
      // alert('Images is empty');
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Images is empty'
       });
      return;
    }

    // if (this.inst_strUrl.length === 0 && this.inst_currentImages.length === 0) {
    //   // alert('Instruction Images is empty');
    //   const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
    //     height: '250px',
    //     width: '350px',
    //     data: 'Instruction Images is empty'
    //    });
    //   return;
    // }

    if (this.f.current_stock.value < this.f.min_stock.value) {
        // alert('Current stock must be higher than min stock.');
        const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: 'Current stock must be higher than min stock.'
         });
        return;
    }

    if (this.files.size === 0 && !this.details) {
      // alert('Images is empty');
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Images is empty'
       });
      return;
    }

    const details = {
      itemcateg_id: this.f.itemcateg_id.value,
      item_no: this.f.item_no.value.toString().trim().replace(/\s\s+/g, ' '),
      name: this.f.name.value.trim().replace(/\s\s+/g, ' '),
      description: this.f.description.value.trim().replace(/\s\s+/g, ' '),
      min_stock: this.f.min_stock.value,
      current_stock: this.f.current_stock.value,
      price: this.f.price.value,
      length: this.f.length.value,
      width: this.f.width.value,
      height: this.f.height.value,
      weight: this.f.weight.value,
      remarks: this.f.remarks.value.trim().replace(/\s\s+/g, ' '),
      shipping_fee_ncr: '0',
      shipping_fee_luz: '0',
      shipping_fee_vis: '0',
      shipping_fee_min: '0',
      item_slug: this.itemSlug,
      has_file: true
    };

    this.submitted = true;
    this.isSubmit = true;

    if (this.details) {

      let Ids = this.currentImages.map(function (data) {
        return data.id;
      });

      let str = Ids.join(', ');
      details['new_id_image'] = str;

      let inst_Ids = this.inst_currentImages.map(function (data) {
        return data.id;
      });

      let inst_str = inst_Ids.join(', ');
      details['inst_new_id_image'] = inst_str;

      this.editItem(details);

    } else {
      this.createItem(details);
    }
  }


  getItem(id: any) {
    this.itemsService.get(id)
      .subscribe(data => {
        const d: any = data;
        this.details = d.data;
        this.currentImages = this.details.images;
        this.inst_currentImages = this.details.instruction_images;
      });
  }

  editItem(details) {
    var keepGoing = true;
    if (keepGoing) {
      this.files.forEach(file => {
        if (keepGoing){
          if (file.size >= 6144000) {
            // console.log(file.size)
            // alert("Item Image File size must not exceed 6mb. Please Try Again!");
            const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
              panelClass: 'app-full-bleed-dialog-p-26',
              disableClose: false,
              data: 'Item Image File size must not exceed 6mb. Please Try Again!'
             });
            this.files= new Set();
            this.strUrl = []
            this.isSubmit = false;
            keepGoing = false;
          }
        }
      })
      this.inst_files.forEach(file => {
        if (keepGoing){
          if (file.size >= 6144000) {
            // console.log(file.size)
            // alert("Item Instruction Image File size must not exceed 6mb. Please Try Again!");
            const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
              panelClass: 'app-full-bleed-dialog-p-26',
              disableClose: false,
              data: 'Item Instruction Image File size must not exceed 6mb. Please Try Again!'
             });
            this.inst_files= new Set();
            this.inst_strUrl = []
            this.isSubmit = false;
            keepGoing = false;
          }
        }
      })
    }
    if(keepGoing){
      this.itemsService.update(details, this.files, this.inst_files, this.details.id)
        .pipe(first())
        .subscribe(
          data => {
            this.isSubmit = false;
            this.submitted = false;
            this.dialogRef.close();
          },
          error => {
            this.isSubmit = false;
            this.submitted = false;
            if (error instanceof HttpErrorResponse) {
              console.log(error.status)
              if (error.status == 409) {
                // alert('Plese try again')//
                const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
                    panelClass: 'app-full-bleed-dialog-p-26',
                    disableClose: false,
                    data: error.error.status.description
                });
              }
            }else if((error.status != 403)){
              const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
                  panelClass: 'app-full-bleed-dialog-p-26',
                  disableClose: false,
                  data: 'Opps! Something went wrong.'
              });
            }
          }
        );
    }
  }

  createItem(details) {
    var keepGoing = true;
    if (keepGoing) {
      this.files.forEach(file => {
        if (keepGoing){
          if (file.size >= 6144000) {
            // console.log(file.size)
            // alert("Item Image File size must not exceed 6mb. Please Try Again!");
            const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
              panelClass: 'app-full-bleed-dialog-p-26',
              disableClose: false,
              data: 'Item Image File size must not exceed 6mb. Please Try Again!'
             });
            this.files= new Set();
            this.strUrl = []
            this.isSubmit = false;
            keepGoing = false;
          }
        }
      })
      this.inst_files.forEach(file => {
        if (keepGoing){
          if (file.size >= 6144000) {
            // console.log(file.size)
            // alert("Item Instruction Image File size must not exceed 6mb. Please Try Again!");
            const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
              panelClass: 'app-full-bleed-dialog-p-26',
              disableClose: false,
              data: 'Item Instruction Image File size must not exceed 6mb. Please Try Again!'
             });
            this.inst_files= new Set();
            this.inst_strUrl = []
            this.isSubmit = false;
            keepGoing = false;
          }
        }
      })
    }
    if(keepGoing){
      this.itemsService.create(details, this.files, this.inst_files)
        .pipe(first())
        .subscribe(
          data => {
            this.isSubmit = false;
            this.submitted = false;
            this.dialogRef.close();
          },
          error => {
            this.isSubmit = false;
            this.submitted = false;
            if (error instanceof HttpErrorResponse) {
              console.log(error.status)
              if (error.status == 409) {
                // alert('Plese try again')//
                const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
                    panelClass: 'app-full-bleed-dialog-p-26',
                    disableClose: false,
                    data: error.error.status.description
                });
              }
            }else if((error.status != 403)){
              const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
                  panelClass: 'app-full-bleed-dialog-p-26',
                  disableClose: false,
                  data: 'Opps! Something went wrong.'
              });
            }
            // console.log();
          });
    }
  }

  getItemCateg() {
    this.itemCategService.getAll()
      .subscribe(data => {
        const d: any = data;
        if (!this.isObjectEmpty(d.data.items)) {
          this.itemCategory = d.data.items;
        }
      });
  }

  removeCurrentImg(i) {
    this.currentImages.splice(i, 1);
  }

  inst_removeCurrentImg(i) {
    this.inst_currentImages.splice(i, 1);
  }

  removeImg(i) {
    this.files.delete(i);
    this.strUrl.splice(i, 1);
  }

  inst_removeImg(i) {
    this.inst_files.delete(i);
    this.inst_strUrl.splice(i, 1);
  }

  isObjectEmpty(Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

}
