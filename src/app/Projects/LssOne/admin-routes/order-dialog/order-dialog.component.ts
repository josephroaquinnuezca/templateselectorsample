import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SystemLogs } from '../shared/systemlogs.model';
import { MatDialog } from '@angular/material';
import { OrderService } from '../../_admin-services';
import { first } from 'rxjs/operators';
import { id } from '@swimlane/ngx-charts/release/utils';
import { PrintService } from '../shared/print.service';

import * as $ from 'jquery';
import * as moment from 'moment';

import { OrderPackageService } from '../../_client-services';
import { HelperService } from '../../_helper-service';
import { DatePipe } from '@angular/common';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';

@Component({
  selector: 'app-order-dialog',
  templateUrl: './order-dialog.component.html',
  styleUrls: ['./order-dialog.component.scss']
})
export class OrderDialogComponent implements OnInit {
	// order_package: any;
	// shipping slip checkbox
	shippingChecked: boolean = false;
	isSpinner: boolean = false;


	@ViewChild('editTemplate', { read: true, static: false }) sort: MatSort;
	reqList: SystemLogs[] = []
	dataSource: MatTableDataSource<SystemLogs>;
  displayedColumns = ['admin_name', 'log_desc', 'log_date'];

  defval = 'deliver';
  updateForm: FormGroup;
  details: any;
  checked: boolean
  today = new Date();
  paymentDatetoday = new Date();

	paginator: any;

  constructor(
    private datePipe: DatePipe,
    private helperService: HelperService,
		private orderPackageService: OrderPackageService,
    public dialogRef: MatDialogRef<OrderDialogComponent>,
    private orderService: OrderService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) data,
    public printService: PrintService) {
    this.details = data;
    console.log(this.details)
		console.log(this.details.row.order_no)
		this.paginator = {
			page: 1,
			limit: '20',
			key: this.details.row.order_no
		}
  }



  pageItem = {
    first: 1,
    total: 0,
    pages: 0,
    has_next: 0,
    has_previous: 0,
    next_page: 0,
    previous_page: 0,
    offset: 0,
    current: 0
  }
	isObjectEmpty(Obj) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

	orderLogPagination(pgn: any) {
    const str = $.param(pgn);
    this.orderService.getSystemActivityLogOrder(str)
      .subscribe(data => {
        const d: any = data;
        if (!this.isObjectEmpty(d.data.logs)) {
          this.pageItem = d.data.pagination;
					for(var dataLog of d.data.logs ){
						if(dataLog.activity_type === "proofofpayment" || dataLog.activity_type === 'proofofpayment-confirm-order') dataLog.activity_details = "uploaded proof of payment."
						else if(dataLog.activity_type === "update") dataLog.activity_details = dataLog.activity_details.replace(",", "")
						else if(dataLog.activity_type === "create" || dataLog.activity_type === "create-guest-order") dataLog.activity_details = "created new transaction."
						console.log(dataLog);
					}
          this.reqList = d.data.logs;
          this.dataSource.data = this.reqList;
          console.log(this.dataSource.data)
        }
      });
  }

  selectLimit(value: any) {
    this.paginator.limit = value;
    this.orderLogPagination(this.paginator);
  }

  pageAction(action: string) {
    switch (action) {
      case 'first': {
        this.paginator.page = 1;
        break;
      }
      case 'prev': {
        this.paginator.page = this.pageItem.previous_page;
        break;
      }
      case 'next': {
        this.paginator.page = this.pageItem.next_page;
        break;
      }
      case 'last': {
        this.paginator.page = this.pageItem.pages;
        break;
      }
    }

    this.orderLogPagination(this.paginator);
  }

  applyFilter(key: string) {
    if (key) {
      this.paginator.page = 1;
      this.paginator.key = key;
      this.orderLogPagination(this.paginator);
    } else {
      this.paginator.page = 1;
      this.paginator.key = '';
      this.orderLogPagination(this.paginator);
    }
  }
	//END PAGINATION
  filterPastDate() {
      //add or substract if u want to select past date or adjust today date into future date
      this.today.setDate(this.today.getDate())
      this.paymentDatetoday.setDate(this.today.getDate() - 5)
  }
  ngOnInit() {
		this.shippingCheck(this.details.row.tracking_no);
		this.orderLogPagination(this.paginator);
    this.dataSource = new MatTableDataSource(this.reqList);
    this.dataSource.sort = this.sort;
    this.filterPastDate()
    // console.log("WEWEW", this.details)
    if (this.details.option === 'update') {
      this.createForm();
    }

		// if (this.details.row.service == 'Delivery'){
		// 	var x = this.details.row.order_no;
		// 	this.orderPackageService.fetchOrderPackage(x).subscribe(res => {
		// 		const res_data: any = res;
    //
		// 		if (res_data.status.error === false ) {
		// 			console.log(res_data);
		// 			this.order_package = res_data.data;
		// 		}
		// 	});
		// }
  }

  get f() { return this.updateForm.controls; }

  get getFormValue() { return this.updateForm.controls; }

  orderStatusChanged(eventValue) {
    if (eventValue == 2) {
      if (this.details.row.service_type == 1) {
        this.getFormValue.est_deliverydate.enable();
        // this.getFormValue.est_deliverydate.setValidators([Validators.required]);
        this.getFormValue.est_deliverydate.updateValueAndValidity();
      }
      if (this.details.row.service_type == 1 || this.details.row.service_type == 2) {
        this.getFormValue.payment_date.enable();
        // this.getFormValue.payment_date.setValidators([Validators.required]);
        this.getFormValue.payment_date.updateValueAndValidity();
      }
      // this.getFormValue.tracking_no.enable();
      this.getFormValue.or_no.enable();
      // this.getFormValue.tracking_no.setValidators([Validators.required]);
      // this.getFormValue.or_no.setValidators([Validators.required]);
      // this.getFormValue.tracking_no.updateValueAndValidity();
      this.getFormValue.or_no.updateValueAndValidity();
    // }else if (eventValue == 1){
    }else{
      if (this.details.row.service_type == 1) {
        this.getFormValue.est_deliverydate.disable();
        this.getFormValue.est_deliverydate.clearValidators();
        this.getFormValue.est_deliverydate.updateValueAndValidity();
      }
      if (this.details.row.service_type == 1 || this.details.row.service_type == 2) {
        this.getFormValue.payment_date.disable();
        this.getFormValue.payment_date.clearValidators();
        this.getFormValue.payment_date.updateValueAndValidity();
      }
      if (eventValue == 4) {
        this.getFormValue.tracking_no.enable();
        this.getFormValue.tracking_no.clearValidators();
        this.getFormValue.tracking_no.updateValueAndValidity();
      }
      // this.getFormValue.tracking_no.disable();
      this.getFormValue.or_no.disable();
      // this.getFormValue.tracking_no.clearValidators();
      this.getFormValue.or_no.clearValidators();
      // this.getFormValue.tracking_no.updateValueAndValidity();
      this.getFormValue.or_no.updateValueAndValidity();
    }
    // console.log(this.getFormValue.status)
    // console.log(this.getFormValue.est_deliverydate)
    if (eventValue == 8) {
      if (this.details.row.service_type == 1) {
        const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
          panelClass: 'app-full-bleed-dialog-p-26',
          disableClose: false,
          data: 'If you proceed and save the status change. This will generate/create courier booking and you will not be able to return to previous status of order.'
        });
        return
      }
    }
  }

  createForm() {
      this.updateForm = this.formBuilder.group({
      status: [this.details.row.status, Validators.required],
      // remarks: [this.details.row.remarks, Validators.required],
      est_deliverydate: [{value: this.details.row.est_deliverydate, disabled: (this.details.row.status != 2 ) ? true : false}],
      payment_date: [{value: (this.details.row.payment_date) ? this.datePipe.transform(this.details.row.payment_date, 'yyyy-MM-dd') : '', disabled: (this.details.row.status != 2 ) ? true : false}],
      or_no: [{value: this.details.row.or_no, disabled: (this.details.row.status != 2 ) ? true : false}],
      tracking_no: [{value: this.details.row.tracking_no, disabled: (this.details.row.status != 4 ) ? true : false}],
      customer_tin_no: [this.details.row.customer_tin_no],
      check_bank: [this.details.row.check_bank],
      check_no: [this.details.row.check_no],
      check_date: [this.details.row.check_date]
    });
  }
   //, (this.details.row.status == 2 && this.details.row.service_type == 1) ? Validators.required : null for estimated
  // , (this.details.row.status == 2 && this.details.row.service_type == 1) ? Validators.required : null para to sa date na tinaggal ko
  // , (this.details.row.status == 2 ) ? Validators.required : null inalis ko to for testing sa or-no

  onSubmit() {
    // stop here if form is invalid
    // console.log(this.updateForm.get('status').value);
    // return;
    this.isSpinner = true;
    if (this.updateForm.invalid) {
      for (let v in this.updateForm.controls) {
        this.updateForm.controls[v].markAsTouched();
      }
      this.helperService.findInvalidControls(this.updateForm.controls);

      this.isSpinner = false;
      return;
    }
		// console.log(this.updateForm.get('tracking_no').hasError('required'))
		// console.log(this.updateForm.get('or_no').hasError('required'))
		// console.log(this.updateForm.get('customer_tin_no').hasError('required'))
		// return




      if (this.getFormValue.est_deliverydate.value === null) {
        const newData = {
          order_id: this.details.row.order_no,
          status: this.getFormValue.status.value,
          shippingslip_status: this.shippingChecked ? 1 : 0,
          // remarks: this.getFormValue.remarks.value,
          or_no: this.getFormValue.or_no.value,
          tracking_no: this.getFormValue.tracking_no.value,
          check_date: this.getFormValue.check_date.value,
          customer_tin_no: this.getFormValue.customer_tin_no.value,
          check_bank: this.getFormValue.check_bank.value,
          check_no: this.getFormValue.check_no.value,
          est_deliverydate: this.getFormValue.est_deliverydate.value,
          payment_date: (this.getFormValue.payment_date.value) ? this.datePipe.transform(this.getFormValue.payment_date.value, 'yyyy-MM-dd HH-mm-ss') : 'None',
        };
        console.log('okitnana value', newData)
        this.orderService.updateStatus(newData)
          .pipe(first())
          .subscribe(
            data => {
              this.dialogRef.close(true)
              this.isSpinner = false;
              console.log('okitnana value', data)
            },
            error => {
              // this.alertService.error(error);
              this.isSpinner = false;
            });

      }else{
        const testDate = moment(new Date(this.getFormValue.est_deliverydate.value));
        const validateNullValue = testDate.utc(true);
        const newData = {
          order_id: this.details.row.order_no,
          status: this.getFormValue.status.value,
          shippingslip_status: this.shippingChecked ? 1 : 0,
          // remarks: this.getFormValue.remarks.value,
          or_no: this.getFormValue.or_no.value,
          tracking_no: this.getFormValue.tracking_no.value,
          check_date: this.getFormValue.check_date.value,
          customer_tin_no: this.getFormValue.customer_tin_no.value,
          check_bank: this.getFormValue.check_bank.value,
          check_no: this.getFormValue.check_no.value,
          est_deliverydate: validateNullValue,
          payment_date: (this.getFormValue.payment_date.value) ? this.datePipe.transform(this.getFormValue.payment_date.value, 'yyyy-MM-dd HH-mm-ss') : 'None',
        };
        console.log('okitnana value', newData)
        this.orderService.updateStatus(newData)
          .pipe(first())
          .subscribe(
            data => {
              this.dialogRef.close(true)
              this.isSpinner = false;
              console.log('okitnana value', data)
            },
            error => {
              // this.alertService.error(error);
              this.isSpinner = false;
            });
      }



  }

  printOR() {
    this.dialogRef.close(false)
    this.printService.printDocument(this.details.row.order_no)
  }
	shippingCheck(event){
		console.log('shippingCheck', event)
		if(event != "" && event && event != "0" && event != 0){
			this.shippingChecked = true;
		}
		else{
			this.shippingChecked = false;
		}
		console.log('shippingCheck', this.shippingChecked)
	}
}
