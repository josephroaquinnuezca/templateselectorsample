import { first } from 'rxjs/operators';
import { SharedDataService } from '.././../shared/shared-data.service';
import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { Transaction } from 'src/app/user-routes/shared/transaction.model';
import { GuideModel } from '../shared/guide.model';
// import { OrderDialogComponent } from '../order-dialog/order-dialog.component';

import { ItemCategService, SettingService } from '../../_admin-services';

import { FormControl } from '@angular/forms';

import * as $ from 'jquery';
import * as moment from 'moment';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { NoResultComponent } from '../items/lists/modals/no-result/no-result.component';
import { GuideFormComponent } from './guide-form/guide-form.component';

@Component({
  selector: 'app-guide',
  templateUrl: './guide.component.html',
  styleUrls: ['./guide.component.scss']
})
export class GuideComponent implements OnInit {

  @ViewChild('editTemplate', { read: true, static: false }) sort: MatSort;
  contentHeight: number;
  reqList: GuideModel[] = [];
  dataSource: MatTableDataSource<GuideModel>;
  displayedColumns = ['id', 'title', 'description', 'viewAction'];
  search_loading = false;


  constructor(private sharedData: SharedDataService, public dialog: MatDialog, private settingService: SettingService,
    private router: Router) { }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // event.target.innerWidth;
    this.contentHeight = document.documentElement.clientHeight - 277.5
  }

  ngOnInit() {
    this.contentHeight = document.documentElement.clientHeight - 277.5
    this.getData();
    this.dataSource = new MatTableDataSource(this.reqList);
    this.dataSource.sort = this.sort;
  }

  getData() {
    this.settingService.getAllGuide()
    .subscribe( data => {
      this.search_loading = false;
      const d: any = data;
      if (!this.isObjectEmpty(d.data.items)) {
        this.reqList = d.data.items;
        // console.log( this.reqList);
        this.dataSource.data = this.reqList;
      } else {
        this.openDialog();
      }
    });
  }

  delete(id: any) {
    if (confirm('Are you sure you want to delete this item?')) {
      this.settingService.deleteGuide(id).subscribe(data => {
        const d: any = data;
        this.getData();
      });
    }
  }

	addGuide(option) {
    const dialogRef = this.dialog.open(GuideFormComponent, {
			panelClass: 'app-full-bleed-dialog',
      data : {
				option
			}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      this.getData();
    });
  }

  edit(data, option) {
    const dialogRef = this.dialog.open(GuideFormComponent, {
		  panelClass: 'app-full-bleed-dialog',
      data : {
				data,
				option
			}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      this.getData();
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NoResultComponent, {
      panelClass: 'app-full-bleed-dialog-p-26',
      disableClose: false
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');

    });
  }

  isObjectEmpty (Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

}
