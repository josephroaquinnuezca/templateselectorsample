
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

import { ItemCategService, ItemsService, SettingService } from '../../../_admin-services';
import { first } from 'rxjs/operators';
import { id } from '@swimlane/ngx-charts/release/utils';
import 'rxjs/add/operator/map';
import * as $ from 'jquery';
import { forkJoin } from 'rxjs';
import { ErrorMesageDialogComponent } from 'src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component';

@Component({
  selector: 'app-guide-form',
  templateUrl: './guide-form.component.html',
  styleUrls: ['./guide-form.component.scss']
})
export class GuideFormComponent implements OnInit {

  mform: FormGroup;
	postData: any;

  submitted = false;
  details: any;
	// image variables
  @ViewChild('file', { static: false }) file;
  public files: Set<File> = new Set();
  strUrl: any[] = [];
	hasFiles:boolean = false;
  currentImages: any[] = [];
	option: any;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<GuideFormComponent>,
    private settingService: SettingService,
    private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) data) {
      this.details = data.data;
      this.option = data.option;
  }

  ngOnInit() {
		if(this.option == 'edit'){
			this.currentImages = this.details.images;
		}
    console.log(this.details);
		console.log(this.currentImages);
    this.mform = this.formBuilder.group({
      title: [(this.details) ? this.details.title : '', Validators.required],
      description: [(this.details) ? this.details.description : '']
    });
  }

  get f() { return this.mform.controls; }

  onSubmit() {

    if (this.mform.invalid) {
      // alert('Pls fillup all required files');
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
          height: '250px',
          width: '350px',
          data: 'Please fillup all required files'
      });

      dialogRef.afterClosed().subscribe(result => {
          console.log(`Dialog result: ${result}`);
      });
      // start
      return;
    }

    this.postData = {
      title: this.f.title.value,
      description: this.f.description.value,
			file_type: 'guide_images',
			has_file: true,
    };
		// console.log(this.postData)
		// let counter = 0;
    // this.files.forEach(file => {
    //   counter ++;
    //   this.postData[`image_${counter}`] = file;
    // });
		//
    // var file_count = counter;
		//
    // this.postData['file_count'] = file_count;
		// console.log(this.postData);
		// return

    if (this.details) {
			if(this.details.images){
				let Ids = this.currentImages.map(function (data) {
					return data.id;
				});

				let str = Ids.join(', ');
				console.log("current images tobe save", str);
				this.postData['new_id_image'] = str;
			}
			// return
      this.edit(this.postData);
    } else {
      this.create(this.postData);
    }
  }

  edit(details) {
    this.settingService.updateGuide(details, this.files, this.details.id)
    .pipe(first())
    .subscribe(
      data => {
				// console.log(data);
        this.submitted = false;
        this.dialogRef.close();
      },
      error => {
        this.submitted = false;
        // console.log();
      });
  }

  create(details) {
    this.settingService.createGuide(details, this.files)
    .pipe(first())
    .subscribe(
      data => {
        this.submitted = false;
        this.dialogRef.close();
      },
      error => {
        this.submitted = false;
        // console.log();
      });
  }






	addFiles() {
    this.file.nativeElement.click();
  }
	onFilesAdded(event) {
		// const files: { [key: string]: File } = this.file.nativeElement.files;
		// for (let key in files) {
		// 	if (!isNaN(parseInt(key))) {
		// 		this.files.add(files[key]);
		// 		let reader = new FileReader();
		// 		reader.onload = (event: any) => {
		// 			console.log(event.target.result);
		// 			this.strUrl.push(event.target.result);
		// 			this.hasFiles = true;
		// 		};
		// 		reader.readAsDataURL(files[key]);
		// 	}
		// }
		let files = event.target.files;
    if (files) {
      for (let file of files) {
				console.log(file);
        let reader = new FileReader();
        reader.onload = (e: any) => {
					//called files here to fix the indexing issue. Must be applied to other function similar to this one
					this.files.add(file);
          this.strUrl.push({'name': file.name, 'file': e.target.result});
					this.hasFiles = true;
        }
        reader.readAsDataURL(file);
      }
    }
	}
	checkFiles(){
		// console.log("PostData", this.postData);
		// console.log("NATIVE File", this.file);
		// console.log("NATIVE FILESET", this.file.nativeElement.files);
		console.log("strFiles", this.files);
		console.log("strURL", this.strUrl);
	}
	removeaddesImg(i) {
		// console.log(`Removed Image: ${i}`);
		// this.files.delete(i, 1);
		//Use forEach + delete instead of splice. Splice is not working for SET() array objects.
		//Must be applied to other function similar to this one
		this.file.nativeElement.value = null;
		var counter = 0
		this.files.forEach(point => {
			if (i === counter) {
				console.log(point);
			  this.files.delete(point);
			}
			console.log(counter);
			counter++
		});
		// delete this.file.nativeElement.files[i];
		this.strUrl.splice(i, 1);
	}
	removeCurrentImg(i) {
    this.currentImages.splice(i, 1);
		console.log(this.currentImages);
  }
}
