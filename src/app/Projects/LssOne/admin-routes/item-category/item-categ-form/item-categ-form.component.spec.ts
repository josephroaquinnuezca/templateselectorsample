import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCategFormComponent } from './item-categ-form.component';

describe('ItemCategFormComponent', () => {
  let component: ItemCategFormComponent;
  let fixture: ComponentFixture<ItemCategFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemCategFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCategFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
