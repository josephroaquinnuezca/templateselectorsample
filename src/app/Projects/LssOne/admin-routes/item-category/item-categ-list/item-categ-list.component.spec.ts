import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCategListComponent } from './item-categ-list.component';

describe('ItemCategListComponent', () => {
  let component: ItemCategListComponent;
  let fixture: ComponentFixture<ItemCategListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemCategListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCategListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
