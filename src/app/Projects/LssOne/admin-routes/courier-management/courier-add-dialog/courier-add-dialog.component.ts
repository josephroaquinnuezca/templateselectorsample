import { OnInit, Component, ViewChild, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from "@angular/material";
import { CourierService } from "src/app/_admin-services/courier.service";
import { ToastrService } from "ngx-toastr";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { first } from "rxjs/operators";
import { ErrorMesageDialogComponent } from "src/app/static-pages/error-mesage-dialog/error-mesage-dialog.component";

@Component({
  selector: 'app-courier-add-dialog',
  templateUrl: './courier-add-dialog.component.html',
  styleUrls: ['./courier-add-dialog.component.scss']
})
export class CourierAddDialogComponent implements OnInit {

  courierForm: FormGroup;
  postData: any;

  imgFile: File;

  submitted = false;
  details: any;
	// image variables
  @ViewChild('file', { static: false }) file;
  public files: Set<File> = new Set();
  strUrl: any[] = [];
	hasFiles:boolean = false;
  currentImages: any[] = [];
  option: any;

  // isUpdate: boolean = false;
  localUrl: any;

  constructor(
    public dialogRef: MatDialogRef<CourierAddDialogComponent>,
    private courierService: CourierService,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    this.courierForm = this.formBuilder.group({
      img: ['', [Validators.required]],
      name: ['', [Validators.required]],
      contact_no: ['', [Validators.required]],
      email_address: ['', [Validators.required]]
    });
  }

  get f() { return this.courierForm.controls; }

  onSubmit() {
    this.postData = {
      name: this.f.name.value,
      contact_no: this.f.contact_no.value,
      email_address: this.f.email_address.value,
			has_file: true,
    };
		console.log('joseph nuezca postdata', this.postData)
    if(this.courierForm.invalid){
      const dialogRef = this.dialog.open(ErrorMesageDialogComponent, {
        panelClass: 'app-full-bleed-dialog-p-26',
        disableClose: false,
        data: 'Incomplete information or invalid input provided!'
    });
    return
    }else{

    this.courierService.create(this.courierForm.value, this.imgFile)
    .pipe(first())
    .subscribe(
      data => {
        this.submitted = false;
        this.dialogRef.close();
        console.log('Jospeh nuezca', data)
        this.toastr.success("Successfully added");
      },
      error => {
        this.submitted = false;
        // console.log();
      });
    }
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrl = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
      this.imgFile = event.target.files[0];
      // this.isUpload = true
    }
  }
}
