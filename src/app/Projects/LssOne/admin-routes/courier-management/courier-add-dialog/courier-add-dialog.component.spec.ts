import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourierAddDialogComponent } from './payment-add-dialog.component';

describe('CourierAddDialogComponent', () => {
  let component: CourierAddDialogComponent;
  let fixture: ComponentFixture<CourierAddDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourierAddDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourierAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
