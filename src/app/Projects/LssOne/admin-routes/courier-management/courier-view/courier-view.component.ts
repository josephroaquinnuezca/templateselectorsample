import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { MatDialog, MatTableDataSource, MatSort, MatSortModule, MatSlideToggleChange, MatSlideToggle } from '@angular/material';
// import { CourierAddDialogComponent } from './courier-add-dialog/courier-add-dialog.component';
// import { CourierEditDialogComponent } from './courier-edit-dialog/courier-edit-dialog.component';
import { CourierReferenceService } from 'src/app/_admin-services/courier-reference.service';
import { CourierService } from 'src/app/_admin-services/courier.service';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { NoResultComponent } from '../../items/lists/modals/no-result/no-result.component';
import * as $ from 'jquery';
import * as moment from 'moment';
import { Courier } from '../../shared/admin.model';
import { FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CourierViewDialogComponent } from './courier-view-dialog/courier-view-dialog.component';


@Component({
  selector: 'app-courier-view',
  templateUrl: './courier-view.component.html',
  styleUrls: ['./courier-view.component.scss']
})
export class CourierViewComponent implements OnInit {

  @ViewChild('dataSort', { read: MatSort, static: true }) sort: MatSort;
  contentHeight: number;

  reqList: Courier[] = [];
  dataSource: MatTableDataSource<Courier>;
  search_loading = false;
  courierDetails: any;

  courier_name: String = ''

  displayedColumns: string[] = [ 'id', 'order_no', 'courier_book_id', 'fee', 'updated_date', 'viewAction'];

  paginator = {
    page : 1,
    limit : '20',
    key : '_all',
    courier_id : '0'
  };

  pageItem = {
    first         : 1,
    total         : 0,
    pages         : 0,
    has_next      : 0,
    has_previous  : 0,
    next_page     : 0,
    previous_page : 0,
    offset        : 0,
    current       : 0
  };

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // event.target.innerWidth;
    this.contentHeight = document.documentElement.clientHeight - 200
  }

  constructor(
    public dialog: MatDialog,
    private courierReferenceService: CourierReferenceService,
    private courierService: CourierService,
    private router: Router,
    private activeRoute: ActivatedRoute
    ) { }

  isObjectEmpty (Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)){
        return false;
      }
    }
    return true;
  }


  ngOnInit() {
    // this.reloaddata(this.datainfo);
    console.log('SNAP!', this.activeRoute.snapshot.params['id'] + ' ' + this.activeRoute.snapshot.params['name']);
    this.courier_name = this.activeRoute.snapshot.params['name'];
    this.paginator.courier_id = this.activeRoute.snapshot.params['id'];

    console.log(this.paginator)
    this.getCourierSingleData(this.activeRoute.snapshot.params['id']);
    this.getData(this.paginator);
    this.dataSource = new MatTableDataSource(this.reqList);
    this.dataSource.sort = this.sort;

    this.contentHeight = document.documentElement.clientHeight - 200


  }
  backToPreviousPage(){
    this.router.navigate(['admin/courier-management']);
  }
  getCourierSingleData(id: any){
    this.courierService.getSingle(id).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
      const dataResult: any = res;
      console.log('dataResult:', dataResult);
      this.courierDetails = dataResult.data.payload[0];
    }, err => {
      console.log('HTTP Error:', err);
      if (err.error) {
        console.log('HTTP Error:', err.error);
        if(err.error.type == 'error'){
          console.log('HTTP Error Type:', err.error.type);
        }
        else{
        }
      }
    }, () => {
      console.log('Done Fetching...');
    });
  }
  getData(pgn: any) {
    console.log('pgn: ', pgn)
    const str = $.param(pgn);
    this.courierReferenceService.getAll(str)
    .subscribe( data => {
      this.search_loading = false;
      const d: any = data;
      console.log('d: ', d)
      if (!this.isObjectEmpty(d.data.payload)) {
        this.pageItem = d.data.pagination;
        this.reqList = d.data.payload;
        this.dataSource.data = this.reqList;
      } else {
        this.openDialog();
      }
    });
  }

  selectLimit(value: any) {
    this.paginator.limit = value;
    this.getData(this.paginator);
  }

  // selectType(event: any) {
  //   this.paginator.type = event;
  // }
  pageAction (action: string) {
    switch (action) {
      case 'first' : {
        this.paginator.page = 1;
        break;
      }
      case 'prev' : {
        this.paginator.page = this.pageItem.previous_page;
        break;
      }
      case 'next' : {
        this.paginator.page = this.pageItem.next_page;
        break;
      }
      case 'last' : {
        this.paginator.page = this.pageItem.pages;
        break;
      }
    }

    this.getData(this.paginator);
  }

  applyFilter(key: string) {
    if (key) {
      this.paginator.page = 1;
      this.paginator.key = key;
      this.getData(this.paginator);
    } else {
      this.paginator.page = 1 ;
      this.paginator.key = '';
      this.getData(this.paginator);
    }
  }

  courierViewDialog(data) {
    const dialogRef = this.dialog.open(CourierViewDialogComponent, {
		  panelClass: 'app-full-bleed-dialog-p-10',
      data: {
				data
			}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
      this.getData(this.paginator);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NoResultComponent, {
      panelClass: 'app-full-bleed-dialog-p-26',
      disableClose: false
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  // addCourier(){
  //   // alert('hello joseph nuezca');
  //   // this.file.nativeElement.click();
  //   const dialogRef = this.dialog.open(CourierAddDialogComponent, {
  //     panelClass: 'app-full-bleed-dialog-p-10',
  //     data: 'home-page',
  //     disableClose: true
  //   });
  //
  //   dialogRef.afterClosed().subscribe(result => {
  //    console.log('The dialog was closed');
  //    this.getData(this.paginator);
  //    // this.getImages();
  //  });
  // }
  //
  // editCourier(id: number): void{
  //   const dialogRef = this.dialog.open(CourierEditDialogComponent, {
  //     panelClass: 'app-full-bleed-dialog-p-10',
  //     data: {id},
  //     disableClose: true
  //   });
  //   //close
  //     dialogRef.afterClosed().subscribe(res => {
  //       this.getData(this.paginator);
  //     });
  // }

  toggleSlide(id: any){
    // this.courierReferenceService.toggleCourier(id).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
    //   const dataResult: any = res;
    //   console.log('dataResult:', dataResult);
    // }, err => {
    //   console.log('HTTP Error:', err);
    //   if (err.error) {
    //     console.log('HTTP Error:', err.error);
    //     if(err.error.type == 'error'){
    //       console.log('HTTP Error Type:', err.error.type);
    //     }
    //     else{
    //     }
    //   }
    // }, () => {
    //   console.log('Done Fetching...');
    //   this.getData(this.paginator);
    // });
  }

  handleError(error) {
    console.log(error);
    return throwError(error);
  }

}
