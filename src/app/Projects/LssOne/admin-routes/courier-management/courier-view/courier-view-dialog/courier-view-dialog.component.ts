import { OnInit, Component, ViewChild, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from "@angular/material";

@Component({
  selector: 'app-courier-view-dialog',
  templateUrl: './courier-view-dialog.component.html',
  styleUrls: ['./courier-view-dialog.component.scss']
})
export class CourierViewDialogComponent implements OnInit {

  details: any

  constructor(
    public dialogRef: MatDialogRef<CourierViewDialogComponent>, @Inject(MAT_DIALOG_DATA) data) {
      this.details = data;
  }

  ngOnInit() {
    console.log(this.details)
  }
}
