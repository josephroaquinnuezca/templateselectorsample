import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourierViewDialogComponent } from './payment-add-dialog.component';

describe('CourierViewDialogComponent', () => {
  let component: CourierViewDialogComponent;
  let fixture: ComponentFixture<CourierViewDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourierViewDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourierViewDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
