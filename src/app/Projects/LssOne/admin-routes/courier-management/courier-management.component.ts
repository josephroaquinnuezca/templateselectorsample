import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { MatDialog, MatTableDataSource, MatSort, MatSortModule, MatSlideToggleChange, MatSlideToggle } from '@angular/material';
import { CourierAddDialogComponent } from './courier-add-dialog/courier-add-dialog.component';
import { CourierEditDialogComponent } from './courier-edit-dialog/courier-edit-dialog.component';
import { CourierService } from 'src/app/_admin-services/courier.service';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { NoResultComponent } from '../items/lists/modals/no-result/no-result.component';
import * as $ from 'jquery';
import * as moment from 'moment';
import { Courier } from '../shared/admin.model';
import { FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-courier-management',
  templateUrl: './courier-management.component.html',
  styleUrls: ['./courier-management.component.scss']
})
export class CourierManagementComponent implements OnInit {

  @ViewChild('dataSort', { read: MatSort, static: true }) sort: MatSort;
  contentHeight: number;

  reqList: Courier[] = [];
  dataSource: MatTableDataSource<Courier>;
  search_loading = false;
  imgany: any;
  idCurrent:any;
  testchecked =  false;
  testNotchecked =  false;

  testVariable: any;

  // new
  isChecked = true;
  formGroup: FormGroup;

  isTest: boolean = false;

  displayedColumns: string[] = [ 'img', 'id', 'contact_no', 'name', 'email_address', 'viewAction'];

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // event.target.innerWidth;
    this.contentHeight = document.documentElement.clientHeight - 200
  }

  constructor(
    public dialog: MatDialog,
    private courierService: CourierService,
    private router: Router,
    private activeRoute: ActivatedRoute
    ) { }



  paginator = {
    page : 1,
    limit : '20',
    key : '_all',
    type : '0'
  };

  pageItem = {
    first         : 1,
    total         : 0,
    pages         : 0,
    has_next      : 0,
    has_previous  : 0,
    next_page     : 0,
    previous_page : 0,
    offset        : 0,
    current       : 0
  };

  isObjectEmpty (Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)){
        return false;
      }
    }
    return true;
  }


  ngOnInit() {
    // this.reloaddata(this.datainfo);
    this.getData(this.paginator);
    this.dataSource = new MatTableDataSource(this.reqList);
    this.dataSource.sort = this.sort;

    this.contentHeight = document.documentElement.clientHeight - 200


  }

  addCourier(){
    // alert('hello joseph nuezca');
    // this.file.nativeElement.click();
    const dialogRef = this.dialog.open(CourierAddDialogComponent, {
      panelClass: 'app-full-bleed-dialog-p-10',
      data: 'home-page',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
     console.log('The dialog was closed');
     this.getData(this.paginator);
     // this.getImages();
   });
  }


  getData(pgn: any) {

    const str = $.param(pgn);
    this.courierService.getAll(str)
    .subscribe( data => {
      this.search_loading = false;
      const d: any = data;
      if (!this.isObjectEmpty(d.data.payload)) {
        this.pageItem = d.data.pagination;
        this.reqList = d.data.payload;
        console.log('hello joseph', this.reqList[0].status);
        // if (this.reqList[0].status === 1) {
        //   this.testchecked = true;
        //   this.isTest = false;
        // } else {
        //   this.testNotchecked = false;
        //   this.isTest = true;
        // }
        this.dataSource.data = this.reqList;
      } else {
        this.openDialog();
      }
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NoResultComponent, {
      panelClass: 'app-full-bleed-dialog-p-26',
      disableClose: false
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  editCourier(id: number): void{
    const dialogRef = this.dialog.open(CourierEditDialogComponent, {
      panelClass: 'app-full-bleed-dialog-p-10',
      data: {id},
      disableClose: true
    });
    //close
      dialogRef.afterClosed().subscribe(res => {
        this.getData(this.paginator);
      });
  }

  viewBookings(id: number, name: string){
    var courier_name = name.trim().replace(/\s\s+/g, ' ');
    courier_name = courier_name.replace(/\s+/g,"-")
    console.log(courier_name);
    this.router.navigate(['admin/courier-view/' + id + '/' + courier_name]);
  }

  toggleSlide(id: any){
    this.courierService.toggleCourier(id).pipe(retry(1),catchError(this.handleError)).subscribe( res => {
      const dataResult: any = res;
      console.log('dataResult:', dataResult);
    }, err => {
      console.log('HTTP Error:', err);
      if (err.error) {
        console.log('HTTP Error:', err.error);
        if(err.error.type == 'error'){
          console.log('HTTP Error Type:', err.error.type);
        }
        else{
        }
      }
    }, () => {
      console.log('Done Fetching...');
      this.getData(this.paginator);
    });
  }

  handleError(error) {
    console.log(error);
    return throwError(error);
  }

}
