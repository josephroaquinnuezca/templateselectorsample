import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourierManagementComponent } from './payment-setting.component';

describe('CourierManagementComponent', () => {
  let component: CourierManagementComponent;
  let fixture: ComponentFixture<CourierManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourierManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourierManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
