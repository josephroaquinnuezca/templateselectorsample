import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourierEditDialogComponent } from './payment-edit-dialog.component';

describe('CourierEditDialogComponent', () => {
  let component: CourierEditDialogComponent;
  let fixture: ComponentFixture<CourierEditDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourierEditDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourierEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
