import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
// import { FormGroup, FormBuilder, Validators } from '@angular/forms'; //
import { CourierService } from 'src/app/_admin-services/courier.service';
import { Courier } from '../../shared/admin.model';
import { map, catchError, first, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { id } from '@swimlane/ngx-charts/release/utils';
import { forkJoin } from 'rxjs';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-courier-edit-dialog',
  templateUrl: './courier-edit-dialog.component.html',
  styleUrls: ['./courier-edit-dialog.component.scss']
})
export class CourierEditDialogComponent implements OnInit {

  courierForm: FormGroup;
  courierData: Courier;
  data: any;
  courierInfo: any = [];
  imgFile: File;
  imgData: any;
  localUrl: any;
  isUpdate: boolean = false;

  constructor( private formBuilder: FormBuilder,
               private router: Router,
               private toastr: ToastrService,
               private courierService: CourierService,
               public dialogRef: MatDialogRef<CourierEditDialogComponent>,
               @Inject(MAT_DIALOG_DATA) data
    ) {
      this.data = data.id
    }

  ngOnInit() {
    this.fetchData();
    this.createForm()
  }

  createForm(){
    this.courierForm = this.formBuilder.group({
      name: ['', Validators.required],
      contact_no: ['', Validators.required],
      email_address: ['', Validators.required]
    });
    // console.log(this.courierForm);
  }

  setData(){
    // console.log(this.courierData)
    this.courierForm.controls['name'].setValue(this.courierData.name);
    this.courierForm.controls['contact_no'].setValue(this.courierData.contact_no);
    this.courierForm.controls['email_address'].setValue(this.courierData.email_address);
  }

  fetchData(){
    this.courierService.getSingle(this.data).subscribe(data => {
        console.log(data);
        // this.courierinfo = data;
        const d: any = data;

        console.log(d);
        if (!this.isObjectEmpty(d.data.payload)) {
          //this.pageItem = d.data.pagination;
          this.courierData = d.data.payload[0];
          this.imgData = this.courierData.img[0].file;
          console.log('hellojosep', this.courierData.img[0].file);
          this.setData();
        }
    });
  }

  get name(){ return this.courierForm.get('name');}
  get account_number(){ return this.courierForm.get('account_number');}

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.isUpdate = true;
        this.localUrl = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
      this.imgFile = event.target.files[0];
      // this.isUpload = true
    }
  }

  get f() { return this.courierForm.controls; }

  // console.log(f)
  updatebtn(){

    const dataUpdate = {
      name: this.f.name.value,
      contact_no: this.f.contact_no.value,
      email_address: this.f.email_address.value,
      has_file: true,
    }

    this.courierService.update(dataUpdate, this.imgFile, this.data).pipe(first()).subscribe(
      data =>{
        this.dialogRef.close(this.router.navigate['/courier-management'])
        this.toastr.success("Successfully updated")
        console.log(data)
      }
    )
  }

  isObjectEmpty (Obj: any) {
    for (const key in Obj) {
      if (Obj.hasOwnProperty(key)){
        return false;
      }
    }
    return true;
  }
}
