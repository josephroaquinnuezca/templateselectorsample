import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectTwoMduleRoutingModule } from './project-two-mdule-routing.module';
import { SecondhomeComponent } from './secondhome/secondhome.component';


@NgModule({
  declarations: [
    SecondhomeComponent
  ],
  imports: [
    CommonModule,
    ProjectTwoMduleRoutingModule
  ]
})
export class ProjectTwoMduleModule { }
