import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SecondhomeComponent } from './secondhome/secondhome.component';

const routes: Routes = [
  { path:  '', component:  SecondhomeComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectTwoMduleRoutingModule { }
