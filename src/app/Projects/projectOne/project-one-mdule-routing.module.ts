import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirsthomeComponent } from './firsthome/firsthome.component';


const routes: Routes = [
  { path: '', component:  FirsthomeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectOneMduleRoutingModule { }
