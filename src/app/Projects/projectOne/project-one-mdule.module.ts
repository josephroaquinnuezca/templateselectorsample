import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectOneMduleRoutingModule } from './project-one-mdule-routing.module';
import { FirsthomeComponent } from './firsthome/firsthome.component';


@NgModule({
  declarations: [FirsthomeComponent],
  imports: [
    CommonModule,
    ProjectOneMduleRoutingModule
  ]
})
export class ProjectOneMduleModule { }
