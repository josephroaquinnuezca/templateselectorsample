import { ServiceHandlerService } from './../service/service-handler.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-selector-page',
  templateUrl: './selector-page.component.html',
  styleUrls: ['./selector-page.component.scss']
})
export class SelectorPageComponent implements OnInit {

  anyvalue: any;
  constructor(
    private router: Router,
    private service: ServiceHandlerService) { }

  ngOnInit(){

  }

  public changeEvent(event){
    console.log(event.value)
    this.service.serviceHandlerTrue(event.value);
  }


}
